# note of heartbeat

旱冰场 动物园 录音棚 汽车影院 电影餐厅 过马路 做玩偶（秘密签名）  逛过去的地方 今日美术馆



## 积累话题

星座  手相



# 四个步骤

- 激起兴趣：你要先让女人认识你，对你感兴趣，愿意和你交流，否则后面免谈；
- 建立吸引：这里就是让她对你产生吸引这种情绪的时候，让她喜欢上你；
- 巩固吸引：光吸引还不够，你还要巩固吸引才能长期留她在身边，让她爱上你；
- 确立关系：包括肢体亲密度升级的最后一步发生性关系。

## 激起兴趣

怎样让不认识的女人对你感兴趣？

### 你面对女生的心态 - “无所谓但友好”

#### 无所谓

女人喜欢已经有很多女人喜欢的男人，她们认为很多女人喜欢的男人必定有价值。**无所谓**的态度可以在女人的眼里传达自己很受其她女人欢迎的讯号（潜沟通） ，从而激发女人天生的吸引机制之一 —— 喜欢其她女人喜欢的男人。

总结一点就是：不要在乎女生怎么看你。

在交往过程中，你对自己的看法是最重要的，而且， 你对这个女人的看法也是重要的， 唯一不重要的就是这个女人对你的看法。

#### 友好

有时候会进入一个误区：对有些美女有一种过度的故意不友好，来显示我的无所谓。对女人要无所谓她是不是喜欢你，但是同时对她正常的友好。这种滋养男女吸引产生的态度一定是“无所谓但友好”的整合。



### 初识对话

在你们一开始认识的时候，男生要提供你们两人聊天的话题，并且你自己首先要对这个话题提供实质信息。有如下方法可以做到这点：

#### 假定回答法

简单讲，就是对于你要问女人的问题，你自己先假定一个回答，可能是比较有趣的回答，然后直接以陈述句的方式说你这个假定的回答。

假如你想问女人叫什么名字：

直接对女人说：“哦，你就是那个李大妹。你好。”

你可以随便想一个女人的名字，甚至是很搞笑的名字，然后就说她是这个名字。

> 用这个假定回答法有个很关键的一环，就是你不能说着说着就笑场。特别是一开始的时候，绝对不能笑场。当然符合正常环境下的笑是可以的。否则，可能会有被人骂神经病的危险。



问来自哪里：

你一定是来自某某地方的。



问叫什么名字：

哦，你就是某某某（唐老鸭，米妮，白雪公主，天上掉下的林妹妹，抖音里经常被揍的那个……）吧，你好。



问身高：

我觉得你有一米八



问喜欢吃什么：

你肯定很喜欢吃……



#### 用冷读的技巧来让女人对你感兴趣

如果你和一个不认识的女人聊天，你就能告诉她一些关于她的事情：她的出身、她的工作、她的爱好等等，关于她的任何事情。
她会怎么想？她会想，哟今天碰到神人了，她会想这个男人了解我，懂我。

公式：

你其实是一个________的女孩。填空的地方是一个形容词，而且要避开大众化的形容词。比如大家都说她很有爱心，那你再说就不会让她产生好奇。

- 你其实是一个有耐心的女孩。
  - 为什么呀？
- 因为如果我不告诉你，你就会不停的问：为什么呀？为什么呀？为什么呀？



还有就是问她一个开放式的问题：你喜不喜欢周杰伦呀？等等，然后等她回答后，你就可以开始你的毫无厘头的冷读：你是一个___________ 的女孩。

- 你喜不喜欢周杰伦呀？
  - 喜欢呀，我超级喜欢他了
- 嗯~，你肯定是一个特别听话的女孩？
  - 为什么呀？
- 因为喜欢周杰伦的女孩，都很听话呀，当听到喜欢的人告诉你要听妈妈的话……



- 你相信天注定吗？
  - 沉默
- 看来你是相信的
  - 啊？为啥
- 天机不可泄露……   看到77页



利用第三个人进行冷读，可以用来搭讪不认识的女生

假如你和你的朋友还有一个女生在一起，你可以这样对你朋友说：

- 哥们，你看这个女孩，看她的眼睛，看到了没有？她是个坏女孩。

或者

- 你看她的项链……哟……这小姑娘脾气一定很倔犟。

这个时候，女生一般会主动质问你：

- 为什么呀……



#### 用与众不同的话

当女生戴了一顶很大的帽子：

一般男生都会说：你这帽子很大啊…… 这样的对话不能激起女生对你的兴趣，

你应该，先故作惊奇的看着她的帽子，然后说：没想到你的头这么大，看不出来呀……



当女生很沮丧：

一般男生都会说：你怎么啦？看上去很不开心……

你可以这样说：你看上去好像家里养的两条金鱼刚掉到马桶里，被你冲了。



## 建立吸引

**怎样和女人超越普通朋友的关系，并让她喜欢上你？**

### 善意的耍女生

需要在女生心中树立一个这样的形象：他老是欺负我，他很“坏”，但和他在一起很有意思。

关键还是不要让她太轻易地得到她要的东西。你要让她被你耍一下后才得到，就算是你帮她的酬劳。 让女人为你付出，她才会爱上你。



当女生需要你帮忙的时候，这个时候是耍她的好时机。

```
女：你能帮我去拿个快递吗？
男：欢迎使用shalom快递，一单一万，先付钱后交货，欧耶（给钱）
女：什么，还要钱？……
男：看在你我是兄弟的份上，便宜你一次，学猫叫我就帮你拿。
```



平时的时候可以这样耍：

```
男：（莫名其妙看着她的头顶3秒）咦？你头发上是什么东西啊？
女：（用手去摸，但什么都摸不到）有什么啊？
男：（认真的回答）有你的头发呀……
```

还可以这样：

```
男：（看着她的牙齿）咦，你牙里有根青菜
女：（紧张，赶快想办法查看，并去掉菜）哪有啊？
男：哦！我刚看错了……（然后挨揍）

女：你胡说，我今天没吃青菜。
男：啊？原来是昨天的

女：你牙里才有青菜
男：你想吃吗，我抠给你
```



### 故意曲解女人

**吸引女人的超级有效方法，无论她心里是不是有另一个男人，无论有多少其他男人追她**

也许所有人都认为：“男人应该追求女人”，但是真正有效的打开方式是：

应该考虑，我应该做什么或者对女人说什么，让她觉得我是一个**值得被“占有”**的男人，或者说我应该如何营造出一种是她在追我，而并非我整天像苍蝇一样盯着她的感觉。

曲解女生的方向是，把女生曲解到：“她想追求你” “她想和你进一步发展，但自己假装很慢热，不是那么随便的人”

**那应该怎样去曲解女生？**

比如：

```
当女生不小心碰了你一下；
你可以说：不要那么着急嘛，我不是你想象中那么随便的男生哦~
或者：啊，不要乱摸我，我要报警了……
更加高深的：啊，不要乱摸我，我要报警了……（然后看着她说）不过你蛮可爱的，那就委屈我一下，让你摸三下，只能三下
```

将女生曲解为 她在追求你

```
难道你只想得到我的身体，而不是真心爱我吗？
```



当女生在聊天的过程中提到：床，睡觉，洗澡时

```
什么床不床的（什么睡觉不睡觉的，洗澡一样），我两还没那么熟，我不是你想象中的那么随便的男人

女：明天还要上班呢，我们要早点回去睡觉吧。
男：我觉得我们俩还没有到可以睡觉这一步...... 我们俩没那么熟，我不是你想象中那么随便的男人。（坏笑）
```

洗澡 还可以这么说：

```
你一个人洗澡吗？
你会不会进去了就消失了啊？
```



**可以把几乎任何东西，她说的话，都曲解成是她想要追求你的框架，即使莫名其妙，也没问题。**

比如：

```
女： “你知道吗…今天我老板的老婆来公司了…”。
男：（说完你知道吗就紧接着说）（深沉，认真的回答）我知道。
女：我还没说完你就知道了？
男人很认真地回答：我知道。我妈考虑了很久，还是觉得我们俩不合适。 （表情先严肃，然后坏笑）
```



有时候可以曲解到性上面去

```
女：笨啊，这个都不会。
男：是有点笨，灯一关，有时候洞都找不到。
```



还有许多故意曲解女生的方式，需要自己去实践摸索，总结



### 推拉法

所谓推拉的本质，就是“打一下，揉一下”。

女人更喜欢去琢磨男人是不是喜欢她，而如果你直接告诉她，她会觉得你太没意思了，立刻失去对你的那种感觉。这也是为什么女人比男人更热衷于调情。

要想学会推拉，就必须要知道如何说出 你喜欢她的点 和 你不喜欢她的点，然后利用这些来判断是否让她作为你女朋友的话题上。

例如：

```
男：你笑起来好可爱啊，做我的女朋友一定很好（停顿一会，可以看女生的反应）（拉）
女：……
男：但是你会烧菜吗？
女：就会一点点。
男：那不行，不会烧菜的女孩都是坏女孩，兄弟，我们分手（推）
女：但是我会……
男：其实，我是就喜欢你坏坏的样子（拉）
女：……
男：你用的是什么香水啊，我好像过敏，兄弟，这次我们一定要分手（推）或者 但是我觉得你是小学生，我不能和小学生谈恋爱，这次我一定要和你分手
女：分手就分手
男：但是你已经美到我心里了，不要离开我
```



拉：

1. 体重并不影响你是我的宝贝，胖了就是大宝贝，瘦了就是小宝贝

推：

1. 



### 幽默感

幽默就是 **想不到+合理**

```
一天三个人在困在一个有食人族的荒岛上。食人族首领对他们说：“如果你按照我们说的做，我们就不会杀你。”三个人就按照他说的做了。
食人族首领说：“到丛林里去摘 10 个你看到的第一种水果。”
那第一个人摘回来 10 个苹果。食人族首领说：“把你摘回来的苹果全部塞到你的屁眼里，脸上不准有任何表情。”这个人塞到第二个苹果的时候就忍不住开始皱眉头作痛苦状了。 所以，食人族首领下令把这个人杀了吃了。
第二个人带了 10 个樱桃回来。食人族首领说：“把你摘回来的樱桃全部塞到你的屁眼里，脸上不准有任何表情。”这个人要开始塞最后一个樱桃的时候，突然笑了出来。所以，食人族首领下令把这个人杀了吃了。
在天堂里，第一个塞苹果的人问第二个塞樱桃的人：“你干嘛塞第十个樱桃的时候笑出来啊？最后一个，就差一点就能活了。 而且应该不痒啊？”第二个人回答说：“我看到第三个人摘了菠萝从林子里出来。 ”
```



用天马行空的词语造下面的句子：

如果……会怎么样？



### 女人会暗中考验男人，来决定是不是要喜欢他？

女人会问男生一些不太好回答的问题，类似于：你是不是想泡我？ 如果你妈和我一起掉进河里，你会先救谁？我们认识吗？你到底喜欢我哪里？你有房有车吗？你养得起我吗？

这些问题回答是和否都不太对，当你遇到这些问题时，说明女生在考验你了。

对于这样的问题， 你要不把它们当真。不用给她认真地真实答案。

```
女：“谁发给你的短信？”
男：“你妈......她说你不太懂事，让我好好照顾你。”
```



但是要能分辨出，女孩是诚心的问你问题，还是只是想考考你。

如果女生问你的问题是因为她想更多了解你，这个时候的问题一般是很实质性的，你就要给她真诚的答案。

有些模棱两可的问题，两种可能都存在，比如：你在哪里工作？

这个时候，如果你们是刚认识，就是考验，如果认识很久了，就要真诚回答。



## 巩固吸引

112页





怎样问她要联系方式？206