# Nginx学习笔记

分为两个进程：master进程 和 worker进程

master进程负责系统级别的调度，worker进程负责用户级别的调度



worker_processes 有多少个work进程来处理用户的请求，如果设置为auto则是和机器的CPU数量相同。



还可以在配置文件中配置 pid ：`pid /run/nginx.pid`



worker_connections  1024; 每个work进程可以建立的连接数   2w 8核



accept_mutex  off  性能更好



虚拟主机：

基于IP地址，基于port ，基于域名

域名被解析成IP后，会先匹配基于IP的虚拟主机，然后再匹配基于域名的，若IP匹配了，域名的就会被忽略



ip：

![image-20201021095624452](assets/image-20201021095624452.png)

域名：

![image-20201021095635411](assets/image-20201021095635411.png)



这里并不提供域名解析的功能