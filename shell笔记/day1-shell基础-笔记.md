## 1. shell脚本基础

### 1.1 shell脚本介绍

- 什么是shell脚本

  > 一系列的shell命令的集合, 还可以加入一些逻辑操作（if else for） 将这些命令放入一个文件中.
  >
  > - 文件
  > - shell命令 * n个
  > - 可以加入逻辑
  > - 需要在linux的终端中执行

  ```shell
  # test.sh
  ls
  pwd
  if [ xxx ]
  	cd ..
  ```



  - 什么是shell命令?

    ```shell
    $ ls
    $ cd
    $ pwd
    ```

- shell脚本的基本格式
  - 命名格式

    - 一般命名规则 : xxxxx.sh  (建议以.sh为后缀命名)

  - 书写格式

    ```shell
    # test.sh #是shell脚本中的注释
    # 第一行如果忘了了写, 使用默认的命令解析器 /bin/sh
    #!/bin/bash  # 指定解析shell脚本的时候使用的命令解析器 /bin/sh也可以
    # 一系列的shell命令
    ls
    pwd
    cp 
    rm
    ```

- shell脚本的执行

  ```shell
  # shell脚本编写完成之后, 必须添加执行权限
  chmod u+x xxx.sh
  # 执行shell脚本
  ./xxx.sh
  sh test.sh
  ```

### 1.2 shell脚本中的变量

- 变量的定义
  - 普通变量(本地变量)

    ```shell
    # 定义变量, 定义完成, 必须要赋值, =前后不能有空格
    temp=666
    # 普通变量只能在当前进程中使用
    ```

  - ## 环境变量 - 一般大写

    ```shell
    # 可以理解问全局变量, 在当前操作系统中可以全局访问
    # 分类
    	- 系统自带的
    		- PWD
    		- SHELL
    		- PATH
    		- HOME
    	- 用户自定义的
    		- 将普通变量提升为系统级别的环境变量
    		GOPATH=/home/zoro/go/src - > 普通环境变量
    		set GOPATH=/home/zoro/go/src - > 系统环境变量
    		export GOPATH=/home/zoro/go/src
    		~/.bashrc
    ```

- 位置变量

  > 执行脚本的时候, 可以给脚本传递参数, 在脚本内部接收这些参数, 需要使用位置变量

  ```shell
  # 已经存在的脚本test.sh
  #!/bin/bash
  echo "hello , world, $0"
  echo "第一个参数: $1"
  echo "第2参数: $2"
  echo "第3个参数: $3"
  echo "第4个参数: $4"
  echo "第5个参数: $5"
  echo "第6个参数: $6"
  # 执行test.sh
  $ ./test.sh 11 22 3 4 5 6 aa bb
  hello , world, ./test.sh
  第一个参数: 11
  第2参数: 22
  第3个参数: 3
  第4个参数: 4
  第5个参数: 5
  第6个参数: 6
  ```

  - $0:  执行的脚本文件的名字
  - $1:  第一个参数
  - $2: 第2个参数
  - $3: 第三个参数
  - ......

- 特殊变量
  - $#:  获取传递的参数的个数
  - $@: 给脚本传递的所有的参数
  - $?: 脚本执行完成之后的状态, 失败>0 or 成功=0
  - $$: 脚本进程执行之后对应的进程ID

  ```shell
  # test.sh
  #!/bin/bash
  echo "hello , world, $0"
  echo "第一个参数: $1"
  echo "第2参数: $2"
  echo "第3个参数: $3"
  echo "第4个参数: $4"
  echo "第5个参数: $5"
  echo "第6个参数: $6"
  echo "传递的参数个数: $#"
  echo "传递的所有的参数: $@"
  echo "当前脚本的进程ID: $$" 
  
  $ ./test.sh aa bb cc dd ee ff 8 9 0 
  hello , world, ./test.sh
  第一个参数: aa
  第2参数: bb
  第3个参数: cc
  第4个参数: dd
  第5个参数: ee
  第6个参数: ff
  传递的参数个数: 9
  传递的所有的参数: aa bb cc dd ee ff 8 9 0
  当前脚本的进程ID: 47946
  # 脚本执行状态查看
  $ echo $?
  0 -> 成功
  非0 -> 失败
  ```

- 普通变量取值

  ```shell
  # 变量定义
  value=123	# 默认以字符串处理
  value1 = "123 456"
  echo $value
  # 如何取变量的值:
   - $变量名
   - ${变量名}
  ```

- 取命令执行之后的结果值

  ```shell
  # 取值的两种方式:
  var=$(shell命令)
  var=`shell命令`
  
  ```

- 引号的使用

  ```shell
  # 双引号
  echo "当前文件: $var"
  - 打印的时候会将var中的值取出并输出
  # 单引号
  echo '当前文件: $var'
  - 将字符串原样输出
  ```

### 1.3 条件判断和循环

- shell脚本中的if条件判断

  ```shell
  # if语句
  # 注意事项:
  	- if 和 []直接有一个空格
  	- [ 条件 ] : 条件的前后都有空格
  	- else if => elif
  	- 
  if [ 条件判断 ];then
  	逻辑处理 -> shell命令
  	xxx
  	xxxx
  	xxxx
  fi
  # ===================
  if [ 条件判断 ]
  then
  	逻辑处理 -> shell命令
  	xxx
  	xxx
  fi
  # if ... elif .. fi
  if [ 条件判断 ];then
  	逻辑处理 -> shell命令
  	xxx
  	xxxx
  	xxxx
  elif [ 条件判断 ];then
  	shell命令
  elif [ 条件判断 ];then
  	shell命令
  elif [ 条件判断 ];then
  	shell命令
  else
  	shell命令
  fi
  ```

  ```shell
  # if.sh
  #!/bin/bash
  # 需要对传递到脚本内部的文件名做判断
  if [ -d $1 ];then
      echo "$1 是一个目录!"                                                
  elif [ -s $1 ];then
      echo "$1 是一个文件, 并文件不为空"
  else
      echo "$1 不是目录, 有肯能不存在, 或文件大小为0"
  fi
  ```

- shell脚本for循环

  ```shell
  # shell中的循环 for/ while
  # 语法: for 变量 in 集合; do;done
  for var in 集合;do
  	shell命令
  done
  ```

  ```shell
  # for.sh
  #!/bin/bash
  # 对当前目录下的文件进行遍历
  list=`ls`
  for var in $list;do
      echo "当前文件: $var"
      echo '当前文件: $var'                                       
  done
  # 运行脚本
  $ ./for.sh     
  当前文件: a
  当前文件: $var
  当前文件: abc
  当前文件: $var
  当前文件: for.sh
  当前文件: $var
  当前文件: if.sh
  当前文件: $var
  当前文件: test.sh
  当前文件: $var
  ```

#### 1.4 shell脚本中的函数

```shell
# 没有函数修饰, 没有参数, 没有返回值
# 格式
funcName(){
	# 得到第一个参数
	arg1=$1
	# 得到第2个参数
	arg2=$2
	函数体 -> shell命令 + 逻辑循环和判断
	mkdir /root/abc
}
# 没有参数列表, 但是可以传参
# 函数调用
funcName aa bb cc dd
# 函数调用之后的状态: 
0 -> 调用成功
非0 -> 失败
```

```shell
#!/bin/bash
# 判断传递进行来的文件名是不是目录, 如果不是, 创建...
# 定义函数
is_directory()
{
    # 得到文件名, 通过参数得到文件名
    name=$1
    if [ -d $name ];then
        echo "$name 是一个目录!"
    else
        # 创建目录
        mkdir $name
        if [ 0 -ne $? ];then
            echo "目录创建失败..."
            exit
        fi  
        echo "目录创建成功!!!"                                                                                         
    fi  
}

# 函数调用
is_directory $1
```


