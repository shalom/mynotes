# Promise语法

```js
const promise = new Promise(function(resolve, reject) {
  // ... some code 主任务模块

  // 这里的value和error是主任务模块的返回值，成功了返回value，失败了则返回错误信息error
  if (/* 异步操作成功 */){
    resolve(value);
  } else {
    reject(error);
  }
});

//	这里传入的参数分别是 resolve, reject
promise.then(function(){},function(){})
```

上面需要注意的点：

- resolve, reject 两个参数是何时传入的；
- resolve, reject 调用时机是我们控制的，它们分别会改变 Promise 的状态，你也可以不使用 `if` 判断就执行它们，一般业务逻辑是通过 `if` 判断主任务模块是否执行成功，然后再决定调用两个中的哪一个；



**特殊的情况**

看下面例码，若 p2 的 resolve 传入的参数是上一个 Promise p1，那么 p2 的 then 将属于 p1.

```js
const p1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("操作成功");
  }, 2000);
});
const p2 = new Promise((resolve, reject) => {
  resolve(p1); //	point在这里
}).then(
  msg => {
    console.log(msg);
  },
  error => {
    console.log(error);
  }
);
```







# Promise的状态

Promise包含 `pending`、`fulfilled`、`rejected` 三种状态

- `pending` 指初始等待状态，初始化 `promise` 时的状态
- `resolve()` 指已经解决，将 `promise` 状态设置为 `fulfilled`
- `reject()` 指拒绝处理，将 `promise` 状态设置为 `rejected`

注意：状态一旦发生改变，将不能逆转。





链接：[https://houdunren.gitee.io/note/js/15%20Promise.html#promise](https://houdunren.gitee.io/note/js/15 Promise.html#promise)

