# JS笔记

## 第002天

### 变量声明

**- 除了正规的使用var声明外，还可以直接等号声明；如下：**

```javascript
a2 = 5,
a3 = 10;
document.write('a2 = ' + a2 + ' <br>');
document.write('a3 = ' + a3 + ' <br>');
```

#### 声明八进制和十六进制

浏览器自动转成十进制，在浏览器中显示为十进制数

```javascript
// 八进制
var oNum = 070;
document.write('oNum = ' + oNum + '<br>');
// 十六进制
var xNum = 0x1f;
document.write('xNum = ' + xNum + '<br>');
```

#### 浮点数&科学记数法&存储最大值/最小值&正无穷大

```javascript
// 浮点数
var fNum = 5.01;
document.write('fNum = ' + fNum + '<br>');

// 浮点数，科学记数法
fNum = 5.61e7; // 5.61 * 10^7
document.write('fNum = ' + fNum + '<br>');

// 存储最大值
iNum = Number.MAX_VALUE;
document.write('iNum = ' + iNum + '<br>');

// 存储最小值
iNum = Number.MIN_VALUE;
document.write('iNum = ' + iNum + '<br>');

//	正无穷大
iNum = Number.POSITIVE_INFINITY;
document.write('iNum = ' + iNum + '<br>');
```

#### 声明多个变量

```javascript
// iEnd 直接跟在后边声明，若没有赋值，默认为undefined
var iStart = sToken2.indexOf(' '), iEnd = "yes";
document.write('iEnd = ' + iEnd + '<br>');
```




## 第003天
### 运算符

**ASCII码表三个背诵点：**

1. 0  30h
2. A  41h
3. a  61h

#### “&&，||” 特性的应用

**&&特性应用：**

x && y 如果x为false，则js引擎将不会执行y

应用：data && fn(data) 约定俗成的写法，不需要单独判断data是否为空，等价于：

```javascript
if data!=""{
    fn(data);
}
```

**||特性应用：**

```javascript
// 兼容性声明
var event = e || window.event;
```



#### 不使用第三方变量实现两数调换：

```javascript
n1 = 10  n2 = 20	

n1 += n2  	// 30 将两个数的值累加到n1中

n2 = n1 - n2	// 10 n2通过n1-n2，转换成原n1的值

n1 = n1 - n2	// 20 
```



#### 前++ 和 后++ 的区别：

前++ 会立马返回 ++ 的值；后++ 会返回当前值，然后++生效 

**"--" 同理**

```javascript
iNum = 5;
iNum = iNum++ + 10 + ++iNum;
//iNum:22
//iNum = iNum++ + 10 + ++iNum
//iNum = 5 + 10 + ++iNum
//iNum = 15 + ++iNum
//iNum = 15 + 7
//iNum = 22
//22
```

#### 字符串比较

**- 字符串比较规则：**一个一个字符相比较

```javascript
// 第一个字母都为'a',直接比较第二个字母，a的ASCII码值 < b......,所以为 false
document.write('[>] \'aaa\' > \'bbb\' ? ' + ('aaa' > 'abb') + '<br>');
// 第一个字符分别为'2'和'4'，因为2<4,所以为 true
document.write('[>] \'25\' < \'4\' ? ' + ('25' < '4') + '<br>');
```

**- 字符串和数字比较**：字符串如果是数字字符串，则会被转成数字，再进行比较；如果不是数字字符串，则将数字转成字符串，再进行比较

```js
// false
document.write('[>] \'25\' < 4 ? ' + ('25' < 4) + '<br>');
// true , a > 4
document.write('[>] \'aaa\' > 4 ? ' + ('aaa' > 4) + '<br>');
```

#### "=="的特性

> null == undefined 返回 true
>
> NaN == NaN 返回 false
>
> NaN != NaN 返回 true
>
> false == 0 返回 true
>
> 0为false 1或其他数字为true
>
> null == 0 返回 false

### 三目运算符：

```js
var iable = boolean_expression ? true_value : false_value;
```

​	如果 *Boolean_expression* 为 true，就把 *true_value* 赋给变量；如果它是 false，就把 *false_value* 赋给变量。（**标志是 ... ? ... : ...**）



## 数组

### 三种轮询数组的方法：		

```js
// 1. for(i)
for(var i = 0; i < arr.length; i++){
	console.log(arr[i]);
}

// 2. for(in)
for(var i in arr){
	console.log(arr[i]);
}

// for(i)是按照数组的方式改动数组，而for(in)使用map的方式返回所有非稀疏的节点的key。

// 3. forEach(f)
arr.forEach(function(x){
	console.log(x);
});
// forEach, 返回所有数字的且非稀疏的节点的value。
```

### 栈和堆的原理，引用类型和值类型两种变量类型底层原理&GC垃圾回收机制原理

```js
//	值传递，变量和值都存储在栈中，相互赋值为值传递，复制
var a = 10;
var b = a;
//	引用传递
var arr01 = [1,2,3];
var arr02 = arr01;
var arr03 = [4,5,6];
//// 栈内存
//ST001: a 10
//ST002: b 10(从a复制)
//ST003: arr01 HP001(栈区存储的是堆区的地址，指向堆区的一块内存)
//ST004: arr02 HP001(从arr01复制栈区存储的数据，也就是堆区的地址，所以arr01和arr02指向堆区同一块内存空间)
//ST005: arr03 HP002

//// 堆内存 
//HP001:[1,2,3](2)	//	当栈区有两个变量指向HP001，括号计数为2，当括号计数为0时，会被GC回收
//HP002:[4,5,6](1)

//	堆区互指，循环引用
var a1 = [1,2,3];
var a2 = [4,5,6];
a1[0] = a2;	//	带下标之后，代表的是值，不是地址
a2[0] = a1;
//ST006: a1 HP003  
//ST007: a2 HP004
//HP003: [1,2,3]	[HP003,2,3]
//HP004: [4,5,6]	[HP004,5,6]
```



#### 取除数组中重复的元素



#### 几种解决方案的复杂度，SQL和NOSQL





#### var x = '2+3*5-4'; 实现浏览器对 x 的解析器

**算法思路：**

1. 有两个栈数据结构，一个存放数字，一个存放符号
2. 当后边的符号优先级更高时，压栈；若已经达到最高，则出栈

**软件工程思路：**

1. 准备测试用例

   ```
   2+3*5-4
   1+2+3+4
   2*3*4*5
   1*2+3*4/5-4/2
   ```

2. Debug的工具和手段

   谷歌浏览器debug工具



#### 递归思想的两个应用案例

**递归的解题思路：**

> - 从简单的案例出发，找出 F(n) 与 F(n+1) 的联系，然后就可以采用递归调用

1. **有一行台阶，阶数为N，我们可以一次走1步，2步，3步，一共有多少种走法？**

   F(n) = F(n-3) + F(n-2) + F(n-1)

2. **汉诺塔**

   > 三个柱子(A,B,C)，A柱上有n个盘，越靠近地面的盘越大，需要将A柱的盘转移到B柱上，一次只能够转移一个盘，并且期间不允许出现小盘在大盘下面，求出最快捷的转移方式。

   画图推导，（1、doHanoi(n-1, src, hpr, dest); 2、doHanoi(n-1, hpr, dest, src);）





## 对象

#### 为什么能够直接a = 10；声明变量

```js
//	等价于：
windows.aa = 10;
```



## 闭包

**推理过程：**

1. 函数被定义，拷贝父函数的 scope chain 
2. 函数运行，加上自己的 scope chain （AO）





## 注释习惯

- Solidity 社区所使用的一个标准是使用一种被称作 **natspec** 的格式，看起来像这样： 

```solidity
/// @title 一个简单的基础运算合约
/// @author H4XF13LD MORRIS 💯💯😎💯💯
/// @notice 现在，这个合约只添加一个乘法
contract Math {
  /// @notice 两个数相乘
  /// @param x 第一个 uint
  /// @param y  第二个 uint
  /// @return z  (x * y) 的结果
  /// @dev 现在这个方法不检查溢出
  function multiply(uint x, uint y) returns (uint z) {
    // 这只是个普通的注释，不会被 natspec 解释
    z = x * y;
  }
}
```

- `@title`（标题） 和 `@author` （作者）很直接了.
- `@notice` （须知）向 **用户** 解释这个方法或者合约是做什么的。 `@dev` （开发者） 是向开发者解释更多的细节。
- `@param` （参数）和 `@return` （返回） 用来描述这个方法需要传入什么参数以及返回什么值。
- **注意：**你并不需要每次都用上所有的标签，它们都是可选的。不过最少，写下一个 `@dev` 注释来解释每个方法是做什么的。