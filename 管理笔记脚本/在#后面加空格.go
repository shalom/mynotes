package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

const (
	filePath = `C:\Users\Administrator\Desktop\mynotes\05_Mysql笔记\MySQL基础.md`
	aimPath  = `C:\Users\Administrator\Desktop\mynotes\05_Mysql笔记\MySQL基础1.md`
)

func dealPath(path string) string {
	return strings.Replace(path, `\`, "/", -1)
}

func main() {
	path := dealPath(filePath)
	aimPath := dealPath(aimPath)
	aimFile, err := os.Create(aimPath)
	if err != nil {
		log.Fatalf("os.Create(%s) fail err: %s\n", aimPath, err)
		return
	}

	file, err := os.OpenFile(path, os.O_RDWR, 0660)
	if err != nil {
		log.Fatalf("os.Open(%s) fail err: %s\n", path, err)
		return
	}
	defer func() {
		aimFile.Close()
		file.Close()
	}()

	buf := bufio.NewReader(file)

	for {
		dataLine, err := buf.ReadBytes('\n')
		if err != nil && err == io.EOF {
			log.Println("read to the end of the file.")
			break
		} else if err != nil {
			log.Fatalf("buf.ReadBytes err: %s\n", err)
			continue
		}

		//	如果第一个字符不是'#',写入然后跳过
		if string(dataLine[0]) != "#" {
			_, err = aimFile.Write(dataLine)
			//fmt.Println(string(dataLine))
			if err != nil {
				log.Fatalf("file.Write err: %s\n", err)
			}
			continue
		}

		//fmt.Println("=====here")
		var leftLine, rightLine []byte
		for i, b := range dataLine {
			if string(b) != "#" && string(b) != " " {
				leftLine = dataLine[:i]
				rightLine = dataLine[i:]
				break
			}
		}

		str:=string(leftLine)+" "+string(rightLine)

		fmt.Println(str)

		//leftLine = append(leftLine, ' ')
		//
		//data := append(leftLine, rightLine...)

		_, err = aimFile.Write([]byte(str))
		if err != nil {
			log.Fatalf("file.Write err: %s\n", err)
		}
	}

}
