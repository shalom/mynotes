package main

import (
	"bufio"
	"io"
	"log"
	"os"
	"strings"
)

const (
	filePath = `C:\Users\Administrator\Desktop\myNotes\03_Java笔记\IDEA构建SSM项目（Maven）.md`
	aimPath = `C:\Users\Administrator\Desktop\myNotes\03_Java笔记\IDEA构建SSM项目（Maven）1.md`
	replace  = "##"
)

func dealPath(path string) string {
	return strings.Replace(path, `\`, "/", -1)
}

func main() {
	path := dealPath(filePath)
	aimPath := dealPath(aimPath)
	aimFile, err := os.Create(aimPath)
	if err != nil {
		log.Fatalf("os.Create(%s) fail err: %s\n", aimPath, err)
		return
	}

	file, err := os.OpenFile(path,os.O_RDWR,0660)
	if err != nil {
		log.Fatalf("os.Open(%s) fail err: %s\n", path, err)
		return
	}
	defer func(){
		aimFile.Close()
		file.Close()
	}()

	buf := bufio.NewReader(file)

	for {
		dataLine, err := buf.ReadBytes('\n')
		if err != nil && err == io.EOF {
			log.Println("read to the end of the file.")
			break
		} else if err != nil {
			log.Fatalf("buf.ReadBytes err: %s\n", err)
			continue
		}

		stringLine := string(dataLine)

		if strings.Contains(stringLine, replace) {
			stringLine = strings.Replace(stringLine,replace,"",1)
		}

		_, err = aimFile.Write([]byte(stringLine))
		if err!=nil{
			log.Fatalf("file.Write err: %s\n",err)
		}
	}

}
