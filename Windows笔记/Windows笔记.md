# Windows笔记

## 安装环境

### 安装C环境

在Windows中，使用MinGW来安装C环境。安装时候还需要注意32位和64位的区别。网络上很多地方给出的MinGW下载链接里面已经没有bin文件夹了，所以这里直接贴上目前成功的[下载链接](https://links.jianshu.com/go?to=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fmingw-w64%2Ffiles%2FToolchains%2520targetting%2520Win64%2FPersonal%2520Builds%2Fmingw-builds%2F8.1.0%2Fthreads-posix%2Fseh%2Fx86_64-8.1.0-release-posix-seh-rt_v6-rev0.7z%2Fdownload)。下载解压，然后配置环境变量可以使用。

