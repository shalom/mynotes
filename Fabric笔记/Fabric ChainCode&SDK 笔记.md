# Fabric ChainCode 的编写





# Fabric SDK的使用

## 使用思路

1. 读取配置文件

2. 获得SDK句柄

3. 关联组织和用户

4. 获得代理客户端

5. 通道操作

   - 创建通道

   - 加入通道

6. 链码操作
   - 链码打包
   - 初始化链码
7. 创建通道客户端

8. 然后就可以查询等，调用链码中编写的方法


## 配置文件config.yaml详解
```yaml
# 这个名可以随意取，和之前的配置文件没有关系，也可以不写
name: "gosdk-service-network"
# 指定版本，内容的模式版本，被SDK的对应解析规则所使用
version: 1.1.0
# 客户端
client:
  # 客户端组织的名称，和crypto-config.yaml中的组织名一致
  organization: org1
  # 定义日志服务级别
  logging:
    level: info
  # 所有MSP的根目录
  cryptoconfig:
    path: /home/shalom/study/fabric/testwork/crypto-config
  # 某些SDK支持插件化的KV数据库,通过指定credentialStore属性实现
  # 以下两个路径是sdk去ca注册用户后，证书和密钥存储的路径
  credentialStore:
    # 可选,用于用户证书材料存储,如果所有的证书材料被嵌入到配置文件,则不需要
    path: /tmp/gosdk-service-store
    # 可选,指定GoSDK实现的CryptoSuite实现
    cryptoStore:
      # 指定用于加密密钥存储的底层KV数据库
      path: /tmp/gosdk-service-msp
  # 客户端的BCCSP模块配置
  BCCSP:
    security:
      enabled: true
      default:
        provider: "SW"
      hashAlgorithm: "SHA2"
      softVerify: true
      level: 256
  tlsCerts:
    # 可选,当连接到peers,orderes时使用系统证书池,默认为false
    systemCertPool: true
    # 可选,客户端和peers与orderes进行TLS握手的密钥和证书，组织1的Admin用户的账号文件
    client:
      keyfile: /home/shalom/study/fabric/testwork/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/tls/client.key
      certfile: /home/shalom/study/fabric/testwork/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/tls/client.crt
# 通道
channels:
  # 自定义,通道ID，和自己创建通道文件时指定的channelID一致
  examplechannel:
    # 可选,参与的orderers列表
    orderers:
      - orderer1.example.com
      - orderer2.example.com
      - orderer3.example.com
    # 可选,参与的peers列表，这里有些节点是不能够写上去的，TODO
    peers:
      peer1.org1.example.com:
        endorsingPeer: true
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
      peer1.org2.example.com:
        endorsingPeer: true
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
      peer1.org3.example.com:
        endorsingPeer: true
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
      peer1.org4.example.com:
        endorsingPeer: true
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
    # 可选,执行通道操作
    policies:
      # 可选,获取通道配置区块
      queryChannelConfig:
        # 可选,成功响应节点的最小数量
        minResponses: 1
        # 可选
        maxTargets: 1
        # 可选,查询配置区块的重试选项
        retryOpts:
          # 可选,重试次数
          attempts: 5
          # 可选,第一次重试的后退间隔
          initialBackoff: 500ms
          # 可选,重试的最大后退间隔
          maxBackoff: 5s
          backoffFactor: 2.0
# 组织
organizations:
  # 自定义,组织名
  org1:
    # 组织的MSPID
    mspid: Org1MSP
    # 组织的MSP存储位置,绝对路径或相对路径
    cryptoPath: /home/shalom/study/fabric/testwork/crypto-config/peerOrganizations/org1.example.com/users/{username}@org1.example.com/msp
    peers:
      - peer0.org1.example.com
    # 可选,证书颁发机构,用fabric-ca的时候需要指定
    certificateAuthorities:
      # - ca.org1.example.com
  org2:
    mspid: Org2MSP
    cryptoPath: /home/shalom/study/fabric/testwork/crypto-config/peerOrganizations/org2.example.com/users/{username}@org2.example.com/msp
    peers:
      - peer0.org2.example.com
    certificateAuthorities:
      # - ca.org2.example.com
  # 自定义,排序组织名
  ordererorg:
    # 排序组织的MSPID
    mspID: OrdererMSP
    # 排序组织的MSP存储位置,绝对路径或相对路径
    cryptoPath: /home/shalom/study/fabric/testwork/crypto-config/ordererOrganizations/example.com/users/Admin@example.com/msp
# 排序    
orderers:
  # 自定义,排序节点域名
  orderer0.example.com:
    url: grpcs://192.168.1.211:7050
    grpcOptions:
      ssl-target-name-override: orderer0.example.com
      # 当keep-alive-time被设置为0或小于激活客户端的参数,下列参数失效
      keep-alive-time: 5s
      keep-alive-timeout: 6s
      keep-alive-permit: false
      fail-fast: true
      allow-insecure: false
    tlsCACerts:
      # 证书的绝对路径
      path: /home/shalom/study/fabric/testwork/crypto-config/ordererOrganizations/example.com/orderers/orderer0.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
  orderer1.example.com:
    url: grpcs://192.168.1.212:7050
    grpcOptions:
      ssl-target-name-override: orderer1.example.com
      keep-alive-time: 5s
      keep-alive-timeout: 6s
      keep-alive-permit: false
      fail-fast: true
      allow-insecure: false
    tlsCACerts:
      path: /home/shalom/study/fabric/testwork/crypto-config/ordererOrganizations/example.com/orderers/orderer1.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
  orderer2.example.com:
    url: grpcs://192.168.1.213:7050
    grpcOptions:
      ssl-target-name-override: orderer2.example.com
      keep-alive-time: 5s
      keep-alive-timeout: 6s
      keep-alive-permit: false
      fail-fast: true
      allow-insecure: false
    tlsCACerts:
      path: /home/shalom/study/fabric/testwork/crypto-config/ordererOrganizations/example.com/orderers/orderer2.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
# 节点
peers:
  # 自定义,peer节点域名
  peer0.org1.example.com:
    # 用于发送背书和查询请求
    url: peer0.org1.example.com:7051
    grpcOptions:
      ssl-target-name-override: peer0.org1.example.com
      allow-insecure: false
    tlsCACerts:
      # 证书的绝对路径
      path: /home/shalom/study/fabric/testwork/crypto-config/peerOrganizations/org1.example.com/tlsca/tlsca.org1.example.com-cert.pem
  peer1.org1.example.com:
    url: peer1.org1.example.com:7051
    grpcOptions:
      ssl-target-name-override: peer1.org1.example.com
      allow-insecure: false
    tlsCACerts:
      path: /home/shalom/study/fabric/testwork/crypto-config/peerOrganizations/org1.example.com/tlsca/tlsca.org1.example.com-cert.pem
  peer0.org2.example.com:
    url: peer0.org2.example.com:7051
    grpcOptions:
      ssl-target-name-override: peer0.org2.example.com
      allow-insecure: false
    tlsCACerts:
      path: /home/shalom/study/fabric/testwork/crypto-config/peerOrganizations/org2.example.com/tlsca/tlsca.org2.example.com-cert.pem
  peer1.org2.example.com:
    url: peer1.org2.example.com:7051
    grpcOptions:
      ssl-target-name-override: peer1.org2.example.com
      allow-insecure: false
    tlsCACerts:
      path: /home/shalom/study/fabric/testwork/crypto-config/peerOrganizations/org2.example.com/tlsca/tlsca.org2.example.com-cert.pem
# 证书，使用了fabric-ca就需要这段配置
certificateAuthorities:
  # 自定义,ca节点域名
  ca.org1.example.com:
    url: http://192.168.1.251:7054
    # 包含CA的TLS证书的路径或直接包含证书的PEM内容,协议url必须是https
    tlsCACerts:
      path: /home/wy/go/src/gosdk/conf/ca.org1.example.com/msp/cacerts/192-168-1-251-7054.pem
      client:
        key:
          path: /home/wy/go/src/gosdk/conf/ca.org1.example.com/msp/keystore/e936910ed0f24ba01dca1afedecedf43027f1ee15415c58a10ef1e1e9bcdf8b0_sk
        cert:
          path: /home/wy/go/src/gosdk/conf/ca.org1.example.com/msp/signcerts/cert.pem
    # 注册商ID和密码的集合,每个CA只支持一个注册商
    registrar:
      enrollId: admin
      enrollSecret: adminpw
    # 可选,CA机构名称
    caName: ca.org1.example.com
  ca.org2.example.com:
    url: http://192.168.1.252:7054
    tlsCACerts:
      path: /home/wy/go/src/gosdk/conf/ca.org2.example.com/msp/cacerts/192-168-1-252-7054.pem
      client:
        key:
          path: /home/wy/go/src/gosdk/conf/ca.org2.example.com/msp/keystore/44adf491fa454dbbb1d110372fbbb5387786580c90f227d5d0ce1f23f7995b1f_sk
        cert:
          path: /home/wy/go/src/gosdk/conf/ca.org2.example.com/msp/signcerts/cert.pem
    registrar:
      enrollId: admin
      enrollSecret: adminpw
    caName: ca.org2.example.com
  ca.org3.example.com:
    url: http://192.168.1.253:7054
    tlsCACerts:
      path: /home/wy/go/src/gosdk/conf/ca.org3.example.com/msp/cacerts/192-168-1-253-7054.pem
      client:
        key:
          path: /home/wy/go/src/gosdk/conf/ca.org3.example.com/msp/keystore/21e582f4db08bfe1195b9d9a4ada58250c3c030d23f450aadbbab0bfe3e25463_sk
        cert:
          path: /home/wy/go/src/gosdk/conf/ca.org3.example.com/msp/signcerts/cert.pem
    registrar:
      enrollId: admin
      enrollSecret: adminpw
    caName: ca.org3.example.com
  ca.org4.example.com:
    url: http://192.168.1.254:7054
    tlsCACerts:
      path: /home/wy/go/src/gosdk/conf/ca.org4.example.com/msp/cacerts/192-168-1-254-7054.pem
      client:
        key:
          path: /home/wy/go/src/gosdk/conf/ca.org4.example.com/msp/keystore/08e3382067143b202555631c372fbcf7e567d79dfdaf3c5c96a5d63bcde156e1_sk
        cert:
          path: /home/wy/go/src/gosdk/conf/ca.org4.example.com/msp/signcerts/cert.pem
    registrar:
      enrollId: admin
      enrollSecret: adminpw
    caName: ca.org4.example.com
# 映射
entityMatchers:
  peer:
  - pattern: (\w*)peer1.org1(\w*)
    urlSubstitutionExp: 192.168.1.251:7051
    sslTargetOverrideUrlSubstitutionExp: peer1.org1.example.com
    mappedHost: peer1.org1.example.com
  - pattern: (\w*)peer1.org2(\w*)
    urlSubstitutionExp: 192.168.1.252:7051
    sslTargetOverrideUrlSubstitutionExp: peer1.org2.example.com
    mappedHost: peer1.org2.example.com
  - pattern: (\w*)peer1.org3(\w*)
    urlSubstitutionExp: 192.168.1.253:7051
    sslTargetOverrideUrlSubstitutionExp: peer1.org3.example.com
    mappedHost: peer1.org3.example.com
  - pattern: (\w*)peer1.org4(\w*)
    urlSubstitutionExp: 192.168.1.254:7051
    sslTargetOverrideUrlSubstitutionExp: peer1.org4.example.com
    mappedHost: peer1.org4.example.com
  orderer:
  - pattern: (\w*)orderer1(\w*)
    urlSubstitutionExp: 192.168.1.251:7050
    sslTargetOverrideUrlSubstitutionExp: orderer1.example.com
    mappedHost: orderer1.example.com
  - pattern: (\w*)orderer2(\w*)
    urlSubstitutionExp: 192.168.1.252:7050
    sslTargetOverrideUrlSubstitutionExp: orderer2.example.com
    mappedHost: orderer2.example.com
  - pattern: (\w*)orderer3(\w*)
    urlSubstitutionExp: 192.168.1.253:7050
    sslTargetOverrideUrlSubstitutionExp: orderer3.example.com
    mappedHost: orderer3.example.com
  certificateAuthority:
  - pattern: (\w*)ca.org1(\w*)
    urlSubstitutionExp: 192.168.1.251:7054
    sslTargetOverrideUrlSubstitutionExp: ca.org1.example.com
    mappedHost: ca.org1.example.com
  - pattern: (\w*)ca.org2(\w*)
    urlSubstitutionExp: 192.168.1.252:7054
    sslTargetOverrideUrlSubstitutionExp: ca.org2.example.com
    mappedHost: ca.org2.example.com
  - pattern: (\w*)ca.org3(\w*)
    urlSubstitutionExp: 192.168.1.253:7054
    sslTargetOverrideUrlSubstitutionExp: ca.org3.example.com
    mappedHost: ca.org3.example.com
  - pattern: (\w*)ca.org4(\w*)
    urlSubstitutionExp: 192.168.1.254:7054
    sslTargetOverrideUrlSubstitutionExp: ca.org4.example.com
    mappedHost: ca.org4.example.com
```


## GO SDK

### 读取配置文件
```go
file := config.FromFile("conf/config.yaml")
```

### 获得SDK句柄
```go
sdk, err := fabsdk.New(file)
if err != nil {
	panic(err)
}
```
### 关联组织和用户
这里要指定组织名和用户名，和 crypto-config.yaml 一样
```go
user := fabsdk.WithUser("Admin")
org := fabsdk.WithOrg("org1")
```

### 获得代理客户端
```go
context := sdk.Context(user, org)
client, err := resmgmt.New(context)
if err != nil {
	panic(err)
}
```

### 通道操作
#### 创建通道
```go
request := resmgmt.SaveChannelRequest{ChannelID: channelID, ChannelConfigPath: channelConfigPath}
response, err := client.SaveChannel(request)
if err != nil {
	panic(err)
}
if response.TransactionID == "" {
	fmt.Println("err: response.TransactionID==''")
	return
}
```

#### 加入通道
```go
withRetry := resmgmt.WithRetry(retry.DefaultResMgmtOpts)
//	指定order节点的域名
endpoint := resmgmt.WithOrdererEndpoint("orderer0.example.com")
err = client.JoinChannel(channelID, withRetry, endpoint)
if err != nil {
	panic(err)
}
```

### 链码操作
#### 链码打包
```go
//	会在GOPATH下的src下找chaincode目录，然后打包里边的链码
ccPackage, err := gopackager.NewCCPackage("chaincode", os.Getenv("GOPATH"))
if err != nil {
	panic(err)
}
```

#### 链码安装
```go
installCCReq := resmgmt.InstallCCRequest{
	Name:    channelName,//	channelID
	Path:    "chaincode",//	还是src里边的目录
	Version: "1.0",
	Package: ccPackage,
}
withRetry := resmgmt.WithRetry(retry.DefaultResMgmtOpts)
_, err = client.InstallCC(installCCReq, withRetry)
if err != nil {
	panic(err)
}
```

#### 链码初始化
```go
ccPolicy := cauthdsl.SignedByAnyMember([]string{"Org1MSP"})
instantiateCCReq := resmgmt.InstantiateCCRequest{
	Name:    channelName,
	Path:    "chaincode",
	Version: "1.0",
	//	这里需要根据自己编写的链码来输入参数
	Args:    [][]byte{[]byte("init"),[]byte("a"),[]byte("100"),[]byte("b"),[]byte("200")},
	Policy:  ccPolicy}
ccResponse, err := client.InstantiateCC(channelID, instantiateCCReq)
if err != nil {
	panic(err)
}
if ccResponse.TransactionID == "" {
	fmt.Println("err: ccResponse.TransactionID==''")
	return
}
```

### 创建通道客户端
```go
//	TODO: 这里的User1是哪里来的
cuser := fabsdk.WithUser("User1")
ccContext := sdk.ChannelContext(channelID, cuser)
cClient, err := channel.New(ccContext)
if err != nil {
	panic(err)
}
```

### 查询
```go
queryRequest := channel.Request{
	ChaincodeID: channelName,
	Fcn:         "query",
	//	同样需要根据链码来输入参数
	Args:        [][]byte{[]byte("a")},
}
res, err := cClient.Query(queryRequest)
if err != nil {
	panic(err)
}
fmt.Println(string(res.Payload))
```





# Fabric ca

```yaml
# Version of config file
version: 1.4.4

# Server's listening port (default: 7054)
port: 7054

# Cross-Origin Resource Sharing (CORS)
cors:
    enabled: false
    origins:
      - "*"

# Enables debug logging (default: false)
debug: false

# Size limit of an acceptable CRL in bytes (default: 512000)
# 证书的最大size
crlsizelimit: 512000

crl:
  # 授权证书（crl）的有效期
  expiry: 24h

# 支持ca的数目
cacount:

# 相关ca的配置文件
cafiles:

# TLS通信相关配置
tls:
  # Enable TLS (default: false)
  enabled: false
  # TLS for the server's listening port
  # TLS证书文件
  certfile:
  # TLS私钥文件
  keyfile:
  clientauth:
    # 客户端类型
    type: noclientcert
    # 客户端证书类型
    certfiles:

# ca服务器属性配置
ca:
  # ca的名字，不可重复
  name:
  # Key file (is only used to import a private key into BCCSP)
  # 私钥文件
  keyfile:
  # Certificate file (default: ca-cert.pem)
  # 证书文件
  certfile:
  # Chain file
  # 证书链文件
  chainfile:

registry:
  # Maximum number of times a password/secret can be reused for enrollment
  # 注册时可以重用密码/密匙的最大次数
  # (default: -1, which means there is no limit) 0表示不支持登记
  maxenrollments: -1

  # Contains identity information which is used when LDAP is disabled
  # 注册的实体信息，可以有多个
  identities:
     - name: admin
       pass: adminpw
       type: client
       affiliation: ""
       attrs:
          hf.Registrar.Roles: "*"
          hf.Registrar.DelegateRoles: "*"
          hf.Revoker: true
          hf.IntermediateCA: true
          hf.GenCRL: true
          hf.Registrar.Attributes: "*"
          hf.AffiliationMgr: true

# 数据库相关配置，除了sqlite3，还支持MySQL和postgres，配置方式见书112页
db:
  type: sqlite3
  datasource: fabric-ca-server.db
  tls:
      enabled: false
      certfiles:
      client:
        certfile:
        keyfile:

# 配置使用远端LDAP服务器来进行注册管理，并且保存注册相关的数据
ldap:
   # Enables or disables the LDAP client (default: false)
   # If this is set to true, the "registry" section is ignored.
   enabled: false
   # The URL of the LDAP server
   url: ldap://<adminDN>:<adminPassword>@<host>:<port>/<base>
   # TLS configuration for the client connection to the LDAP server
   tls:
      certfiles:
      client:
         certfile:
         keyfile:
   # Attribute related configuration for mapping from LDAP entries to Fabric CA attributes
   attribute:
      # 'names' is an array of strings containing the LDAP attribute names which are
      # requested from the LDAP server for an LDAP identity's entry
      names: ['uid','member']
      converters:
         - name:
           value:

      maps:
         groups:
            - name:
              value:

# 组织中部门相关配置信息，客户端和SDK调用的时候用到时，需要和这里一致
affiliations:
   org1:
      - department1
      - department2
   org2:
      - department1


signing:
    default:
      usage:
        - digital signature
      expiry: 8760h
    profiles:
      ca:
         usage:
           - cert sign
           - crl sign
         expiry: 43800h
         caconstraint:
           isca: true
           maxpathlen: 0
      tls:
         usage:
            - signing
            - key encipherment
            - server auth
            - client auth
            - key agreement
         expiry: 8760h


csr:
   cn: fabric-ca-server
   keyrequest:
     algo: ecdsa
     size: 256
   names:
      - C: US
        ST: "North Carolina"
        L:
        O: Hyperledger
        OU: Fabric
   hosts:
     - shalom
     - localhost
   ca:
      expiry: 131400h
      pathlength: 1


idemix:
  
  rhpoolsize: 1000

  nonceexpiration: 15s

  # Specifies interval at which expired nonces are removed from datastore. Default value is 15 minutes.
  #  The value is expressed in the time.Duration format (see https://golang.org/pkg/time/#ParseDuration)
  noncesweepinterval: 15m

bccsp:
    default: SW
    sw:
        hash: SHA2
        security: 256
        filekeystore:
            # The directory used for the software file-based keystore
            keystore: msp/keystore



intermediate:
  parentserver:
    url:
    caname:

  enrollment:
    hosts:
    profile:
    label:

  tls:
    certfiles:
    client:
      certfile:
      keyfile:

cfg:
  identities:
    passwordattempts: 10


operations:
    # host and port for the operations server
    listenAddress: 127.0.0.1:9443

    # TLS configuration for the operations endpoint
    tls:
        # TLS enabled
        enabled: false

        # path to PEM encoded server certificate for the operations server
        cert:
            file:

        # path to PEM encoded server key for the operations server
        key:
            file:

        # require client certificate authentication to access all resources
        clientAuthRequired: false

        # paths to PEM encoded ca certificates to trust for client authentication
        clientRootCAs:
            files: []

metrics:
    # statsd, prometheus, or disabled
    provider: disabled

    # statsd configuration
    statsd:
        # network type: tcp or udp
        network: udp

        # statsd server address
        address: 127.0.0.1:8125

        # the interval at which locally cached counters and gauges are pushsed
        # to statsd; timings are pushed immediately
        writeInterval: 10s

        # prefix is prepended to all emitted statsd merics
        prefix: server
```

