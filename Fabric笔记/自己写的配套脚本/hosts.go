package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"sync"
)

var (
	oldStr01 = `      - "orderer0.example.com:192.168.1.111"
      - "orderer1.example.com:192.168.1.112"
      - "orderer2.example.com:192.168.1.113"
      - "peer0.org1.example.com:192.168.1.111"
      - "peer1.org1.example.com:192.168.1.112"
      - "peer0.org2.example.com:192.168.1.113"
      - "peer1.org2.example.com:192.168.1.114"`

	oldStr02 = `      - "zookeeper0:192.168.1.111"
      - "zookeeper1:192.168.1.112"
      - "zookeeper2:192.168.1.113"
      - "kafka0:192.168.1.111"
      - "kafka1:192.168.1.112"
      - "kafka2:192.168.1.113"
      - "kafka3:192.168.1.114"`

	oldStr03 = `192.168.1.111  peer0.org1.example.com
192.168.1.112  peer1.org1.example.com
192.168.1.113  peer0.org2.example.com
192.168.1.114  peer1.org2.example.com

192.168.1.111  orderer0.example.com
192.168.1.112  orderer1.example.com
192.168.1.113  orderer2.example.com

192.168.1.111  zookeeper0
192.168.1.112  zookeeper1
192.168.1.113  zookeeper2

192.168.1.111  kafka0
192.168.1.112  kafka1
192.168.1.113  kafka2
192.168.1.114  kafka3`

	oldStr04 = "192.168.1.111:9092,192.168.1.112:9092,192.168.1.113:9092,192.168.1.114:9092"

	paths = []string{
		`C:\Users\Administrator\Desktop\fabric\share\000003share\kafka\192.168.43.11`,
		`C:\Users\Administrator\Desktop\fabric\share\000003share\kafka\192.168.43.12`,
		`C:\Users\Administrator\Desktop\fabric\share\000003share\kafka\192.168.43.13`,
		`C:\Users\Administrator\Desktop\fabric\share\000003share\kafka\192.168.43.14`,
	}
	members = []string{
		"/docker-compose-kafka.yaml",
		"/docker-compose-zookeeper.yaml",
		"/docker-compose-peer.yaml",
		"/docker-compose-orderer.yaml",
	}
)

func makeMap(m map[string]string) {
	m["192.168.1.111"] = "192.168.1.211"
	m["192.168.1.112"] = "192.168.1.212"
	m["192.168.1.113"] = "192.168.1.213"
	m["192.168.1.114"] = "192.168.1.214"
}

func dealOldStr(m map[string]string, s string) string {
	for key, value := range m {
		s = strings.Replace(s, key, value, 4)
	}

	return s
}

func dealPath(path string) string {
	return strings.Replace(path, `\`, "/", -1)
}

func Exist(filename string) bool {
	_, err := os.Stat(filename)
	return err == nil || os.IsExist(err)
}

func main() {
	ipMap := make(map[string]string)
	makeMap(ipMap)
	fmt.Println(ipMap)
	lock := sync.Mutex{}

	wg := &sync.WaitGroup{}

	for _, path := range paths {
		s := dealPath(path)
		//fmt.Println(s)
		for _, m := range members {
			filePath := s + m
			if Exist(filePath) {
				wg.Add(1)
				go func(path string) {
					defer wg.Done()
					var resStr string

					bytes, e := ioutil.ReadFile(path)
					if e != nil {
						fmt.Printf("err ioutil.ReadFile: %s", e)
						return
					}

					lock.Lock()
					fmt.Printf("--------------------- start ------------------------ path: %s\n",path)
					resStr = string(bytes)
					fmt.Println(resStr)
					for key, value := range ipMap {
						fmt.Println("key->",key)
						fmt.Println("value->",value)

						resStr = strings.Replace(resStr, key, value, -1)
					}
					fmt.Println(resStr)
					err := ioutil.WriteFile(path, []byte(resStr), 0666)
					if err != nil {
						fmt.Printf("err ioutil.WriteFile: %s", e)
						return
					}
					fmt.Printf("--------------------- end ------------------------ path: %s\n",path)
					lock.Unlock()
				}(filePath)
			}else {
				fmt.Println("=============file is not exit==============")
			}
		}
	}

	wg.Wait()
}
