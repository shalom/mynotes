#!/bin/bash
docker-compose -f ./docker-compose-zookeeper.yaml up -d
sleep 10
docker-compose -f ./docker-compose-kafka.yaml up -d
sleep 10
docker-compose -f ./docker-compose-orderer.yaml up -d
sleep 10
docker-compose -f ./docker-compose-peer.yaml up -d
