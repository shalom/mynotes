#!/bin/sh
docker-compose -f ./docker-compose-peer.yaml down -v
docker-compose -f ./docker-compose-kafka.yaml down -v
docker rm -vf $(docker ps -aq)
docker rmi --force `docker images | grep dev-peer | awk '{print $3}'`