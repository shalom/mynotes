# Fabric 1.4.1 配置文件解读

## 1. 解读crypto-config.yaml

```yaml
# 排序节点组织的配置信息
OrdererOrgs:
  # 排序节点组织的名称,自定义
  - Name: Orderer
    # 排序节点组织的根域名,自定义
    Domain: example.com
    CA:
      Country: US
      Province: California
      Locality: San Francisco
    # 手动指定生成
    Specs:
      # 排序节点对应的域名,自定义
      - Hostname: orderer1.example.com
        # 排序节点使用的域名,自定义
        CommonName: orderer1.example.com
      - Hostname: orderer2.example.com
        CommonName: orderer2.example.com
      - Hostname: orderer3.example.com
        CommonName: orderer3.example.com
# peer节点组织的配置信息
PeerOrgs:
  # 第一个组织的名称,自定义
  - Name: Org1
    # 第一个组织的根域名,自定义
    Domain: org1.example.com
    # 是否在msp下生成config.yaml文件
    EnableNodeOUs: true
    CA:
      Country: US
      Province: California
      Locality: San Francisco
    # 手动指定生成
    Specs:
      # peer节点对应的域名,自定义
      - Hostname: peer1.org1.example.com
        # peer节点使用的域名,自定义
        CommonName: peer1.org1.example.com
    # 模版自动生成
    Template:
      # 生成1套公私钥+证书,默认生成规则是peer0-9.组织的域名
      Count: 1
    # 创建的普通用户的个数
    Users:
      # 除了admin用户,额外生成1个普通用户
      Count: 1
```



## 2. 解读configtx.yaml

```yaml
# 定义具体的组织描述信息,被Profiles模板引用
Organizations:
  # 排序节点组织的配置信息,自定义
  - &OrdererOrg
    # 排序节点组织的名称,自定义
    Name: OrdererOrg
    # 排序节点组织的MSP标识ID,自定义
    ID: OrdererMSP
    # 排序节点组织的MSP配置文件路径
    MSPDir: crypto-config/ordererOrganizations/example.com/msp
    # 设置权限规则
    Policies:
      Readers:
        Type: Signature
        Rule: "OR('OrdererMSP.member')"
      Writers:
        Type: Signature
        Rule: "OR('OrdererMSP.member')"
      Admins:
        Type: Signature
        Rule: "OR('OrdererMSP.admin')"
  # 第一个组织的配置信息,自定义
  - &Org1
    # 第一个组织的名称,自定义
    Name: Org1MSP
    # 第一个组织的MSP标识ID,自定义
    ID: Org1MSP
    # 第一个组织的MSP配置文件路径,自定义
    MSPDir: crypto-config/peerOrganizations/org1.example.com/msp
    # 设置权限规则
    Policies:
      Readers:
        Type: Signature
        Rule: "OR('Org1MSP.admin','Org1MSP.peer','Org1MSP.client')"
      Writers:
        Type: Signature
        Rule: "OR('Org1MSP.admin','Org1MSP.client')"
      Admins:
        Type: Signature
        Rule: "OR('Org1MSP.admin')"
    # 第一个组织的锚节点
    AnchorPeers:
      # 指定一个peer节点的域名,端口7051固定不变
      - Host: peer1.org1.example.com
        Port: 7051
# 定义全局描述信息
Capabilities:
  # 全局通道
  Channel: &ChannelCapabilities
    V1_3: true
  # 系统通道
  Orderer: &OrdererCapabilities
    V1_1: true
  # 应用通道
  Application: &ApplicationCapabilities
    V1_3: true
    V1_2: false
    V1_1: false
# 定义具体的应用通道相关描述信息,被Profiles模板引用
Application: &ApplicationDefaults
  # 应用通道组织
  Organizations:
  # 设置权限规则
  Policies:
    Readers:
      Type: ImplicitMeta
      Rule: "ANY Readers"
    Writers:
      Type: ImplicitMeta
      Rule: "ANY Writers"
    Admins:
      Type: ImplicitMeta
      Rule: "MAJORITY Admins"
  Capabilities:
    <<: *ApplicationCapabilities
# 定义具体的排序相关描述信息,被Profiles模板引用
Orderer: &OrdererDefaults
  # 排序节点的共识机制
  OrdererType: etcdraft
  # 排序节点监听的地址,端口7050固定不变
  Addresses:
    - orderer1.example.com:7050
    - orderer2.example.com:7050
    - orderer3.example.com:7050
  # 打包交易消息出块的超时时间
  BatchTimeout: 2s
  BatchSize:
    # 打包交易消息出块的最大消息个数
    MaxMessageCount: 200
    # 打包交易消息出块的最大字节数,可以适当调大以防止同步区块数量过多
    AbsoluteMaxBytes: 2 MB
    # 通常情况下打包交易消息出块的建议字节数
    PreferredMaxBytes: 512 KB
  # kafka的配置信息
  Kafka:
    # Broker服务器地址列表
    Brokers:
      - kafka1.example.com:9092
      - kafka2.example.com:9092
      - kafka3.example.com:9092
      - kafka4.example.com:9092
  # EtcdRaft的配置信息
  EtcdRaft:
    Consenters:
      - Host: orderer1.example.com
        Port: 7050
        ClientTLSCert: crypto-config/ordererOrganizations/example.com/orderers/orderer1.example.com/tls/server.crt
        ServerTLSCert: crypto-config/ordererOrganizations/example.com/orderers/orderer1.example.com/tls/server.crt
      - Host: orderer2.example.com
        Port: 7050
        ClientTLSCert: crypto-config/ordererOrganizations/example.com/orderers/orderer2.example.com/tls/server.crt
        ServerTLSCert: crypto-config/ordererOrganizations/example.com/orderers/orderer2.example.com/tls/server.crt
      - Host: orderer3.example.com
        Port: 7050
        ClientTLSCert: crypto-config/ordererOrganizations/example.com/orderers/orderer3.example.com/tls/server.crt
        ServerTLSCert: crypto-config/ordererOrganizations/example.com/orderers/orderer3.example.com/tls/server.crt
    Options:
      # 两次Node.Tick调用的时间间隔
      TickInterval: 500ms
      # 如果follower在当前阶段,经过ElectionTick次的时间后,没有从主节点收到任何消息,follower将会成为候选节点并发起选举
      ElectionTick: 10
      # 主节点在HeartbeatTick间隔内发送心跳信息给从节点,来保持它主节点身份
      HeartbeatTick: 1
      # 在乐观复制阶段限制正在进行的附加块的最大数量
      MaxInflightBlocks: 5
      # 每个快照的大小
      SnapshotIntervalSize: 200
  Organizations:
  # 设置权限规则
  Policies:
    Readers:
      Type: ImplicitMeta
      Rule: "ANY Readers"
    Writers:
      Type: ImplicitMeta
      Rule: "ANY Writers"
    Admins:
      Type: ImplicitMeta
      Rule: "MAJORITY Admins"
    BlockValidation:
      Type: ImplicitMeta
      Rule: "ANY Writers"
# 定义具体的通道相关描述信息,被Profiles模板引用
Channel: &ChannelDefaults
  # 设置权限规则
  Policies:
    Readers:
      Type: ImplicitMeta
      Rule: "ANY Readers"
    Writers:
      Type: ImplicitMeta
      Rule: "ANY Writers"
    Admins:
      Type: ImplicitMeta
      Rule: "MAJORITY Admins"
  Capabilities:
    <<: *ChannelCapabilities
# 定义系统通道和应用通道配置信息,从Profiles获取-profile后边的参数
Profiles:
  # 系统通道配置标识符,自定义
  ExampleOrdererGenesis:
    # 具体通道相关配置信息引用ChannelDefaults
    <<: *ChannelDefaults
    # 系统通道配置信息
    Orderer:
      # 具体配置信息引用OrdererDefaults
      <<: *OrdererDefaults
      # 系统通道组织
      Organizations:
        # 具体配置信息引用OrdererOrg
        - *OrdererOrg
      # 定义系统通道全局功能特性
      Capabilities:
        # 具体通道排序配置信息引用OrdererCapabilities
        <<: *OrdererCapabilities
    # 系统通道信息
    Application:
      # 具体应用通道配置信息引用ApplicationDefaults
      <<: *ApplicationDefaults
      # 系统通道组织
      Organizations:
      # 具体配置信息引用OrdererOrg
      - <<: *OrdererOrg
    # 联盟的列表
    Consortiums:
      # 联盟的名称
      ExampleConsortium:
        # 联盟的组织列表
        Organizations:
          - *Org1
  # 应用通道配置标识符,自定义
  ExampleChannel:
    # 应用通道关联的联盟名称
    Consortium: ExampleConsortium
    # 具体通道相关配置信息引用ChannelDefaults
    <<: *ChannelDefaults
    # 应用通道信息
    Application:
      # 具体应用通道配置信息引用ApplicationDefaults
      <<: *ApplicationDefaults
      # 联盟的组织列表
      Organizations:
        - *Org1
      # 定义应用通道全局功能特性
      Capabilities:
        # 具体应用通道配置信息引用ApplicationCapabilities
        <<: *ApplicationCapabilities
```



## 3. 解读zookeeper.yaml

```yaml
version: '2'
services:
  # 服务器名,自定义
  zookeeper1.example.com:
    # 容器名,自定义
    container_name: zookeeper1.example.com
    # 访问的主机名,需要和IP有对应关系,自定义
    hostname: zookeeper1.example.com
    # 启动容器使用的镜像名
    image: hyperledger/fabric-zookeeper:latest
    # 指定为always,不管退出状态码是什么始终重启容器
    restart: always
    # 配置环境变量
    environment:
      - ZOO_DATA_DIR=/data
      - ZOO_DATA_LOG_DIR=/data-log
      # 服务器在集群中的ID,必须是唯一的,取值范围1~255
      - ZOO_MY_ID=1
      # 服务器集群的服务器列表
      - ZOO_SERVERS=server.1=zookeeper1.example.com:2888:3888 server.2=zookeeper2.example.com:2888:3888 server.3=zookeeper3.example.com:2888:3888
    # 设置容器和宿主机的端口映射
    ports:
      # 访问zookeeper的端口
      - 2181:2181
      # 集群中追随者连接Leader的端口
      - 2888:2888
      # 集群中选举Leader的端口
      - 3888:3888
    # 设置容器卷和宿主机的映射
    volumes:
      - /opt/fabric/zookeeper1.example.com/data:/data
      - /opt/fabric/zookeeper1.example.com/data-log:/data-log
    # 设置服务器名和IP的映射
    extra_hosts:
      - "zookeeper1.example.com:192.168.1.251"
      - "zookeeper2.example.com:192.168.1.252"
      - "zookeeper3.example.com:192.168.1.253"
      - "kafka1.example.com:192.168.1.251"
      - "kafka2.example.com:192.168.1.252"
      - "kafka3.example.com:192.168.1.253"
      - "kafka4.example.com:192.168.1.254"
```



## 4. 解读kafka.yaml

```yaml
version: '2'
services:
  # 服务器名,自定义
  kafka1.example.com:
    # 容器名,自定义
    container_name: kafka1.example.com
    # 访问的主机名,需要和IP有对应关系,自定义
    hostname: kafka1.example.com
    # 启动容器使用的镜像名
    image: hyperledger/fabric-kafka:latest
    # 指定为always,不管退出状态码是什么始终重启容器
    restart: always
    # 配置环境变量
    environment:
      - LOG_DIR=/kafka-data
      # 消息的最大字节数,AbsoluteMaxBytes+1M的数据头
      - KAFKA_MESSAGE_MAX_BYTES=103809024 # 99 * 1024 * 1024 B
      # 副本最大字节数,值要小于等于KAFKA_MESSAGE_MAX_BYTES
      - KAFKA_REPLICA_FETCH_MAX_BYTES=103809024 # 99 * 1024 * 1024 B
      # 非一致性的Leader选举,开启true,关闭false
      - KAFKA_UNCLEAN_LEADER_ELECTION_ENABLE=false
      # 是一个唯一的非负整数,可以作为代理Broker的名字
      - KAFKA_BROKER_ID=1
      # kakfa集群个数>KAFKA_DEFAULT_REPLICATION_FACTOR>KAFKA_MIN_INSYNC_REPLICAS
      # 最小同步备份
      - KAFKA_MIN_INSYNC_REPLICAS=2
      # 默认同步备份
      - KAFKA_DEFAULT_REPLICATION_FACTOR=3
      # 指向zookeeper节点的集合,顺序必须和zookeeper配置文件中的ZOO_SERVERS配置顺序保持一致
      - KAFKA_ZOOKEEPER_CONNECT=zookeeper1.example.com:2181,zookeeper2.example.com:2181,zookeeper3.example.com:2181
    # 设置容器和宿主机的端口映射
    ports:
      # 访问kafka的端口
      - 9092:9092
    # 设置容器卷和宿主机的映射
    volumes:
      - /opt/fabric/kafka1.example.com:/kafka-data
    # 设置服务器名和IP的映射
    extra_hosts:
      - "zookeeper1.example.com:192.168.1.251"
      - "zookeeper2.example.com:192.168.1.252"
      - "zookeeper3.example.com:192.168.1.253"
      - "kafka1.example.com:192.168.1.251"
      - "kafka2.example.com:192.168.1.252"
      - "kafka3.example.com:192.168.1.253"
      - "kafka4.example.com:192.168.1.254"
```



## 5. 解读orderer.yaml

```yaml
version: '2'
services:
  # 服务器名,自定义
  orderer1.example.com:
    # 容器名,自定义
    container_name: orderer1.example.com
    # 访问的主机名,需要和IP有对应关系,自定义
    hostname: orderer1.example.com
    # 启动容器使用的镜像名
    image: hyperledger/fabric-orderer:latest
    # 指定为always,不管退出状态码是什么始终重启容器
    restart: always
    # 配置环境变量
    environment:
      # 不采用cgo resolver,而采用pure go resolver
      - GODEBUG=netdns=go
      # 日志级别,critical、error、warning、notice、info、debug
      - FABRIC_LOGGING_SPEC=INFO
      # 服务器监听的地址
      - ORDERER_GENERAL_LISTENADDRESS=0.0.0.0
      # 初始化块(Genesis)的来源方式,一般赋值为file即可
      - ORDERER_GENERAL_GENESISMETHOD=file
      # 存储初始块文件的路径
      - ORDERER_GENERAL_GENESISFILE=/var/hyperledger/orderer/orderer.genesis.block
      # 节点的编号,在configtxgen模块配置文件中指定的,configtx.yaml配置文件中排序节点的组织的ID
      - ORDERER_GENERAL_LOCALMSPID=OrdererMSP
      # 节点的msp文件路径
      - ORDERER_GENERAL_LOCALMSPDIR=/var/hyperledger/orderer/msp
      # 是否启用TLS,true/false
      - ORDERER_GENERAL_TLS_ENABLED=true
      # 节点的私钥文件
      - ORDERER_GENERAL_TLS_PRIVATEKEY=/var/hyperledger/orderer/tls/server.key
      # 节点的证书文件
      - ORDERER_GENERAL_TLS_CERTIFICATE=/var/hyperledger/orderer/tls/server.crt
      # 节点的根证书文件
      - ORDERER_GENERAL_TLS_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]
      # EtcdRaft私钥文件
      - ORDERER_GENERAL_CLUSTER_CLIENTPRIVATEKEY=/var/hyperledger/orderer/tls/server.key
      # EtcdRaft证书文件
      - ORDERER_GENERAL_CLUSTER_CLIENTCERTIFICATE=/var/hyperledger/orderer/tls/server.crt
      # EtcdRaft根证书文件
      - ORDERER_GENERAL_CLUSTER_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]
      # 断线重连第二阶段,每隔多长时间进行一次重试
      - ORDERER_KAFKA_RETRY_LONGINTERVAL=10s
      # 断线重连第二阶段,总共重试的时长
      - ORDERER_KAFKA_RETRY_LONGTOTAL=100s
      # 断线重连第一阶段,每隔多长时间进行一次重试
      - ORDERER_KAFKA_RETRY_SHORTINTERVAL=1s
      # 断线重连第一阶段,总共重试的时长
      - ORDERER_KAFKA_RETRY_SHORTTOTAL=30s
      # 启用日志与kafka进行交互,启用true,不启用false
      - ORDERER_KAFKA_VERBOSE=true
      # 指向kafka节点的集合
      - ORDERER_KAFKA_BROKERS=[192.168.1.251:9092,192.168.1.252:9092,192.168.1.253:9092,192.168.1.254:9092]
    # 设置工作目录(落脚点)
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric
    # 设置启动命令
    command: orderer
    # 设置容器卷和宿主机的映射
    volumes:
      - ./channel-artifacts/genesis.block:/var/hyperledger/orderer/orderer.genesis.block
      - ./crypto-config/ordererOrganizations/example.com/orderers/orderer1.example.com/msp:/var/hyperledger/orderer/msp
      - ./crypto-config/ordererOrganizations/example.com/orderers/orderer1.example.com/tls/:/var/hyperledger/orderer/tls
      - /opt/fabric/orderer1.example.com:/var/hyperledger/production
    # 设置容器和宿主机的端口映射
    ports:
      # 访问orderer的端口
      - 7050:7050
    # 设置服务器名和IP的映射
    extra_hosts:
      - "orderer1.example.com:192.168.1.251"
      - "orderer2.example.com:192.168.1.252"
      - "orderer3.example.com:192.168.1.253"
      - "kafka1.example.com:192.168.1.251"
      - "kafka2.example.com:192.168.1.252"
      - "kafka3.example.com:192.168.1.253"
      - "kafka4.example.com:192.168.1.254"
```



## 6. 解读ca.yaml

```yaml
version: '2'
services:
  # 服务器名,自定义
  ca.org1.example.com:
    # 容器名,自定义
    container_name: ca.org1.example.com
    # 访问的主机名,需要和IP有对应关系,自定义
    hostname: ca.org1.example.com
    # 启动容器使用的镜像名
    image: hyperledger/fabric-ca:latest
    # 指定为always,不管退出状态码是什么始终重启容器
    restart: always
    # 配置环境变量
    environment:
      # 设置容器中的home目录
      - FABRIC_CA_HOME=/etc/hyperledger/fabric-ca-server
      # 节点的名字,自定义
      - FABRIC_CA_SERVER_CA_NAME=ca.org1.example.com
      # 节点的证书文件
      - FABRIC_CA_SERVER_CA_CERTFILE=/etc/hyperledger/fabric-ca-server-
      config/ca.org1.example.com-cert.pem
      # 节点的私钥文件
      - FABRIC_CA_SERVER_CA_KEYFILE=/etc/hyperledger/fabric-ca-server-config/c16b0f55288466c9cd1939b16dd93b090a2357230e3a07c599fb563c02fb9893_sk
    # 设置容器和宿主机的端口映射
    ports:
      # 访问ca的端口
      - 7054:7054
    # 设置启动命令,--cfg参数表示允许删除联盟和用户
    command: sh -c 'fabric-ca-server start -b admin:adminpw --cfg.affiliations.allowremove --cfg.identities.allowremove -d'
    # 设置容器卷和宿主机的映射
    volumes:
      - ./crypto-config/peerOrganizations/org1.example.com/ca/:/etc/hyperledger/fabric-ca-server-config
      - ./ca/:/etc/hyperledger/fabric-ca-server
      - /opt/fabric/ca.org1.example.com:/var/hyperledger/production
```



## 7. 解读peer.yaml

```yaml
version: '2'
services:
  # 服务器名,自定义
  peer1.org1.couchdb:
    # 容器名,自定义
    container_name: peer1.org1.couchdb
    # 访问的主机名,需要和IP有对应关系,自定义
    hostname: peer1.org1.couchdb
    # 启动容器使用的镜像名
    image: hyperledger/fabric-couchdb:latest
    # 指定为always,不管退出状态码是什么始终重启容器
    restart: always
    # 配置环境变量
    environment:
      # 数据库的用户名
      - COUCHDB_USER=example
      # 数据库的密码
      - COUCHDB_PASSWORD=123
    # 设置容器和宿主机的端口映射
    ports:
      # 访问couchdb的端口
      - 5984:5984
    # 设置容器卷和宿主机的映射
    volumes:
      - /opt/fabric/peer1.org1.couchdb/data:/data/_users.couch
  # 服务器名,自定义
  peer1.org1.example.com:
    # 容器名,自定义
    container_name: peer1.org1.example.com
    # 访问的主机名,需要和IP有对应关系,自定义
    hostname: peer1.org1.example.com
    # 启动容器使用的镜像名
    image: hyperledger/fabric-peer:latest
    # 指定为always,不管退出状态码是什么始终重启容器
    restart: always
    # 配置环境变量
    environment:
      # 不采用cgo resolver,而采用pure go resolver
      - GODEBUG=netdns=go
      # 日志级别,critical、error、warning、notice、info、debug
      - FABRIC_LOGGING_SPEC=INFO
      # docker服务器的Deamon地址,默认取端口的套接字,unix:///host/var/run/docker.sock
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      # peer节点的编号,自定义
      - CORE_PEER_ID=peer1.org1.example.com
      # 当前peer节点的访问地址,域名:端口/IP:端口
      - CORE_PEER_ADDRESS=peer1.org1.example.com:7051
      # chaincode的监听地址
      - CORE_PEER_CHAINCODELISTENADDRESS=peer1.org1.example.com:7052
      # 节点被组织外节点感知时的地址,默认为空,代表不被其他组织节点所感知
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer1.org1.example.com:7051
      # 启动的时候,指定连接谁,一般写自己就行,自定义
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer1.org1.example.com:7051
      # peer节点所属的组织的编号,在configtxgen.yaml中设置的
      - CORE_PEER_LOCALMSPID=Org1MSP
      # 是否自动选举leader节点,自动true
      - CORE_PEER_GOSSIP_USELEADERELECTION=true
      # 当前节点是否为leader节点,是true
      - CORE_PEER_GOSSIP_ORGLEADER=false
      # 使用peer内置的profile server,默认时运行在6060端口上的并且默认关闭
      - CORE_PEER_PROFILE_ENABLED=true
      # 是否激活tls,激活:true,不激活:false
      - CORE_PEER_TLS_ENABLED=true
      # 节点的私钥文件
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      # 节点的证书文件
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      # 节点的根证书文件
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
      # 设置使用couchdb存储
      - CORE_LEDGER_STATE_STATEDATABASE=CouchDB
      # 设置连接的couchdb域名和端口
      - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=peer1.org1.couchdb:5984
      # 设置连接的couchdb用户名
      - CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=example
      # 设置连接的couchdb密码
      - CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=123
    # 设置工作目录(落脚点)
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    # 设置启动命令
    command: peer node start
    # 设置容器卷和宿主机的映射
    volumes:
      - /var/run/:/host/var/run/
      - ./crypto-config/peerOrganizations/org1.example.com/peers/peer1.org1.example.com/msp:/etc/hyperledger/fabric/msp
      - ./crypto-config/peerOrganizations/org1.example.com/peers/peer1.org1.example.com/tls:/etc/hyperledger/fabric/tls
      - /opt/fabric/peer1.org1.example.com:/var/hyperledger/production
    # 设置容器和宿主机的端口映射
    ports:
      # gRPC服务监听端口
      - 7051:7051
      # CLI端口
      - 7052:7052
      # 事件服务端口
      - 7053:7053
    # 设置服务器名和IP的映射
    extra_hosts:
      - "orderer1.example.com:192.168.1.251"
      - "orderer2.example.com:192.168.1.252"
      - "orderer3.example.com:192.168.1.253"
      - "peer1.org1.couchdb:192.168.1.251"
```



## 8. 解读config.yaml

```yaml
name: "gosdk-service-network"
# 指定版本
version: 1.1.0
# 客户端
client:
  # 客户端组织的名称
  organization: org1
  # 定义日志服务
  logging:
    level: info
  # MSP根目录
  cryptoconfig:
    path: /home/wy/go/src/gosdk/conf/crypto-config
  # 某些SDK支持插件化的KV数据库,通过指定credentialStore属性实现
  credentialStore:
    # 可选,用于用户证书材料存储,如果所有的证书材料被嵌入到配置文件,则不需要
    path: /tmp/gosdk-service-store
    # 可选,指定GoSDK实现的CryptoSuite实现
    cryptoStore:
      # 指定用于加密密钥存储的底层KV数据库
      path: /tmp/gosdk-service-msp
  # 客户端的BCCSP模块配置
  BCCSP:
    security:
      enabled: true
      default:
        provider: "SW"
      hashAlgorithm: "SHA2"
      softVerify: true
      level: 256
  tlsCerts:
    # 可选,当连接到peers,orderes时使用系统证书池,默认为false
    systemCertPool: true
    # 可选,客户端和peers与orderes进行TLS握手的密钥和证书
    client:
      keyfile: /home/wy/go/src/gosdk/conf/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/tls/client.key
      certfile: /home/wy/go/src/gosdk/conf/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/tls/client.crt
# 通道
channels:
  # 自定义,通道名
  examplechannel:
    # 可选,参与的orderers列表
    orderers:
      - orderer1.example.com
      - orderer2.example.com
      - orderer3.example.com
    # 可选,参与的peers列表
    peers:
      peer1.org1.example.com:
        endorsingPeer: true
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
      peer1.org2.example.com:
        endorsingPeer: true
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
      peer1.org3.example.com:
        endorsingPeer: true
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
      peer1.org4.example.com:
        endorsingPeer: true
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
    # 可选,执行通道操作
    policies:
      # 可选,获取通道配置区块
      queryChannelConfig:
        # 可选,成功响应节点的最小数量
        minResponses: 1
        # 可选
        maxTargets: 1
        # 可选,查询配置区块的重试选项
        retryOpts:
          # 可选,重试次数
          attempts: 5
          # 可选,第一次重试的后退间隔
          initialBackoff: 500ms
          # 可选,重试的最大后退间隔
          maxBackoff: 5s
          backoffFactor: 2.0
# 组织         
organizations:
  # 自定义,组织名
  org1:
    # 组织的MSPID
    mspid: Org1MSP
    # 组织的MSP存储位置,绝对路径或相对路径
    cryptoPath: /home/wy/go/src/gosdk/conf/crypto-config/peerOrganizations/org1.example.com/users/{username}@org1.example.com/msp
    peers:
      - peer1.org1.example.com
    # 可选,证书颁发机构
    certificateAuthorities:
      - ca.org1.example.com
  org2:
    mspid: Org2MSP
    cryptoPath: /home/wy/go/src/gosdk/conf/crypto-config/peerOrganizations/org2.example.com/users/{username}@org2.example.com/msp
    peers:
      - peer1.org2.example.com
    certificateAuthorities:
      - ca.org2.example.com
  org3:
    mspid: Org3MSP
    cryptoPath: /home/wy/go/src/gosdk/conf/crypto-config/peerOrganizations/org3.example.com/users/{username}@org3.example.com/msp
    peers:
      - peer1.org3.example.com
    certificateAuthorities:
      - ca.org3.example.com
  org4:
    mspid: Org4MSP
    cryptoPath: /home/wy/go/src/gosdk/conf/crypto-config/peerOrganizations/org4.example.com/users/{username}@org4.example.com/msp
    peers:
      - peer1.org4.example.com
    certificateAuthorities:
      - ca.org4.example.com
  # 自定义,排序组织名
  ordererorg:
    # 排序组织的MSPID
    mspID: OrdererMSP
    # 排序组织的MSP存储位置,绝对路径或相对路径
    cryptoPath: /home/wy/go/src/gosdk/conf/crypto-config/ordererOrganizations/example.com/users/Admin@example.com/msp
# 排序    
orderers:
  # 自定义,排序节点域名
  orderer1.example.com:
    url: grpcs://192.168.1.251:7050
    grpcOptions:
      ssl-target-name-override: orderer1.example.com
      # 当keep-alive-time被设置为0或小于激活客户端的参数,下列参数失效
      keep-alive-time: 5s
      keep-alive-timeout: 6s
      keep-alive-permit: false
      fail-fast: true
      allow-insecure: false
    tlsCACerts:
      # 证书的绝对路径
      path: /home/wy/go/src/gosdk/conf/crypto-config/ordererOrganizations/example.com/orderers/orderer1.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
  orderer2.example.com:
    url: grpcs://192.168.1.252:7050
    grpcOptions:
      ssl-target-name-override: orderer2.example.com
      keep-alive-time: 5s
      keep-alive-timeout: 6s
      keep-alive-permit: false
      fail-fast: true
      allow-insecure: false
    tlsCACerts:
      path: /home/wy/go/src/gosdk/conf/crypto-config/ordererOrganizations/example.com/orderers/orderer2.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
  orderer3.example.com:
    url: grpcs://192.168.1.253:7050
    grpcOptions:
      ssl-target-name-override: orderer3.example.com
      keep-alive-time: 5s
      keep-alive-timeout: 6s
      keep-alive-permit: false
      fail-fast: true
      allow-insecure: false
    tlsCACerts:
      path: /home/wy/go/src/gosdk/conf/crypto-config/ordererOrganizations/example.com/orderers/orderer3.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
# 节点
peers:
  # 自定义,peer节点域名
  peer1.org1.example.com:
    # 用于发送背书和查询请求
    url: peer1.org1.example.com:7051
    grpcOptions:
      ssl-target-name-override: peer1.org1.example.com
      allow-insecure: false
    tlsCACerts:
      # 证书的绝对路径
      path: /home/wy/go/src/gosdk/conf/crypto-config/peerOrganizations/org1.example.com/tlsca/tlsca.org1.example.com-cert.pem
  peer1.org2.example.com:
    url: peer1.org2.example.com:7051
    grpcOptions:
      ssl-target-name-override: peer1.org2.example.com
      allow-insecure: false
    tlsCACerts:
      path: /home/wy/go/src/gosdk/conf/crypto-config/peerOrganizations/org2.example.com/tlsca/tlsca.org2.example.com-cert.pem
  peer1.org3.example.com:
    url: peer1.org3.example.com:7051
    grpcOptions:
      ssl-target-name-override: peer1.org3.example.com
      allow-insecure: false
    tlsCACerts:
      path: /home/wy/go/src/gosdk/conf/crypto-config/peerOrganizations/org3.example.com/tlsca/tlsca.org3.example.com-cert.pem
  peer1.org4.example.com:
    url: peer1.org4.example.com:7051
    grpcOptions:
      ssl-target-name-override: peer1.org4.example.com
      allow-insecure: false
    tlsCACerts:
      path: /home/wy/go/src/gosdk/conf/crypto-config/peerOrganizations/org4.example.com/tlsca/tlsca.org4.example.com-cert.pem
# 证书
certificateAuthorities:
  # 自定义,ca节点域名
  ca.org1.example.com:
    url: http://192.168.1.251:7054
    # 包含CA的TLS证书的路径或直接包含证书的PEM内容,协议url必须是https
    tlsCACerts:
      path: /home/wy/go/src/gosdk/conf/ca.org1.example.com/msp/cacerts/192-168-1-251-7054.pem
      client:
        key:
          path: /home/wy/go/src/gosdk/conf/ca.org1.example.com/msp/keystore/e936910ed0f24ba01dca1afedecedf43027f1ee15415c58a10ef1e1e9bcdf8b0_sk
        cert:
          path: /home/wy/go/src/gosdk/conf/ca.org1.example.com/msp/signcerts/cert.pem
    # 注册商ID和密码的集合,每个CA只支持一个注册商
    registrar:
      enrollId: admin
      enrollSecret: adminpw
    # 可选,CA机构名称
    caName: ca.org1.example.com
  ca.org2.example.com:
    url: http://192.168.1.252:7054
    tlsCACerts:
      path: /home/wy/go/src/gosdk/conf/ca.org2.example.com/msp/cacerts/192-168-1-252-7054.pem
      client:
        key:
          path: /home/wy/go/src/gosdk/conf/ca.org2.example.com/msp/keystore/44adf491fa454dbbb1d110372fbbb5387786580c90f227d5d0ce1f23f7995b1f_sk
        cert:
          path: /home/wy/go/src/gosdk/conf/ca.org2.example.com/msp/signcerts/cert.pem
    registrar:
      enrollId: admin
      enrollSecret: adminpw
    caName: ca.org2.example.com
  ca.org3.example.com:
    url: http://192.168.1.253:7054
    tlsCACerts:
      path: /home/wy/go/src/gosdk/conf/ca.org3.example.com/msp/cacerts/192-168-1-253-7054.pem
      client:
        key:
          path: /home/wy/go/src/gosdk/conf/ca.org3.example.com/msp/keystore/21e582f4db08bfe1195b9d9a4ada58250c3c030d23f450aadbbab0bfe3e25463_sk
        cert:
          path: /home/wy/go/src/gosdk/conf/ca.org3.example.com/msp/signcerts/cert.pem
    registrar:
      enrollId: admin
      enrollSecret: adminpw
    caName: ca.org3.example.com
  ca.org4.example.com:
    url: http://192.168.1.254:7054
    tlsCACerts:
      path: /home/wy/go/src/gosdk/conf/ca.org4.example.com/msp/cacerts/192-168-1-254-7054.pem
      client:
        key:
          path: /home/wy/go/src/gosdk/conf/ca.org4.example.com/msp/keystore/08e3382067143b202555631c372fbcf7e567d79dfdaf3c5c96a5d63bcde156e1_sk
        cert:
          path: /home/wy/go/src/gosdk/conf/ca.org4.example.com/msp/signcerts/cert.pem
    registrar:
      enrollId: admin
      enrollSecret: adminpw
    caName: ca.org4.example.com
# 映射
entityMatchers:
  peer:
  - pattern: (\w*)peer1.org1(\w*)
    urlSubstitutionExp: 192.168.1.251:7051
    sslTargetOverrideUrlSubstitutionExp: peer1.org1.example.com
    mappedHost: peer1.org1.example.com
  - pattern: (\w*)peer1.org2(\w*)
    urlSubstitutionExp: 192.168.1.252:7051
    sslTargetOverrideUrlSubstitutionExp: peer1.org2.example.com
    mappedHost: peer1.org2.example.com
  - pattern: (\w*)peer1.org3(\w*)
    urlSubstitutionExp: 192.168.1.253:7051
    sslTargetOverrideUrlSubstitutionExp: peer1.org3.example.com
    mappedHost: peer1.org3.example.com
  - pattern: (\w*)peer1.org4(\w*)
    urlSubstitutionExp: 192.168.1.254:7051
    sslTargetOverrideUrlSubstitutionExp: peer1.org4.example.com
    mappedHost: peer1.org4.example.com
  orderer:
  - pattern: (\w*)orderer1(\w*)
    urlSubstitutionExp: 192.168.1.251:7050
    sslTargetOverrideUrlSubstitutionExp: orderer1.example.com
    mappedHost: orderer1.example.com
  - pattern: (\w*)orderer2(\w*)
    urlSubstitutionExp: 192.168.1.252:7050
    sslTargetOverrideUrlSubstitutionExp: orderer2.example.com
    mappedHost: orderer2.example.com
  - pattern: (\w*)orderer3(\w*)
    urlSubstitutionExp: 192.168.1.253:7050
    sslTargetOverrideUrlSubstitutionExp: orderer3.example.com
    mappedHost: orderer3.example.com
  certificateAuthority:
  - pattern: (\w*)ca.org1(\w*)
    urlSubstitutionExp: 192.168.1.251:7054
    sslTargetOverrideUrlSubstitutionExp: ca.org1.example.com
    mappedHost: ca.org1.example.com
  - pattern: (\w*)ca.org2(\w*)
    urlSubstitutionExp: 192.168.1.252:7054
    sslTargetOverrideUrlSubstitutionExp: ca.org2.example.com
    mappedHost: ca.org2.example.com
  - pattern: (\w*)ca.org3(\w*)
    urlSubstitutionExp: 192.168.1.253:7054
    sslTargetOverrideUrlSubstitutionExp: ca.org3.example.com
    mappedHost: ca.org3.example.com
  - pattern: (\w*)ca.org4(\w*)
    urlSubstitutionExp: 192.168.1.254:7054
    sslTargetOverrideUrlSubstitutionExp: ca.org4.example.com
    mappedHost: ca.org4.example.com
```









