# raft 单组织 笔记

## 1.1、编写crypto-config.yaml

```yaml
OrdererOrgs:
  - Name: FishBlockChain
    Domain: fishblockchain.com
    CA:
      Country: US
      Province: California
      Locality: San Francisco
    Specs:
      - Hostname: orderer1.fishblockchain.com
        CommonName: orderer1.fishblockchain.com
PeerOrgs:
  - Name: CopyRightChain1
    Domain: copyrightchain1.fishblockchain.com
    EnableNodeOUs: true
    CA:
      Country: US
      Province: California
      Locality: San Francisco
    Specs:
      - Hostname: peer1.copyrightchain1.fishblockchain.com
        CommonName: peer1.copyrightchain1.fishblockchain.com
      - Hostname: peer2.copyrightchain1.fishblockchain.com
        CommonName: peer2.copyrightchain1.fishblockchain.com
      - Hostname: peer3.copyrightchain1.fishblockchain.com
        CommonName: peer3.copyrightchain1.fishblockchain.com
      - Hostname: peer4.copyrightchain1.fishblockchain.com
        CommonName: peer4.copyrightchain1.fishblockchain.com
    Users:
      Count: 1
```



## 1.2、执行生成组织和证书命令

```shell
cryptogen generate --config ./crypto-config.yaml
```



## 2.1、编写configtx.yaml

```yaml
Organizations:
  - &FishBlockChain
    Name: FishBlockChain
    ID: FishBlockChainMSP
    MSPDir: crypto-config/ordererOrganizations/fishblockchain.com/msp
    Policies:
      Readers:
        Type: Signature
        Rule: "OR('FishBlockChainMSP.member')"
      Writers:
        Type: Signature
        Rule: "OR('FishBlockChainMSP.member')"
      Admins:
        Type: Signature
        Rule: "OR('FishBlockChainMSP.admin')"
  - &CopyRightChain1
    Name: CopyRightChain1
    ID: CopyRightChain1MSP
    MSPDir: crypto-config/peerOrganizations/copyrightchain1.fishblockchain.com/msp
    Policies:
      Readers:
        Type: Signature
        Rule: "OR('CopyRightChain1MSP.admin','CopyRightChain1MSP.peer','CopyRightChain1MSP.client')"
      Writers:
        Type: Signature
        Rule: "OR('CopyRightChain1MSP.admin','CopyRightChain1MSP.client')"
      Admins:
        Type: Signature
        Rule: "OR('CopyRightChain1MSP.admin')"
    AnchorPeers:
      - Host: peer1.copyrightchain1.fishblockchain.com
        Port: 7051
Capabilities:
  Channel: &ChannelCapabilities
    V1_3: true
  Orderer: &OrdererCapabilities
    V1_1: true
  Application: &ApplicationCapabilities
    V1_3: true
    V1_2: false
    V1_1: false
Application: &ApplicationDefaults
  Organizations:
  Policies:
    Readers:
      Type: ImplicitMeta
      Rule: "ANY Readers"
    Writers:
      Type: ImplicitMeta
      Rule: "ANY Writers"
    Admins:
      Type: ImplicitMeta
      Rule: "MAJORITY Admins"
  Capabilities:
    <<: *ApplicationCapabilities
Orderer: &OrdererDefaults
  OrdererType: etcdraft
  Addresses:
    - orderer1.fishblockchain.com:7050
  BatchTimeout: 2s
  BatchSize:
    MaxMessageCount: 200
    AbsoluteMaxBytes: 20 MB
    PreferredMaxBytes: 512 KB
  EtcdRaft:
    Consenters:
      - Host: orderer1.fishblockchain.com
        Port: 7050
        ClientTLSCert: crypto-config/ordererOrganizations/fishblockchain.com/orderers/orderer1.fishblockchain.com/tls/server.crt
        ServerTLSCert: crypto-config/ordererOrganizations/fishblockchain.com/orderers/orderer1.fishblockchain.com/tls/server.crt
    Options:
      TickInterval: 500ms
      ElectionTick: 10
      HeartbeatTick: 1
      MaxInflightBlocks: 5
      SnapshotIntervalSize: 200
  Organizations:
  Policies:
    Readers:
      Type: ImplicitMeta
      Rule: "ANY Readers"
    Writers:
      Type: ImplicitMeta
      Rule: "ANY Writers"
    Admins:
      Type: ImplicitMeta
      Rule: "MAJORITY Admins"
    BlockValidation:
      Type: ImplicitMeta
      Rule: "ANY Writers"
Channel: &ChannelDefaults
  Policies:
    Readers:
      Type: ImplicitMeta
      Rule: "ANY Readers"
    Writers:
      Type: ImplicitMeta
      Rule: "ANY Writers"
    Admins:
      Type: ImplicitMeta
      Rule: "MAJORITY Admins"
  Capabilities:
    <<: *ChannelCapabilities
Profiles:
  FishBlockChainGenesis:
    <<: *ChannelDefaults
    Orderer:
      <<: *OrdererDefaults
      Organizations:
        - *FishBlockChain
      Capabilities:
        <<: *OrdererCapabilities
    Application:
      <<: *ApplicationDefaults
      Organizations:
      - <<: *FishBlockChain
    Consortiums:
      FishBlockChainConsortium:
        Organizations:
          - *CopyRightChain1
  CopyRightChainChannel:
    Consortium: FishBlockChainConsortium
    <<: *ChannelDefaults
    Application:
      <<: *ApplicationDefaults
      Organizations:
        - *CopyRightChain1
      Capabilities:
        <<: *ApplicationCapabilities
```



## 2.2、执行生成创世块和通道命令

```shell
mkdir channel-artifacts
configtxgen -profile FishBlockChainGenesis -channelID genesischannel -outputBlock ./channel-artifacts/genesis.block
configtxgen -profile CopyRightChainChannel -outputCreateChannelTx ./channel-artifacts/copyrightchainchannel.tx -channelID copyrightchainchannel
```



## 3、服务器执行文件拷贝操作

```shell
scp -r wy@192.168.1.250:$HOME/blockchain/channel-artifacts ./
scp -r wy@192.168.1.250:$HOME/blockchain/crypto-config ./
```



## 4.1、编写orderer.yaml

```yaml
version: '2'
services:
  orderer1.fishblockchain.com:
    container_name: orderer1.fishblockchain.com
    hostname: orderer1.fishblockchain.com
    image: hyperledger/fabric-orderer:latest
    restart: always
    environment:
      - GODEBUG=netdns=go
      - FABRIC_LOGGING_SPEC=INFO
      - ORDERER_GENERAL_LISTENADDRESS=0.0.0.0
      - ORDERER_GENERAL_GENESISMETHOD=file
      - ORDERER_GENERAL_GENESISFILE=/var/hyperledger/orderer/orderer.genesis.block
      - ORDERER_GENERAL_LOCALMSPID=FishBlockChainMSP
      - ORDERER_GENERAL_LOCALMSPDIR=/var/hyperledger/orderer/msp
      - ORDERER_GENERAL_TLS_ENABLED=true
      - ORDERER_GENERAL_TLS_PRIVATEKEY=/var/hyperledger/orderer/tls/server.key
      - ORDERER_GENERAL_TLS_CERTIFICATE=/var/hyperledger/orderer/tls/server.crt
      - ORDERER_GENERAL_TLS_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]
      - ORDERER_GENERAL_CLUSTER_CLIENTPRIVATEKEY=/var/hyperledger/orderer/tls/server.key
      - ORDERER_GENERAL_CLUSTER_CLIENTCERTIFICATE=/var/hyperledger/orderer/tls/server.crt
      - ORDERER_GENERAL_CLUSTER_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric
    command: orderer
    volumes:
      - ./channel-artifacts/genesis.block:/var/hyperledger/orderer/orderer.genesis.block
      - ./crypto-config/ordererOrganizations/fishblockchain.com/orderers/orderer1.fishblockchain.com/msp:/var/hyperledger/orderer/msp
      - ./crypto-config/ordererOrganizations/fishblockchain.com/orderers/orderer1.fishblockchain.com/tls/:/var/hyperledger/orderer/tls
    ports:
      - 7050:7050
    extra_hosts:
      - "orderer1.fishblockchain.com:192.168.1.250"
```



## 4.2、编写peer.yaml

```yaml
version: '2'
services:
  peer1.copyrightchain1.fishblockchain.com:
    container_name: peer1.copyrightchain1.fishblockchain.com
    hostname: peer1.copyrightchain1.fishblockchain.com
    image: hyperledger/fabric-peer:latest
    restart: always
    environment:
      - GODEBUG=netdns=go
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - CORE_PEER_ID=peer1.copyrightchain1.fishblockchain.com
      - CORE_PEER_ADDRESS=peer1.copyrightchain1.fishblockchain.com:7051
      - CORE_PEER_CHAINCODELISTENADDRESS=peer1.copyrightchain1.fishblockchain.com:7052
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer1.copyrightchain1.fishblockchain.com:7051
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer1.copyrightchain1.fishblockchain.com:7051
      - CORE_PEER_LOCALMSPID=CopyRightChain1MSP
      - CORE_PEER_GOSSIP_USELEADERELECTION=true
      - CORE_PEER_GOSSIP_ORGLEADER=false
      - CORE_PEER_PROFILE_ENABLED=true
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: peer node start
    volumes:
      - /var/run/:/host/var/run/
      - ./crypto-config/peerOrganizations/copyrightchain1.fishblockchain.com/peers/peer1.copyrightchain1.fishblockchain.com/msp:/etc/hyperledger/fabric/msp
      - ./crypto-config/peerOrganizations/copyrightchain1.fishblockchain.com/peers/peer1.copyrightchain1.fishblockchain.com/tls:/etc/hyperledger/fabric/tls
    ports:
      - 7051:7051
      - 7052:7052
      - 7053:7053
    extra_hosts:
      - "orderer1.fishblockchain.com:192.168.1.250"
```



## 5、设置hosts

```shell
sudo vim /etc/hosts
192.168.1.250 peer1.copyrightchain1.fishblockchain.com
```



## 6.1、编写启动脚本up.sh

```shell
#!/bin/sh
docker-compose -f ./orderer.yaml up -d
docker-compose -f ./peer.yaml up -d
```



## 6.2、编写清除脚本down.sh

```shell
#!/bin/sh
docker-compose -f ./peer.yaml down -v
docker-compose -f ./orderer.yaml down -v
docker rm -vf $(docker ps -aq)
docker rmi $(docker images dev-peer* -q)
docker volume prune
```



## 6.3、执行脚本

```shell
// 添加脚本执行权限
chmod +x up.sh
chmod +x down.sh
// 依次执行
./up.sh
```



## 7、编写config.yaml

```yaml
name: "fishblockchain-service-network"
version: 1.1.0
client:
  organization: copyrightchain1
  logging:
    level: info
  cryptoconfig:
    path: /home/wy/go/src/github.com/fish/blockchain/crypto-config
  credentialStore:
    path: /tmp/fishblockchain-service-store
    cryptoStore:
      path: /tmp/fishblockchain-service-msp
  BCCSP:
    security:
      enabled: true
      default:
        provider: "SW"
      hashAlgorithm: "SHA2"
      softVerify: true
      level: 256
  tlsCerts:
    systemCertPool: true
    client:
      keyfile: /home/wy/go/src/github.com/fish/blockchain/crypto-config/peerOrganizations/copyrightchain1.fishblockchain.com/users/Admin@copyrightchain1.fishblockchain.com/tls/client.key
      certfile: /home/wy/go/src/github.com/fish/blockchain/crypto-config/peerOrganizations/copyrightchain1.fishblockchain.com/users/Admin@copyrightchain1.fishblockchain.com/tls/client.crt
channels:
  copyrightchainchannel:
    orderers:
      - orderer1.fishblockchain.com
    peers:
      peer1.copyrightchain1.fishblockchain.com:
        endorsingPeer: true
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
    policies:
      queryChannelConfig:
        minResponses: 1
        maxTargets: 1
        retryOpts:
          attempts: 5
          initialBackoff: 500ms
          maxBackoff: 5s
          backoffFactor: 2.0
organizations:
  copyrightchain1:
    mspid: CopyRightChain1MSP
    cryptoPath: /home/wy/go/src/github.com/fish/blockchain/crypto-config/peerOrganizations/copyrightchain1.fishblockchain.com/users/{username}@copyrightchain1.fishblockchain.com/msp
    peers:
      - peer1.copyrightchain1.fishblockchain.com
  fishblockchain:
    mspID: FishBlockChainMSP
    cryptoPath: /home/wy/go/src/github.com/fish/blockchain/crypto-config/ordererOrganizations/fishblockchain.com/users/Admin@fishblockchain.com/msp
orderers:
  orderer1.fishblockchain.com:
    url: grpcs://192.168.1.250:7050
    grpcOptions:
      ssl-target-name-override: orderer1.fishblockchain.com
      keep-alive-time: 5s
      keep-alive-timeout: 6s
      keep-alive-permit: false
      fail-fast: true
      allow-insecure: false
    tlsCACerts:
      path: /home/wy/go/src/github.com/fish/blockchain/crypto-config/ordererOrganizations/fishblockchain.com/orderers/orderer1.fishblockchain.com/msp/tlscacerts/tlsca.fishblockchain.com-cert.pem
peers:
  peer1.copyrightchain1.fishblockchain.com:
    url: peer1.copyrightchain1.fishblockchain.com:7051
    grpcOptions:
      ssl-target-name-override: peer1.copyrightchain1.fishblockchain.com
      allow-insecure: false
    tlsCACerts:
      path: /home/wy/go/src/github.com/fish/blockchain/crypto-config/peerOrganizations/copyrightchain1.fishblockchain.com/tlsca/tlsca.copyrightchain1.fishblockchain.com-cert.pem
entityMatchers:
  peer:
  - pattern: (\w*)peer1.copyrightchain1(\w*)
    urlSubstitutionExp: 192.168.1.250:7051
    sslTargetOverrideUrlSubstitutionExp: peer1.copyrightchain1.fishblockchain.com
    mappedHost: peer1.copyrightchain1.fishblockchain.com
  orderer:
  - pattern: (\w*)orderer1(\w*)
    urlSubstitutionExp: 192.168.1.250:7050
    sslTargetOverrideUrlSubstitutionExp: orderer1.fishblockchain.com
    mappedHost: orderer1.fishblockchain.com
```

