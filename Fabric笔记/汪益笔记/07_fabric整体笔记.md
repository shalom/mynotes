# Fabric逻辑架构

## 成员管理MemberShip

​		会员注册
​			注册成功一个账号得到的不是用户名密码
​			使用证书作用身份认证的标志
​		身份保护
​		交易审计
​		内容保密
​			可以多条区块链,通过通道来区分的

## 账本管理

​		区块链
​			保存所有的交易记录
​		世界状态
​			数据的最新状态
​			数据存储在当前节点的数据库中
​				自带的默认数据库：levelDB,也可以使用couchdb
​				以键值对的方式进行存储的

## 交易管理

​		部署交易
​			部署的是链码,就是给节点安装链码(chaincode)
​		调用交易
​			通过API调用已部署好的某个chaincode的某个函数执行交易

## 智能合约

​		一段代码,处理网络成员所同意的业务逻辑
​		真正实现了链码和账本的分离(逻辑和数据分离)

# Fabric基础概念

## 组织

​		有用户
​		有进行数据处理的节点(peer)
​			put：写数据到区块链中
​			get：数据查询

## 节点

​		client 进行交易管理
​			cli：通过linux的命令行进行操作,使用的是shell命令对象数据进行提交和查询
​			go sdk：通过golang api实现一个客户端
​			node sdk：通过node.js api实现一个客户端
​			java sdk：通过java api实现一个客户端
​		peer 存储和同步账本数据
​			用户通过客户端工具对数据进行put操作,数据写入到一个节点里边
​			数据同步是fabric框架实现的
​			一个Peer节点可以充当多种角色,一个区块链网络中会有多个Peer节点
​		orderer 排序和分发交易
​			解决双花问题
​			每发起一项交易都会在orderer节点进行排序,排序服务可以实现为多种不同的方式
​			交易数据需要先进行打包,然后写入到区块中
​			一个区块链网络中会有多个Orderer节点,共同提供排序服务

## 通道/频道

​		channel是由共识服务(ordering)提供的一种通讯机制,将peer和orderer连接在一起,形成一个个具有保密性的通讯链路(虚拟),实现了业务隔离的要求
​		一个peer节点是可以同时加入到不同的通道中的
​		peer节点每加入到一个新的通道,存储数据的区块链就需要添加一条,只要加入到通道中就可以拥有这个通道中的数据,每个通道对应一个区块链

## 背书策略

​		交易的完成过程叫背书
​		要完成交易必须要有背书策略

## 交易流程

​		Application/SDK客户端发起一个提案,给到peer节点
​		peer节点将交易进行预演,将交易结果发送给客户端
​			如果模拟交易失败,整个交易流程终止
​			如果模拟交易成功,将交易提交给排序节点
​		orderer排序节点对交易打包,将打包数据发送给peer,peer节点将数据写入区块中
​			打包数据的发送,这不是时时的
​			有设定条件,在配置文件中

# Fabric核心模块

## peer：

主节点模块,主要负责存储区块链数据、运行维护链码、提供对外服务接口等作用
### 通过docker客户端来操作peer节点

进入docker容器

```shell
docker exec -it cli /bin/bash
```



#### peer channel 通道操作

create 创建通道,在当前工作目录下生成mychannel.block文件

```shell
peer channel create -o orderer.orderer.com:7050 -c mychannel -f ./channel-artifacts/channel.tx --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/orderer.com/msp/tlscacerts/tlsca.orderer.com-cert.pem
```



join 将peer加入到通道中

```shell
peer channel join -b mychannel.block
```



update 更新锚节点

```shell
peer channel update -o orderer.orderer.com:7050 -c mychannel -f ./channel-artifacts/GoMSPanchors.tx --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/orderer.com/msp/tlscacerts/tlsca.orderer.com-cert.pem
```



list 列出peer加入的通道

```shell
peer channel list
```



fetch 遍历查询区块

```shell
peer channel fetch <newest|oldest|config|(number)> [outputfile] [flags]
    # signconfigtx
    	peer channel signconfigtx [flags]
    # getinfo
    	peer channel getinfo [flags]
```



#### peer chaincode 链码操作

install 安装链码,生成链码打包文件mycc.1.0

```shell
peer chaincode install -n mycc -v 1.0 -l golang -p github.com/chaincode
```



instantiate 链码初始化,执行后可以通过docker ps命令查看己经启动的运行链码的docker容器

```shell
peer chaincode instantiate -o orderer.orderer.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/orderer.com/msp/tlscacerts/tlsca.orderer.com-cert.pem -C mychannel -n mycc -l golang -v 1.0 -c '{"Args":["init","a","100","b","200"]}' -P "AND ('OrgGoMSP.member', 'OrgCppMSP.member')"
```



invoke 链码调用(交易)

```shell
peer chaincode invoke -o orderer.orderer.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/orderer.com/msp/tlscacerts/tlsca.orderer.com-cert.pem -C mychannel -n mycc --peerAddresses peer0.orggo.code.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/orggo.code.com/peers/peer0.orggo.code.com/tls/ca.crt --peerAddresses peer0.orgcpp.code.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/orgcpp.code.com/peers/peer0.orgcpp.code.com/tls/ca.crt -c '{"Args":["invoke","a","b","10"]}'
```



query 链码查询

```shell
peer chaincode query -C mychannel -n mycc -c '{"Args":["query","a"]}'
```



list 查询已经安装的链码

```shell
// 获取当前Peer节点已经被安装的chaincode
peer chaincode list --installed
// 获取当前channel中已经被实例化的chaincode
peer chaincode list --instantiated -C mychannel
```



package 链码打包

```shell
peer chaincode package -p github.com/chaincode -n mycc -v 1.0 mycc.1.0.out
```



signpackage 对打包好的链码进行签名

```shell
peer chaincode signpackage mycc.1.0.out sign_mycc.1.0.out
```



upgrade 更新已经存在的链码

```shell
peer chaincode upgrade -o orderer.itcast.com:7050 -n mycc -v 1.1 -C mychannel -c '{"Args":["init","a","100","b","200"]}'
```

​		

#### peer logging 日志操作

```shell
# 三个子命令
getlevel、setlevel、revertlevels
```



#### peer node 节点操作

```shell
start、status
```



#### peer模块配置信息

```yaml
#docker服务器的Deamon地址,默认取端口的套接字,unix:///var/run/docker.sock
CORE_VM_ENDPOINT
#chaincode容器的网络命名模式,自定义,节点运行在同一个网络中才能相互通信,不同网络中的节点相互隔离
CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE	
#使用peer内置的profile server,默认时运行在6060端口上的并且默认关闭
CORE_PEER_PROFILE_ENABLED	
#日志的级别,critical、error、warning、notice、info、debug
CORE_LOGGING_LEVEL
#peer节点的编号,自定义
CORE_PEER_ID
#是否自动选举leader节点,自动true
CORE_PEER_GOSSIP_USELEADERELECTION
# 当前节点是否为leader节点,是true
CORE_PEER_GOSSIP_ORGLEADER
#当前peer节点的访问地址,域名:端口/IP:端口			
CORE_PEER_ADDRESS					
#chaincode的监听地址
CORE_PEER_CHAINCODELISTENADDRESS
#节点被组织外节点感知时的地址,默认为空,代表不被其他组织节点所感知
CORE_PEER_GOSSIP_EXTERNALENDPOINT	
#启动节点后向哪些节点发起gossip连接,以加入网络,这些节点与本地节点需要属于同一个网络
CORE_PEER_GOSSIP_BOOTSTRAP	
#peer节点所属的组织的编号,在configtxgen.yaml中设置的
CORE_PEER_LOCALMSPID
#chaincode的运行模式,net网络模式,dev开发模式可以在容器外运行chaincode
CORE_CHAINCODE_MODE		
#当前节点的msp文件路径
#启动peer的时候需要使用msp账号文件crypto-config/peerOrganizations/org1.x.com/peers/peer0.org1.x.com/msp
#创建channel的时候需要使用msp账号文件crypto-config/peerOrganizations/org1.x.com/users/Admin@org1.x.com/msp
CORE_PEER_MSPCONFIGPATH
# 是否激活tls,激活:true,不激活:false
CORE_PEER_TLS_ENABLED
#服务器身份验证证书,路径格式crypto-config/peerOrganizations/org1.x.com/peers/peer0.org1.x.com/tls/server.crt
CORE_PEER_TLS_CERT_FILE
#服务器的私钥文件,路径格式crypto-config/peerOrganizations/org1.x.com/peers/peer0.org1.x.com/tls/server.key
CORE_PEER_TLS_KEY_FILE	
#根服务器证书,路径格式crypto-config/peerOrganizations/org1.x.com/peers/peer0.org1.x.com/tls/ca.crt
CORE_PEER_TLS_ROOTCERT_FILE
```



#### leader peer

负责连接到orderer，从orderer拿到新块的信息后分发给其他peer,可以存在多个。

##### 静态选择leader peer 

需要自行保证leader的可用性

```shell
export CORE_PEER_GOSSIP_USELEADERELECTION=false
export CORE_PEER_GOSSIP_ORGLEADER=true

#如果都配置为false,那么peer不会尝试变成一个leader
#如果都配置为true,会引发异常
#指定某一个peer为leader peer
```



##### 动态选择leader peer (推荐)

```shell
export CORE_PEER_GOSSIP_USELEADERELECTION=true
export CORE_PEER_GOSSIP_ORGLEADER=false
```



#### peer默认监听的端口

- 7050：REST 服务端口
- 7051：peer gRPC服务监听端口
- 7052：peer CLI端口
- 7053：peer 事件服务端口
- 7054：eCAP
- 7055：eCAA
- 7056：tCAP
- 7057：tCAA
- 7058：tlsCAP
- 7059：tlsCAA	





## orderer

负责对交易进行排序,并将排序好的交易打包成区块

### order模块命令 

```shell
orderer 子命令 [参数]
orderer start* // 启动orderer节点
orderer benchmark // 采用基准模式运行orderer
```



### orderer模块配置信息

```yaml
#日志级别,critical、error、warning、notice、info、debug
ORDERER_GENERAL_LOGLEVEL	
#orderer服务器监听的地址
ORDERER_GENERAL_LISTENADDRESS			
#orderer服务器监听的端口	
ORDERER_GENERAL_LISTENPORT
#初始化块(Genesis)的来源方式,一般赋值为file即可		
ORDERER_GENERAL_GENESTSMETHOD
#存储初始块文件的路径
ORDERER_GENERAL_GENESISFILE
#orderer节点的编号,在configtxgen模块配置文件中指定的,configtx.yaml配置文件中排序节点的组织的ID
ORDERER_GENERAL_LOCALMSPID		
#orderer节点msp文件路径
ORDERER_GENERAL_LOCALMSPDIR
#账本类型,ram账本数据存储在内存,一般用于测试环境,json/file,账本数据保存在文件中,生产环境中推荐使用file 	
ORDERER_GENERAL_LEDGERTYPE		
#批处理超时,创建批处理之前的等待时间,每隔一个BATCHTIMEOUT时长就会生成一个新的区块
ORDERER_GENERAL_BATCHTIMEOUT	
#最大消息计数,批处理的最大消息数量,只要一个区块的消息达到MAXMESSAGECOUNT指定的数量就会生成一个新的区块
ORDERER_GENERAL_MAXMESSAGECOUNT	
#是否启用TLS,true/false
ORDERER_GENERAL_TLS_ENABLED
#orderer节点的私钥文件,路径格式crypto-config/ordererOrganizations/xx.com/orderers/orderer.xx.com/tls/server.key
ORDERER_GENERAL_TLS_PRIVATEKEY	
#证书文件,路径格式crypto-config/ordererOrganizations/xx.com/orderers/orderer.xx.com/tls/server.crt
ORDERER_GENERAL_TLS_CERTIFICATE	
#根证书文件,路径格式crypto-config/ordererOrganizations/xx.com/orderers/orderer.xx.com/tls/ca.crt
ORDERER—GENERAL_TLS_ROOTCAS		
cryptogen：组织和证书生成模块,生成组织结构和账号相关的文件
cryptogen模块命令 cryptogen 子命令 [参数]
cryptogen generate [<flags>] // 根据配置文件生成证书信息
--config // 指定配置文件,默认./crypto-config.yaml
--output // 指定证书文件的存储位置,会在对应路径生成目录,默认./crypto-config
cryptogen showtemplate // 显示系统默认的cryptogen模块配置文件信息
cryptogen extend [<flags>] // 扩展现有网络
crypto-config.yaml配置文件解读
#排序节点组织的配置信息
OrdererOrgs:
#排序节点组织的名称
- Name: Orderer
#排序节点组织的根域名
Domain: example.com
#手动指定生成
Specs:
#排序节点组织对应的域名
- Hostname: orderer
#peer节点的配置信息
PeerOrgs:
#第一个组织的名称,自定义
- Name: Org1
#第一个组织的根域名
Domain: org1.example.com
#是否支持node.js
EnableNodeOUs: true
#模版自动生成
Template:
#生成2套公私钥+证书,默认生成规则是peer0-9.组织的域名
Count: 2
#创建的普通用户的个数
Users:
#除了admin用户,额外生成3个普通用户
Count: 3
```

**注意**：生产环境必须使用真实域名(已备案的域名)，开发和测试环境域名随便指定



## configtxgen

区块和交易生成模块，生成orderer节点和channel的初始化文件

### configtxgen模块命令

```shell
configtxgen [参数]
	-asOrg // 指定所属的组织名称
	-channelID // 指定创建的channel的名字,默认mychannel
	-configPath // 执行命令要加载的配置文件的路径,不指定会在当前目录下查找
    -inspectBlock // 打印创始块文件的配置内容
    -inspectChannelCreateTx // 打印通道文件的配置内容
    -outputAnchorPeersUpdate // 更新channel的配置信息
    -outputBlock // 输出创始块文件的路径和名字
    -outputCreateChannelTx // 输出通道文件的路径和名字
    -printOrg // 将组织的定义打印为JSON
    -profile // 指定配置文件中的节点
```



### configtx.yaml配置文件解读

```yaml
#组织节点的配置信息,固定不变
Organizations:
#排序节点组织的配置信息,自定义
- &OrdererOrg
#排序节点组织的名称
Name: OrdererOrg
#排序节点组织的编号
ID: OrdererMSP
#排序节点组织的msp文件路径
MSPDir: ./crypto-config/ordererOrganizations/example.com/msp
#第一个组织的配置信息,自定义
- &Org1
#第一个组织的名称
Name: Org1MSP
#第一个组织的编号
ID: Org1MSP
#第一个组织的msp文件路径
MSPDir: ./crypto-config/peerOrganizations/org1.example.com/msp
#第一个组织的锚节点
AnchorPeers:
#指定一个peer节点的域名,端口固定不变
- Host: peer0.org1.example.com
Port: 7051
#在fabric1.1之前没有,设置的时候全部设置为true
Capabilities:
Global: &ChannelCapabilities
V1_1: true
Orderer: &OrdererCapabilities
V1_1: true
Application: &ApplicationCapabilities
V1_2: true
Application: &ApplicationDefaults
Organizations:
#排序节点的配置信息
Orderer: &OrdererDefaults
#排序节点的共识机制,有效值"solo"和"kafka"
OrdererType: solo
#排序节点监听的地址,端口固定不变
Addresses:
- orderer.example.com:7050
#BatchTimeout、MaxMessageCount、AbsoluteMaxBytes只要一个满足,区块就会产生
#多长时间产生一个区块
BatchTimeout: 2s
BatchSize:
#交易的最大数据量,数量达到之后会产生区块,建议100左右
MaxMessageCount: 100
#数据量达到这个值,会产生一个区块,建议32M/64M
AbsoluteMaxBytes: 32 MB
PreferredMaxBytes: 512 KB
#kafka的配置信息
Kafka:
Brokers:
- 127.0.0.1:9092
Organizations:
#固定不变,-profile后边的参数从Profiles获取
Profiles:
#组织定义标识符,自定义
OneOrgsOrdererGenesis:
Capabilities:
<<: *ChannelCapabilities
#配置属性,系统关键字,固定不变
Orderer:
#引用上面OrdererDefaults的属性
<<: *OrdererDefaults
Organizations:
#引用上面OrdererOrg的属性
- *OrdererOrg
Capabilities:
<<: *OrdererCapabilities
#定义了系统中包含的组织
Consortiums:
#自定义,但是需要和下面的SampleConsortium一致
SampleConsortium:
#系统中包含的组织
Organizations:
#引用上面Org1的属性
- *Org1
#通道定义标识符,自定义
OneOrgsChannel:
#自定义,但是需要和上面的SampleConsortium一致
Consortium: SampleConsortium
Application:
<<: *ApplicationDefaults
Organizations:
- *Org1
Capabilities:
<<: *ApplicationCapabilities
```



## configtxlator

区块和交易解析模块



# 启动docker-compose解读

## 客户端角色需要使用的环境变量

```yaml
#docker容器启动之后,go的工作目录,固定不变
- GOPATH=/opt/gopath
#docker容器启动之后,对应的守护进程的本地套接字,固定不变
- CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
#日志级别,生产环境INFO,开发、测试环境DEBUG
- CORE_LOGGING_LEVEL=INFO
#当前客户端节点的ID,自定义	
- CORE_PEER_ID=cli
#客户端连接的当前peer节点的地址信息,自定义
- CORE_PEER_ADDRESS=peer0.org1.example.com:7051
#组织ID,自定义
- CORE_PEER_LOCALMSPID=Org1MSP
#通信是否使用tls加密
- CORE_PEER_TLS_ENABLED=true
#自定义/org1.example.com/peers/peer0.org1.example.com
#证书文件
- CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/server.crt
#私钥文件
- CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/server.key
#根证书文件
- CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
#自定义/org1.example.com/users/Admin@org1.example.com
#指定当前客户端的身份
-CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
```


	

## orderer节点需要使用的环境变量

```yaml
#日志级别,生产环境INFO,开发、测试环境DEBUG
- ORDERER_GENERAL_LOGLEVEL=INFO
#orderer节点监听的地址
- ORDERER_GENERAL_LISTENADDRESS=0.0.0.0
#创始块的来源,指定file来源就是文件中
- ORDERER_GENERAL_GENESISMETHOD=file
#创始块对应的文件,固定不变
- ORDERER_GENERAL_GENESISFILE=/var/hyperledger/orderer/orderer.genesis.block
#orderer节点所属的组的ID,自定义
- ORDERER_GENERAL_LOCALMSPID=OrdererMSP
#当前节点的msp账号路径
- ORDERER_GENERAL_LOCALMSPDIR=/var/hyperledger/orderer/msp	
#通信是否使用tls加密
- ORDERER_GENERAL_TLS_ENABLED=true
#私钥文件
- ORDERER_GENERAL_TLS_PRIVATEKEY=/var/hyperledger/orderer/tls/server.key
#证书文件
- ORDERER_GENERAL_TLS_CERTIFICATE=/var/hyperledger/orderer/tls/server.crt
#根证书文件
- ORDERER_GENERAL_TLS_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]
```



## peer节点需要使用的环境变量

```yaml
#日志级别,生产环境INFO,开发、测试环境DEBUG
- ORDERER_GENERAL_LOGLEVEL=INFO
#orderer节点监听的地址
- ORDERER_GENERAL_LISTENADDRESS=0.0.0.0
#创始块的来源,指定file来源就是文件中
- ORDERER_GENERAL_GENESISMETHOD=file
#创始块对应的文件,固定不变
- ORDERER_GENERAL_GENESISFILE=/var/hyperledger/orderer/orderer.genesis.block
#orderer节点所属的组的ID,自定义
- ORDERER_GENERAL_LOCALMSPID=OrdererMSP
#当前节点的msp账号路径
- ORDERER_GENERAL_LOCALMSPDIR=/var/hyperledger/orderer/msp	
#通信是否使用tls加密
- ORDERER_GENERAL_TLS_ENABLED=true
#私钥文件
- ORDERER_GENERAL_TLS_PRIVATEKEY=/var/hyperledger/orderer/tls/server.key
#证书文件
- ORDERER_GENERAL_TLS_CERTIFICATE=/var/hyperledger/orderer/tls/server.crt
#根证书文件
- ORDERER_GENERAL_TLS_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]
```



# 智能合约/ChainCode/链码

ChainCode介绍
Chaincode代码
Chaincode是一段运行在容器中的程序,这些程序可以是Go、Java、Nodejs等语言开发的
Chaincode管理命令
主要用来对Chaincode进行安装、实例化、调用、打包、签名等操作,包含在peer模块中,是peer模块的一个子命令
ChainCode的代码结构
包名必须是main
package main
必须要引入的包
import (
	// shim是Fabric提供的上下文环境,包含了Fabric和ChainCode交互的接口,执行赋值、查询等功能
    "github.com/hyperledger/fabric/core/chaincode/shim"
    pb "github.com/hyperledger/fabric/protos/peer"
)
定义结构体并实现peer接口的两个方法
// 每个ChainCode都需要定义一个结构体
type TestStudy struct {}
// 结构体必须实现peer接口的两个方法
func (t *TestStudy) Init(stub shim.ChaincodeStubInterface) pb.Response {} // 系统初始化
func (t *TestStudy) Invoke(stub shim.ChaincodeStubInterface) pb.Response{} // 数据写入
Init方法
func (t *TestStudy) Init(stub shim.ChaincodeStubInterface) pb.Response {
	// 获取客户端传入的参数,args是一个字符串,存储传入的字符串参数
    _, args := stub.GetFunctionAndParameters()
    return shim.Success([]byte("sucess init!!!"))
}
// 命令行执行nit
peer chaincode instantiate -o orderer.itcast.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/itcast.com/msp/tlscacerts/tlsca.itcast.com-cert.pem -C mychannel -n mycc -l golang -v 1.0 -c '{"Args":["init","a","100","b","200"]}' -P "AND ('OrgGoMSP.member', 'OrgCppMSP.member')"
Invoke方法
func (t *TestStudy) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
    // 进行交易操作的源代码, 调用ChaincodeStubInterface接口中的方法
    return shim.Success([]byte("sucess invoke!!!"))
}
// 命令行执行Invoke
peer chaincode invoke -o orderer.itcast.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/itcast.com/msp/tlscacerts/tlsca.itcast.com-cert.pem -C mychannel -n mycc --peerAddresses peer0.orggo.itcast.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/orggo.itcast.com/peers/peer0.orggo.itcast.com/tls/ca.crt --peerAddresses peer0.orgcpp.itcast.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/orgcpp.itcast.com/peers/peer0.orgcpp.itcast.com/tls/ca.crt -c '{"Args":["invoke","a","b","10"]}'
shim包的核心方法
Success：负责将正确的消息返回给调用ChainCode的客户端
func (t *TestStudy) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
    return shim.Success([]byte("sucess invoke!!!"))
}
Error：负责将错误信息返回给调用ChainCode的客户端
func (t *TestStudy) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
    return shim.Error("operation fail!!!")
}
LogLevel：负责修改ChainCode中运行日志的级别
func (t *TestStudy) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
    loglevel, _ := shim.LogLevel("debug") // 将日志级别描述字符串转为LoggingLevel类型
    shim.setLoggingLevel(loglevel) // 设置日志级别
    return shim.Success([]byte("operation fail!!!"))
}
日志级别
	CRITICAL 级别最高,写日志最少
	ERROR
	WARNING
	NOTICE
	INFO 
	DEBUG 级别最低,写日志最多
ChaincodeStubInterface接口中的核心方法
系统管理相关的方法
GetFunctionAndParameters：赋值接收调用chaincode的客户端传递过来的参数
	func (t *TestStudy) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	    // 获取客户端传入的参数,args是一个字符串,存储传入的字符串参数
	    _, args := stub.GetFunctionAndParameters()
	   	a_param, b_param, c_param := args[0], args[1], args[2]
	    return shim.Success([]byte("sucess init!!!"))
	}
存储管理相关的方法
PutState：把客户端传递过来的数据保存到Fabric中,数据格式为键值对
	func (t *TestStudy) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	    stub.PutState("user1", []byte("putvalue")) // 数据写入
	    return shim.Success([]byte("sucess invoke user1"))
	}
GetState：从Fabric中取出数据,然后把这些数据交给chaincode处理
	func (t *TestStudy) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	    keyvalue, err := stub.GetState("user1") // 读数据
	    return shim.Success(keyvalue)
	};
GetStateByRange：根据key访问查询相关数据
	func (t *TestStudy) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	    startKey, endKey := "startkey", "endkey"
	    // 根据范围查询,得到StateQueryIteratorInterface迭代器接口
	    keysIter, err := stub.getStateByRange(startKey, endKey)
	    // defer关闭迭代器接口
	    defer keysIter.Close()
	    var keys []string
	    for keysIter.HasNext() {
	        // 遍历集合,如果有下一个节点,得到下一个键值对
	        response, iterErr := keysIter.Next()
	        if iterErr != nil {
	            return shim.Error(fmt.Sprintf("find an error %s", iterErr))
	        }
	        // 存储键值到数组中
	        keys = append(keys, response.Key)	
	    }
	    // 编码keys数组成json格式
	    jsonKeys, err := json.Marshal(keys)
	    if err := nil {
	        return shim.Error(fmt.Sprintf("data Marshal json error: %s", err))
	    }
	    // 将编码之后的json字符串传递给客户端
	    return shim.Success(jsonKeys)
	}
GetHistoryForKey：查询某个键的历史记录
	func (t *TestStudy) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
		// 根据key查询,得到StateQueryIteratorInterface迭代器接口
	    keysIter, err := stub.GetHistoryForKey("user1")
	    if err := nil {
	        return shim.Error(fmt.Sprintf("GetHistoryForKey error: %s", err))
	    }
	    // defer关闭迭代器接口
	    defer keysIter.Close()
	    var keys []string
	    for keysIter.HasNext() {	
	        // 遍历集合,如果有下一个节点,得到下一个键值对
	        response, iterErr := keysIter.Next()
	        if iterErr != nil {
	            return shim.Error(fmt.Sprintf("find an error %s", iterErr))
	        }
	        txid := response.TxId // 交易编号
	        txvalue := response.Value // 交易的值
	        txStatus := response.IsDelete // 当前交易的状态
	        txtimestamp := response.Timestamp // 交易发生的时间戳
	        tm := time.Unix(txtimestamp.Seconds, 0) // 计算从1970.1.1到当前时间时间戳的秒数
	        datestr := tm.Format("2018-11-11 11:11:11 AM") // 根据指定的格式将日期格式化
	        fmt.Printf("info - txid:%s, value:%s, isDel:%t, dateTime:%s\n", txid, string(txvalue), txStatus, datestr)
	        keys = append(keys, txid)
	    }
	    // 编码keys数组成json格式
	    jsonKeys, err := json.Marshal(keys)
	    if err := nil {
	        return shim.Error(fmt.Sprintf("data Marshal json error: %s", err))
	    }
	    // 将编码之后的json字符串传递给客户端
	    return shim.Success(jsonKeys)
	}
DelState：删除一个key
	func (t *TestStudy) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	    if err := stub.DelState("delKey"); err != nil {
	    	return shim.Error("delete key error !!!")
	    }
	    return shim.Success("delete key Success !!!")
	}
CreateCompositeKey：给定一组属性,将这些属性组合起来构造一个复合键
	func (t *TestStudy) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	    parms := []string("go1", "go2", "go3", "go4", "go5", "go6")
	    ckey, _ := stub.CreateCompositeKey("testkey", parms)
	    // 将复合键存储到账本中
	    err := stub.putState(ckey, []byte("hello, go"))
	    if err != nil {
	        fmt.Println("find errors %s", err)
	    }
	    return shim.Success([]byte(ckey))
	}
GetStateByPartialCompositeKey/SplitCompositeKey
	GetStateByPartialCompositeKey：根据局部的复合键返回所有的匹配的键值
	SplitCompositeKey：给定一个复合键,将其拆分为复合键所有的属性
	func (t *TestStudy) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	    searchparm := []string{"go1"}
	    rs, err := stub.GetStateByPartialCompositeKey("testkey", searchparm)
	    if err != nil {
	        error_str := fmt.Sprintf("find error %s", err)
	        return shim.Error(error_str)
	    }
	    defer rs.Close()
	    var tlist []string
	    for rs.HasNext() {
	        responseRange, err := rs.Next()
	        if err != nil {
	            error_str := fmt.Sprintf("find error %s", err)
	            return shim.Error(error_str)
	        }
	        value1, compositeKeyParts, _ := stub.SplitCompositeKey(responseRange, key)
	        value2 := compositeKeyParts[0]
	        value3 := compositeKeyParts[1]
	        fmt.Printf("find value v1:%s, v2:%s, V3:%s\n", value1, value2, value3)
	    }
	    return shim.Success("success")
	}
交易管理相关的方法
GetTxTimestamp：获取当前客户端发送的交易时间戳
	func (t *TestStudy) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	    txtime, err := stub.GetTxTimestamp()
	    if err != nil {
	        fmt.printf("Error getting transaction timestamp: %s", error)
	        return shim.Error(fmt.Sprintf("get transaction timestamp error: %s", error))
	    }
	    tm := time.Unix(txtime.Second, 0)
	    return shim.Success([]byte(fmt.Sprint("time is: %s", tm.Format("2018-11-11 11:11:11 AM"))))
	}
调动外部chaincode的方法
InvokeChaincode：调用另一个链码中的Invoke方法
	func (t *TestStudy) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	    // 设置参数,a向b转账11
	    trans:=[][]byte{[]byte("invoke"),[]byte("a"),[]byte("b"),[]byte("11")}
	    // 调用chaincode,并判断是否操作成功
	    if response := stub.InvokeChaincode("mycc", trans, "mychannel"); response.Status != shim.OK {
	        errStr := fmt.Sprintf("Invoke failed, error: %s", response.Payload)
	        return shim.Error(errStr)
	    }
	    return shim.Success([]byte("转账成功..."))
	}
GetTxID：获取客户端发送的交易编号
	func (t *TestStudy) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	    txid := stub.GetTxID()
	    return shim.Success([]byte(txid))
	}
ChainCode交易的背书(endorse)
Endorsement背书的概念
背书的过程是一笔交易被确认的过程
背书策略被用来指示对相关的参与方如何对交易讲行确认
当一个节点接收到一个交易请求的时候,会调用VSCC与交易的chaincode共同来验证交易的合法性
VSCC：系统Chaincode,专门负责处理背书相关的操作
交易确认的校验
	所有的背书是否有效(参与的背书的签名是否有效)
	参与背书的数量是否满足要求
	所有背书参与方是否满足要求
背书规则示例
// 按照该背书规则进行交易,必须通过组织Org1MSP、Org2MSP、Org3MSP中的用户共同验证交易,才能生效
-P "AND ('Org1MSP.member', 'Org2MSP.member', 'Org3MSP.member')" 
// 按照该背书规则进行交易,只需要通过组织Org1MSP或Org2MSP或Org3MSP中的任何一个用户验证,即可生效
-P "OR ('Org1MSP.member', 'Org2MSP.member', 'Org3MSP.member')" 
// 组织Org1MSP中的某个用户对交易进行验证,或组织Org2MSP和组织Org3MSP中的用户共同验证交易,即可生效
-P "OR ('Org1MSP.member', AND ('Org2MSP.member', 'Org3MSP.member'))" 
背书参数解读
AND：表示所有参与方共同对交易进行确认
OR：表示参与方的任何一方参与背书即完成交易的确认
NOutOf：表示满足m个条件中的n个即可(m≥n)
Org1MSP：表示组织的编号
admin：指组织的管理员用户
member：泛指组织内的任何一个用户,当然也可以是组织某个具体的用户
Org1MSP.member：表示参与背书的组和组织中参与背书的用户
背书规则只针对chaincode中写入数据的操作进行校验,对于查询类操作不背书
ChainCode中需要利用背书规则对操作进行校验的方法
	// 把客户端传递过来的数据保存到Fabric中,数据格式为键值对
	PutState(key string, value []byte) error
	// 删除一个key
	DelState(key string) error
Fabric中的背书是发生在客户端的,需要进行相关的代码的编写才能完成整个背书的操作

# Fabric账号

联盟链的特点是用户非权时不能接人区块链

## Fabric账号是什么

是根据PKI规范生成的一组证书和秘钥文件。

由于记录在区块链中的数据具有不可逆、不可篡改的特性；Fabric中每条交易都会加上发起者的标签(签名证书)，同时用发起人的私钥进行加密。如果交易需要其他组织的节点提供背书功能，那么背书节点也会在交易中加入自己的签名，这样每一笔交易的操作过程会非常清晰并且不可篡改。

## Fabric账号的结构

msp：存放签名用的证书文件和加密用的私钥文件
	- admincerts：管理员证书
		Admin@xxx-cert.pem
		User@xxx-cert.pem
	- cacerts：根CA服务器的证书
		ca.xxx-cert.pem
		ca.xxx-cert.pem
	- keystore：节点或者账号的私钥
		xxx_sk
	- signcerts：符合x.509的节点或者用户证书文件
		Admin@xxx-cert.pem
		User@xxx-cert.pem
	- tlscacerts：TLS根CA的证书
		tlsca.xxx-cert.pem
		tlsca.xxx-cert.pem
tls：存放加密通信相关的证书文件
	- ca.crt 根证书文件
	- client.crt 证书文件
	- client.key 私钥文件

## 什么地方需要Fabric账号

```yaml
// 启动orderer：需要通过环境变量或者配置文件给当前启动的Orderer设定相应的账号
ORDERER_GENERAL_LOCALMSPDIR=/var/hyperledger/orderer/msp
// 启动peer：需要通过环境变量或者配置文件给当前启动的peer设定相应的账号
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/orggo.itcast.com/users/Admin@orggo.itcast.com/msp
// 创建channel：需要用户账号
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/orggo.itcast.com/users/Admin@orggo.itcast.com/msp
```



# Fabric-ca

作用：实现动态添加账号

Fabric-ca提供两种访问方式调用Server服务

- 通过Fabric-Client调用
- 通过SDK调用(go、nodejs、java)

一个组织会对应一个fabric-server服务器

## 将fabric-ca加入到网络

在docker-compose启动使用的配置文件docker-compose.yam中添加配置项

```yaml
#fabric-ca的服务器名,自定义
ca.example.com:
    #容器名,自定义
    container_name: ca.example.com
    #fabric-ca的镜像文件
    image: hyperledger/fabric-ca:latest 
    environment:
        #fabric-ca容器中的home目录
        - FABRIC_CA_HOME=/etc/hyperledger/fabric-ca-server
        #fabric-ca服务器的名字,自定义
        - FABRIC_CA_SERVER_CA_NAME=ca.example.com	
        #fabric-ca服务器证书文件目录中的证书文件,明确当前fabric-ca属于哪个组织
        - FABRIC_CA_SERVER_CA_CERTFILE=/etc/hyperledger/fabric-ca-server-config/ca.org1.example.com-cert.pem
        #fabric-ca服务器的私钥文件
        - FABRIC_CA_SERVER_CA_KEYFILE=/etc/hyperledger/fabric-ca-server-config/ee54a6cc9868ffa72f0556895020739409dc69da844628ae804934b7d7f68e92_sk
    #启动fabric-ca-server服务,登录用户名:登录密码
    command: sh -c 'fabric-ca-server start -b admin:123456'
    volumes:
      - ./crypto-config/peerOrganizations/org1.example.com/ca/:/etc/hyperledger/fabric-ca-server-config
    #fabric-ca服务器绑定的端口
    ports:
      - "7054:7054"
    #工作的网络
    networks:
      - byfn
```



## 编写nodejs客户端

```shell
// 跳过向导,快速生成package.json文件
npm init -y
// 安装第三方依赖库
npm i fabric-client
npm i fabric-ca-client
npm i grpc
```

