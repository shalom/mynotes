# hyperledger fabric的安装

提前安装好 go nodejs docker docker-compose

使用 fabirc 有两种方式：

- 使用docker运行fabric（后续采用这种方式）
- 直接在本地跑fabric的可执行文件

## 拉取fabric的镜像

需要拉取的docker镜像：

```
hyperledger/fabric-ca       	# fabric的CA证书模块
hyperledger/fabric-tools    
hyperledger/fabric-ccenv    
hyperledger/fabric-orderer  	# fabric的排序节点
hyperledger/fabric-peer     	
hyperledger/fabric-javaenv  
hyperledger/fabric-zookeeper
hyperledger/fabric-kafka   		# 采用kafka排序算法的时候需要用 
hyperledger/fabric-couchdb  	# 默认集成的是leveldb，富查询可以使用couchdb
```

输入命令：

```shell
docker pull ${镜像名}:${版本号}

docker pull hyperledger/fabric-ca:1.4.0
……
```

1.4.0 版本后fabric实现了raft排序算法，后续的环境搭建都采用此版本



## 测试运行

官方提供了一个简单的案例，用来测试本地是否安装好 fabric 的运行环境

```shell
git clone https://github.com/hyperledger/fabric-samples

cd fabric-samples/first-network
```

将分支切换到与 fabric 1.4.0 对应的版本：

```shell
git checkout remotes/origin/release-1.3
```

**注意：** remotes/origin/release-1.4 版本运行会报错，原因是 `first-network/configtx.yaml` 中的 Capabilities 选项会有版本问题，不向下兼容，这个是 `remotes/origin/release-1.3` 分支中的 Capabilities 选项内容：

```
Capabilities:
    Channel: &ChannelCapabilities
        V1_3: true

    Orderer: &OrdererCapabilities
        V1_1: true

    Application: &ApplicationCapabilities
        V1_3: true
        V1_2: false
```



执行命令：

```shell
./byfn.sh generate	# 用于生成MSP文件 和 通道文件
```

这个命令执行成功后，会在 `first-network/channel-artifacts` 中生成通道文件，在 `first-network/crypto-config` 中生成私钥证书文件



执行命令启动测试网络：

```shell
./byfn.sh up 		# 启动测试网络
```



一段时间后，界面出现如下字样，代表 fabric 安装成功：

```
 ____    _____      _      ____    _____ 
/ ___|  |_   _|    / \    |  _ \  |_   _|
\___ \    | |     / _ \   | |_) |   | |  
 ___) |   | |    / ___ \  |  _ <    | |  
|____/    |_|   /_/   \_\ |_| \_\   |_|  

...

_____   _   _   ____   
| ____| | \ | | |  _ \  
|  _|   |  \| | | | | | 
| |___  | |\  | | |_| | 
|_____| |_| \_| |____/  
```



byfn.sh 的相关命令：

```shell
./byfn.sh generate  	# 生成 创世块，证书文件 等相关文件
./byfn.sh up			# 启动网络
./byfn.sh restart		# 重启网络
./byfn.sh down			# 停止网络
./byfn.sh help			# 查看可用参数，这里不赘述
```



# 茅台项目网络的搭建

## 清理docker残余文件

使用docker时，会出现端口被占用，容器名重复，网络名重复等情况，所以重复启动网络时，不清理会报错，所以启动前要做好清理工作

检查现有的 容器 数据卷 网络 和 镜像：

```shell
docker ps -a
docker volume ls
docker images
docker network ls
```



清理：

```shell
docker rm -f $(docker ps -a -q)	# 强制删除所有容器，谨慎使用
docker volume prune				# 删除没用的数据卷
docker network prune			# 删除没用的网络
```

容器的删除根据具体情况酌情删除





