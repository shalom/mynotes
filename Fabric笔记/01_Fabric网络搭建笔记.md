# Fabric安装 solo&Kafka搭建

## Fabric环境安装

### 安装go nodejs docker docker-compose

> 这些安装步骤都在 Linux 笔记中，docker-compose安装在 docker 笔记文件夹中

### 部署Fabric

#### 命令

1. 需要翻墙方式：  

   ```shell 
   # 三个版本号分别是 Fabric版本, Fabric-ca版本, 第三方docker images版本（包括zookeeper，kafka等，fabric官方提供的镜像版本还是1.4.0）
   curl -sSL http://bit.ly/2ysbOFE | bash -s 1.4.0 1.4.0 0.4.14
   ```

2. 不需要翻墙方式： 
   
   ``` shell
   curl -sSL https://raw.githubusercontent.com/hyperledger/fabric/master/scripts/bootstrap.sh | bash -s 1.4.0 1.4.0 0.4.14
   ```
   
   

##### 命令说明：

- curl 命令是一个利用URL规则 在命令行下工作的 文件传输工具，也就是执行`curl -sSL http://bit.ly/2ysbOFE`的效果是：下载该URL对应的文件并执行；`curl命令`详细的使用细节如下：[linux curl 命令详解，以及实例](<http://blog.51yip.com/linux/1049.html>)

- 翻墙后，便可以打开网址：`http://bit.ly/2ysbOFE`，但是此网站打开后会发现：URL还是变成了`https://raw.githubusercontent.com/hyperledger/fabric/master/scripts/bootstrap.sh` ，两个网站的结果没有区别；在Linux系统执行翻墙命令时，会提示：“连接被对方重设”；

- 从URL可以看出，`https://raw.githubusercontent.com/hyperledger/fabric/master/scripts/bootstrap.sh`对应的是一个名为`bootstrap.sh`的 shell脚本 ，可以用浏览器访问看看；

- bootstrap.sh脚本是fabric官方提供的，该脚本主要做以下3件事：

  1. fabric-samples文件下载；

     > 和使用 git clone  https://github.com/hyperledger/fabric-samples  命令效果一样

  2. 下载二进制文件，这一步可能非常慢，可以提前在别的地方下载，官网下载特别慢；具体下载的文件是下边两个：

     ![1566545619907](assets/1566545619907.png)

     - 第一个是CA证书相关的
     - 第二个则是 fabric 的可执行文件

  3. 拉取 fabric 需要的 docker镜像，镜像文件不小，需要为docker配置加速器，详情看docker笔记；以下是执行成功拉取的镜像列表，执行`docker images` 可以查看列表：

     ![1566546050908](assets/1566546050908.png)

     

     并且这个脚本支持三个参数，可以灵活地控制它的执行；分别是：

     `-b:加上此参数,则不下载二进制文件`

     `-d:加上此参数则不拉取docker镜像`

     `-s:加上此参数则不下载fabric-samples`

- 执行命令成功后，当前目录中会出现`fabric-samples`文件夹，目录结构如下：
  
```shell
  [shalom@ubuntu fabric-samples->（头指针分离于 v1.4.0）]$ tree -L 1
  .
  ├── balance-transfer
  ├── basic-network
  ├── bin							# 是二进制文件解压出来的
  ├── chaincode
  ├── chaincode-docker-devmode
  ├── CODE_OF_CONDUCT.md
  ├── commercial-paper
  ├── config						# 是二进制文件解压出来的
  ├── CONTRIBUTING.md
  ├── fabcar
  ├── first-network				# 测试目录
  ├── high-throughput
  ├── interest_rate_swaps
  ├── Jenkinsfile
  ├── LICENSE
  ├── MAINTAINERS.md
  ├── README.md
  └── scripts
```

  其中 bin 存储的是 fabric 的可执行文件，bin 和 config 都是由`hyperledger-fabric-linux-amd64-1.4.0.tar.gz`解压出来的，其余和GitHub链接 ` https://github.com/hyperledger/fabric-samples` 里的一样，可见`bootstrap.sh`自动做了解压操作.


- `fabric-samples/bin/ ` 的目录结构如下：
  
```shell
  [shalom@ubuntu bin]$ tree
  .
  ├── configtxgen
  ├── configtxlator
  ├── cryptogen
  ├── discover
  ├── get-docker-images.sh
  ├── idemixgen
  ├── orderer
  └── peer
```

  

- 将该文件里的全部二进制文件拷贝到`/usr/local/bin`目录下 ，设为Linux全局访问

  ```shell
  sudo cp * /usr/local/bin
  ```




### 测试是否安装成功

执行 `fabric-samples/first-network` 中的测试脚本，执行命令如下：

```shell
./byfn.sh generate  	# 生成 创世块，证书文件 等相关文件
./byfn.sh up			# 启动网络
./byfn.sh restart		# 重启网络
./byfn.sh down			# 停止网络
./byfn.sh help			# 查看可用参数，这里不赘述
```

- 先生成证书文件

- 再启动网络，启动成功后，最终会出现如下字样，代表 fabric 安装成功：

  ```shell
   ____    _____      _      ____    _____ 
  / ___|  |_   _|    / \    |  _ \  |_   _|
  \___ \    | |     / _ \   | |_) |   | |  
   ___) |   | |    / ___ \  |  _ <    | |  
  |____/    |_|   /_/   \_\ |_| \_\   |_|  
  
  ...
  
  _____   _   _   ____   
  | ____| | \ | | |  _ \  
  |  _|   |  \| | | | | | 
  | |___  | |\  | | |_| | 
  |_____| |_| \_| |____/  
  ```

- 测试完后，记得将网络停止，不停止会影响后续的操作；



## Fabric网络环境搭建

<font color=red>注意：搭建前需要将docker的数据卷，容器，链码镜像和网络 清除干净</font>

```shell
# 删除镜像
docker rmi [镜像ID]	# 若加 -f 参数为强制删除
# 删除容器
docker ps -a	# 查看所有容器 
docker rm [容器ID]
docker rm -f $(docker ps -a -q)	# 强制删除所有容器
# 删除数据卷
docker volume ls # 查看所有的卷
docker volume prune # 删除未使用的卷
docker volume rm # 删除一个或多个卷
# 删除网络
docker network ls/list # 忘记了两个命令哪一个是正确的，两个都可以
docker network prune # 删除未使用的网络
```



### 单机solo

#### 1. 生成各个成员

- 1个排序节点，

- 两个组织分别各两个 peer节点

##### 1.1 编写配置文件 crypto-config.yaml

```yaml
OrdererOrgs:    # order节点组织
  - Name: Orderer   # 组织的名称，可以根据实际情况指定
    Domain: example.com     # 组织的根域名
    CA:                     # 指定 CA 机构
      Country: US
      Province: California
      Locality: San Francisco
    Specs:
      - Hostname: orderer  # order节点组织中各节点的名称，和根域名最终组成个节点的域名 orderer.example.com  

PeerOrgs:   # peer节点的组织，有两个peer组织
  - Name: Org1  # 其中一个组织的名称
    Domain: org1.example.com    # 当前组织的根域名,文件夹的名字就是从域名来的
    EnableNodeOUs: true         # 是否支持nodejs，这里选择是，否则会出问题，具体原因还需要后续深入研究fabric源码的时候才能够知道
    CA:                         # 指定 CA 机构
      Country: US
      Province: California
      Locality: San Francisco
    Template:       # 生成peer节点的模板
      Count: 2      # 指定该组织生成几个节点
    Users:          # 当前组织中用户的个数
      Count: 1      # 这里指定的是普通用户个数，Admin用户是默认生成的，不需要在这里指定
  - Name: Org2
    Domain: org2.example.com
    EnableNodeOUs: true
    CA:
      Country: US
      Province: California
      Locality: San Francisco
    Template:
      Count: 2
    Users:
      Count: 1
```

##### 1.2 执行成组织和证书命令

```shell
cryptogen generate --config ./crypto-config.yaml
```

执行成功后，会发现当前目录出现一个名为 `crypto-config` 的文件夹；执行命令 `tree -L 3` 来查看目录结构：

```shell
[shalom@ubuntu crypto-config]$ tree -L 3
.
├── ordererOrganizations
│   └── example.com
│       ├── ca
│       ├── msp
│       ├── orderers		# order节点存放在这里
│       ├── tlsca
│       └── users			# 每个组织的用户存放在这里
└── peerOrganizations
    ├── org1.example.com
    │   ├── ca
    │   ├── msp
    │   ├── peers			# peer节点存放在这里
    │   ├── tlsca
    │   └── users
    └── org2.example.com
        ├── ca
        ├── msp
        ├── peers
        ├── tlsca
        └── users
```

可以看出每个组织都有 `ca msp tlsca` 三个文件夹

TODO：他们分别代表的意思



每个节点内存放的文件结构：

```shell
[shalom@ubuntu orderers]$ tree -L 3
.
└── orderer.example.com
    ├── msp
    │   ├── admincerts
    │   ├── cacerts
    │   ├── keystore
    │   ├── signcerts
    │   └── tlscacerts
    └── tls
        ├── ca.crt
        ├── server.crt
        └── server.key
```

每个用户存放的文件结构：

```shell
[shalom@ubuntu users]$ tree -L 3
.
└── Admin@example.com
    ├── msp
    │   ├── admincerts
    │   ├── cacerts
    │   ├── keystore
    │   ├── signcerts
    │   └── tlscacerts
    └── tls
        ├── ca.crt
        ├── client.crt
        └── client.key
```



#### 2. 生成创世块，通道文件

##### 2.1 编写配置文件 configtx.yaml

```yaml
---
Organizations:      # 开始配置组织
    - &OrdererOrg   # 开始配置排序节点组织
        Name: OrdererOrg    # 组织名，要和crypto-config.yaml里边的一致
        ID: OrdererMSP   # 组织ID，自定义
        MSPDir: crypto-config/ordererOrganizations/example.com/msp  # 组织的msp文件路径，注意相对路径
        Policies:
            Readers:
                Type: Signature
                Rule: "OR('OrdererMSP.member')"     # 这里是 组织ID
            Writers:
                Type: Signature
                Rule: "OR('OrdererMSP.member')"
            Admins:
                Type: Signature
                Rule: "OR('OrdererMSP.admin')"

    - &Org1
        Name: Org1MSP
        ID: Org1MSP		# docker-compose配置文件中要使用
        MSPDir: crypto-config/peerOrganizations/org1.example.com/msp
        AnchorPeers:        # 指定锚节点
            - Host: peer0.org1.example.com      # 指定锚节点域名
              Port: 7051
        Policies:
            Readers:
                Type: Signature
                Rule: "OR('Org1MSP.admin', 'Org1MSP.peer', 'Org1MSP.client')"
            Writers:
                Type: Signature
                Rule: "OR('Org1MSP.admin', 'Org1MSP.client')"
            Admins:
                Type: Signature
                Rule: "OR('Org1MSP.admin')"
    - &Org2
        Name: Org2MSP
        ID: Org2MSP
        MSPDir: crypto-config/peerOrganizations/org2.example.com/msp
        AnchorPeers:
            - Host: peer0.org2.example.com
              Port: 7051
        Policies:
            Readers:
                Type: Signature
                Rule: "OR('Org2MSP.admin', 'Org2MSP.peer', 'Org2MSP.client')"
            Writers:
                Type: Signature
                Rule: "OR('Org2MSP.admin', 'Org2MSP.client')"
            Admins:
                Type: Signature
                Rule: "OR('Org2MSP.admin')"

Capabilities:   #对于通道、排序、应用的功能的配置
    Channel: &ChannelCapabilities
        V1_3: true
    Orderer: &OrdererCapabilities
        V1_1: true
    Application: &ApplicationCapabilities
        V1_3: true
        V1_2: false
        V1_1: false
        
Application: &ApplicationDefaults       #对应用程序的 ACL 策略
    Organizations:
    Policies:
        Readers:
            Type: ImplicitMeta
            Rule: "ANY Readers"
        Writers:
            Type: ImplicitMeta
            Rule: "ANY Writers"
        Admins:
            Type: ImplicitMeta
            Rule: "MAJORITY Admins"
    Capabilities:
        <<: *ApplicationCapabilities

Orderer: &OrdererDefaults               #对 orderer 节点的配置
    OrdererType: kafka              # 指定共识算法
    Addresses:          #orderer 节点监听的域名和端口
        - orderer0.example.com:7050 # 各个排序节点的域名加端口号，端口号不需要改变
        - orderer1.example.com:7050
        - orderer2.example.com:7050
    BatchTimeout: 2s                # 每隔多长时间打包一个区块
    BatchSize:
        MaxMessageCount: 100        # 单个区块的最大消息数
        AbsoluteMaxBytes: 32 MB     # 单个区块的最大容量，到了自动打包区块
        PreferredMaxBytes: 512 KB   #一个区块最小的容量
    Kafka:                          #对于 kafka 的配置
        Brokers:
            - kafka0:9092           # kafka服务器地址
            - kafka1:9092
            - kafka2:9092
            - kafka3:9092
    Organizations:
    Policies:
        Readers:
            Type: ImplicitMeta
            Rule: "ANY Readers"
        Writers:
            Type: ImplicitMeta
            Rule: "ANY Writers"
        Admins:
            Type: ImplicitMeta
            Rule: "MAJORITY Admins"
        BlockValidation:
            Type: ImplicitMeta
            Rule: "ANY Writers"

Channel: &ChannelDefaults               #对于通道的配置
    Policies:
        Readers:
            Type: ImplicitMeta
            Rule: "ANY Readers"
        Writers:
            Type: ImplicitMeta
            Rule: "ANY Writers"
        Admins:
            Type: ImplicitMeta
            Rule: "MAJORITY Admins"
    Capabilities:
        <<: *ChannelCapabilities
        
Profiles:                   # 配置集

    TwoOrgsOrdererGenesis:          # 创始块信息关键字, 这个名字可以改, 命令中会被使用
        <<: *ChannelDefaults        #引用下面为 channelcapabilities 的属性
        Orderer:
            <<: *OrdererDefaults    #引用下面为 OrdererOrg 的属性
            Organizations:
                - *OrdererOrg       #引用下面为 OrdererOrg 的属性
            Capabilities:
                <<: *OrdererCapabilities
        Consortiums:                #定义系统中包含的组织
            SampleConsortium:
                Organizations:      #系统包含的组织，是组织名，和crypto-config.yaml里边的一致
                    - *Org1
                    - *Org2
    TwoOrgsChannel:                 # 通道信息关键字, 可以改, 会在命令中使用
        Consortium: SampleConsortium
        Application:
            <<: *ApplicationDefaults
            Organizations:
                - *Org1
                - *Org2
```



##### 2.2 生成通道文件和创世块文件

1. 先将存储两个文件的文件夹创建好，不然会报错

   ```shell
   mkdir channel-artifacts
   ```

2. 执行生成通道文件的命令

   ```shell
   sudo configtxgen -profile TwoOrgsChannel -outputCreateChannelTx ./channel-artifacts/channel.tx -channelID testchannel01
   ```

   命令说明：

   - `-profile` 后边指定的和配置文件里的要一致，意思是指定使用的配置集 
   - 通道ID自定义

3. 执行生成创世块的命令

   ```shell
   sudo configtxgen -profile TwoOrgsOrdererGenesis -outputBlock ./channel-artifacts/genesis.block -channelID testchannel01
   ```

   命令说明：

   - `-profile` 后边指定的和配置文件里的要一致

   - 两个命令的通道ID要一致



#### 启动容器

##### 将order，peer和cli的docker文件编写好











1. 实践一个简化版本的 fabeic1.4 网络，在一台服务器中部署完成，并执行chaincode:
   (1) 包括：
   ① 1 个 orderer 节点
   ② 2 个组织，每个组织 2 个 peer 节点
   ③ Chaincode 使用 fabric1.4 版本中 `chaincode/chaincode_example02/go/chaincode_example02.go`
   (2) 思路：
   ① 生成基础文件，如权限、创世块相关文件
   ② 部署 orderer 服务
   ③ 部署一个 peer 服务在 peer 上安装 chaincode,执行测试操作，这时，一个最小化的 fabric 网络就完成了。
   ④ 部署其他 peer 服务，加入到现有的网络中，并在其中执行 chaincode进行测试。

2. 详细步骤如下：

3. 创建测试目录 fabric/,并将 chaincode_example02 拷贝到 fabric/chaincode/目录下。

4. 编写配置文件 fabric/crypto-config.yaml 的内容：

   ```yam
   这里的配置文件内容需要根据数据结构来理解，后面再填  TODO……
   ```

   

5. 执行如下命令，执行后会发现目录中多出一个 crypto-config 的目录，在目录里面存放的就是刚刚生成的关于 orderer 以及 peer 的证书。

   ```shell
   cryptogen generate --config ./crypto-config.yaml
   ```

   

6. 编写配置文件 configtx.yaml，具内容入下:

   ```yam
   这里的配置文件内容需要根据数据结构来理解，后面再填  TODO……
   ```

7. 创建文件夹channel-artifacts，命令： `mkdir channel-artifacts` ；这里一定要先创建，否则下边命令会执行失败。

8. 执行命令，创建通道文件
   
```shell
   sudo configtxgen -profile TwoOrgsChannel -outputCreateChannelTx ./channel-artifacts/channel.tx -channelID mychannel
```

   

命令解析：

   ```shell
   -profile TwoOrgsChannel 									# 指定使用的配置集 TwoOrgsChannel 
   -outputCreateChannelTx ./channel-artifacts/channel.tx 		# 在路径./channel-artifacts/channel.tx下生成通道文件
   -channelID mychannel										# 指定通道ID
   ```

9. 创建创世块文件，执行 

   ```shell
   sudo configtxgen -profile TwoOrgsOrdererGenesis -outputBlock ./channel-artifacts/genesis.block
   ```

10. 



#### 锚节点相关

1. 创建每个组织的锚节点的信息，执行 
   
   ```shell
   sudo configtxgen -profile TwoOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/GoMSPanchors.tx -channelID mychannel -asOrg OrgGoMSP
   
   sudo configtxgen -profile TwoOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/CppMSPanchors.tx -channelID mychannel -asOrg OrgCppMSP
   ```
   
   
   
2. 将以上文件创建完成后，在`~/fabric/channel-artifacts` 目录下应包含创世块文件、通道文件、各个组织的锚节点更新的文件。





1. 配置客户端的 docker-compose-cli.yaml 文件，具体内容如下:

   ```yaml
   这里的配置文件内容需要根据数据结构来理解，后面再填  TODO……
   ```

2. 配置 orderer 节点的配置文件 docker-compose-base.yaml,具体的内容如下：

   ```yaml
   这里的配置文件内容需要根据数据结构来理解，后面再填  TODO……
   ```

3. 编写 peer 的配置文件 peer-base.yaml,具体内容如下：

   ```go
   这里的配置文件内容需要根据数据结构来理解，后面再填  TODO……
   ```

4. 编写完以上三个配置文件后，将 docker-compose-base.yaml 以及 peer-base.yaml 文件放入~/fabric/base/目录下。

5. 启动所有的容器，执行命令 

   ```shell
   docker-compose -f docker-compose-cli.yaml up -d
   ```

   

6. 所有容器启动后，通过执行 `docker ps -a` 来查看所有的容器的状态，进入到客户端容器中，执行 

   ```shell
   docker exec -it cli bash
   ```

   

7. 查看当前客户端所连接的 peer 地址，执行 

   ```shell
   echo $CORE_PEER_ADDRESS
   ```

   

8. 创建通道，执行命令 

   ```shell
   peer channel create -o orderer.itcast.com:7050 -c mychannel -f ./channel-artifacts/channel.tx --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/itcast.com/tlsca/tlsca.itcast.com-cert.pem
   ```

   - 命令解析

     ```shell
     -o 				# 指定服务和端口号
     -c				# 指定channelID
     -f 				# 指定通道文件，用于提交给orderer
     --tls 			# 在与orderer端通信时，是否使用使用TLS
     --cafile		# 指定容器内的证书文件
     ```

   

9. 将当前 peer 节点加入到通道中，执行命令 

   ```shell
   peer channel join -b mychannel.block
   ```

   - 命令解析

     ```shell
     -b 				# 包含genesis块的文件的路径
     ```

   

10. 更新锚节点，执行命令 

    ```shell
    peer channel update -o orderer.itcast.com:7050 -c mychannel -f ./channel-artifacts/GoMSPanchors.tx --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/itcast.com/msp/tlscacerts/tlsca.itcast.com-cert.pem
    ```

    - 命令解析

      ```shell
      -o 				# 指定服务和端口号
      -c				# 指定channelID
      -f 				# 指定通道文件，用于提交给orderer
      --tls 			# 在与orderer端通信时，是否使用使用TLS
      --cafile		# 容器内的证书文件
      ```

    

11. 在这个节点上安装链码，需要每个节点都安装，执行命令

    ```shell
    peer chaincode install -n mycc -v 1.0 -l golang -p github.com/chaincode
    ```

    - 命令解析

      ```shell
      -n				# 指定链码的名称
      -v				# 在 install/instantiate/upgrade 的命令中指定的链码版本
      -l				# 指定链码的语言
      -p				# 指定链码的路径
      ```

    

12. 在 peer 节点上初始化链码，在一个节点上安装就可以在网络中同步，执行命令  <font color=red>**注意：**因为这里指定了两个组织的背书策略，所有必须两个组织都有节点安装了链码才能够坐初始化链码的操作</font>

    ```shell
    peer chaincode instantiate -o orderer.itcast.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/itcast.com/msp/tlscacerts/tlsca.itcast.com-cert.pem -C mychannel -n mycc -l golang -v 1.0 -c '{"Args":["init","a","100","b","200"]}' -P "AND ('OrgGoMSP.member','OrgCppMSP.member')"
    ```

    - 命令解析

      ```shell
      peer chaincode instantiate [flags] 				# 将指定的链码部署到网络上
      	-c 											# 指定链码的构造器信息
      	-P											# 指定与此链码关联的背书策略
      ```

13. 查询数据，执行命令

    ```shell
    peer chaincode query -C mychannel -n mycc -c '{"Args":["query","a"]}'
    ```

14. 执行转账

    ```shell
    peer chaincode invoke -o orderer.itcast.com:7050 --tls true --cafile  /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/itcast.com/msp/tlscacerts/tlsca.itcast.com-cert.pem -C mychannel -n mycc --peerAddresses peer1.orgcpp.itcast.com:7051 --tlsRootCertFiles  /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/orgcpp.itcast.com/peers/peer1.orgcpp.itcast.com/tls/ca.crt --peerAddresses  peer0.orggo.itcast.com:7051 --tlsRootCertFiles  /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/orggo.itcast.com/peers/peer0.orggo.itcast.com/tls/ca.crt -c '{"Args":["invoke","a","b","10"]}'
    ```

<<<<<<< HEAD
15. 删除网络

    ```shell
    docker-compose -f docker-compose-peer.yaml down --rmi local -v
    	--rmi 
    		local 删除镜像名为空的镜像
    		all	  删除compose文件中定义的所有镜像
        -v	删除已经在compose文件中定义的和匿名的附在容器上的数据卷
        –remove-orphans 删除服务中没有在compose中定义的容器
    ```

    

    
=======
15. 升级链码
>>>>>>> origin/newComputer

    

    

    

    

    

    

    
    

16. 

### 多机solo

#### 三台虚拟机IP

```shell
192.168.43.11		# 虚拟机 a
192.168.43.12		# 虚拟机 b
192.168.43.13		# 虚拟机 c
```

#### 步骤

##### 1. 编写用于生成证书文件等的配置文件 crypto-config.yaml ( 192.168.43.11 )

```yaml
# Copyright IBM Corp. All Rights Reserved.
#
# SPDX-License-Identifier: Apache-2.0
#

# ---------------------------------------------------------------------------
# "OrdererOrgs" - Definition of organizations managing orderer nodes
# ---------------------------------------------------------------------------
OrdererOrgs:	# 排序节点组织
  # ---------------------------------------------------------------------------
  # Orderer
  # ---------------------------------------------------------------------------
  - Name: Orderer	# 组织的名称
    Domain: example.com		# 访问orderer组织的根域名
    CA:							# 指定 CA 机构
      Country: US
      Province: California
      Locality: San Francisco
    # ---------------------------------------------------------------------------
    # "Specs" - See PeerOrgs below for complete description
    # ---------------------------------------------------------------------------
    Specs:
      - Hostname: orderer	# orderer组织中的排序节点的名称，这样就组建了排序节点的域名为：orderer.example.com
# ---------------------------------------------------------------------------
# "PeerOrgs" - Definition of organizations managing peer nodes
# ---------------------------------------------------------------------------
PeerOrgs:		# peer节点组织
  # ---------------------------------------------------------------------------
  # Org1
  # ---------------------------------------------------------------------------
  - Name: Org1		# 其中一个组织的名称
    Domain: org1.example.com	# 当前组织的根域名
    CA:
      Country: US
      Province: California
      Locality: San Francisco
    EnableNodeOUs: true			# 是否支持nodejs
    # ---------------------------------------------------------------------------
    # "Specs"
    # ---------------------------------------------------------------------------
    # Uncomment this section to enable the explicit definition of hosts in your
    # configuration.  Most users will want to use Template, below
    #
    # Specs is an array of Spec entries.  Each Spec entry consists of two fields:
    #   - Hostname:   (Required) The desired hostname, sans the domain.
    #   - CommonName: (Optional) Specifies the template or explicit override for
    #                 the CN.  By default, this is the template:
    #
    #                              "{{.Hostname}}.{{.Domain}}"
    #
    #                 which obtains its values from the Spec.Hostname and
    #                 Org.Domain, respectively.
    # ---------------------------------------------------------------------------
    # Specs:
    #   - Hostname: foo # implicitly "foo.org1.example.com"
    #     CommonName: foo27.org5.example.com # overrides Hostname-based FQDN set above
    #   - Hostname: bar
    #   - Hostname: baz
    # ---------------------------------------------------------------------------
    # "Template"
    # ---------------------------------------------------------------------------
    # Allows for the definition of 1 or more hosts that are created sequentially
    # from a template. By default, this looks like "peer%d" from 0 to Count-1.
    # You may override the number of nodes (Count), the starting index (Start)
    # or the template used to construct the name (Hostname).
    #
    # Note: Template and Specs are not mutually exclusive.  You may define both
    # sections and the aggregate nodes will be created for you.  Take care with
    # name collisions
    # ---------------------------------------------------------------------------
    Template:		# 生成peer节点的模板
      Count: 1		# 指定该组织生成几个节点
      # Start: 5
      # Hostname: {{.Prefix}}{{.Index}} # default
    # ---------------------------------------------------------------------------
    # "Users"
    # ---------------------------------------------------------------------------
    # Count: The number of user accounts _in addition_ to Admin
    # ---------------------------------------------------------------------------
    Users:			# 当前组织中用户的个数
      Count: 1		# 这里指定的是普通用户个数，Admin用户是默认生成的
  # ---------------------------------------------------------------------------
  # Org2: See "Org1" for full specification
  # ---------------------------------------------------------------------------
  - Name: Org2		
    Domain: org2.example.com
    EnableNodeOUs: true
    CA:
      Country: US
      Province: California
      Locality: San Francisco
    Template:
      Count: 1
    Users:
      Count: 1
```



##### 2. 执行生成组织和证书命令 ( 192.168.43.11 )

```shell
cryptogen generate --config ./crypto-config.yaml

# 生成的目录结构
crypto-config
├── ordererOrganizations	# 排序节点组织文件夹
│   └── example.com			
│       ├── ca				# ca 证书
│       │   ├── 52e8a1a40f193a14bbd7c8422eca5331632f1d47c57e3ae95bc78f0fbb63aa5b_sk
│       │   └── ca.example.com-cert.pem
│       ├── msp
│       │   ├── admincerts
│       │   │   └── Admin@example.com-cert.pem
│       │   ├── cacerts
│       │   │   └── ca.example.com-cert.pem
│       │   └── tlscacerts
│       │       └── tlsca.example.com-cert.pem
│       ├── orderers		# 这里只有一个排序节点
│       │   └── orderer.example.com
│       │       ├── msp
│       │       │   ├── admincerts
│       │       │   │   └── Admin@example.com-cert.pem
│       │       │   ├── cacerts
│       │       │   │   └── ca.example.com-cert.pem
│       │       │   ├── keystore
│       │       │   │   └── 82d794d411094bb257b75b080dd8360f9663b394d435cf77aabf0072f219fbda_sk
│       │       │   ├── signcerts
│       │       │   │   └── orderer.example.com-cert.pem
│       │       │   └── tlscacerts
│       │       │       └── tlsca.example.com-cert.pem
│       │       └── tls
│       │           ├── ca.crt
│       │           ├── server.crt
│       │           └── server.key
│       ├── tlsca
│       │   ├── 8a833cbbf2b9b69391b654919a57db028daa3fc790a8e3eaff928bf02473dc05_sk
│       │   └── tlsca.example.com-cert.pem
│       └── users
│           └── Admin@example.com
│               ├── msp
│               │   ├── admincerts
│               │   │   └── Admin@example.com-cert.pem
│               │   ├── cacerts
│               │   │   └── ca.example.com-cert.pem
│               │   ├── keystore
│               │   │   └── 1dd58683337a1d6518b8a89981df0ccf071fc5454607f7dbbf566395d2bee0c3_sk
│               │   ├── signcerts
│               │   │   └── Admin@example.com-cert.pem
│               │   └── tlscacerts
│               │       └── tlsca.example.com-cert.pem
│               └── tls
│                   ├── ca.crt
│                   ├── client.crt
│                   └── client.key
└── peerOrganizations
    ├── org1.example.com
    │   ├── ca
    │   │   ├── b71aa969e09e2d9c5b05705ebf1233af8266cff989e7fa2b3f70964b013c2313_sk
    │   │   └── ca.org1.example.com-cert.pem
    │   ├── msp
    │   │   ├── admincerts
    │   │   │   └── Admin@org1.example.com-cert.pem
    │   │   ├── cacerts
    │   │   │   └── ca.org1.example.com-cert.pem
    │   │   ├── config.yaml
    │   │   └── tlscacerts
    │   │       └── tlsca.org1.example.com-cert.pem
    │   ├── peers
    │   │   └── peer0.org1.example.com
    │   │       ├── msp
    │   │       │   ├── admincerts
    │   │       │   │   └── Admin@org1.example.com-cert.pem
    │   │       │   ├── cacerts
    │   │       │   │   └── ca.org1.example.com-cert.pem
    │   │       │   ├── config.yaml
    │   │       │   ├── keystore
    │   │       │   │   └── 27fd58978c77d551080087582ad788737177cd37c316749ec2be9b01e67006e6_sk
    │   │       │   ├── signcerts
    │   │       │   │   └── peer0.org1.example.com-cert.pem
    │   │       │   └── tlscacerts
    │   │       │       └── tlsca.org1.example.com-cert.pem
    │   │       └── tls
    │   │           ├── ca.crt
    │   │           ├── server.crt
    │   │           └── server.key
    │   ├── tlsca
    │   │   ├── 5ef783c0697d55be48fe9688958141cff4f0e6843acee21ca642502e854f88f2_sk
    │   │   └── tlsca.org1.example.com-cert.pem
    │   └── users
    │       ├── Admin@org1.example.com
    │       │   ├── msp
    │       │   │   ├── admincerts
    │       │   │   │   └── Admin@org1.example.com-cert.pem
    │       │   │   ├── cacerts
    │       │   │   │   └── ca.org1.example.com-cert.pem
    │       │   │   ├── keystore
    │       │   │   │   └── ebfbd5bc98a2cc972dd24585ceb55bb6a88c503b3e380fbacac026b8c987fe1a_sk
    │       │   │   ├── signcerts
    │       │   │   │   └── Admin@org1.example.com-cert.pem
    │       │   │   └── tlscacerts
    │       │   │       └── tlsca.org1.example.com-cert.pem
    │       │   └── tls
    │       │       ├── ca.crt
    │       │       ├── client.crt
    │       │       └── client.key
    │       └── User1@org1.example.com
    │           ├── msp
    │           │   ├── admincerts
    │           │   │   └── User1@org1.example.com-cert.pem
    │           │   ├── cacerts
    │           │   │   └── ca.org1.example.com-cert.pem
    │           │   ├── keystore
    │           │   │   └── 8af0dabd8a4542133d2ef6e831d2247d0b572212569ac2a6a68fbe63bbc84f69_sk
    │           │   ├── signcerts
    │           │   │   └── User1@org1.example.com-cert.pem
    │           │   └── tlscacerts
    │           │       └── tlsca.org1.example.com-cert.pem
    │           └── tls
    │               ├── ca.crt
    │               ├── client.crt
    │               └── client.key
    └── org2.example.com
        ├── ca
        │   ├── 26248eadabba331e3be698b9e5ea295581b45f21deb440732d4e2a7f0aef4adc_sk
        │   └── ca.org2.example.com-cert.pem
        ├── msp
        │   ├── admincerts
        │   │   └── Admin@org2.example.com-cert.pem
        │   ├── cacerts
        │   │   └── ca.org2.example.com-cert.pem
        │   ├── config.yaml
        │   └── tlscacerts
        │       └── tlsca.org2.example.com-cert.pem
        ├── peers
        │   └── peer0.org2.example.com
        │       ├── msp
        │       │   ├── admincerts
        │       │   │   └── Admin@org2.example.com-cert.pem
        │       │   ├── cacerts
        │       │   │   └── ca.org2.example.com-cert.pem
        │       │   ├── config.yaml
        │       │   ├── keystore
        │       │   │   └── 0e13871b9270a26784862b9c6ba6dfb007ddfe9a3ee447851e9d1ef6973ebfa0_sk
        │       │   ├── signcerts
        │       │   │   └── peer0.org2.example.com-cert.pem
        │       │   └── tlscacerts
        │       │       └── tlsca.org2.example.com-cert.pem
        │       └── tls
        │           ├── ca.crt
        │           ├── server.crt
        │           └── server.key
        ├── tlsca
        │   ├── 7dbf1d2620beaab8b9a95732857468457973073b5afeaf8b6a526cdab0ee840c_sk
        │   └── tlsca.org2.example.com-cert.pem
        └── users
            ├── Admin@org2.example.com
            │   ├── msp
            │   │   ├── admincerts
            │   │   │   └── Admin@org2.example.com-cert.pem
            │   │   ├── cacerts
            │   │   │   └── ca.org2.example.com-cert.pem
            │   │   ├── keystore
            │   │   │   └── 66b1d7bfa079122d23cd4231a590a41d8a2e0d387b7df3fe50a48736e4692ca8_sk
            │   │   ├── signcerts
            │   │   │   └── Admin@org2.example.com-cert.pem
            │   │   └── tlscacerts
            │   │       └── tlsca.org2.example.com-cert.pem
            │   └── tls
            │       ├── ca.crt
            │       ├── client.crt
            │       └── client.key
            └── User1@org2.example.com
                ├── msp
                │   ├── admincerts
                │   │   └── User1@org2.example.com-cert.pem
                │   ├── cacerts
                │   │   └── ca.org2.example.com-cert.pem
                │   ├── keystore
                │   │   └── a3e771994d0384b0b82ab28672648ec8f6d6afa8cf66dc96f512efca19efd187_sk
                │   ├── signcerts
                │   │   └── User1@org2.example.com-cert.pem
                │   └── tlscacerts
                │       └── tlsca.org2.example.com-cert.pem
                └── tls
                    ├── ca.crt
                    ├── client.crt
                    └── client.key
```



##### 3. 编写生成创世区块和通道的配置文件configtx.yaml ( 192.168.43.11 )

```yaml
# Copyright IBM Corp. All Rights Reserved.
#
# SPDX-License-Identifier: Apache-2.0
#

---
################################################################################
#
#   Section: Organizations
#
#   - This section defines the different organizational identities which will
#   be referenced later in the configuration.
#
################################################################################
Organizations:		# 组织

    # SampleOrg defines an MSP using the sampleconfig.  It should never be used
    # in production but may be used as a template for other definitions
    - &OrdererOrg	# 排序节点组织
        # DefaultOrg defines the organization which is used in the sampleconfig
        # of the fabric.git development environment
        Name: OrdererOrg	# 组织名，要和crypto-config.yaml一致

        # ID to load the MSP definition as
        ID: OrdererMSP		# 组织ID

        # MSPDir is the filesystem path which contains the MSP configuration
        MSPDir: crypto-config/ordererOrganizations/example.com/msp 	# orderer组织的账号目录

        # Policies defines the set of policies at this level of the config tree
        # For organization policies, their canonical path is usually
        #   /Channel/<Application|Orderer>/<OrgName>/<PolicyName>
        Policies:
            Readers:
                Type: Signature
                Rule: "OR('OrdererMSP.member')"
            Writers:
                Type: Signature
                Rule: "OR('OrdererMSP.member')"
            Admins:
                Type: Signature
                Rule: "OR('OrdererMSP.admin')"

    - &Org1
        # DefaultOrg defines the organization which is used in the sampleconfig
        # of the fabric.git development environment
        Name: Org1MSP

        # ID to load the MSP definition as
        ID: Org1MSP

        MSPDir: crypto-config/peerOrganizations/org1.example.com/msp

        # Policies defines the set of policies at this level of the config tree
        # For organization policies, their canonical path is usually
        #   /Channel/<Application|Orderer>/<OrgName>/<PolicyName>
        Policies:
            Readers:
                Type: Signature
                Rule: "OR('Org1MSP.admin', 'Org1MSP.peer', 'Org1MSP.client')"
            Writers:
                Type: Signature
                Rule: "OR('Org1MSP.admin', 'Org1MSP.client')"
            Admins:
                Type: Signature
                Rule: "OR('Org1MSP.admin')"

        # leave this flag set to true.
        AnchorPeers:		# 组织的锚节点
            # AnchorPeers defines the location of peers which can be used
            # for cross org gossip communication.  Note, this value is only
            # encoded in the genesis block in the Application section context
            - Host: peer0.org1.example.com
              Port: 7051

    - &Org2
        # DefaultOrg defines the organization which is used in the sampleconfig
        # of the fabric.git development environment
        Name: Org2MSP

        # ID to load the MSP definition as
        ID: Org2MSP

        MSPDir: crypto-config/peerOrganizations/org2.example.com/msp

        # Policies defines the set of policies at this level of the config tree
        # For organization policies, their canonical path is usually
        #   /Channel/<Application|Orderer>/<OrgName>/<PolicyName>
        Policies:
            Readers:
                Type: Signature
                Rule: "OR('Org2MSP.admin', 'Org2MSP.peer', 'Org2MSP.client')"
            Writers:
                Type: Signature
                Rule: "OR('Org2MSP.admin', 'Org2MSP.client')"
            Admins:
                Type: Signature
                Rule: "OR('Org2MSP.admin')"

        AnchorPeers:
            # AnchorPeers defines the location of peers which can be used
            # for cross org gossip communication.  Note, this value is only
            # encoded in the genesis block in the Application section context
            - Host: peer0.org2.example.com
              Port: 7051

################################################################################
#
#   SECTION: Capabilities
#
#   - This section defines the capabilities of fabric network. This is a new
#   concept as of v1.1.0 and should not be utilized in mixed networks with
#   v1.0.x peers and orderers.  Capabilities define features which must be
#   present in a fabric binary for that binary to safely participate in the
#   fabric network.  For instance, if a new MSP type is added, newer binaries
#   might recognize and validate the signatures from this type, while older
#   binaries without this support would be unable to validate those
#   transactions.  This could lead to different versions of the fabric binaries
#   having different world states.  Instead, defining a capability for a channel
#   informs those binaries without this capability that they must cease
#   processing transactions until they have been upgraded.  For v1.0.x if any
#   capabilities are defined (including a map with all capabilities turned off)
#   then the v1.0.x peer will deliberately crash.
#
################################################################################
Capabilities:
    # Channel capabilities apply to both the orderers and the peers and must be
    # supported by both.
    # Set the value of the capability to true to require it.
    Channel: &ChannelCapabilities
        # V1.3 for Channel is a catchall flag for behavior which has been
        # determined to be desired for all orderers and peers running at the v1.3.x
        # level, but which would be incompatible with orderers and peers from
        # prior releases.
        # Prior to enabling V1.3 channel capabilities, ensure that all
        # orderers and peers on a channel are at v1.3.0 or later.
        V1_3: true

    # Orderer capabilities apply only to the orderers, and may be safely
    # used with prior release peers.
    # Set the value of the capability to true to require it.
    Orderer: &OrdererCapabilities
        # V1.1 for Orderer is a catchall flag for behavior which has been
        # determined to be desired for all orderers running at the v1.1.x
        # level, but which would be incompatible with orderers from prior releases.
        # Prior to enabling V1.1 orderer capabilities, ensure that all
        # orderers on a channel are at v1.1.0 or later.
        V1_1: true

    # Application capabilities apply only to the peer network, and may be safely
    # used with prior release orderers.
    # Set the value of the capability to true to require it.
    Application: &ApplicationCapabilities
        # V1.3 for Application enables the new non-backwards compatible
        # features and fixes of fabric v1.3.
        V1_3: true
        # V1.2 for Application enables the new non-backwards compatible
        # features and fixes of fabric v1.2 (note, this need not be set if
        # later version capabilities are set)
        V1_2: false
        # V1.1 for Application enables the new non-backwards compatible
        # features and fixes of fabric v1.1 (note, this need not be set if
        # later version capabilities are set).
        V1_1: false

################################################################################
#
#   SECTION: Application
#
#   - This section defines the values to encode into a config transaction or
#   genesis block for application related parameters
#
################################################################################
Application: &ApplicationDefaults

    # Organizations is the list of orgs which are defined as participants on
    # the application side of the network
    Organizations:

    # Policies defines the set of policies at this level of the config tree
    # For Application policies, their canonical path is
    #   /Channel/Application/<PolicyName>
    Policies:
        Readers:
            Type: ImplicitMeta
            Rule: "ANY Readers"
        Writers:
            Type: ImplicitMeta
            Rule: "ANY Writers"
        Admins:
            Type: ImplicitMeta
            Rule: "MAJORITY Admins"

    Capabilities:
        <<: *ApplicationCapabilities
################################################################################
#
#   SECTION: Orderer
#
#   - This section defines the values to encode into a config transaction or
#   genesis block for orderer related parameters
#
################################################################################
Orderer: &OrdererDefaults

    # Orderer Type: The orderer implementation to start
    # Available types are "solo" and "kafka"
    OrdererType: solo		# 使用的共识算法

    Addresses:				# order节点的访问地址
        - orderer.example.com:7050

    # Batch Timeout: The amount of time to wait before creating a batch
    BatchTimeout: 2s		# 每隔多长时间打包一个区块

    # Batch Size: Controls the number of messages batched into a block
    BatchSize:

        # Max Message Count: The maximum number of messages to permit in a batch
        MaxMessageCount: 100		# 单个区块的最大消息数

        # Absolute Max Bytes: The absolute maximum number of bytes allowed for
        # the serialized messages in a batch.
        AbsoluteMaxBytes: 32 MB		# 单个区块的最大容量，到了自动打包区块

        # Preferred Max Bytes: The preferred maximum number of bytes allowed for
        # the serialized messages in a batch. A message larger than the preferred
        # max bytes will result in a batch larger than preferred max bytes.
        PreferredMaxBytes: 512 KB

    Kafka:
        # Brokers: A list of Kafka brokers to which the orderer connects
        # NOTE: Use IP:port notation
        Brokers:
            - 127.0.0.1:9092	# kafka服务器地址

    # Organizations is the list of orgs which are defined as participants on
    # the orderer side of the network
    Organizations:

    # Policies defines the set of policies at this level of the config tree
    # For Orderer policies, their canonical path is
    #   /Channel/Orderer/<PolicyName>
    Policies:
        Readers:
            Type: ImplicitMeta
            Rule: "ANY Readers"
        Writers:
            Type: ImplicitMeta
            Rule: "ANY Writers"
        Admins:
            Type: ImplicitMeta
            Rule: "MAJORITY Admins"
        # BlockValidation specifies what signatures must be included in the block
        # from the orderer for the peer to validate it.
        BlockValidation:
            Type: ImplicitMeta
            Rule: "ANY Writers"


Channel: &ChannelDefaults
    # Policies defines the set of policies at this level of the config tree
    # For Channel policies, their canonical path is
    #   /Channel/<PolicyName>
    Policies:
        # Who may invoke the 'Deliver' API
        Readers:
            Type: ImplicitMeta
            Rule: "ANY Readers"
        # Who may invoke the 'Broadcast' API
        Writers:
            Type: ImplicitMeta
            Rule: "ANY Writers"
        # By default, who may modify elements at this config level
        Admins:
            Type: ImplicitMeta
            Rule: "MAJORITY Admins"

    # Capabilities describes the channel level capabilities, see the
    # dedicated Capabilities section elsewhere in this file for a full
    # description
    Capabilities:
        <<: *ChannelCapabilities


Profiles:	# 关键字，配置集

    TwoOrgsOrdererGenesis:		# 创始块信息, 这个名字可以改, 命令中会被使用
        <<: *ChannelDefaults
        Orderer:
            <<: *OrdererDefaults
            Organizations:
                - *OrdererOrg
            Capabilities:
                <<: *OrdererCapabilities
        Consortiums:
            SampleConsortium:
                Organizations:
                    - *Org1
                    - *Org2
    TwoOrgsChannel:				# 通道信息关键字, 可以改, 会在命令中使用
        Consortium: SampleConsortium
        Application:
            <<: *ApplicationDefaults
            Organizations:
                - *Org1
                - *Org2
```



##### 4. 执行生成创世块和通道命令( 192.168.43.11 )

```shell
mkdir channel-artifacts 		# 必须先创建这个文件夹
sudo configtxgen -profile TwoOrgsChannel -outputCreateChannelTx ./channel-artifacts/channel.tx -channelID testchannel		# 生成通道文件
sudo configtxgen -profile TwoOrgsOrdererGenesis -outputBlock ./channel-artifacts/genesis.block -channelID testchannel	# 生成创世区块文件
```



##### 5. 设置hosts( 192.168.43.11  192.168.43.12  192.168.43.13 )

```shell
sudo vim /etc/hosts

# 在 /etc/hosts 中添加一下三行
192.168.43.11 	orderer.example.com
192.168.43.12 	peer0.org1.example.com
192.168.43.13 	peer0.org2.example.com
```

##### 6. 编写文件docker-compose-orderer.yaml用于启动orderer节点( 192.168.43.11 )

```yaml
# Copyright IBM Corp. All Rights Reserved.
#
# SPDX-License-Identifier: Apache-2.0
#

version: '2'

services:
  orderer.example.com:				# 服务名,
    image: hyperledger/fabric-orderer:latest	# 指定docker镜像和版本号
    environment:
      - FABRIC_LOGGING_SPEC=INFO	# 日志级别
      - ORDERER_GENERAL_LISTENADDRESS=0.0.0.0	# orderer节点监听的地址
      - ORDERER_GENERAL_GENESISMETHOD=file		# 生成创始区块的数据来源, 一般写file, 来自于文件
      - ORDERER_GENERAL_GENESISFILE=/var/hyperledger/orderer/orderer.genesis.block	# 创世区块在容器内的路径
      - ORDERER_GENERAL_LOCALMSPID=OrdererMSP	# 当前orderer节点所属的组织的组织ID，configtx.yaml中有
      - ORDERER_GENERAL_LOCALMSPDIR=/var/hyperledger/orderer/msp	# 当前orderer节点的账号目录路径
      # enabled TLS
      - ORDERER_GENERAL_TLS_ENABLED=true	# 通信的时候是否需要tls加密
      - ORDERER_GENERAL_TLS_PRIVATEKEY=/var/hyperledger/orderer/tls/server.key	# tls私钥路径
      - ORDERER_GENERAL_TLS_CERTIFICATE=/var/hyperledger/orderer/tls/server.crt	# tls证书路径
      - ORDERER_GENERAL_TLS_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]		# tls根证书路径
      - ORDERER_KAFKA_TOPIC_REPLICATIONFACTOR=1
      - ORDERER_KAFKA_VERBOSE=true
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric		# 进入容器后的工作目录
    command: orderer	# 进入容器后执行的命令
    volumes:
      - ./channel-artifacts/genesis.block:/var/hyperledger/orderer/orderer.genesis.block	# 挂载创世区块文件
      - ./crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/msp:/var/hyperledger/orderer/msp	# 挂载msp
      - ./crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/tls/:/var/hyperledger/orderer/tls	# 挂载tls
    ports:
      - 7050:7050
```



##### 7. 执行启动orderer节点容器命令( 192.168.43.11 )

```shell
docker-compose -f ./docker-compose-orderer.yaml up -d		# 启动orderer节点容器命令，-d 以守护进程的方式启动
docker-compose -f ./docker-compose-peer.yaml down -v	# 关闭orderer节点容器命令，-v 将关联的数据卷删除，若不删除会影响下次搭建fabric网络
```



##### 8. 编写用于启动peer节点的文件 docker-compose-peer.yaml 

> ( 192.168.43.12 )

```yaml
# Copyright IBM Corp. All Rights Reserved.
#
# SPDX-License-Identifier: Apache-2.0
#

version: '2'

services:
  peer0.org1.example.com:		# 组织org1的peer0的服务名
    container_name: peer0.org1.example.com	# 容器卷名称
    image: hyperledger/fabric-peer:latest
    environment:
      - CORE_PEER_ID=peer0.org1.example.com
      - CORE_PEER_ADDRESS=peer0.org1.example.com:7051	 # 当前peer节点的地址
      - CORE_PEER_CHAINCODEADDRESS=peer0.org1.example.com:7052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:7052
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer0.org1.example.com:7051 # 节点启动之后连接的地址，最好写自己的地址
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer0.org1.example.com:7051	# 设置是否被外部感知
      - CORE_PEER_LOCALMSPID=Org1MSP	# 当前peer节点所属的组织的组织ID
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=testwork_default	# docker加入的网络名
      # the following setting starts chaincode containers on the same
      # bridge network as the peers
      # https://docs.docker.com/compose/networking/
      - FABRIC_LOGGING_SPEC=INFO	# 日志级别
      #- FABRIC_LOGGING_SPEC=DEBUG
      - CORE_PEER_TLS_ENABLED=true	# peer通信的时候是不是使用tls加密
      - CORE_PEER_GOSSIP_USELEADERELECTION=true		# 是不是自动选择leader节点
      - CORE_PEER_GOSSIP_ORGLEADER=false			# 当前节点不是leader节点
      - CORE_PEER_PROFILE_ENABLED=true				# 在peer节点内部有一profile服务器, 默认是启动的
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: peer node start
    volumes:
      - /var/run/:/host/var/run/
      - ./crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/msp:/etc/hyperledger/fabric/msp
      - ./crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls:/etc/hyperledger/fabric/tls
    ports:
      - 7051:7051
      - 7052:7052
      - 7053:7053
    extra_hosts:	# 声明域名和IP的对应关系
      - "orderer.example.com:192.168.43.11"
  cli:
    container_name: cli
    image: hyperledger/fabric-tools:latest
    tty: true
    environment:
      - GOPATH=/opt/gopath
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock		# 宿主机的本地套接字文件, 无需修改
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_ID=cli			# 客户端容器的名字, 随便起名
      - CORE_PEER_ADDRESS=peer0.org1.example.com:7051		# 客户端操作的peer节点的地址
      - CORE_PEER_LOCALMSPID=Org1MSP		# 连接的peer节点所属的组织的组织ID
      - CORE_PEER_TLS_ENABLED=true			# 通信数据是不是要加密
      - CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/server.crt	# tls证书, 容器内部路径
      - CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/server.key		# tls私钥
      - CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt	# tls根证书
      - CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/msp		# cli客户端, 是用户操作的, 指定的是客户端使用的账号文件目录
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: /bin/bash
    volumes:
      - /var/run/:/host/var/run/
      - ./chaincode/:/opt/gopath/src/github.com/chaincode
      - ./crypto-config:/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/
      - ./channel-artifacts:/opt/gopath/src/github.com/hyperledger/fabric/peer/channel-artifacts
    depends_on:
      - peer0.org1.example.com
    extra_hosts:
      - "orderer.example.com:192.168.43.11"
      - "peer0.org1.example.com:192.168.43.12"
      - "peer0.org2.example.com:192.168.43.13"
```



##### 9. 远程目录拷贝到本地

> ( 192.168.43.12 )

```shell
# 要通过scp命令来完成远程文件传输，被链接的一方必须安装 openssh.server,安装命令
sudo apt-get install openssh-server
# 但是执行这个命令时可能会报错，因为安装 openssh-server 需要依赖 openssh-client，而Ubuntu自带的openssh-client版本和依赖的不同，所以这里采取猥琐方案
# 卸载 openssh-client，再安装 openssh-server
sudo apt-get remove openssh-client

# copy 远程的channel-artifacts文件夹到本地当前文件夹
scp -r shalom@192.168.147.131:$HOME/study/solo/channel-artifacts ./
scp -r shalom@192.168.147.131:$HOME/study/solo/crypto-config ./
```



##### 10. 拷贝示例chaincode

> ( 192.168.43.12 )

```shell
mkdir chaincode
cp /home/shalom/hyperledger-fabric/fabric-samples/chaincode/chaincode_example02/go/chaincode_example02.go ./chaincode/
```



##### 11. 执行启动peer节点容器命令

> ( 192.168.43.12 )

```shell
# 启动
docker-compose -f docker-compose-peer.yaml up -d
# 关闭，并删除关联容器卷
docker-compose -f docker-compose-peer.yaml down -v
```



##### 12. 进入客户端容器

> ( 192.168.43.12 )

```shell
docker exec -it cli bash
```



##### 13. 创建通道

> ( 192.168.43.12 )

```shell
peer channel create -o orderer.example.com:7050 -c testchannel -f ./channel-artifacts/channel.tx --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/msp/tlscacerts/tlsca.example.com-cert.pem

peer channel create -o orderer.example.com:7050 -c testchannel -f ./channel-artifacts/channel.tx --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem

peer channel create -o tata.logs.com:7050 -c testchannel -f ./channel-artifacts/channel.tx --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/logs.com/orderers/tata.logs.com/msp/tlscacerts/tlsca.logs.com-cert.pem
```



##### 14. 加入通道

> ( 192.168.43.12 )

```shell
peer channel join -b testchannel.block
```



##### 15. 安装链码

> ( 192.168.43.12 )

```shell
peer chaincode install -n mycc -v 1.0 -l golang -p github.com/chaincode
```



##### 16. 初始化链码

> ( 192.168.43.12 )

```shell
peer chaincode instantiate -o orderer.example.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C testchannel -n mycc -l golang -v 1.0 -c '{"Args":["init","a","100","b","200"]}' -P "AND ('Org1MSP.member','Org2MSP.member')"


peer chaincode instantiate -o orderer.example.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C testchannel -n mycc -l golang -v 1.0 -c '{"Args":["init","a","100","b","200"]}' -P "AND ('Org1MSP.member','Org2MSP.member')"
```



##### 17. 查询数据

> ( 192.168.43.12 )

```shell
peer chaincode query -C testchannel -n mycc -c '{"Args":["query","a"]}'
```



##### 18. 链码打包

> ( 192.168.43.12 )

```shell
peer chaincode package -p github.com/chaincode -n mycc -v 1.0 mycc.1.0.out
```



##### 19. cp区块和……

> 将testchannel.block和mycc.1.0.out从cli容器拷贝到宿主机,操作要在宿主机中进行

> ( 192.168.43.12 )

```shell
exit
docker cp cli:/opt/gopath/src/github.com/hyperledger/fabric/peer/testchannel.block ./channel-artifacts/
docker cp cli:/opt/gopath/src/github.com/hyperledger/fabric/peer/mycc.1.0.out ./channel-artifacts/
```



##### 20. 第三台虚拟机的操作

( 192.168.43.13 )

​	步骤和第二台虚拟机一样

**注意：**

1. 编写 docker-compose-peer.yaml  文件

2. 远程目录拷贝到本地，拷贝peer组织节点的

   ```shell
   scp -r shalom@192.168.43.12:$HOME/testwork/channel-artifacts ./
   scp -r shalom@192.168.43.12:$HOME/testwork/crypto-config ./
   ```

3. 执行启动peer节点容器命令

   ```shell
   # 启动
   docker-compose -f docker-compose-peer.yaml up -d
   # 关闭，并删除关联容器卷
   docker-compose -f docker-compose-peer.yaml down -v
   ```

4. 进入客户端容器

   ```shell
   docker exec -it cli bash
   ```

   

5. 移出两个文件到当前目录

   ```shell
   mv channel-artifacts/testchannel.block /opt/gopath/src/github.com/hyperledger/fabric/peer
   mv channel-artifacts/mycc.1.0.out /opt/gopath/src/github.com/hyperledger/fabric/peer
   ```

6. 加入通道

   ```shell
   peer channel join -b testchannel.block
   ```

   

7. 安装链码

   ```shell
   peer chaincode install mycc.1.0.out
   ```

   

8. 执行交易

   ```shell
   peer chaincode invoke -o orderer.example.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C testchannel -n mycc --peerAddresses peer0.org1.example.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses peer0.org2.example.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '{"Args":["invoke","a","b","10"]}'
   ```

9. 查询数据

   ```shell
   peer chaincode query -C testchannel -n mycc -c '{"Args":["query","a"]}'
   ```

   



### 多机Kafka

在每个服务器上创建一个目录执行下边的操作，这里就以 testwork 文件夹为例

各个服务器要搭载的服务节点如下表：

|  服务器  |                            节点名                            |
| :------: | :----------------------------------------------------------: |
| 服务器01 | orderer0.example.com<br/>kafka0<br/>zookeeper0<br/>peer0.org1.example.com |
| 服务器02 | orderer1.example.com<br/>kafka1<br/>zookeeper1<br/>peer1.org1.example.com |
| 服务器03 | orderer2.example.com<br/>kafka2<br/>zookeeper2<br/>peer0.org2.example.com |
| 服务器04 |              kafka3<br/>peer1.org2.example.com               |



#### 1. 生成各个成员

3个排序节点，两个组织分别各两个peer节点

##### 1.1 编写配置文件 crypto-config.yaml

```yaml
OrdererOrgs:    # order节点组织，无需更改
  - Name: Orderer   # 组织的名称，可以根据实际情况指定
    Domain: example.com     # 组织的根域名
    CA:                     # 指定 CA 机构
      Country: US
      Province: California
      Locality: San Francisco
    Specs:
      - Hostname: orderer0  # order节点组织中各节点的名称，和根域名最终组成个节点的域名 orderer0.example.com  
      - Hostname: orderer1  # orderer1.example.com
      - Hostname: orderer2  # orderer2.example.com  可以看出这里设置了3个排序节点，也就是order节点
# ---------------------------------------------------------------------------
# "PeerOrgs" - Definition of organizations managing peer nodes
# ---------------------------------------------------------------------------
PeerOrgs:   # peer节点的组织，有两个peer组织
  # ---------------------------------------------------------------------------
  # Org1
  # ---------------------------------------------------------------------------
  - Name: Org1  # 其中一个组织的名称
    Domain: org1.example.com    # 当前组织的根域名
    EnableNodeOUs: true         # 启用节点身份
    CA:                         # 指定 CA 机构
      Country: US
      Province: California
      Locality: San Francisco
    Template:       # 生成peer节点的模板
      Count: 2      # 指定该组织生成几个节点
    Users:          # 当前组织中用户的个数
      Count: 1      # 这里指定的是普通用户个数，Admin用户是默认生成的，不需要在这里指定
  # ---------------------------------------------------------------------------
  # Org2: See "Org1" for full specification
  # ---------------------------------------------------------------------------
  - Name: Org2
    Domain: org2.example.com
    EnableNodeOUs: true
    CA:
      Country: US
      Province: California
      Locality: San Francisco
    Template:
      Count: 2
    Users:
      Count: 1
```



##### 1.2 执行成组织和证书命令

```shell
cryptogen generate --config ./crypto-config.yaml
```

**生成的文件目录结构**

```shell
shalom@ubuntu:~/study/testwork/crypto-config$ tree -L 2
.
├── ordererOrganizations
│   └── example.com				# 对应组织根域名
└── peerOrganizations
    ├── org1.example.com		# 对应组织根域名
    └── org2.example.com
```

每个组织目录结构：

```shell
# 每个排序节点组织都包含 ca msp tlsca（这三个目录代表组织的身份）orderers users（这两个是组织中的节点成员和用户成员）
.
└── example.com
    ├── ca				# CA服务器的签名文件
    ├── msp
    ├── orderers
    ├── tlsca
    └── users
    
.
└── org2.example.com
    ├── ca
    ├── msp
    ├── peers
    ├── tlsca
    └── users
```

tls：是与其他节点连接时用的证书

msp：应该怎么理解？

每个节点的目录结构：

```shell
peer0.org1.example.com
.
├── msp
│   ├── admincerts			# 组织的管理证书, 创建通道必须要有该证书
│   │   └── Admin@org1.example.com-cert.pem
│   ├── cacerts				# 组织根证书
│   │   └── ca.org1.example.com-cert.pem
│   ├── config.yaml
│   ├── keystore			# 当前节点的私钥
│   │   └── b9d61224c2c723e653f680e053c8a6c74aadf5eea29d40412eed1fdbfd9452ec_sk
│   ├── signcerts			# 当前节点签名的数字证书
│   │   └── peer0.org1.example.com-cert.pem
│   └── tlscacerts			# tls连接的身份证书
│       └── tlsca.org1.example.com-cert.pem
└── tls
    ├── ca.crt				# 组织的根证书
    ├── server.crt			# 验证本节点签名的证书
    └── server.key			# 当前节点的私钥
```



#### 2. 生成创世块，通道文件

##### 2.1 编写配置文件 configtx.yaml

**注意：**这个文件名必须为 `configtx.yaml`

```yaml
---
Organizations:      # 开始配置组织
    - &OrdererOrg   # 开始配置排序节点组织
        Name: OrdererOrg    # 组织名，要和crypto-config.yaml里边的一致
        ID: OrdererMSP   # 组织ID，自定义
        MSPDir: crypto-config/ordererOrganizations/example.com/msp  # 组织的msp文件路径，注意相对路径
        Policies:
            Readers:
                Type: Signature
                Rule: "OR('OrdererMSP.member')"     # 这里是 组织ID
            Writers:
                Type: Signature
                Rule: "OR('OrdererMSP.member')"
            Admins:
                Type: Signature
                Rule: "OR('OrdererMSP.admin')"
    - &Org1
        Name: Org1MSP
        ID: Org1MSP
        MSPDir: crypto-config/peerOrganizations/org1.example.com/msp
        AnchorPeers:        # 指定锚节点
            - Host: peer0.org1.example.com      # 指定锚节点域名
              Port: 7051
        Policies:
            Readers:
                Type: Signature
                Rule: "OR('Org1MSP.admin', 'Org1MSP.peer', 'Org1MSP.client')"
            Writers:
                Type: Signature
                Rule: "OR('Org1MSP.admin', 'Org1MSP.client')"
            Admins:
                Type: Signature
                Rule: "OR('Org1MSP.admin')"
    - &Org2
        Name: Org2MSP
        ID: Org2MSP
        MSPDir: crypto-config/peerOrganizations/org2.example.com/msp
        AnchorPeers:
            - Host: peer0.org2.example.com
              Port: 7051
        Policies:
            Readers:
                Type: Signature
                Rule: "OR('Org2MSP.admin', 'Org2MSP.peer', 'Org2MSP.client')"
            Writers:
                Type: Signature
                Rule: "OR('Org2MSP.admin', 'Org2MSP.client')"
            Admins:
                Type: Signature
                Rule: "OR('Org2MSP.admin')"

Capabilities:   #对于通道、排序、应用的功能的配置，所有的值使用默认值即可， 不要修改
    Channel: &ChannelCapabilities
        V1_3: true  # 这里版本号过高的话会报错
    Orderer: &OrdererCapabilities
        V1_1: true
    Application: &ApplicationCapabilities
        V1_3: true
        V1_2: false
        V1_1: false

Application: &ApplicationDefaults       #对应用程序的 ACL 策略
    Organizations:
    Policies:
        Readers:
            Type: ImplicitMeta
            Rule: "ANY Readers"
        Writers:
            Type: ImplicitMeta
            Rule: "ANY Writers"
        Admins:
            Type: ImplicitMeta
            Rule: "MAJORITY Admins"

    Capabilities:
        <<: *ApplicationCapabilities

Orderer: &OrdererDefaults               #对 orderer 节点的配置
    OrdererType: kafka              # 指定共识算法
    Addresses:          #orderer 节点监听的域名和端口
        - orderer0.example.com:7050 # 各个排序节点的域名加端口号，端口号不需要改变
        - orderer1.example.com:7050
        - orderer2.example.com:7050
    BatchTimeout: 2s                # 每隔多长时间打包一个区块
    BatchSize:
        MaxMessageCount: 100        # 单个区块的最大消息数
        AbsoluteMaxBytes: 32 MB     # 单个区块的最大容量，到了自动打包区块
        PreferredMaxBytes: 512 KB   #一个区块最小的容量
    Kafka:                          #对于 kafka 的配置
        Brokers:
            - kafka0:9092           # kafka服务器地址
            - kafka1:9092
            - kafka2:9092
            - kafka3:9092
    Organizations:
    Policies:
        Readers:
            Type: ImplicitMeta
            Rule: "ANY Readers"
        Writers:
            Type: ImplicitMeta
            Rule: "ANY Writers"
        Admins:
            Type: ImplicitMeta
            Rule: "MAJORITY Admins"
        BlockValidation:
            Type: ImplicitMeta
            Rule: "ANY Writers"

Channel: &ChannelDefaults               #对于通道的配置
    Policies:
        Readers:
            Type: ImplicitMeta
            Rule: "ANY Readers"
        Writers:
            Type: ImplicitMeta
            Rule: "ANY Writers"
        Admins:
            Type: ImplicitMeta
            Rule: "MAJORITY Admins"
    Capabilities:
        <<: *ChannelCapabilities

Profiles:                   # 配置集
    TwoOrgsOrdererGenesis:          # 创始块信息关键字, 这个名字可以改, 命令中会被使用
        <<: *ChannelDefaults        # 引用上面为 channelcapabilities 的属性
        Orderer:                    # 配置属性，系统关键字，不能修改
            <<: *OrdererDefaults    # 引用下面为 OrdererOrg 的属性
            Organizations:
                - *OrdererOrg       # 引用下面为 OrdererOrg 的属性（带*号的都是引用）
            Capabilities:
                <<: *OrdererCapabilities
        Consortiums:                # 定义系统中包含的组织
            SampleConsortium:
                Organizations:      # 系统包含的组织，是对上边的引用 TODO：这里不使用org1可不可以？
                    - *Org1
                    - *Org2
    TwoOrgsChannel:                 # 通道信息关键字, 可以改, 会在命令中使用
        Consortium: SampleConsortium
        Application:
            <<: *ApplicationDefaults
            Organizations:
                - *Org1
                - *Org2
```



##### 2.2 执行生成创世块和通道命令

1. 先在当前工作目录创建文件夹`channel-artifacts`，因为这次的命令不会自动创建该目录，找不到会报错

   ```shell
   mkdir channel-artifacts
   ```

2. 生成通道文件

   ```shell
   sudo configtxgen -profile TwoOrgsChannel -outputCreateChannelTx ./channel-artifacts/channel.tx -channelID testchannel
   # 解释：
   -profile 					# 指定采用配置文件中的配置集，对应配置文件中的末尾
   -outputCreateChannelTx		# 指定输出的通道文件
   -channelID					# 指定通道ID
   ```

3. 生成创世块

   ```shell
   sudo configtxgen -profile TwoOrgsOrdererGenesis -outputBlock ./channel-artifacts/genesis.block  # [-channelID testchannel] 是否指定通道ID后边再确认
   ```

   TODO：这里我没有指定通道ID，不知道指定了后会不会影响后续操作

   执行成功后，会发现 channel-artifacts 中生成了 `channel.tx  genesis.block` 两个文件。

4. 因为其他服务器需要同样的配置文件，通道和创世块文件，远程拷贝到各个服务器中

   ```shell
   # IP地址和文件路径根据实际情况更改
   scp -r shalom@192.168.0.211:$HOME/study/fabric/testwork/channel-artifacts ./
   scp -r shalom@192.168.0.211:$HOME/study/fabric/testwork/crypto-config ./
   ```

   TODO：同样的配置文件生成两次，生成的文件会不会不一样？



5. 知识点：configtxgen 命令说明

   命令在执行的时候自动加载一个叫做configtx.yaml的配置文件，默认从当前目录下寻找，也可以通过参数 `-configPath`进行指定，或者通过修改环境变量的值`export FABRIC_CFG_PATH=$(pwd)/networks/config/`

   ```shell
   $ Configtxgen --help
   Usage of ./Configtxgen:
     # 指定所属的组织
     -asOrg string
           Performs the config generation as a particular organization (by name), only 
           including values in the write set that org (likely) has privilege to set
     # 指定创建的channel的名字, 如果没指定系统会提供一个默认的名字.
     -channelID string
           The channel ID to use in the configtx
     # 执行命令要加载的配置文件的路径, 不指定会在当前目录下查找
     -configPath string
           The path containing the configuration to use (if set)
     # 打印指定区块文件中的配置内容，string：查看的区块文件的名字
     -inspectBlock string
           Prints the configuration contained in the block at the specified path
     # 打印创建通道的交易的配置文件
     -inspectChannelCreateTx string
           Prints the configuration contained in the transaction at the specified path
     # 更新channel的配置信息
     -outputAnchorPeersUpdate string
           Creates an config update to update an anchor peer (works only with the default 
           channel creation, and only for the first update)
     # 输出区块文件的路径
     -outputBlock string
           The path to write the genesis block to (if set)
     # 标示输出创始区块文件
     -outputCreateChannelTx string
           The path to write a channel creation configtx to (if set)
     #  将组织的定义打印为JSON(这对在组织中手动添加一个通道很有用)。
     -printOrg string
           Prints the definition of an organization as JSON. (useful for adding an org to
           a channel manually)
     # 指定配置文件中的节点
     -profile string
           The profile from configtx.yaml to use for generation. (default
           "SampleInsecureSolo")
     # 显示版本信息
     -version
           Show version information
   ```

   



#### 3. 服务器01配置文件编写

每个服务节点都要编写配置文件，所以我们要给01编写4个配置文件

##### 3.1 zookeeper配置文件

```yaml
version: '2'

services:
  zookeeper0:  # 服务器名, 自己起
    container_name: zookeeper0  # 容器名, 自己起，尽量和服务器名一样
    hostname: zookeeper0        # 访问的主机名, 自己起, 需要和IP有对应关系
    image: hyperledger/fabric-zookeeper:latest
    restart: always    # 指定docker的重启策略
    environment:
      - ZOO_MY_ID=1  # ID在集合中必须是唯一的并且应该有值，在1和255之间。
      - ZOO_SERVERS=server.1=zookeeper0:2888:3888 server.2=zookeeper1:2888:3888 server.3=zookeeper2:2888:3888   # 组成zookeeper集群的服务器列表
    ports:
      - 2181:2181
      - 2888:2888
      - 3888:3888
    extra_hosts:    # 设置 服务器名 和其指向的IP地址的对应关系
      - "zookeeper0:192.168.147.131"
      - "zookeeper1:192.168.147.133"
      - "zookeeper2:192.168.147.130"
      - "kafka0:192.168.147.131"
      - "kafka1:192.168.147.133"
      - "kafka2:192.168.147.130"
      - "kafka3:192.168.147.132"
```

###### 需要具备的知识

1. docker 的`restart`策略
   - no – 容器退出时不要自动重启，这个是默认值。
   - on-failure[:max-retries] – 只在容器以非0状态码退出时重启， 例如：`on-failure:10 `
   - **always** – 不管退出状态码是什么始终重启容器
   - unless-stopped– 不管退出状态码是什么始终重启容器，不过当daemon启动时，如果容器之前已经为停止状态，不要尝试启动它。

2. ZOO_SERVERS

   - 组成zookeeper集群的服务器列表
   - 列表中每个服务器的值都附带两个端口号
     - <font color="red">第一个: 追随者用来连接 Leader 使用的</font>
     - <font color="red">第二个: 用户选举 Leader</font>

3. zookeeper服务器中的三个重要端口：

   - 访问zookeeper的端口: 2181
   - zookeeper集群中追随者连接 Leader 的端口: 2888
   - zookeeper集群中选举 Leader 的端口: 3888

4. 集群数量

   Zookeeper 集群的数量可以是 `3, 5, 7,` 它值需要是一个奇数以避免脑裂问题（split-brain）的情况。同时选择大于1的值是为了避免单点故障，如果集群的数量超过7个Zookeeper服务将会无法承受。

##### 3.2 kafka配置文件

```yaml
version: '2'

services:
  kafka0:
    container_name: kafka0
    hostname: kafka0
    image: hyperledger/fabric-kafka:latest
    restart: always
    environment:
      - KAFKA_MESSAGE_MAX_BYTES=103809024 # 消息的最大字节数，和配置文件`configtx.yaml`中的`Orderer.BatchSize.AbsoluteMaxBytes`对应（并不是相等关系）  99 * 1024 * 1024 B 这里是99MB
      - KAFKA_REPLICA_FETCH_MAX_BYTES=103809024 # 99 * 1024 * 1024 B  每个channel获取的消息的最大字节数
      - KAFKA_UNCLEAN_LEADER_ELECTION_ENABLE=false # 非一致性的 Leader 选举,一般为false
      - KAFKA_BROKER_ID=1  # 是一个唯一的非负整数, 可以作为代理`Broker`的名字
      - KAFKA_MIN_INSYNC_REPLICAS=2  # 最小同步备份数量，要小于下边这个变量的值
      - KAFKA_DEFAULT_REPLICATION_FACTOR=3  # 默认同步备份数量, 该值要小于kafka集群数量
      - KAFKA_ZOOKEEPER_CONNECT=zookeeper0:2181,zookeeper1:2181,zookeeper2:2181 # zookeeper节点的集合
    ports:
      - 9092:9092   # Kafka 默认端口为: 9092
    extra_hosts:
      - "zookeeper0:192.168.147.131"
      - "zookeeper1:192.168.147.133"
      - "zookeeper2:192.168.147.130"
      - "kafka0:192.168.147.131"
      - "kafka1:192.168.147.133"
      - "kafka2:192.168.147.130"
      - "kafka3:192.168.147.132"
```

###### 需要具备的知识

1. 三个变量之间的关系

   - AbsoluteMaxBytes：每个区块的最大容量
   - KAFKA_REPLICA_FETCH_MAX_BYTES：每个channel获取的消息的最大字节数
   - KAFKA_MESSAGE_MAX_BYTES：消息的最大字节数

   `AbsoluteMaxBytes  ` < `KAFKA_REPLICA_FETCH_MAX_BYTES` <= `KAFKA_MESSAGE_MAX_BYTES`



##### 3.3 order节点配置文件

```yaml
version: '2'

services:
  orderer0.example.com:
    container_name: orderer0.example.com
    image: hyperledger/fabric-orderer:latest
    environment:
      - FABRIC_LOGGING_SPEC=INFO
      - ORDERER_GENERAL_LISTENADDRESS=0.0.0.0    # Orderer服务器监听的地址
      - ORDERER_GENERAL_GENESISMETHOD=file       # 初始化块(Genesis)的来源方式, 一般赋值为 file 即可
      - ORDERER_GENERAL_GENESISFILE=/var/hyperledger/orderer/orderer.genesis.block   # 存储创世块文件的路径
      - ORDERER_GENERAL_LOCALMSPID=OrdererMSP    # Orderer节点的编号,在configtx.yaml模块配置文件中指定的
      - ORDERER_GENERAL_LOCALMSPDIR=/var/hyperledger/orderer/msp    # Orderer节点msp文件路径
      # enabled TLS
      - ORDERER_GENERAL_TLS_ENABLED=true  # 是否启用TLS, true/false
      - ORDERER_GENERAL_TLS_PRIVATEKEY=/var/hyperledger/orderer/tls/server.key
      - ORDERER_GENERAL_TLS_CERTIFICATE=/var/hyperledger/orderer/tls/server.crt
      - ORDERER_GENERAL_TLS_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt] # 根证书
      - ORDERER_KAFKA_RETRY_LONGINTERVAL=10s # 每隔多长时间进行一次重试, 单位:秒
      - ORDERER_KAFKA_RETRY_LONGTOTAL=100s   # 总共重试的时长, 单位: 秒
      - ORDERER_KAFKA_RETRY_SHORTINTERVAL=1s # 每隔多长时间进行一次重试, 单位:秒
      - ORDERER_KAFKA_RETRY_SHORTTOTAL=30s   # 总共重试的时长, 单位: 秒 
      - ORDERER_KAFKA_VERBOSE=true # 启用日志与kafka进行交互, 启用: true, 不启用: false
      - ORDERER_KAFKA_BROKERS=[192.168.147.131:9092,192.168.147.133:9092,192.168.147.130:9092,192.168.147.132:9092]  # kafka节点的集合
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric
    command: orderer
    volumes:
      - ./channel-artifacts/genesis.block:/var/hyperledger/orderer/orderer.genesis.block
      - ./crypto-config/ordererOrganizations/example.com/orderers/orderer0.example.com/msp:/var/hyperledger/orderer/msp
      - ./crypto-config/ordererOrganizations/example.com/orderers/orderer0.example.com/tls/:/var/hyperledger/orderer/tls
    ports:
      - 7050:7050
    extra_hosts:
      - "kafka0:192.168.147.131"
      - "kafka1:192.168.147.133"
      - "kafka2:192.168.147.130"
      - "kafka3:192.168.147.132"
```

###### 需要具备的知识

SHORTINTERVAL 和 LONGINTERVAL 的联系

- 先使用`ORDERER_KAFKA_RETRY_SHORTINTERVAL`进行重连, 重连的总时长为`ORDERER_KAFKA_RETRY_SHORTTOTAL`
- 如果上述步骤没有重连成功, 使用`ORDERER_KAFKA_RETRY_LONGINTERVAL`进行重连, 重连的总时长为`ORDERER_KAFKA_RETRY_LONGTOTAL`



##### 3.4 peer节点的配置文件

```yaml
version: '2'

services:
  peer0.org1.example.com:
    container_name: peer0.org1.example.com
    hostname: peer0.org1.example.com
    image: hyperledger/fabric-peer:latest
    environment:
      - CORE_PEER_ID=peer0.org1.example.com
      - CORE_PEER_ADDRESS=peer0.org1.example.com:7051
      - CORE_PEER_CHAINCODEADDRESS=peer0.org1.example.com:7052
      # - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:7052
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer0.org1.example.com:7051
      - CORE_PEER_LOCALMSPID=Org1MSP
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=testwork_default
      # the following setting starts chaincode containers on the same
      # bridge network as the peers
      # https://docs.docker.com/compose/networking/
      - FABRIC_LOGGING_SPEC=INFO
      #- FABRIC_LOGGING_SPEC=DEBUG
      - CORE_PEER_GOSSIP_USELEADERELECTION=true
      - CORE_PEER_GOSSIP_ORGLEADER=false
      - CORE_PEER_PROFILE_ENABLED=true
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: peer node start
    volumes:
      - /var/run/:/host/var/run/
      - ./crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/msp:/etc/hyperledger/fabric/msp
      - ./crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls:/etc/hyperledger/fabric/tls
    ports:
      - 7051:7051
      - 7052:7052
      - 7053:7053
    extra_hosts:
      - "orderer0.example.com:192.168.147.131"
      - "orderer1.example.com:192.168.147.133"
      - "orderer2.example.com:192.168.147.130"
  cli:
    container_name: cli
    image: hyperledger/fabric-tools:latest
    tty: true
    environment:
      - GOPATH=/opt/gopath
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_ID=cli
      - CORE_PEER_ADDRESS=peer0.org1.example.com:7051
      - CORE_PEER_LOCALMSPID=Org1MSP
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
      - CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: /bin/bash
    volumes:
      - /var/run/:/host/var/run/
      - ./chaincode/:/opt/gopath/src/github.com/chaincode
      - ./crypto-config:/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/
      - ./channel-artifacts:/opt/gopath/src/github.com/hyperledger/fabric/peer/channel-artifacts
    depends_on:
      - peer0.org1.example.com
    extra_hosts:
      - "orderer0.example.com:192.168.147.131"
      - "orderer1.example.com:192.168.147.133"
      - "orderer2.example.com:192.168.147.130"
      - "peer0.org1.example.com:192.168.147.131"
      - "peer1.org1.example.com:192.168.147.133"
      - "peer0.org2.example.com:192.168.147.130"
      - "peer1.org2.example.com:192.168.147.132"
```



#### 4. 服务器02配置文件编写

##### 4.1 zookeeper配置文件

```yaml
version: '2'

services:
  zookeeper1:
    container_name: zookeeper1
    hostname: zookeeper1
    image: hyperledger/fabric-zookeeper:latest
    restart: always
    environment:
      - ZOO_MY_ID=2
      - ZOO_SERVERS=server.1=zookeeper0:2888:3888 server.2=zookeeper1:2888:3888 server.3=zookeeper2:2888:3888
    ports:
      - 2181:2181
      - 2888:2888
      - 3888:3888
    extra_hosts:
      - "zookeeper0:192.168.147.131"
      - "zookeeper1:192.168.147.133"
      - "zookeeper2:192.168.147.130"
      - "kafka0:192.168.147.131"
      - "kafka1:192.168.147.133"
      - "kafka2:192.168.147.130"
      - "kafka3:192.168.147.132"
```



##### 4.2 kafka配置文件

```yaml
version: '2'

services:
  kafka1:
    container_name: kafka1
    hostname: kafka1
    image: hyperledger/fabric-kafka:latest
    restart: always
    environment:
      - KAFKA_MESSAGE_MAX_BYTES=103809024 # 99 * 1024 * 1024 B
      - KAFKA_REPLICA_FETCH_MAX_BYTES=103809024 # 99 * 1024 * 1024 B
      - KAFKA_UNCLEAN_LEADER_ELECTION_ENABLE=false
      - KAFKA_BROKER_ID=2
      - KAFKA_MIN_INSYNC_REPLICAS=2
      - KAFKA_DEFAULT_REPLICATION_FACTOR=3
      - KAFKA_ZOOKEEPER_CONNECT=zookeeper0:2181,zookeeper1:2181,zookeeper2:2181
    ports:
      - 9092:9092
    extra_hosts:
      - "zookeeper0:192.168.147.131"
      - "zookeeper1:192.168.147.133"
      - "zookeeper2:192.168.147.130"
      - "kafka0:192.168.147.131"
      - "kafka1:192.168.147.133"
      - "kafka2:192.168.147.130"
      - "kafka3:192.168.147.132"
```



##### 4.3 order节点配置文件

```yaml
version: '2'

services:
  orderer1.example.com:
    container_name: orderer1.example.com
    image: hyperledger/fabric-orderer:latest
    environment:
      - FABRIC_LOGGING_SPEC=INFO
      - ORDERER_GENERAL_LISTENADDRESS=0.0.0.0
      - ORDERER_GENERAL_GENESISMETHOD=file
      - ORDERER_GENERAL_GENESISFILE=/var/hyperledger/orderer/orderer.genesis.block
      - ORDERER_GENERAL_LOCALMSPID=OrdererMSP
      - ORDERER_GENERAL_LOCALMSPDIR=/var/hyperledger/orderer/msp
      # enabled TLS
      - ORDERER_GENERAL_TLS_ENABLED=true
      - ORDERER_GENERAL_TLS_PRIVATEKEY=/var/hyperledger/orderer/tls/server.key
      - ORDERER_GENERAL_TLS_CERTIFICATE=/var/hyperledger/orderer/tls/server.crt
      - ORDERER_GENERAL_TLS_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]
      - ORDERER_KAFKA_RETRY_LONGINTERVAL=10s
      - ORDERER_KAFKA_RETRY_LONGTOTAL=100s
      - ORDERER_KAFKA_RETRY_SHORTINTERVAL=1s
      - ORDERER_KAFKA_RETRY_SHORTTOTAL=30s
      - ORDERER_KAFKA_VERBOSE=true
      - ORDERER_KAFKA_BROKERS=[192.168.147.131:9092,192.168.147.133:9092,192.168.147.130:9092,192.168.147.132:9092]
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric
    command: orderer
    volumes:
      - ./channel-artifacts/genesis.block:/var/hyperledger/orderer/orderer.genesis.block
      - ./crypto-config/ordererOrganizations/example.com/orderers/orderer1.example.com/msp:/var/hyperledger/orderer/msp
      - ./crypto-config/ordererOrganizations/example.com/orderers/orderer1.example.com/tls/:/var/hyperledger/orderer/tls
    ports:
      - 7050:7050
    extra_hosts:
      - "kafka0:192.168.147.131"
      - "kafka1:192.168.147.133"
      - "kafka2:192.168.147.130"
      - "kafka3:192.168.147.132"
```



##### 4.4 peer节点的配置文件

```yaml
version: '2'

services:
  peer1.org1.example.com:
    container_name: peer1.org1.example.com
    hostname: peer1.org1.example.com
    image: hyperledger/fabric-peer:latest
    environment:
      - CORE_PEER_ID=peer1.org1.example.com
      - CORE_PEER_ADDRESS=peer1.org1.example.com:7051
      - CORE_PEER_CHAINCODEADDRESS=peer1.org1.example.com:7052
      # - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:7052
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer1.org1.example.com:7051
      - CORE_PEER_LOCALMSPID=Org1MSP
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=testwork_default
      # the following setting starts chaincode containers on the same
      # bridge network as the peers
      # https://docs.docker.com/compose/networking/
      - FABRIC_LOGGING_SPEC=INFO
      #- FABRIC_LOGGING_SPEC=DEBUG
      - CORE_PEER_GOSSIP_USELEADERELECTION=true
      - CORE_PEER_GOSSIP_ORGLEADER=false
      - CORE_PEER_PROFILE_ENABLED=true
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: peer node start
    volumes:
      - /var/run/:/host/var/run/
      - ./crypto-config/peerOrganizations/org1.example.com/peers/peer1.org1.example.com/msp:/etc/hyperledger/fabric/msp
      - ./crypto-config/peerOrganizations/org1.example.com/peers/peer1.org1.example.com/tls:/etc/hyperledger/fabric/tls
    ports:
      - 7051:7051
      - 7052:7052
      - 7053:7053
    extra_hosts:
      - "orderer0.example.com:192.168.147.131"
      - "orderer1.example.com:192.168.147.133"
      - "orderer2.example.com:192.168.147.132"
  cli:
    container_name: cli
    image: hyperledger/fabric-tools:latest
    tty: true
    environment:
      - GOPATH=/opt/gopath
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_ID=cli
      - CORE_PEER_ADDRESS=peer1.org1.example.com:7051
      - CORE_PEER_LOCALMSPID=Org1MSP
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer1.org1.example.com/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer1.org1.example.com/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer1.org1.example.com/tls/ca.crt
      - CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: /bin/bash
    volumes:
      - /var/run/:/host/var/run/
      - ./chaincode/:/opt/gopath/src/github.com/chaincode
      - ./crypto-config:/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/
      - ./channel-artifacts:/opt/gopath/src/github.com/hyperledger/fabric/peer/channel-artifacts
    depends_on:
      - peer1.org1.example.com
    extra_hosts:
      - "orderer0.example.com:192.168.147.131"
      - "orderer1.example.com:192.168.147.133"
      - "orderer2.example.com:192.168.147.132"
      - "peer0.org1.example.com:192.168.147.131"
      - "peer1.org1.example.com:192.168.147.133"
      - "peer0.org2.example.com:192.168.147.132"
      - "peer1.org2.example.com:192.168.147.130"
```





#### 5. 服务器03配置文件编写

##### 5.1 zookeeper配置文件

```yaml
version: '2'

services:
  zookeeper2:
    container_name: zookeeper2
    hostname: zookeeper2
    image: hyperledger/fabric-zookeeper:latest
    restart: always
    environment:
      - ZOO_MY_ID=3
      - ZOO_SERVERS=server.1=zookeeper0:2888:3888 server.2=zookeeper1:2888:3888 server.3=zookeeper2:2888:3888
    ports:
      - 2181:2181
      - 2888:2888
      - 3888:3888
    extra_hosts:
      - "zookeeper0:192.168.147.131"
      - "zookeeper1:192.168.147.133"
      - "zookeeper2:192.168.147.130"
      - "kafka0:192.168.147.131"
      - "kafka1:192.168.147.133"
      - "kafka2:192.168.147.130"
      - "kafka3:192.168.147.132"
```



##### 5.2 kafka配置文件

```yaml
version: '2'

services:
  kafka2:
    container_name: kafka2
    hostname: kafka2
    image: hyperledger/fabric-kafka:latest
    restart: always
    environment:
      - KAFKA_MESSAGE_MAX_BYTES=103809024 # 99 * 1024 * 1024 B
      - KAFKA_REPLICA_FETCH_MAX_BYTES=103809024 # 99 * 1024 * 1024 B
      - KAFKA_UNCLEAN_LEADER_ELECTION_ENABLE=false
      - KAFKA_BROKER_ID=3
      - KAFKA_MIN_INSYNC_REPLICAS=2
      - KAFKA_DEFAULT_REPLICATION_FACTOR=3
      - KAFKA_ZOOKEEPER_CONNECT=zookeeper0:2181,zookeeper1:2181,zookeeper2:2181
    ports:
      - 9092:9092
    extra_hosts:
      - "zookeeper0:192.168.147.131"
      - "zookeeper1:192.168.147.133"
      - "zookeeper2:192.168.147.130"
      - "kafka0:192.168.147.131"
      - "kafka1:192.168.147.133"
      - "kafka2:192.168.147.130"
      - "kafka3:192.168.147.132"
```



##### 5.3 order节点配置文件

```yaml
version: '2'

services:
  orderer2.example.com:
    container_name: orderer2.example.com
    image: hyperledger/fabric-orderer:latest
    environment:
      - FABRIC_LOGGING_SPEC=INFO
      - ORDERER_GENERAL_LISTENADDRESS=0.0.0.0
      - ORDERER_GENERAL_GENESISMETHOD=file
      - ORDERER_GENERAL_GENESISFILE=/var/hyperledger/orderer/orderer.genesis.block
      - ORDERER_GENERAL_LOCALMSPID=OrdererMSP
      - ORDERER_GENERAL_LOCALMSPDIR=/var/hyperledger/orderer/msp
      # enabled TLS
      - ORDERER_GENERAL_TLS_ENABLED=true
      - ORDERER_GENERAL_TLS_PRIVATEKEY=/var/hyperledger/orderer/tls/server.key
      - ORDERER_GENERAL_TLS_CERTIFICATE=/var/hyperledger/orderer/tls/server.crt
      - ORDERER_GENERAL_TLS_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]
      - ORDERER_KAFKA_RETRY_LONGINTERVAL=10s
      - ORDERER_KAFKA_RETRY_LONGTOTAL=100s
      - ORDERER_KAFKA_RETRY_SHORTINTERVAL=1s
      - ORDERER_KAFKA_RETRY_SHORTTOTAL=30s
      - ORDERER_KAFKA_VERBOSE=true
      - ORDERER_KAFKA_BROKERS=[192.168.147.131:9092,192.168.147.133:9092,192.168.147.130:9092,192.168.147.132:9092]
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric
    command: orderer
    volumes:
      - ./channel-artifacts/genesis.block:/var/hyperledger/orderer/orderer.genesis.block
      - ./crypto-config/ordererOrganizations/example.com/orderers/orderer2.example.com/msp:/var/hyperledger/orderer/msp
      - ./crypto-config/ordererOrganizations/example.com/orderers/orderer2.example.com/tls/:/var/hyperledger/orderer/tls
    ports:
      - 7050:7050
    extra_hosts:
      - "kafka0:192.168.147.131"
      - "kafka1:192.168.147.133"
      - "kafka2:192.168.147.130"
      - "kafka3:192.168.147.132"
```



##### 5.4 peer节点的配置文件

```yaml
version: '2'

services:
  peer0.org2.example.com:
    container_name: peer0.org2.example.com
    hostname: peer0.org2.example.com
    image: hyperledger/fabric-peer:latest
    environment:
      - CORE_PEER_ID=peer0.org2.example.com
      - CORE_PEER_ADDRESS=peer0.org2.example.com:7051
      - CORE_PEER_CHAINCODEADDRESS=peer0.org2.example.com:7052
      # - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:7052
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer0.org2.example.com:7051
      - CORE_PEER_LOCALMSPID=Org2MSP
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=testwork_default
      # the following setting starts chaincode containers on the same
      # bridge network as the peers
      # https://docs.docker.com/compose/networking/
      - FABRIC_LOGGING_SPEC=INFO
      #- FABRIC_LOGGING_SPEC=DEBUG
      - CORE_PEER_GOSSIP_USELEADERELECTION=true
      - CORE_PEER_GOSSIP_ORGLEADER=false
      - CORE_PEER_PROFILE_ENABLED=true
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: peer node start
    volumes:
      - /var/run/:/host/var/run/
      - ./crypto-config/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/msp:/etc/hyperledger/fabric/msp
      - ./crypto-config/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls:/etc/hyperledger/fabric/tls
    ports:
      - 7051:7051
      - 7052:7052
      - 7053:7053
    extra_hosts:
      - "orderer0.example.com:192.168.147.131"
      - "orderer1.example.com:192.168.147.133"
      - "orderer2.example.com:192.168.147.130"
  cli:
    container_name: cli
    image: hyperledger/fabric-tools:latest
    tty: true
    environment:
      - GOPATH=/opt/gopath
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_ID=cli
      - CORE_PEER_ADDRESS=peer0.org2.example.com:7051
      - CORE_PEER_LOCALMSPID=Org2MSP
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
      - CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: /bin/bash
    volumes:
      - /var/run/:/host/var/run/
      - ./chaincode/:/opt/gopath/src/github.com/chaincode
      - ./crypto-config:/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/
      - ./channel-artifacts:/opt/gopath/src/github.com/hyperledger/fabric/peer/channel-artifacts
    depends_on:
      - peer0.org2.example.com
    extra_hosts:
      - "orderer0.example.com:192.168.147.131"
      - "orderer1.example.com:192.168.147.133"
      - "orderer2.example.com:192.168.147.132"
      - "peer0.org1.example.com:192.168.147.131"
      - "peer1.org1.example.com:192.168.147.133"
      - "peer0.org2.example.com:192.168.147.130"
      - "peer1.org2.example.com:192.168.147.132"
```



#### 6. 服务器04配置文件编写

##### 6.1 kafka配置文件

```yaml
version: '2'

services:
  kafka3:
    container_name: kafka3
    hostname: kafka3
    image: hyperledger/fabric-kafka:latest
    restart: always
    environment:
      - KAFKA_MESSAGE_MAX_BYTES=103809024 # 99 * 1024 * 1024 B
      - KAFKA_REPLICA_FETCH_MAX_BYTES=103809024 # 99 * 1024 * 1024 B
      - KAFKA_UNCLEAN_LEADER_ELECTION_ENABLE=false
      - KAFKA_BROKER_ID=4
      - KAFKA_MIN_INSYNC_REPLICAS=2
      - KAFKA_DEFAULT_REPLICATION_FACTOR=3
      - KAFKA_ZOOKEEPER_CONNECT=zookeeper0:2181,zookeeper1:2181,zookeeper2:2181
    ports:
      - 9092:9092
    extra_hosts:
      - "zookeeper0:192.168.147.131"
      - "zookeeper1:192.168.147.133"
      - "zookeeper2:192.168.147.130"
      - "kafka0:192.168.147.131"
      - "kafka1:192.168.147.133"
      - "kafka2:192.168.147.130"
      - "kafka3:192.168.147.132"
```



##### 6.2 order节点配置文件

```yaml
version: '2'

services:
  peer1.org2.example.com:
    container_name: peer1.org2.example.com
    hostname: peer1.org2.example.com
    image: hyperledger/fabric-peer:latest
    environment:
      - CORE_PEER_ID=peer1.org2.example.com
      - CORE_PEER_ADDRESS=peer1.org2.example.com:7051
      - CORE_PEER_CHAINCODEADDRESS=peer1.org2.example.com:7052
      # - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:7052
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer1.org2.example.com:7051
      - CORE_PEER_LOCALMSPID=Org2MSP
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=testwork_default
      # the following setting starts chaincode containers on the same
      # bridge network as the peers
      # https://docs.docker.com/compose/networking/
      - FABRIC_LOGGING_SPEC=INFO
      #- FABRIC_LOGGING_SPEC=DEBUG
      - CORE_PEER_GOSSIP_USELEADERELECTION=true
      - CORE_PEER_GOSSIP_ORGLEADER=false
      - CORE_PEER_PROFILE_ENABLED=true
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: peer node start
    volumes:
      - /var/run/:/host/var/run/
      - ./crypto-config/peerOrganizations/org2.example.com/peers/peer1.org2.example.com/msp:/etc/hyperledger/fabric/msp
      - ./crypto-config/peerOrganizations/org2.example.com/peers/peer1.org2.example.com/tls:/etc/hyperledger/fabric/tls
    ports:
      - 7051:7051
      - 7052:7052
      - 7053:7053
    extra_hosts:
      - "orderer0.example.com:192.168.147.131"
      - "orderer1.example.com:192.168.147.133"
      - "orderer2.example.com:192.168.147.130"
  cli:
    container_name: cli
    image: hyperledger/fabric-tools:latest
    tty: true
    environment:
      - GOPATH=/opt/gopath
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_ID=cli
      - CORE_PEER_ADDRESS=peer1.org2.example.com:7051
      - CORE_PEER_LOCALMSPID=Org2MSP
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer1.org2.example.com/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer1.org2.example.com/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer1.org2.example.com/tls/ca.crt
      - CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: /bin/bash
    volumes:
      - /var/run/:/host/var/run/
      - ./chaincode/:/opt/gopath/src/github.com/chaincode
      - ./crypto-config:/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/
      - ./channel-artifacts:/opt/gopath/src/github.com/hyperledger/fabric/peer/channel-artifacts
    depends_on:
      - peer1.org2.example.com
    extra_hosts:
      - "orderer0.example.com:192.168.147.131"
      - "orderer1.example.com:192.168.147.133"
      - "orderer2.example.com:192.168.147.130"
      - "peer0.org1.example.com:192.168.147.131"
      - "peer1.org1.example.com:192.168.147.133"
      - "peer0.org2.example.com:192.168.147.130"
      - "peer1.org2.example.com:192.168.147.132"
```



#### 7. 给所有服务器编辑 `/etc/hosts`文件

`/etc/hosts`的作用是：服务器会先从`/etc/hosts`中看有没有IP地址和域名的映射关系，若有就直接取来用；若没有，再通过域名解析器解析。

我们这里域名不一定和当前服务器IP绑定，所以需要自定义域名和IP的映射。

在 `/etc/hosts` 下方添加如下内容：

```shell
192.168.0.211  peer0.org1.example.com
192.168.0.212  peer1.org1.example.com
192.168.0.213  peer0.org2.example.com
192.168.0.214  peer1.org2.example.com

192.168.0.211  orderer0.example.com
192.168.0.212  orderer1.example.com
192.168.0.213  orderer2.example.com

192.168.0.211  zookeeper0
192.168.0.212  zookeeper1
192.168.0.213  zookeeper2

192.168.0.211  kafka0
192.168.0.212  kafka1
192.168.0.213  kafka2
192.168.0.214  kafka3
```

**注意：**所有参与的服务器都要配置



#### 8. 服务器01拷贝示例链码

```shell
mkdir chaincode

cp $HOME/hyperledger-fabric/fabric-samples/chaincode/chaincode_example02/go/chaincode_example02.go ./chaincode
```



#### 9. 启动各服务器的节点

##### 9.1 依次启动zookeeper服务

这里只有3台服务器有zookeeper服务

```shell
docker-compose -f ./docker-compose-zookeeper.yaml up -d
```

##### 9.2 依次启动kafka服务

```shell
docker-compose -f ./docker-compose-kafka.yaml up -d
```

##### 9.3 依次启动order节点服务

这里只有3台服务器有order节点服务

```shell
docker-compose -f ./docker-compose-orderer.yaml up -d
```

##### 9.4 依次启动peer节点服务

```shell
docker-compose -f ./docker-compose-peer.yaml up -d
```



#### 10. 将清除脚本准备好

因为测试完或者失败后，一定要将所有docker相关的文件清除，才可以进行下一次的操作，不然一定会失败；所以这里提前准备好，以备后用

```shell
#!/bin/bash
docker-compose -f ./docker-compose-peer.yaml down -v
docker-compose -f ./docker-compose-orderer.yaml down -v
docker-compose -f ./docker-compose-kafka.yaml down -v
docker-compose -f ./docker-compose-zookeeper.yaml down -v

# docker ps -aq : 列出所有容器的ID ； 强制删除容器和关联卷
docker rm -vf $(docker ps -aq)	

# docker images | grep dev-peer ：将docker images 输出的包含dev-peer的字段
# awk '{print $3}' $fileName :   一行一行的读取指定的文件， 以空格作为分隔符，打印第三个字段
docker rmi --force `docker images | grep dev-peer | awk '{print $3}'`
```

添加脚本执行权限

```shell
chmod +x down.sh
```



#### 11. 创建并加入通道，安装链码

##### 11.1 服务器01的操作

- 进入客户端容器

  ```shell
  docker exec -it cli bash
  ```

- 创建通道

  ```shell
  peer channel create -o orderer0.example.com:7050 -c testchannel -f ./channel-artifacts/channel.tx --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/msp/tlscacerts/tlsca.example.com-cert.pem
  
  # 命令说明：
  	`-o, --orderer: orderer节点的地址
  	`-c, --channelID: 要创建的通道的ID, 必须小写, 在250个字符以内
  	`-f, --file: 由configtxgen 生成的通道文件, 用于提交给orderer
  	`-t, --timeout: 创建通道的超时时长, 默认为5s
  	`--tls: 通信时是否使用tls加密
  	`--cafile: 当前orderer节点pem格式的tls证书文件, 要使用绝对路径.
  ```

  - 这步遇到的坑：连接失败

    原因：各个虚拟机启动的顺序错误

  - 执行成功后，文件夹 channel-artifacts 下会生成 testchannel.block 文件

- 加入通道

  ```shell
  peer channel join -b testchannel.block
  	`-b, --blockpath: 指定通过 peer channel create 命令生成的通道文件 
  ```

- 安装链码

  ```shell
  peer chaincode install -n mycc -v 1.0 -l golang -p github.com/chaincode
  
  # 命令说明：
  	`-c, --ctor: JSON格式的构造参数, 默认是"{}"
  	`-l, --lang: 编写chaincode的编程语言, 默认值是 golang
  	`-n, --name: chaincode的名字
  	`-p, --path: chaincode源代码的目录, 从 $GOPATH/src 路径后开始写
  	`-v, --version: 当前操作的chaincode的版本, 这些命令install/instantiate/upgrade都适用
  ```

- 链码打包

  ```shell
  peer chaincode package -p github.com/chaincode -n mycc -v 1.0 mycc.1.0.out
  ```

- 将 testchannel.block 和 mycc.1.0.out 从 cli 容器拷贝到宿主机,操作要在宿主机中进行

  ```shell
  exit
  docker cp cli:/opt/gopath/src/github.com/hyperledger/fabric/peer/testchannel.block ./channel-artifacts
  docker cp cli:/opt/gopath/src/github.com/hyperledger/fabric/peer/mycc.1.0.out ./channel-artifacts
  ```



##### 11.2 服务器02的操作

- 远程目录 channel-artifacts 拷贝到本地，因为里边有新生成的 通道区块 和 打包好的链码

  ```shell
  scp -r shalom@192.168.0.211:$HOME/study/fabric/testwork/channel-artifacts ./
  ```

- 通过docker客户端来操作peer节点

  ```shell
  docker exec -it cli bash
  ```

- 将 testchannel.block 和 mycc.1.0.out 从 channel-artifacts 目录移出

  ```shell
  mv ./channel-artifacts/testchannel.block /opt/gopath/src/github.com/hyperledger/fabric/peer
  mv ./channel-artifacts/mycc.1.0.out /opt/gopath/src/github.com/hyperledger/fabric/peer
  ```

- 加入通道

  ```shell
  peer channel join -b testchannel.block
  ```

- 安装链码

  ```shell
  peer chaincode install mycc.1.0.out
  ```

  

##### 11.3 服务器03的操作

- 远程目录 channel-artifacts 拷贝到本地，因为里边有新生成的 通道区块 和 打包的链码

  ```shell
  scp -r shalom@192.168.0.211:$HOME/study/fabric/testwork/channel-artifacts ./
  ```

- 通过docker客户端来操作peer节点

  ```shell
  docker exec -it cli bash
  ```

- 将 testchannel.block 和 mycc.1.0.out 从 channel-artifacts 目录移出

  ```shell
  mv ./channel-artifacts/testchannel.block /opt/gopath/src/github.com/hyperledger/fabric/peer
  mv ./channel-artifacts/mycc.1.0.out /opt/gopath/src/github.com/hyperledger/fabric/peer
  ```

- 加入通道

  ```shell
  peer channel join -b testchannel.block
  ```

- 安装链码

  ```shell
  peer chaincode install mycc.1.0.out
  ```

  

##### 11.4 服务器04的操作

- 远程目录 channel-artifacts 拷贝到本地，因为里边有新生成的 通道区块 和 打包的链码

  ```shell
  scp -r shalom@192.168.0.211:$HOME/study/fabric/testwork/channel-artifacts ./
  ```

- 通过docker客户端来操作peer节点

  ```shell
  docker exec -it cli bash
  ```

- 将 testchannel.block 和 mycc.1.0.out 从 channel-artifacts 目录移出

  ```shell
  mv ./channel-artifacts/testchannel.block /opt/gopath/src/github.com/hyperledger/fabric/peer
  mv ./channel-artifacts/mycc.1.0.out /opt/gopath/src/github.com/hyperledger/fabric/peer
  ```

- 加入通道

  ```shell
  peer channel join -b testchannel.block
  ```

- 安装链码

  ```shell
  peer chaincode install mycc.1.0.out
  ```



#### 12. 链码初始化

对网络内一个节点执行，就会在全网内同步。这里只在服务器01上的peer节点执行

```shell
peer chaincode instantiate -o orderer0.example.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C testchannel -n mycc -l golang -v 1.0 -c '{"Args":["init","a","100","b","200"]}' -P "OR ('Org1MSP.member','Org2MSP.member')"

# 命令说明：
	`-o, --orderer: orderer节点的地址
	`-C，--channelID：当前命令运行的通道，默认值是“testchainid"。
	`-c, --ctor：JSON格式的构	造参数，默认值是“{}"
	`-l，--lang：编写Chaincode的编程语言，默认值是golang
	`-n，--name：Chaincode的名字。
	`-P，--policy：当前Chaincode的背书策略。
	`-v，--version：当前操作的Chaincode的版本，适用于install/instantiate/upgrade等命令
	`--tls: 通信时是否使用tls加密
	`--cafile: 当前orderer节点pem格式的tls证书文件, 要使用绝对路径.
```



#### 13. 执行交易并查询数据

##### 13.1 服务器01和02查询数据

```shell
docker exec -it cli bash
# 查询语句
peer chaincode query -C testchannel -n mycc -c '{"Args":["query","a"]}'

# 命令说明：
	`-n，--name：Chaincode的名字。
	`-C，--channelID：当前命令运行的通道，默认值是“testchainid"
	`-c, --ctor：JSON格式的构造参数，默认值是“{}"
	`-x，--hex：是否对输出的内容进行编码处理
	`-r，--raw：是否输出二进制内容
	`-t, --tid: 指定当前查询的编号
```

两台服务器命令一样

##### 13.2 服务器03执行交易

```shell
peer chaincode invoke -o orderer0.example.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C testchannel -n mycc --peerAddresses peer0.org1.example.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses peer0.org2.example.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '{"Args":["invoke","a","b","10"]}'

# 命令说明：
	`-o, --orderer: orderer节点的地址
	`-C，--channelID：当前命令运行的通道，默认值是“testchainid"
	`-c, --ctor：JSON格式的构造参数，默认值是“{}"
	`-n，--name：Chaincode的名字
	`--tls: 通信时是否使用tls加密
	`--cafile: 当前orderer节点pem格式的tls证书文件, 要使用绝对路径.
	`--peerAddresses: 指定要连接的peer节点的地址
	`--tlsRootCertFiles: 连接的peer节点的TLS根证书
```



##### 13.3 服务器04查询数据

```shell
peer chaincode query -C testchannel -n mycc -c '{"Args":["query","a"]}'
```



#### zookeeper和kafka

##### zookeeper













#### bug汇总

- 启动顺序错误，一定要先将zookeeper，kafka启动好，再启动order和peer



#### 更新锚节点

TODO: 这里先记着，后边理解了为什么要更新锚节点后，再整合到上边笔记中

```shell
$ peer channel update [flags], 常用参数为:
	`-o, --orderer: orderer节点的地址
	`-c, --channelID: 要创建的通道的ID, 必须小写, 在250个字符以内
	`-f, --file: 由configtxgen 生成的组织锚节点文件, 用于提交给orderer
	`--tls: 通信时是否使用tls加密
	`--cafile: 当前orderer节点pem格式的tls证书文件, 要使用绝对路径.
# orderer节点pem格式的tls证书文件路径参考: 
crypto-config/ordererOrganizations/ydqy.com/orderers/orderer.ydqy.com/msp/tlscacerts/tlsca.ydqy.com-cert.pem
# example
$ peer channel update -o orderer节点地址:端口 -c 通道名 -f 锚节点更新文件 --tls true --cafile orderer节点pem格式的证书文件
```





## Fabric的SDK

### SDK配置文件 config.yaml

要使用SDK，就要先将这个配置文件准备好

```yaml
name: "gosdk-service-network"
# 指定版本
version: 1.1.4
# 客户端
client:
  # 客户端组织的名称
  organization: org1
  # 定义日志服务
  logging:
    level: info
  # MSP根目录
  cryptoconfig:
    path: /home/shalom/study/testwork/crypto-config
  # 某些SDK支持插件化的KV数据库,通过指定credentialStore属性实现
  credentialStore:
    # 可选,用于用户证书材料存储,如果所有的证书材料被嵌入到配置文件,则不需要
    path: /tmp/gosdk-service-store
    # 可选,指定GoSDK实现的CryptoSuite实现
    cryptoStore:
      # 指定用于加密密钥存储的底层KV数据库
      path: /tmp/gosdk-service-msp
  # 客户端的BCCSP模块配置
  BCCSP:
    security:
      enabled: true
      default:
        provider: "SW"
      hashAlgorithm: "SHA2"
      softVerify: true
      level: 256
  tlsCerts:
    # 可选,当连接到peers,orderes时使用系统证书池,默认为false
    systemCertPool: true
    # 可选,客户端和peers与orderes进行TLS握手的密钥和证书
    client:
      keyfile: /home/shalom/study/testwork/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/tls/client.key
      certfile: /home/shalom/study/testwork/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/tls/client.crt
# 通道
channels:
  # 自定义,通道名
  examplechannel:
    # 可选,参与的orderers列表
    orderers:
      - orderer1.example.com
      - orderer2.example.com
      - orderer3.example.com
    # 可选,参与的peers列表
    peers:
      peer0.org1.example.com:
        endorsingPeer: true
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
      peer1.org1.example.com:
        endorsingPeer: true
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
      peer0.org2.example.com:
        endorsingPeer: true
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
      peer1.org2.example.com:
        endorsingPeer: true
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
    # 可选,执行通道操作
    policies:
      # 可选,获取通道配置区块
      queryChannelConfig:
        # 可选,成功响应节点的最小数量
        minResponses: 1
        # 可选
        maxTargets: 1
        # 可选,查询配置区块的重试选项
        retryOpts:
          # 可选,重试次数
          attempts: 5
          # 可选,第一次重试的后退间隔
          initialBackoff: 500ms
          # 可选,重试的最大后退间隔
          maxBackoff: 5s
          backoffFactor: 2.0
# 组织         
organizations:
  # 自定义,组织名
  org1:
    # 组织的MSPID
    mspid: Org1MSP
    # 组织的MSP存储位置,绝对路径或相对路径
    cryptoPath: /home/shalom/study/testwork/crypto-config/peerOrganizations/org1.example.com/users/{username}@org1.example.com/msp
    peers:
      - peer0.org1.example.com
    # 可选,证书颁发机构
    certificateAuthorities:
      # - ca.org1.example.com
  org2:
    mspid: Org2MSP
    cryptoPath: /home/shalom/study/testwork/crypto-config/peerOrganizations/org2.example.com/users/{username}@org2.example.com/msp
    peers:
      - peer0.org2.example.com
    certificateAuthorities:
      # - ca.org2.example.com
  # 自定义,排序组织名
  ordererorg:
    # 排序组织的MSPID
    mspID: OrdererMSP
    # 排序组织的MSP存储位置,绝对路径或相对路径
    cryptoPath: /home/shalom/study/testwork/crypto-config/ordererOrganizations/example.com/users/Admin@example.com/msp
# 排序    
orderers:
  # 自定义,排序节点域名
  orderer0.example.com:
    url: grpcs://192.168.147.137:7050
    grpcOptions:
      ssl-target-name-override: orderer0.example.com
      # 当keep-alive-time被设置为0或小于激活客户端的参数,下列参数失效
      keep-alive-time: 5s
      keep-alive-timeout: 6s
      keep-alive-permit: false
      fail-fast: true
      allow-insecure: false
    tlsCACerts:
      # 证书的绝对路径
      path: /home/shalom/study/testwork/crypto-config/ordererOrganizations/example.com/orderers/orderer0.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
  orderer1.example.com:
    url: grpcs://192.168.147.134:7050
    grpcOptions:
      ssl-target-name-override: orderer1.example.com
      keep-alive-time: 5s
      keep-alive-timeout: 6s
      keep-alive-permit: false
      fail-fast: true
      allow-insecure: false
    tlsCACerts:
      path: /home/shalom/study/testwork/crypto-config/ordererOrganizations/example.com/orderers/orderer1.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
  orderer2.example.com:
    url: grpcs://192.168.147.133:7050
    grpcOptions:
      ssl-target-name-override: orderer2.example.com
      keep-alive-time: 5s
      keep-alive-timeout: 6s
      keep-alive-permit: false
      fail-fast: true
      allow-insecure: false
    tlsCACerts:
      path: /home/shalom/study/testwork/crypto-config/ordererOrganizations/example.com/orderers/orderer2.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
# 节点
peers:
  # 自定义,peer节点域名
  peer0.org1.example.com:
    # 用于发送背书和查询请求
    url: peer0.org1.example.com:7051
    grpcOptions:
      ssl-target-name-override: peer0.org1.example.com
      allow-insecure: false
    tlsCACerts:
      # 证书的绝对路径
      path: /home/shalom/study/testwork/crypto-config/peerOrganizations/org1.example.com/tlsca/tlsca.org1.example.com-cert.pem
  peer1.org1.example.com:
    url: peer1.org1.example.com:7051
    grpcOptions:
      ssl-target-name-override: peer1.org1.example.com
      allow-insecure: false
    tlsCACerts:
      path: /home/shalom/study/testwork/crypto-config/peerOrganizations/org1.example.com/tlsca/tlsca.org1.example.com-cert.pem
  peer0.org2.example.com:
    url: peer0.org2.example.com:7051
    grpcOptions:
      ssl-target-name-override: peer0.org2.example.com
      allow-insecure: false
    tlsCACerts:
      path: /home/shalom/study/testwork/crypto-config/peerOrganizations/org2.example.com/tlsca/tlsca.org2.example.com-cert.pem
  peer1.org2.example.com:
    url: peer1.org2.example.com:7051
    grpcOptions:
      ssl-target-name-override: peer1.org2.example.com
      allow-insecure: false
    tlsCACerts:
      path: /home/shalom/study/testwork/crypto-config/peerOrganizations/org2.example.com/tlsca/tlsca.org2.example.com-cert.pem

# 后边的是复制的，没有做修改
# 证书
certificateAuthorities:
  # 自定义,ca节点域名
  ca.org1.example.com:
    url: http://192.168.1.251:7054
    # 包含CA的TLS证书的路径或直接包含证书的PEM内容,协议url必须是https
    tlsCACerts:
      path: /home/wy/go/src/gosdk/conf/ca.org1.example.com/msp/cacerts/192-168-1-251-7054.pem
      client:
        key:
          path: /home/wy/go/src/gosdk/conf/ca.org1.example.com/msp/keystore/e936910ed0f24ba01dca1afedecedf43027f1ee15415c58a10ef1e1e9bcdf8b0_sk
        cert:
          path: /home/wy/go/src/gosdk/conf/ca.org1.example.com/msp/signcerts/cert.pem
    # 注册商ID和密码的集合,每个CA只支持一个注册商
    registrar:
      enrollId: admin
      enrollSecret: adminpw
    # 可选,CA机构名称
    caName: ca.org1.example.com
  ca.org2.example.com:
    url: http://192.168.1.252:7054
    tlsCACerts:
      path: /home/wy/go/src/gosdk/conf/ca.org2.example.com/msp/cacerts/192-168-1-252-7054.pem
      client:
        key:
          path: /home/wy/go/src/gosdk/conf/ca.org2.example.com/msp/keystore/44adf491fa454dbbb1d110372fbbb5387786580c90f227d5d0ce1f23f7995b1f_sk
        cert:
          path: /home/wy/go/src/gosdk/conf/ca.org2.example.com/msp/signcerts/cert.pem
    registrar:
      enrollId: admin
      enrollSecret: adminpw
    caName: ca.org2.example.com
  ca.org3.example.com:
    url: http://192.168.1.253:7054
    tlsCACerts:
      path: /home/wy/go/src/gosdk/conf/ca.org3.example.com/msp/cacerts/192-168-1-253-7054.pem
      client:
        key:
          path: /home/wy/go/src/gosdk/conf/ca.org3.example.com/msp/keystore/21e582f4db08bfe1195b9d9a4ada58250c3c030d23f450aadbbab0bfe3e25463_sk
        cert:
          path: /home/wy/go/src/gosdk/conf/ca.org3.example.com/msp/signcerts/cert.pem
    registrar:
      enrollId: admin
      enrollSecret: adminpw
    caName: ca.org3.example.com
  ca.org4.example.com:
    url: http://192.168.1.254:7054
    tlsCACerts:
      path: /home/wy/go/src/gosdk/conf/ca.org4.example.com/msp/cacerts/192-168-1-254-7054.pem
      client:
        key:
          path: /home/wy/go/src/gosdk/conf/ca.org4.example.com/msp/keystore/08e3382067143b202555631c372fbcf7e567d79dfdaf3c5c96a5d63bcde156e1_sk
        cert:
          path: /home/wy/go/src/gosdk/conf/ca.org4.example.com/msp/signcerts/cert.pem
    registrar:
      enrollId: admin
      enrollSecret: adminpw
    caName: ca.org4.example.com
# 映射
entityMatchers:
  peer:
  - pattern: (\w*)peer1.org1(\w*)
    urlSubstitutionExp: 192.168.1.251:7051
    sslTargetOverrideUrlSubstitutionExp: peer1.org1.example.com
    mappedHost: peer1.org1.example.com
  - pattern: (\w*)peer1.org2(\w*)
    urlSubstitutionExp: 192.168.1.252:7051
    sslTargetOverrideUrlSubstitutionExp: peer1.org2.example.com
    mappedHost: peer1.org2.example.com
  - pattern: (\w*)peer1.org3(\w*)
    urlSubstitutionExp: 192.168.1.253:7051
    sslTargetOverrideUrlSubstitutionExp: peer1.org3.example.com
    mappedHost: peer1.org3.example.com
  - pattern: (\w*)peer1.org4(\w*)
    urlSubstitutionExp: 192.168.1.254:7051
    sslTargetOverrideUrlSubstitutionExp: peer1.org4.example.com
    mappedHost: peer1.org4.example.com
  orderer:
  - pattern: (\w*)orderer1(\w*)
    urlSubstitutionExp: 192.168.1.251:7050
    sslTargetOverrideUrlSubstitutionExp: orderer1.example.com
    mappedHost: orderer1.example.com
  - pattern: (\w*)orderer2(\w*)
    urlSubstitutionExp: 192.168.1.252:7050
    sslTargetOverrideUrlSubstitutionExp: orderer2.example.com
    mappedHost: orderer2.example.com
  - pattern: (\w*)orderer3(\w*)
    urlSubstitutionExp: 192.168.1.253:7050
    sslTargetOverrideUrlSubstitutionExp: orderer3.example.com
    mappedHost: orderer3.example.com
  certificateAuthority:
  - pattern: (\w*)ca.org1(\w*)
    urlSubstitutionExp: 192.168.1.251:7054
    sslTargetOverrideUrlSubstitutionExp: ca.org1.example.com
    mappedHost: ca.org1.example.com
  - pattern: (\w*)ca.org2(\w*)
    urlSubstitutionExp: 192.168.1.252:7054
    sslTargetOverrideUrlSubstitutionExp: ca.org2.example.com
    mappedHost: ca.org2.example.com
  - pattern: (\w*)ca.org3(\w*)
    urlSubstitutionExp: 192.168.1.253:7054
    sslTargetOverrideUrlSubstitutionExp: ca.org3.example.com
    mappedHost: ca.org3.example.com
  - pattern: (\w*)ca.org4(\w*)
    urlSubstitutionExp: 192.168.1.254:7054
    sslTargetOverrideUrlSubstitutionExp: ca.org4.example.com
    mappedHost: ca.org4.example.com
```



### 编写好链码



### 创建一个对象

此对象包含所有需要的信息，这是在model层声名的

```go
type SetUp struct {
	ConfigFile       []string
	ChainCodeGoPath  string
	ChainCodePath    []string
	ChainCodeVersion []string
	ChainCodeID      []string
	ChannelConfig    []string
	ChannelID        []string
	OrdererID        []string
	OrgAdmin         string
	OrgName          []string
	OrgID            []string
	UserName         []string
	Args             []string
	EventID          string
	ChannelClient    *channel.Client
	LedgerClient     *ledger.Client
	AdminClient      *resmgmt.Client
	MspClient        *msp.Client
	EventClient      *event.Client
	SDK              *fabsdk.FabricSDK
}
```

看代码操作





