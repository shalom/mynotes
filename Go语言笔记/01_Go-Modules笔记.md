# Go Modules笔记

## 介绍

module是go官方推出的模块管理工具，每个模块就相当于java中的一个包。

每个 module 中都包含一个 go.mod 文件，反之：没有go.mod文件，就不是一个module。

go.mod文件定义了该模块自身的 module path，同时也记录了该模块的依赖包；看下面的一个简单的 go.mod 的例码：

```shell
$ cat go.mod
module example.com/hello # 自己 module 的 “module path”

go 1.12

require rsc.io/quote v1.5.2 # 当前 module 依赖的 “module path” 和 版本号
$
```

除了 `go.mod` 之外，go 命令行工具还维护了一个 `go.sum` 文件，它包含了 指定版本模块内容的哈希值，用于校验。



## 简单使用

**首先需要知道的两点：**

- 如果你使用的是1.11和1.12版本的GO，当你工作目录是在 `$GOPATH/src` 里面的时候，go modules 功能不会被启用；依旧按照旧版本的规则，从GOPATH中寻找你导入的包，即使你目录中有 go.mod 文件，也不会启用。

- 如果你使用的1.13版本的GO时，无论你的项目路径在哪里，只要项目中存在go.mod文件，都会启用 go moduls 功能。



### 创建一个新 module

你可以在一个新目录下编写一些简单的go代码，或者用自己之前写过的go项目，在最上层目录中执行初始化module的命令：

```shell
# go mod init ${自定义的模块路径}
go mod init example.com/helloworld
```

#### 效果

你会发现，当前目录下生成了一个 go.mod 文件，里边就会有自己定义的 module 路径，也就是上边命令中的 `example.com/helloworld`。这样你就创建好了一个新 module，这个文件夹里边的所有包，包括子文件中的子包共同组成了这个 module。



### 导入其他 module

当你用的是以前写的项目时，文件中导的包将不会从GOPATH中读取，而是会从远程仓库中拉取，并且缓存到`$GOPATH/pkg/mod`中。

代码中 import 了一个包，但 `go.mod` 文件里面又没有指定这个包的时候，尝试着去编译代码时，系统会自动寻找包含这个代码包的模块的**最新稳定版本**。

这个时候可能会报错，其中出现次数最多的几个原因是：

- 使用GOPATH导包的路径和 module 不一样，去远程仓库拉取时，找不到想要的包；
- 因为拉取的包是最新稳定版，和当前使用的版本不兼容，导致代码运行不起来；



### 如何让他人使用自己的module

#### 创建仓库

在GitHub上创建自己的仓库，假设我的仓库名为 gomodule-test。

#### 编写一段简单的代码

```go
package foo

import "fmt"

func Foo(name string) {
	fmt.Println("foo print -> "+name)
}
```



#### 初始化自己的module

在自己仓库的根目录下执行命令：

```shell
# github/shalom/gomodule-test 是自定义的 module path
go mod init github/shalom/gomodule-test
```

**注意：**这个 module path 要和GitHub上的路径一样



#### 他人如何使用

你推送到GitHub上后，他人就可以像下边代码一样使用你的module，例码：

```go
package main

import "github/shalom/gomodule-test/foo"

func main() {
	foo.Foo("shalom")
}
```

执行编译命令 `go build` 后，go会根据  `github/shalom/gomodule-test`（module path） 自动拉取相关代码，并缓存到 `$GOPATH/pkg/mod` 里边





## 在IDEA中设置go module

当自己在使用 go module 时，为了让IDEA不飘红，可以这么设置：Setting -> 搜索 module -> 设置下图两个地方：

![1568958934887](assets/1568958934887.png)

- 一个是在IDEA中开启 module 功能
- 另一个是设置代理



## 仓库，模块和包之间的关系

- 仓库包含一个或多个 Go 模块
- 每个模块包含一个或多个 Go 包
- 每个包由一个目录中的一个或多个 Go 源文件组成





## go modules相关的环境变量

### `GO111MODULE`

此环境变量用来：开启和关闭 modules功能。

它有三个可选值：off、on、auto，默认值是 auto。

#### `export GO111MODULE=off`

-  无模块支持，go 会从 GOPATH 和 vendor 文件夹寻找包。

#### `export GO111MODULE=on`

-  强制模块支持；
  - 1.11和1.12版本，只要项目目录中存在go.mod文件，go 会忽略 GOPATH 和 vendor 文件夹，只根据 go.mod 下载依赖；
  - 1.13版，即使目录中或者上级目录中不存在 go.mod 文件，也同样会忽略 GOPATH 和 vendor 文件夹，使用module功能拉取代码中导入的包。

#### `export GO111MODULE=auto` 

- 1.11和1.12版本，在 $GOPATH/src 外面且根目录有 go.mod 文件时，开启模块支持；
- 1.13版，是否启用module功能取决于当前目录，若当前目录存在 go.mod 文件，或者任意级的父目录中存在 go.mod 文件，无论是否在GOPATH下，都将使用 go module。



### `GOPROXY`

此环境变量用来：指定 Go 模块代理。这个代理可以理解为一个 云端的包管理平台，用于存储并提供下载包。

 Go 1.13 将 `GOPROXY` 默认为：https://proxy.golang.org；这个网址是中国大陆无法访问的。

#### 国内代理介绍

- [goproxy.cn](https://goproxy.cn/)：由七牛云运行，支持代理 [sum.golang.org](https://sum.golang.org/)，经过 CDN 加速，高可用，可应用进公司复杂的开发环境中，亦可用作上游代理；
- [mirrors.aliyun.com/goproxy](https://mirrors.aliyun.com/goproxy)：由阿里云运行，不支持 sumdb 代理，经过 CDN 加速，高可用，可应用进公司复杂的开发环境中，亦可用作上游代理；
  - 此代理在拉取 `github.com/kubernetes/kubernetes` 库时，会出现 404
- [goproxy.io](https://goproxy.io/)：由个人运行，支持代理 [sum.golang.org](https://sum.golang.org/)。



有人对[goproxy.cn](https://goproxy.cn/)和[goproxy.io](https://goproxy.io/)进行了一组测试：在相同的网络环境下，分别拉取超大型库 [github.com/kubernetes/kubernetes](https://github.com/kubernetes/kubernetes)。测试结果为：

- goproxy.cn 耗时 19.009s；
- goproxy.io 耗时 70.03s；

所以综合对比下来，使用 goproxy.cn 最佳。



#### 如何设置`GOPROXY`

```shell
go env -w GOPROXY=https://goproxy.cn,direct
```

- `-w` 用来修改环境变量的默认值，具体细节可以使用 `go help env` 命令查看；
- `,direct`是1.13版本的新功能，可以在一定程度上解决私有库的问题，细节如下：
  - 首先 go 会在 GOPROXY 设置的代理平台上抓取目标模块；
  - 如果是私库或者其他原因导致了 404 错误，那么就回退到 `direct`；`direct`指的是 module 的源头，比如你导入的模块是 `github.com/test`，那么源头就是 GitHub，由GitHub来判断你是否有权限访问目标库；
  - 而当你在GitHub等类似的代码托管网站抓取的时候，若你无权访问，他就会告诉你此包不存在，若有权访问，则可以正常拉取；



#### 配套变量`GONOPROXY`

上边加`,direct`的方式并不十分完美，因为每次拉取私库时，都要经过代理。通过设置`GONOPROXY`，可以直接无脑绕过代理，。





### `GOSUMDB`

GOSUMDB 的全称为 Go CheckSum Database，用来下载的包的安全性校验问题。包的安全性在使用 GoProxy 之后更容易出现，比如我们引用了一个不安全的 GoProxy 之后然后下载了一个不安全的包，这个时候就出现了安全性问题。对于这种情况，可以通过 GOSUMDB 来对包的哈希值进行校验。当然如果想要关闭哈希校验，可以将 GOSUMDB 设置为 off；如果要对部分包关闭哈希校验，则可以将包的前缀设置到环境变量中 GONOSUMDB 中，设置规则类似 GOPRIVATE。

关于 GOSUMDB 的配置格式为：`<db_name>+<publickey>+<url>`。

```
GOSUMDB="sum.golang.org"GOSUMDB="sum.golang.org+<publickey>"GOSUMDB="sum.golang.org+<publickey> https://sum.golang.org"
```

上面三种配置都是合理的，因为对于 sum.golang.org，Go 自己知道其对应的 publickey 和 url，所以我们只要配置一个名字即可，对于另外一个 sum.golang.google.cn 也是一样。除此之外的，都需要指明 publickey，url 默认是 https://<db_name>。



### `GOPRIVATE`

前面也说到对于一些内部的 package，GoProxy 并不能很好的处理，Go 1.13 推出了 GOPRIVATE 机制。只需要设置这个环境变量，然后标识出哪些 package 是 private 的，那么对于这个 package 的处理将不会从 proxy 下载。GOPRIVATE 的值是一个以逗号分隔的列表，支持正则（正则语法遵守 Golang 的 包 path.Match）。下面是一个 GOPRIVATE 的示例：

```
GOPRIVATE=*.corp.example.com,rsc.io/private
```

上面的 GOPRIVATE 表示以 *.corp.example.com 或者 rsc.io/private 开头的 package 都是私有的。



### 私库处理

如何才能够畅通的解决私库问题，









## go.mod文件

go.mod 文件有四个指令：`module`，`require`，`replace`，`exclude`。

1. `module` 指令在 go.mod 中声明模块标识，该指令声明模块的路径。
2. `require` 标明当前module的依赖。
3. `// indirect` 间接依赖



### replace

对于有些 golang.org/x/ 下面的包由于某些原因在国内是下载不了的，但是对应的包在 github 上面是有一份拷贝的，这个时候我们就可以将 go.mod 中的包进行 replace 操作。

看下边的例码：

```go
replace golang.org/x/crypto v0.0.0-20181127143415-eb0de9b17e85 => github.com/golang/crypto v0.0.0-20181127143415-eb0de9b17e85

replace gopkg.in/yaml.v2 v2.2.1 => github.com/go-yaml/yaml v0.0.0-20180328195020-5420a8b6744d
```







## go.sum文件

TODO ...



## go mod 子命令

```shell
download 	download modules to local cache (下载依赖的module到本地cache))
edit 		edit go.mod from tools or scripts (编辑go.mod文件)
graph 		print module requirement graph (打印模块依赖图))
init $modulePath		initialize new module in current directory (在当前文件夹下初始化一个新的module, 创建go.mod文件))
tidy 		add missing and remove unused modules (增加丢失的module，去掉未用的module)
vendor 		make vendored copy of dependencies (将依赖复制到vendor下)
verify 		verify dependencies have expected content (校验依赖)
why 		explain why packages or modules are needed (解释为什么需要依赖)
```

### 辅助命令

- `go list -m all` 命令会把当前的模块所有的依赖项都列出来（包括间接依赖）；





## go module版本问题

### 版本号介绍

模块的版本包括三个部分：主版本号（major）、次版本号（minor）、修订号（patch）。举个例子：对于版本 `v0.1.2`，主版本号是 0，次版本号是 1，修订号是 2。



### 作为Author(作者)

#### 给module打tag

当我们修改完代码，并且已经commit了后，在push之前，给module打上tag

```shell
$ git tag v1.0.0
$ git push --tag origin master
```

如果不打 tag，go会在 consumer(module的使用者) 的go.mod中使用伪版本号，比如：

> github.com/shalom/testmodule/v2 v2.0.0-20190603050009-28a5b8da279e



#### module的升级

完善代码后，要给原本是 v1.0.0 的module 升级为 v2.0.0



### 作为Customer(使用者)

若新版本 module 的path发生的改变，则需要手动修改代码中的导包路径

#### 使用go get更新module版本

在 modules 模式开启和关闭的情况下，`go get` 的使用方式不是完全相同的。

在 modules 模式开启的情况下，可以通过在 package 后面添加 *@version* 来表明要升级（降级）到某个版本。如果没有指明 version 的情况下，则默认先下载打了 tag 的 release 版本，比如 v0.4.5 或者 v1.2.3；如果没有 release 版本，则下载最新的 pre release 版本，比如 v0.0.1-pre1。如果还没有则下载最新的 commit。这个地方给我们的一个启示是如果我们不按规范的方式来命名我们的 package 的 tag，则 modules 是无法管理的。



可使用` go get rsc.io/sampler@v1.3.1`命令进行module的版本更新

运行 `go get -u=patch` 将会升级到最新的修订版本（比如说，将会升级到 1.0.1 版本，但不会升级到 1.1.0 版本）

执行完 go get 命令后，会发现 go.mod 文件中的module版本也改变了。





## 学习连接

- [Go Module入门](<https://segmentfault.com/a/1190000016676359>)
- [Go Module实践](https://studygolang.com/articles/19334)
- [项目中可能会用到的 module](<https://www.cnblogs.com/baizx/p/9925509.html>)







模块是相关Go包的集合。
模块是源代码交换和版本控制的单元。
go命令直接支持使用模块，
包括记录和解决对其他模块的依赖性。
模块取代了旧的基于GOPATH的方法来指定哪个源文件在给定的构建中使用。

模块支持

Go 1.13包括对Go模块的支持。模块感知模式默认处于活动状态，每当在当前目录中或在父目录中找到go.mod文件时。

利用模块支持的最快方法是检查你的
在那里创建一个go.mod文件（在下一节中描述）并运行
从该文件树中转发命令。

对于更精细的控制，Go 1.13继续受到尊重
临时环境变量GO111MODULE，可以设置为1
三个字符串值：off，on或auto（默认值）。
如果GO111MODULE = on，那么go命令需要使用模块，
从不咨询GOPATH。我们将此称为命令
模块感知或以“模块感知模式”运行。
如果GO111MODULE = off，那么go命令永远不会使用
模块支持。相反，它在供应商目录和GOPATH中查找
找到依赖关系; 我们现在将其称为“GOPATH模式”。
如果GO111MODULE = auto或未设置，则go命令启用或禁用
模块支持基于当前目录。

## 仅当当前目录包含a时才启用模块支持

go.mod文件或位于包含go.mod文件的目录下。

在模块感知模式下，GOPATH不再定义导入的含义
在构建期间，但它仍然存储下载的依赖项（在GOPATH / pkg / mod中）
和已安装的命令（在GOPATH / bin中，除非设置了GOBIN）。

定义一个模块

模块由Go源文件树和go.mod文件定义
在树的根目录中。包含go.mod文件的目录
被称为模块根。通常，模块根也将对应
到源代码库根目录（但通常不需要）。
该模块是模块根目录及其中所有Go包的集合
子目录，但不包括使用自己的go.mod文件的子树。

“模块路径”是与模块根对应的导入路径前缀。
go.mod文件定义模块路径并列出特定版本
在构建期间解析导入时应使用的其他模块，
通过提供他们的模块路径和版本。

例如，这个go.mod声明包含它的目录是root
带有路径example.com/m的模块，它还声明了该模块
取决于golang.org/x/text和gopkg.in/yaml.v2的特定版本：

	module example.com/m
	
	要求（
		golang.org/x/text v0.3.0
		gopkg.in/yaml.v2 v2.1.0
	）

go.mod文件还可以指定替换和排除的版本
仅适用于直接构建模块; 他们被忽略了
当模块合并到更大的构建中时。
有关go.mod文件的更多信息，请参阅“go help go.mod”。

要启动一个新模块，只需在该根目录中创建一个go.mod文件
模块的目录树，仅包含模块语句。
'go mod init'命令可用于执行此操作：

	去mod init example.com/m

在已经使用现有依赖关系管理工具的项目中
godep，glide或dep，'go mod init'也会添加require语句
匹配现有配置。

go.mod文件存在后，无需其他步骤：
像'go build'，'go test'，甚至'go list'这样的命令会自动执行
根据需要添加新的依赖项以满足导入。

主模块和构建列表

“主模块”是包含go命令所在目录的模块
运行。go命令通过查找中的go.mod来查找模块root
当前目录，或者当前目录的父目录，
或者父级的父目录，依此类推。

主模块的go.mod文件定义了可用的精确包
供go命令使用，通过require，replace和exclude语句。
通过以下require语句找到的依赖关系模块也有所贡献
到那组包的定义，但只能通过他们的go.mod
files的require语句：依赖的任何replace和exclude语句
模块被忽略。因此，replace和exclude语句允许
主模块完全控制自己的构建，而不是主题
通过依赖完成控制。

提供构建包的模块集称为“构建列表”。
构建列表最初仅包含主模块。然后是go命令
将已经模块所需的确切模块版本添加到列表中
在列表中，递归地，直到没有任何东西可以添加到列表中。
如果将特定模块的多个版本添加到列表中，
然后最后只有最新版本（根据语义版本
订购）保留用于构建。

'go list'命令提供有关主模块的信息
和构建列表。例如：

	go list -m＃主模块的打印路径
	go list -m -f = {{.dir}} #print主模块的根目录
	go list -m all #print build list

维护模块要求

go.mod文件是两者都可读和可编辑的
程序员和工具。go命令本身会自动更新go.mod文件
保持标准格式和require语句的准确性。

任何找到不熟悉导入的go命令都会查找该模块
包含该导入并添加该模块的最新版本
go.mod自动。因此，在大多数情况下，它就足够了
添加导入到源代码并运行'go build'，'go test'，甚至'go list'：
作为分析包的一部分，go命令将会发现
并解决导入并更新go.mod文件。

任何go命令都可以确定模块要求
缺少并且必须添加，即使只考虑单个
来自模块的包。另一方面，确定模块要求
不再需要，可以删除需要完整视图
所有可能的构建配置中的模块中的所有包
（体系结构，操作系统，构建标记等）。
'go mod tidy'命令构建该视图然后
添加任何缺少的模块要求并删除不必要的模块

作为在go.mod中维护require语句的一部分，go命令
跟踪哪些提供由当前模块直接导入的包
哪些提供仅由其他模块间接使用的包
依赖。仅为间接使用所需的要求标有a
go.mod文件中的“// indirect”注释。间接要求是
一旦它们被其他人隐含，就会自动从go.mod文件中删除
直接要求。间接要求仅在使用模块时出现
没有说明他们自己的某些依赖关系或明确说明
在其自己声明的要求之前升级模块的依赖项。

由于这种自动维护，go.mod中的信息是一个
最新的，可读的构建描述。

'go get'命令更新go.mod以更改a中使用的模块版本
建立。升级一个模块可能意味着升级其他模块，类似地a
降级一个模块可能意味着降级其他模块。'go get'命令
做出这些暗示的变化。如果直接编辑go.mod，则命令
比如'go build'或'go list'会假设升级是有意的
自动进行任何隐含的升级并更新go.mod以反映它们。

'go mod'命令提供了用于维护的其他功能
并了解模块和go.mod文件。请参阅'go help mod'。

-mod build标志提供了对go.mod更新和使用的额外控制。

如果使用-mod = readonly调用，则不允许使用隐式命令
自动更新上面描述的go.mod。相反，它会在任何更改时失败
to go.mod是必要的。此设置对于检查go.mod是否最有用
不需要更新，例如在持续集成和测试系统中。
即使使用-mod = readonly，“go get”命令仍然允许更新go.mod，
并且“go mod”命令不接受-mod标志（或任何其他构建标志）。

如果使用-mod = vendor调用，则go命令假定供应商
directory保存正确的依赖项副本并忽略
go.mod中的依赖描述。

伪版本

go.mod文件和go命令通常使用语义版本
描述模块版本的标准形式，以便版本可以
比较确定哪个应该比另一个更早或更晚考虑。
通过在标记中标记修订来引入类似v1.2.3的模块版本
底层源存储库。可以参考未标记的修订版
使用像v0.0.0-yyyymmddhhmmss-abcdefabcdef这样的“伪版本”，
其中时间是UTC的提交时间，最后的后缀是前缀
提交哈希。时间部分确保两个伪版本可以
进行比较以确定稍后发生的事件，提交哈希标识
底层提交和前缀（在此示例中为v0.0.0-）派生自
此提交之前提交图中最新的标记版本。

有三种伪版本形式：

没有早期使用vX.0.0-yyyymmddhhmmss-abcdefabcdef
在目标提交之前使用适当的主要版本进行版本化提交。
（这是最初的唯一形式，因此一些较旧的go.mod文件使用此表单
即使对于遵循标签的提交也是如此。）

vX.YZ-pre.0.yyyymmddhhmmss-abcdefabcdef用于最多
目标提交之前的最近版本化提交是vX.YZ-pre。

vX.Y.（Z + 1）-0.yyyymmddhhmmss-abcdefabcdef用于最多
目标提交之前的最近版本化提交是vX.YZ

伪版本永远不需要手动输入：go命令将接受
普通的提交哈希并将其转换为伪版本（或标记的）
版本（如果可用））自动。这种转换是一个例子
模块查询。

模块查询

go命令接受“模块查询”来代替模块版本
在命令行和主模块的go.mod文件中。
（在评估主模块的go.mod文件中的查询后，
go命令更新文件以将查询替换为其结果。）

完全指定的语义版本，例如“v1.2.3”，
评估该特定版本。

语义版本前缀，例如“v1”或“v1.2”，
使用该前缀评估最新的可用标记版本。

语义版本比较，例如“<v1.2.3”或“> = v1.5.6”，
评估最接近比较目标的可用标记版本
（<和<=的最新版本，>和> =的最早版本）。

字符串“latest”与最新的可用标记版本匹配，
或者是底层源代码库的最新未标记修订版。

字符串“upgrade”就像“latest”，但是如果模块是
目前需要的版本高于“最新版本”
会选择（例如，较新的预发布版本），“升级”
将改为选择更高版本。

字符串“patch”与最新的可用标记版本匹配
具有相同主要和次要版本号的模块的
目前需要的版本。如果当前不需要版本，
“补丁”相当于“最新”。

底层源存储库的修订标识符，例如
提交哈希前缀，修订标记或分支名称，选择它
具体代码修订。如果修订版也标有
语义版本，查询评估该语义版本。
否则，查询将评估为提交的伪版本。
请注意名称与其他名称匹配的分支和标记
查询语法不能以这种方式选择。例如，查询
“v2”表示以“v2”开头的最新版本，而不是分支
名为“v2”。

所有查询都喜欢发布版本到预发布版本。
例如，“<v1.2.3”将更喜欢返回“v1.2.2”
而不是“v1.2.3-pre1”，即使“v1.2.3-pre1”更近
到比较目标。

模块版本中的exclude语句不允许使用
主模块的go.mod被认为是不可用的，不能
由查询返回。

例如，这些命令都是有效的：

	去获取github.com/gorilla/mux@latest#相同（@latest是'go get'的默认值）
	go get github.com/gorilla/mux@v1.6.2# records v1.6.2
	go get github.com/gorilla/mux@e3702bed2# records v1.6.2
	go get github.com/gorilla/mux@c856192#recored v0.0.0-20180517173623-c85619274f5d
	go get github.com/gorilla/mux@master#记录master的当前含义

模块兼容性和语义版本控制

go命令要求模块使用语义版本并期望它
这些版本准确地描述了兼容性：它假定v1.5.4是一个
v1.5.3，v1.4.0甚至v1.0.0的向后兼容替代品。
更常见的是，go命令期望包遵循
“导入兼容性规则”，其中说：

“如果旧包和新包具有相同的导入路径，
新包装必须向后兼容旧包装。“

因为go命令采用导入兼容性规则，
模块定义只能设置一个所需的最低版本
其依赖项：它无法设置最大值或排除选定的版本。
但是，导入兼容性规则并不是一种保证：它可能就是这样
v1.5.4是错误的，不是v1.5.3的向后兼容替代品。
因此，go命令永远不会从旧版本更新
到未更新的模块的更新版本。

在语义版本控制中，更改主版本号表示缺少
向后兼容早期版本。保持进口
兼容性，go命令要求具有主要版本v2的模块
或者稍后使用具有该主要版本的模块路径作为最终元素。
例如，example.com/m的v2.0.0版必须使用模块路径
example.com/m/v2，该模块中的包将使用该路径
它们的导入路径前缀，如example.com/m/v2/sub/pkg。包括
模块路径中的主要版本号和以这种方式导入路径是
称为“语义导入版本控制”。具有主要模块的模块的伪版本
版本v2和更高版本以该主要版本而不是v0开头，如
v2.0.0-20180326061214-4fc5987536ef。

作为一种特殊情况，以gopkg.in/开头的模块路径继续使用
在该系统上建立的惯例：主要版本始终存在，
它前面有一个点而不是斜杠：gopkg.in/yaml.v1
和gopkg.in/yaml.v2，而不是gopkg.in/yaml和gopkg.in/yaml/v2。

go命令将具有不同模块路径的模块视为不相关：
它与example.com/m和example.com/m/v2之间没有任何关联。
具有不同主要版本的模块可以在构建中一起使用
由于他们的包装使用不同而被分开
导入路径。

在语义版本控制中，主要版本v0用于初始开发，
表示没有期望稳定性或向后兼容性。
主要版本v0没有出现在模块路径中，因为那些
版本是为v1.0.0做准备，而v1没有出现在
模块路径。

在语义导入版本控制约定之前编写的代码
介绍可能会使用主要版本v2及以后来描述
与v0和v1中使用的相同的未版本化导入路径集。
为了适应这样的代码，如果源代码存储库有一个
v2.0.0或更高版本标记没有go.mod的文件树，版本是
被认为是v1模块可用版本的一部分
并且在转换为模块时给出+不兼容的后缀
版本，如在v2.0.0 +不兼容。+不兼容的标签也是
应用于从这些版本派生的伪版本，如
v2.0.1-0.yyyymmddhhmmss-abcdefabcdef +不兼容。

通常，在构建列表中具有依赖关系（由'go list -m all'报告）
在v0版本，预发行版本，伪版本或+不兼容版本上
表明在升级时更有可能出现问题
依赖性，因为没有期望兼容性。

有关的更多信息，请参阅https://research.swtch.com/vgo-import
语义导入版本控制，请参阅https://semver.org/了解更多信息
语义版本控制。

模块代码布局

目前，请参阅https://research.swtch.com/vgo-module以获取信息
关于如何映射版本控制系统中的源代码
模块文件树。

模块下载和验证

go命令可以从代理获取模块或连接到源代码控制
根据GOPROXY环境的设置直接服务器
变量（参见'go help env'）。GOPROXY的默认设置是
“https：//proxy.golang.org,direct”，这意味着尝试
Go模块镜像由Google运行并回退到直接连接
如果代理报告它没有模块（HTTP错误404或410）。
有关服务的隐私政策，请参阅https://proxy.golang.org/privacy。
如果GOPROXY设置为字符串“direct”，则下载使用直接连接
源控制服务器。将GOPROXY设置为“off”不允许下载
来自任何来源的模块。否则，GOPROXY预计将以逗号分隔
模块代理的URL列表，在这种情况下go命令将被获取
来自那些代理的模块。对于每个请求，go命令尝试每个代理
按顺序，如果当前代理返回404或410，则仅移动到下一个
HTTP响应。字符串“direct”可能出现在代理列表中，
导致在搜索中的那一点尝试直接连接。
从不咨询“直接”之后列出的任何代理。

GOPRIVATE和GONOPROXY环境变量允许绕过
所选模块的代理。有关详细信息，请参阅“go help module-private”。

无论模块的来源如何，go命令都会检查下载
已知的校验和，用于检测任何特定内容的意外更改
模块版本从一天到下一天。该检查首先参考当前的情况
模块的go.sum文件，但是回退到Go校验和数据库，由
GOSUMDB和GONOSUMDB环境变量。请参阅'go help module-auth'
详情。

有关代理协议的详细信息，请参阅“go help goproxy”
缓存下载的包的格式。

模块和销售

使用模块时，go命令完全忽略供应商目录。

缺省情况下，go命令通过下载模块来满足依赖性
从他们的来源和使用下载的副本（经过验证，
如前一节所述）。允许与旧版本进行互操作
Go的版本，或确保存储用于构建的所有文件
在一个文件树中，'go mod vendor'创建一个名为的目录
供应商在主模块的根目录中存储所有的
来自依赖模块的包，用于支持构建和
主模块中的包测试。

使用主模块的顶级供应商目录来构建以满足
依赖关系（禁用常用网络源和本地网络
缓存），使用'go build -mod = vendor'。请注意，只有主模块的
使用顶级供应商目录; 其他位置的供应商目录
仍被忽略。