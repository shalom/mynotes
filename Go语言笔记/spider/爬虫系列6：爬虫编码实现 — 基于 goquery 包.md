# 爬虫系列6：爬虫编码实现 — 基于 goquery 包

在讲解具体编码实现时，我们按照程序的执行顺序来讲，这部分需要你对照着代码学习。

## 通过 flag 包来控制命令行参数

在 main 包中，我们启动一个 goroutine 来执行具体的爬取工作。

```go
go ServeBackGround()
```

我们将爬取的工作和提供 Web 服务的代码集成在一个可执行程序中（也就是一个 main.main 中）。这里存在的一些问题说明一下：

1. 实际工作中，涉及到定时任务时，我们需要考虑是否存在多机器或多实例部署的问题，避免过多支持或混乱等问题；
2. 实际工作中，我们跟多可能会将爬取的工作生成专门的可执行程序（也就是有自己的 main.main），这样和 Web 服务分离，更方便使用。

在 `ServeBackGround()` 中，我们控制是否全量爬取，还可以控制抓取指定的某个站点。

这里使用了两个命令行参数：alll 和 site，一个是布尔类型，一个是字符串类型。我这么使用：

> ./crawler -all false -site "www.zhipin.com"

是否有问题？

## 多个站点并发抓取

这里对多个站点进行并发抓取。

```go
for _, autoRule := range autoRuleSlice {
    var worker Worker

    if parser == "goquery" {
        worker = NewGoQueryParser(isAll, autoRule, d)
    } else {
        worker = NewCollyParser(isAll, autoRule, d)
    }

    pool.Run(worker)
}
```

具体解析时，使用 goquery 还是 colly，亦或是其他的，可以通过配置文件指定。以上代码是工厂模式的应用。这里的设计并不很好，如果我们额外再实现一个解析器，需要改这里的代码。**有没有什么方法，支持注册解析器，然后这里直接根据名称获取解析器，不需要修改这里的代码呢？欢迎留言交流！**

此外，一个解析器只要实现 Worker 接口。

```go
type Worker interface {
    Work()
}
```

上文我说过 Worker 和 Pool 的设计，用来控制 goroutine 的数量，该设计值得认真体会。

为什么没有对一个站点做并发抓取？而且，代码中，我对抓取速度还进行了控制，这样避免抓取过快，被对方禁掉。关于反爬虫，这个有兴趣可以花时间研究，我也不专业，不进行展开。

## goquery 解析器

解析器的定义如下：

```go
type GoQueryParser struct {
    isAll    bool                 // 是全量抓取
    autoRule *model.AutoCrawlRule // 要自动抓取的网站列表配置信息
    d        time.Duration        // 抓取间隔，避免过快

    crawlRule *model.CrawlRule    // 网站详情页配置信息
}
```

### 列表解析

在继续讲解前，看一下自动抓取数据表存的数据：

```
INSERT INTO `auto_crawl_rule` (`domain`, `all_url`, `incr_url`, `keywords`, `list_selector`, `result_selector`, `page_field`, `max_page`, `ext`, `state`)
VALUES
    ('www.zhipin.com', 'https://www.zhipin.com/job_detail/?query=%s&city=100010000', 'https://www.zhipin.com/job_detail/?query=%s&city=100010000', 'golang,go语言', '.job-list .job-primary', '.info-primary .name a', 'page', 5, '', 0);
```

这里配置的是「BOSS直聘」的信息。请对照着这个数据，打开页面 https://www.zhipin.com/job_detail/?query=golang&city=10001000 查看页面结构，保证几个关键 selector 的正确性。

解析器通过 `Work()` 方法启动，直接调用 `ListParse()` 方法进行列表解析。

列表解析要处理的事情如下:

- 通过关键词和分页字段构造列表页 URL；
- 对每一个关键词、每一页进行处理；
- 通过 list_selector 选择器获取列表页的每一个列表项（一个职位信息）；
- 通过 result_selector 选择器获取职位详情页的 URL；

这里 list_selector 一定程度上可以没有，有的话，结构会更清晰些。

具体的代码不一一分析，着重说一下如何获取 goquery.Document 实例。

goquery 有 5 个导出的、获取 Document 实例的函数：

- NewDocument
- NewDocumentFromNode
- NewDocumentFromResponse
- NewDocumentFromReader
- CloneDocument

其中 NewDocument 和 NewDocumentFromResponse 已经不建议使用了，建议使用 NewDocumentFromReader 函数。也就是说，发起网络请求、处理相应流的关闭等，由使用者自己来维护，goquery 不负责处理。这样其实是更灵活的，我们对请求可以做更多的控制信息，用于避免被反爬虫、处理 cookie 等。

### 职位详情解析

在列表页获取到一个职位信息的 URL 后，会转到职位详情解析：DetailParse。同样，我们看下表数据：

```
INSERT INTO `crawl_rule` (`domain`, `name`, `job_name`, `job_company`, `job_city`, `job_work_exp`, `job_salary`, `job_education`, `job_jd`, `job_welfare`)
VALUES
    ('www.zhipin.com', 'Boss直聘', '.info-primary .name h1', '.sider-company .company-info a[ka=job-detail-company]', '<p>([^<]*)<em class=\"dolt\"', '<p>[^<]*<em .*</em>([^<]*)<em .*</em>', '.info-primary .salary', '<p>[^<]*<em .*</em>[^<]*<em .*</em>([^<]*)</p>', '.detail-content .job-sec:first-child', '.info-primary .tag-container .tag-all');
```

详情页的处理，无非就是根据该表定义的选择器获取对应属性的值。

细心的读者可能会发现，上面数据，有一些并非 CSS 选择器，而是正则表达式。这是因为，有一些数据，选择器没办法很好地获取到，这个时候，正则表达式就派上用场了，所以，我们在 parseContent 这个方法中会兼容这两种方式。

## 总结

基于 goquery 实现解析器就完成了。回顾一下要点：

- flag 的使用，尤其注意 bool 值的处理；
- 如何多站点并发抓取；
- goquery 如何使用？核心在于 CSS 选择器的使用，所以，需要有一些选择器基础知识；
- 正则表达式的使用；

基于 colly 的解析器，我希望大家能够自己尝试实现，并发起 PR，我会给大家 Review。