# 中文文档

[中文文档](https://github.com/skyhee/gin-doc-cn#middleware)

# gin 核心设计

| 模块                                                         | 实现方式                         | 依赖                     |
| ------------------------------------------------------------ | -------------------------------- | ------------------------ |
| [gin.Engine.pool](https://github.com/gin-gonic/gin/blob/master/gin.go#L108) | 使用 sync.Pool 临时对象池        | 标准库 sync.Pool         |
| [gin.ServeHTTP()](https://github.com/hhstore/blog/issues/131#issuecomment-475563889) | 使用 sync.pool 优化              | 标准库 net.http 接口实现 |
| [gin.Context](https://github.com/hhstore/blog/issues/131#issuecomment-475570693) | 核心模块, 包含HTTP请求处理全过程 | 标准库                   |
| xxxx                                                         | xxxxx                            | xxxx                     |
| xxxx                                                         | xxxxx                            | xxxx                     |
| xxxx                                                         | xxxxx                            | xxxx                     |



# gin 框架的中间件

[Go Web轻量级框架Gin学习系列：中间件使用详解](https://juejin.im/post/5cc2826bf265da03b2043639)



## gin 框架中的日志中间件的使用

[Go Web轻量级框架Gin学习系列：HTTP请求日志](https://juejin.im/post/5cc84cfa51882562457fe311)



# 学习连接

[拆轮子系列：gin框架](https://juejin.im/post/5b38a6b16fb9a00e6714ab3a#heading-21)  							$

好文章，里边的路由分析以及请求返回的处理，还需再好好消化消化  TODO：请求内容的处理与返回内容的处理

[Gin框架源码解析](https://www.cnblogs.com/yjf512/p/9670990.html)									 											

[gin v1.3.0 源码剖析](https://github.com/hhstore/blog/issues/131)

[Golang 微框架 Gin 简介](https://www.jianshu.com/p/a31e4ee25305)

[Go语言WEB框架(Gin)详解](http://c.biancheng.net/view/5574.html)

[Go框架解析-Gin](https://mp.weixin.qq.com/s?__biz=MzA5MDEwMDYyOA==&mid=2454619029&idx=1&sn=00c6976d2ff435a67b0754b1badb3261&chksm=87aae56eb0dd6c78e25bf21b19be49c5628b161ee800b753423e63e957339548a6adcd05e458&mpshare=1&scene=1&srcid=0924iIlfbCrGwG0eIjUMbfaU&sharer_sharetime=1569315443389&sharer_shareid=15448e25b3e6d68ea2c4c87850fcab65&key=980bae413a112bf3fd49b3da4b5ec61a24e72e0efbb416707bb4420785a42e027370c15ec76d3f8da154df0d1fa3b38ad3942360e2d02b26d12620d9d4a9225afd27e1fee4fbc69be8e17f16062e24da&ascene=1&uin=MjQ3Mjk5ODMyNA%3D%3D&devicetype=Windows+10&version=62060833&lang=zh_CN&pass_ticket=8ryMV1FDbiNseZMptBHGTIjym5Kn%2B7r1TLC8HdmvDpAmvcYjJAj86rp0vRDWy1xf)

[轻量级 Web 框架 Gin 结构分析](https://mp.weixin.qq.com/s?__biz=MzI0MzQyMTYzOQ==&mid=2247484981&idx=1&sn=27212a168ec33ffcc905407cdfce6a1e&chksm=e96c1e9dde1b978b4771898dc7e190b9b8db68b5ddebd5a426b07a3ddbd56c4872140ac24b97&mpshare=1&scene=1&srcid=0924l2LbfHraggF5Z1KIV3eE&sharer_sharetime=1569315463038&sharer_shareid=15448e25b3e6d68ea2c4c87850fcab65&key=0f2144d20783e8aef0fb733e749832e0bb76b1ead0bf3230c0b259f31fe6cc100a9b2e7f7b3c0d957bd88930a5a647cc2f4b5e39fe7f071b3f3aa2e5ce0f15304bf7dc402472b039976b0d98b9929f23&ascene=1&uin=MjQ3Mjk5ODMyNA%3D%3D&devicetype=Windows+10&version=62060833&lang=zh_CN&pass_ticket=8ryMV1FDbiNseZMptBHGTIjym5Kn%2B7r1TLC8HdmvDpAmvcYjJAj86rp0vRDWy1xf) $$

TODO: 渲染器 和上边的TODO项内容一样

[Gin框架上传文件](https://mp.weixin.qq.com/s?__biz=MzA5ODU5MTk3Mw==&mid=2647718489&idx=1&sn=6c7c1b61de833268bcffb44689239d2d&chksm=88ab6fd3bfdce6c51cb9c71c998b684ac14ed0c0f9b469191e14a86dc7be2a64d6ef7d933832&mpshare=1&scene=1&srcid=0924bmofRAqlpIfd8CfLQiwL&sharer_sharetime=1569315484969&sharer_shareid=15448e25b3e6d68ea2c4c87850fcab65&key=980bae413a112bf38f2c9365f542eb298a991dbc99f78c94ecd4a335531d0089590e09fa3353432cc5b74775d9a3d9da1c926bf9b13b956d22dcee71d6518ddb126d4ebe1dc6fc020cb246de341645ee&ascene=1&uin=MjQ3Mjk5ODMyNA%3D%3D&devicetype=Windows+10&version=62060833&lang=zh_CN&pass_ticket=8ryMV1FDbiNseZMptBHGTIjym5Kn%2B7r1TLC8HdmvDpAmvcYjJAj86rp0vRDWy1xf)

[Golang Web框架Gin解析(二)](https://mp.weixin.qq.com/s?__biz=MzU2MDQ5MTc1OA==&mid=2247483678&idx=1&sn=076be4e241e3efc9928cb2bf528a15d4&chksm=fc067c9fcb71f5890fa372fff9233abd3ace460d64ef04c3ee2fba46951ae77ccb9c21444656&mpshare=1&scene=1&srcid=0924Kj4HIncM7bdOGDBFJqPO&sharer_sharetime=1569315523576&sharer_shareid=15448e25b3e6d68ea2c4c87850fcab65&key=980bae413a112bf38ff669586045d991fa7711515cccc25f4b96517402c530fe776438d9568cdd61f0498c9a4ac917d5751e5c1cc44d05e3440e5669fb71e2307b2d7b171a98df99897875f963d16ca5&ascene=1&uin=MjQ3Mjk5ODMyNA%3D%3D&devicetype=Windows+10&version=62060833&lang=zh_CN&pass_ticket=8ryMV1FDbiNseZMptBHGTIjym5Kn%2B7r1TLC8HdmvDpAmvcYjJAj86rp0vRDWy1xf)

[gin源码阅读](https://mp.weixin.qq.com/s?__biz=MzA3MTQ4NjU3Mw==&mid=2247483683&idx=1&sn=59becd23fef994fd5f2f52bdf775d8b2&chksm=9f2d9c21a85a15376122be03f3d2bc25300a91a7ee13c016344342321c33140cadda88921bdd&mpshare=1&scene=1&srcid=0924GKG1X22gq4TTyflZNdbO&sharer_sharetime=1569315545748&sharer_shareid=15448e25b3e6d68ea2c4c87850fcab65&key=7c44f01adf2e586d2f5eefe2bd8597d5aa3508c431b9ee1a817dd05bba5fec1414225426b50decb49ac880a5ac783f4c93435197c513670ece002b60dbba6d64da0f7768d4d961d9cef2bfd78911132d&ascene=1&uin=MjQ3Mjk5ODMyNA%3D%3D&devicetype=Windows+10&version=62060833&lang=zh_CN&pass_ticket=8ryMV1FDbiNseZMptBHGTIjym5Kn%2B7r1TLC8HdmvDpAmvcYjJAj86rp0vRDWy1xf)



#### Gin 不支持 HTTPS，官方建议是使用 Nginx 来转发 HTTPS 请求到 Gin。



需要学习的模块：

- `Golang` 在 `HTTP` 服务方面的热更新的几种解决方案