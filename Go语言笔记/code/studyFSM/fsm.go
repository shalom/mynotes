package main

import (
	"fmt"
	"github.com/syndtr/goleveldb/leveldb/errors"
	"sync"
)

//
const (
	powerOff = iota
	firstGear
	secondGear
	thirdGear
)
var(

)

type (
	State uint8
	Event string
	Handle func() error
)

type FSM struct {
	state   State
	handler map[State]map[Event]Handle
	sync.Mutex
}

func (f *FSM) GetState() State {
	return f.state
}
func (f *FSM) SetStart(newState State) {
	f.state = newState
}
func NewFSM(initState State) *FSM {
	return &FSM{
		state:   initState,
		handler: make(map[State]map[Event]Handle),
	}
}

func (f *FSM) AddHandle(state State, event Event, handle Handle) {
	f.Lock()
	defer f.Unlock()
	if f.handler == nil {
		f.handler = make(map[State]map[Event]Handle)
	}
	_, ok := f.handler[state]
	if !ok {
		f.handler[state] = make(map[Event]Handle)
	}
	_, ok = f.handler[state][event]
	if ok {
		fmt.Printf("警告: 事件%s已经注册过方法\n", event)
	}
	f.handler[state][event] = handle
}

func (f *FSM) CallEvent(event Event) error{
	f.Lock()
	defer f.Unlock()
	oldState := f.GetState()
	events := f.handler[oldState]
	if events==nil{
		return errors.New("current state has no event.")
	}
	h,ok:=events[event]
	if !ok{
		return errors.New("event has no handle.")
	}
	return h()
}
