# 一周汇总 2019-08-18

## 每日一学

### 问题01：

[付费用户「每日一学」2019-08-12：Go 有 GC 还会内存泄露吗？  - Go语言中文网 -...](https://studygolang.com/topics/9830)




## 面试题

### 问题01：

里面的一些题，可以研究研究
[头条面经-PHP/Golang  - Go语言中文网 - Golang中文社区](https://studygolang.com/articles/22663#reply1)



## 同学问的问题

### 问题01：

请问星主：职场上对应聘的应届毕业生和即将一起工作的新人们都有什么要求？

今天面试问我一点点数据库基础，然后答得不好，后面让手写两道代码，我都写出来了，可能当时有些小bug而且时间复杂度有点高，但是自己回来测试都能通过，结果面试没通过…

我是非科班出身的，因为喜欢计算机，学了不少编程语言。是对计算机网络，数据库之类的基础知识掌握的不好被刷？还是因为表达不够清晰被刷呢？



#### **解决思路：**

1. 对于新手，我一般比较看重如下方面：

   1. 基础是否夯实。基础对将来深入、进阶来讲是很重要的；

   2. 学习能力，什么方式进行学习，怎么向面试官证明自己学习能力；

   3. 好的编码习惯；有潜力，勤奋好学；

   我感觉，主要可能还是因为你非科班，然后基础也不是特别好，所以被刷。

2. 我讲讲实习生的吧。去面试前要复习一下常见的数据结构与算法，数据库的话MySQL与Redis问的频率比较高，还有计算机网络，这些在牛客有很多汇总。总之和星主说得一样啦~基础和学习能力。我出来实习刚好一个月，主要是写写接口，写Bug，希望能共同进步。
3. 保持兴趣，实现一些自己感兴趣的项目，先别管代码写的怎么样，想尽办法实现它，然后再持续更新改进，使代码质量不断提高。切记不要只看教程，当时以为看懂了，用的时候还是无从下手，只有在项目中用到了，熟练了，才是自己的东西。不断积累自己用到的知识点，总结并归纳知识点，分类整理形成自己的知识树，不要太在意某个知识点，要知道那点东西在编程的海洋里屁都不是，不要急于学哪些奇淫技巧，那是人类在没有办法的情况折中方案，要学习开源项目的整体架构和编程思维，以我的经验是编程思想的提高是最高的，比知识重要1万倍。一定要不断突破自己的编程思维，虽然很难，因为你是要改变自己的习惯呀！每当编程思维改进一点，再看以前的代码怎么跟屎一样，这时候我要说：“嘿嘿，哥们，你进步了”。

### 问题02：

我本地电脑是mac，通过执行`go build main.go` 生成的文件放到阿里云服务器上，执行`chmod 755 main`，报错：-bash: ./main: 无法执行二进制文件，我的阿里云服务器是 centos7 系统。



#### **解决思路：**

1. 在mac下编译成linux可以执行的文件 执行如下命令:

   ```shell
   CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build
   ```

   

### 问题03：

请教下星主，一般项目打tag号有没有什么需要注意的地方？比如项目开始版本为0.0.0，修复bug就变为0.0.1，如果新增功能就变为0.1.1，如果结构发生发生变化就变为1.1.1等等。因为我目前所接手的项目tag号打的很随意，就是tag号最后一个数字一直递增下去，后期我会自己开始负责一些项目，想把tag号规范一下，感谢！	



#### **解决思路：**

1. 前面的数字变，后面的置为0



## 资源分享

- 推荐三本Go语言相关的书籍， 《Network Programming with Go》---侧重网络编程方面的，顺便把网络基础也细致讲解了一遍；《Data Structures & Algorithms In Go》---侧重数据结构算法，Go程序设计和找工作方面的，书中分别给出了使用本书准备1个月/3个月/5个月后面试的计划；《Go in Practice》---列举出了70种工作中写代码可能会碰到的问题，并对每个问题带有详细的讨论过程和解决方案。
- 分享一份收集整理了各大技术领域面试资料列表，含有前端、后端、客户端等等。
  Github: [GitHub - Awesome-Interview/Awesome-Interview: Coll...](https://github.com/Awesome-Interview/Awesome-Interview)




