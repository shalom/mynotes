# GO Web编程笔记

# 学习框架

1. web 基础
2. 表单处理
3. 访问数据库
4. session和cookie
5. 文本和文件处理
6. socket编程
7. 安全与加密
8. 国际化
9. 错误处理



# 表单处理

## 处理表单的输入

表单输入的数据主要保存在 `request.Form` 中，源码：

```go
Form url.Values  // request.go 中的request结构体字段
type Values map[string][]string // url.go 
```

可以看出，存储表单数据的是一个map。所以我们就可以通过 key -> value 的形式，将value取出，但是取出之前，必须要执行如下函数：

```go
req.ParseForm()
```

执行后，`request.Form` 中才有值。

### 一个key多个value的情况

并不会覆盖，都存储在切片中



## 表单验证

正则表达式验证

取值范围验证



## 预防跨站脚本

template.HTMLEscapeString

template.HTMLEscape

```go
t, err := template.New("foo").Parse(`{{define "T"}}Hello, {{.}}!{{end}}`)
err = t.ExecuteTemplate(out, "T", "<script>alert('you have been pwned')</script>")
```



上边的 template 包里边的函数，可以做到预防跨站脚本攻击



# Go语言基础之网络编程

socket: [Go语言基础之网络编程](https://www.liwenzhou.com/posts/Go/15_socket/)

http: [Go语言基础之net/http](https://www.liwenzhou.com/posts/Go/go_http/)





