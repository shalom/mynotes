package main

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func main() {
	// UNIX Time is faster and smaller than most timestamps
	// If you set zerolog.TimeFieldFormat to an empty string,
	// logs will write with UNIX time
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	log.Print("hello world")

	/*
	添加输出字段，对应json类型
	输出：
		{"level":"debug","age":100,"name":"shalom","time":1602984984,"message":"shalom is very handsome"}
		{"level":"debug","hello":"world","time":1602984984}
		{"level":"debug","age":100,"name":"shalom","time":1602984984}
	 */
	log.Debug().Int("age",100).Str("name","shalom").Msg("shalom is very handsome")
	log.Debug().Str("hello","world").Send()
	log.Debug().Int("age",100).Str("name","shalom").Send()

	/*
	字段可以嵌套
	输出：{"level":"debug","shalom":{"name":"shalom","age":18},"time":1602988751,"message":"hear is dict!"}
	*/
	log.Debug().Dict("shalom",zerolog.Dict().
		Str("name","shalom").
		Int("age",18)).Msg("hear is dict!")

	/*
	设置日志级别
	*/
	log.Warn().Int("age",100).Str("name","shalom").Msg("shalom is very handsome")
	//	设置全局日志级别
	//zerolog.SetGlobalLevel(zerolog.InfoLevel)

	/*
	没有级别的输出日志
	*/
	log.Log().Int("age",100).Str("name","shalom").Msg("shalom is very handsome")

	/*
	对人类友好的日志输出，有不同的颜色
	*/
	//log.Logger = log.Output(zerolog.ConsoleWriter{
	//	Out:                 os.Stderr,
	//})
	//log.Info().Str("foo","shalom").Msg("shalom is handsome!")
	//log.Debug().Str("foo","shalom").Msg("shalom is handsome!")
	//log.Warn().Str("foo","shalom").Msg("shalom is handsome!")
	//log.Error().Err(errors.New("error ... ")).Str("foo","shalom").Msg("shalom is handsome!")
	//log.Fatal().Str("foo","shalom").Msg("shalom is handsome!")
	//log.Panic().Str("foo","shalom").Msg("shalom is handsome!")

	/*
	添加文件行数
	*/
	log.Logger = log.With().Caller().Logger()
	log.Info().Str("the truth:","shalom is very handsome!").Msg("yes! yes!")
}
