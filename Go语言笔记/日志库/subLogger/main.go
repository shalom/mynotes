package main

import (
	"github.com/rs/zerolog"
	"os"
)

/*
	调用zerlog.New()传入一个io.Writer作为日志写入器
	基于当前的Logger可以创建一个子Logger，子Logger可以在父Logger上附加一些额外的字段。
	调用logger.With()创建一个上下文，然后为它添加字段，最后调用Logger()返回一个新的Logger：
*/
func main() {
	logger := zerolog.New(os.Stderr)
	subLogger := logger.With().
		Str("foo", "bar").
		Logger()
	subLogger.Info().Msg("hello world")
}
