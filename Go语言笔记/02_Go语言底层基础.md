#  Go语言底层基础笔记

# 导包

 Go 包初始化流程：

![img](assets/Fr2R83ovb9LYtta-DxOJQ1mUtZuq-1569377168205.png)

 go语言的执行顺序的规则

A语句依赖B语句，A虽然在B的前边，执行顺序依然是先B再A；以语句为单位。

下面看一个初始化的例子，例码：

```go
var a, b, c = f() + v(), g(), sqr(u()) + v()

func f() int { return c }
func g() int  { return a }
func sqr(x int) int { return x*x }
func u() int { return 1}
func v() int { return 2}
```

**分析：**

1. 首先执行的是给a赋值的语句，但是f() 依赖于c ，所以必须先执行 c 赋值语句；
2. 执行`sqr(u()) + v()`的时候依赖 u()，所以这里最先执行的是 u()；
3. 按照这种规则，推出函数的执行顺序是：u()、sqr()、v()、f()、v()、g()。



 导包的几种形式

```go
import 	 declaration    Local name of Sin
import   "math"         math.Sin
import m "math"         m.Sin
import . "math"         Sin
import _ "math"			...
```

1. 第3行的方式是将包名重命名
2. 第4行是点导包，一般测试的时候遇到循环导包的时候使用，用来将测试代码伪装成包内文件
3. 第5行需要注意的地方比较多：
   - 将该包引入，只初始化里面的init函数和一些变量，不能通过包名来调用包里的函数；
   - 同一个包同一个文件可以有多个init函数，多个init不会报错，都可以执行；
   - 在服务端经常用于导入数据库驱动。

# 基础数据类型源码位置

> runtime 包里 和 buildin包里



# const

不管常量是如何实现的，它可以被认为永远都是完全精确（mathematically exact）的。Go 的常量的这个特性是 Go 有别于其它语言的地方。其它语言如 C 或 C++ 并不是这样的。只要有足够的内存，整数的值永远都会精确地保存

所有数字常量，不管他们声明的时候是否指定了类型，在内部都用同样的数据结构来保存他们的值，这点跟变量不一样。而且，常量是完全精确的。

常量的底层设计都在这篇文章里（回头研究编译器的时候再看）：

- [GCTT 出品 | 浅析 Go 语言的数常量](https://mp.weixin.qq.com/s/IWzAWFMde_oa13YVepPnPw)



# uint8和int8

计算机存储的是补码

- 正数的补码是它本身
- 负数的补码，0变1,1变0，符号位不变

int8 底层存储的是二进制：0000 0000；第一位是符号位，后边存储补码。

 左移右移溢出的规则

```go
var a uint8 = 128  -> 二进制：1000 0000
// 向左偏移然后向右偏移
a = a << 1  
a = a >> 1
fmt.Println(a)
// 输出结果是0,1000 0000 左偏移后变成 0000 0000 ，再右移还是 0000 0000
```



# uint64  []byte

相互转换，这里使用到了一个全局变量 BigEndian ，后续添加关于这个变量的笔记

```go
// []byte -> uint64
binary.BigEndian.Uint64( ... )
// uint64 -> []byte
binary.BigEndian.PutUint64( ... )
```



# rune

[ASCII，Unicode 和 UTF-8的知识点](<http://www.ruanyifeng.com/blog/2007/10/ascii_unicode_and_utf-8.html>)





# error

 源码：

```go
// 路径：buildin/buildin.go
type error interface {
	Error() string
}
```

- 通过源码可以看出，error本质上是一个带方法的 interface；
- 带方法的interface，底层存储了两个字段：实现该interface的变量的原始类型 和 该变量的值；
- 只有上述两个字段都为 nil 的时候，判断 interface == nil 才为 true。

 可能会踩的坑

使用自定义的 错误类型 的时候，例码：

```go
// 这里定义了一个能够实现 error 的结构体，自定义的错误类型
type MyError struct {
	s string
}
func (e *MyError) Error() string {
	return e.s
}
func test() error {
	// 这里声明了该错误类型的变量，并且赋值为nil
	var a *MyError
	a = nil
	// 这里return的时候，a被自动转成 error类型，里面存储的类型不为nil
	return a
}
func main() {
    // 这里的err是一个接口，存储的类型为 MyError，值为空，所以这里返回的err永远都不为 nil
	err := test()
	fmt.Println(err == nil)
}
```

上述代码引用的是 errors.New() 方法的源代码：

```go
func New(text string) error {
	return &errorString{text}
}

type errorString struct {
	s string
}

func (e *errorString) Error() string {
	return e.s
}
```



#  string

 源码

```go
type stringStruct struct {
    str unsafe.Pointer
    len int
}
// 创建string的底层方法
func rawstring(size int) (s string, b []byte) {
    // 1. 分配符合字符串大小的内存块，并返回指针给它：
    p := mallocgc(uintptr(size), nil, false)

    // 2. 用刚返回的指针创建一个元数据（stringStruct），并指定该字符串的大小，s在这里。
    stringStructOf(&s).str = p
    stringStructOf(&s).len = size

    // 3. 准备一个字节类型的切片，实际上会将字符串数据存储在这里。将s的p和b连接
    *(*slice)(unsafe.Pointer(&b)) = slice{p, size, size}
}
```

从以上源码可以看出：

1. 无论字符串的大小如何，对字符串长度的请求都花费相同的时间。在 Big-O 术语中，它是一个 O(1) 操作。
2. 可以通过b来进行索引，但是不能够改动b中的p，p是一个内存指针，虽然存储在切片中，但是和数组不一样，不可以对每一个元素进行寻址。
3. 一般的slice存储的是一个array，但是string里边的底层内存空间不支持array的操作。



字符串是 不可变 值类型，内部⽤用指针指向 UTF-8编码格式的 字节数组。

- 默认值是空字符串 ""。
- 可以用索引号访问某字节，如 s[i]。
- 不能用取地址符获取字节元素指针，&s[i] 非法。 
- 不可变类型，无法修改字节数组。
- 字节数组尾部不包含 NULL。

 字符串处理

```go
//	1. 字符串按 指定分割符拆分：	Split
ret := strings.Split(str, " I")
//	2. 字符串按 空格拆分： Fields
ret = strings.Fields(str)
//	3. 判断字符串结束标记 HasSuffix
flg := strings.HasSuffix("test.abc", ".mp3")
//	4. 判断字符串起始标记 HasPrefix
flg := strings.HasPrefix("test.abc", "tes.")
```



# struct

 空结构体

```go
a := struct{}{}
```

- 空结构体不占内存

- 空结构体变量的地址是一样的

  ```go
  a := struct{}{}
  b := struct{}{}
  c := &struct {}{}
  fmt.Printf("%p  %p  %p",&a,&b,c)
  ```

  输出的三个值都相同。

 结构体的tag

1. 结构体结构一样，但是tag不一样，属于不同类型，但是可以相互转换
2. 强转后，tag会被忽略



 结构体嵌套的时候

若结构体中嵌套了另一个结构体，那么在初始化的时候，嵌套的结构体要先单独初始化，不然空指针错误，相当于每个对象都需要初始化开辟空间。





 使用细节

1. 在使用struct的时候，请记住它是值传递；
2. 一般优先考虑使用指针变量，在一些特殊的情况才会使用值类型，比如 time 包里的 Time 对象，因为每个时刻的 Time 都是不一样的，所以不能用指针变量表示；
3. 如果结构体中的各个元素都可以使用等号来比较的话，那就可以使用相号 `==` 来比较结构体变量；结构体中的元素无法比较，用 `==` 比较结构体会导致编译错误；



# map

 使用细节

1. 切片，数组，不能成为map的key，但是他们的指针类型可以

2. go语言中的map的元素不可以寻址，因为map中的元素的地址是变化的，这意味着寻址的结果是无意义的，例码：

   ```go
   type Person struct {
       Age int
   }
   func (p *Person) GrowUp() {
       p.Age++
   }
   func main() {
       m := map[string]Person{
           "zhangsan": Person{Age: 20},
       }
       m["zhangsan"].Age = 23
       m["zhangsan"].GrowUp() //这里不会改变
   }
   ```

   以上代码中，`m["zhangsan"]`的值还是未变；若要改变，可将`map[string]Person`改为`map[string]*Person`

3. 因为遍历map是无序的，所以在序列化的时候，千万要注意这一点，显性的将key排序



# recorver

recover 使用的三个要点：

1. recover 必须在 defer 中调用；
2. recover 必须在函数中调用，匿名不匿名都行；
3. recover 不能在多层函数中调用。

具体使用案例：

```go
//	1: 没有在defer中使用
func main() {
    if r := recover(); r != nil {
    	log.Fatal(r)
    }
    panic(123)
    if r := recover(); r != nil {
    	log.Fatal(r)
    }
}
//	2: 不可以在defer的多层函数中调用
func main() {
    defer func() {
        if r := MyRecover(); r != nil {
            fmt.Println(r)
        }
    }()
    panic(1)
}
func MyRecover() interface{} {
    log.Println("trace...")
    return recover()
}
//	3: 不可再defer的多层函数中调用
func main() {
    defer func() {
        defer func() {
            if r := recover(); r != nil {
            	fmt.Println(r)
        	}
    	}()
	}()
	panic(1)
}
//	4: 正确
func MyRecover() interface{} {
	return recover()
}
func main() {
    defer MyRecover()
    panic(1)
}
//	5: 必须要在defer的函数中调用
func main() {
    defer recover()
    panic(1)
}
//	6: 正确
func main() {
    defer func() {
        if r := recover(); r != nil { ... }
    }()
    panic(nil)
}
```





# goroutine

 goroutine的最佳实现

**来源链接：**[GCTT 出品 | Go 语言中的同步队列](https://mp.weixin.qq.com/s/HoslXrqteioDzpjzWb8UvQ)

**思想：**不要通过共享内存来通讯，而要通过通讯来共享内存。

下面这个案例可以更好体验这个思想：

假设我们在运营一家 IT 公司，公司里面有程序员和测试员。为了给个机会他们互相认识对方，并且让他们能够在工作中放松一点，我们买了一个乒乓球台，并且制定了如下规则：

- 每次只能两个人（不能少于或多于两人）玩。
- 只有上一对玩家结束了，下一对玩家才能玩，也就是说，不能只换下一个人。
- 只能是测试员和程序员组成一对来玩，（不能出现两个测试员或者两个程序员一起玩的情况）。如果员工想要玩的话，那么他得等到有合适的对手了才能开始游戏。

```go
func test() {
   work()
}
func code() {
   work()
}
func work() {
   // Sleep up to 10 seconds.
   time.Sleep(time.Duration(rand.Intn(10000)) * time.Millisecond)
}
func pingPong() {
   // Sleep up to 2 seconds.
   time.Sleep(time.Duration(rand.Intn(2000)) * time.Millisecond)
}
func tester(q *queue.Queue) {
   for {
       test()
       q.StartT()
       fmt.Println("Tester starts")
       pingPong()
       fmt.Println("Tester ends")
       q.EndT()
   }
}
func programmer(q *queue.Queue) {
   for {
       code()
       q.StartP()
       fmt.Println("Programmer starts")
       pingPong()
       fmt.Println("Programmer ends")
       q.EndP()
  }
}
func main() {
   q := queue.New()
   for i := 0; i < 10; i++ {
       go programmer(q)
  }
   for i := 0; i < 5; i++ {
       go tester(q)
  }
   select {}
}

// 用goroutine的方式实现queue
package queue
const (
   msgPStart = iota
   msgTStart
   msgPEnd
   msgTEnd
)
type Queue struct {
   waitP, waitT   int
   playP, playT   bool
   queueP, queueT chan int
   msg            chan int
}
func New() *Queue {
   q := Queue{
       msg:    make(chan int),
       queueP: make(chan int),
       queueT: make(chan int),
   }
   go func() {
       for {
           select {
           case n := <-q.msg:
               switch n {
               case msgPStart:
                   q.waitP++
               case msgPEnd:
                   q.playP = false
               case msgTStart:
                   q.waitT++
               case msgTEnd:
                   q.playT = false
              }
               if q.waitP > 0 && q.waitT > 0 && !q.playP && !q.playT {
                   q.playP = true
                   q.playT = true
                   q.waitT--
                   q.waitP--
                   q.queueP <- 1
                   q.queueT <- 1
              }
          }
      }
  }()
   return &q
}
func (q *Queue) StartT() {
   q.msg <- msgTStart
   <-q.queueT
}
func (q *Queue) EndT() {
   q.msg <- msgTEnd
}
func (q *Queue) StartP() {
   q.msg <- msgPStart
   <-q.queueP
}
func (q *Queue) EndP() {
   q.msg <- msgPEnd
}
```

用互斥锁的实现方式：

```go
func test() {
   work()
}
func code() {
   work()
}
func work() {
   // Sleep up to 10 seconds.
   time.Sleep(time.Duration(rand.Intn(10000)) * time.Millisecond)
}
func pingPong() {
   // Sleep up to 2 seconds.
   time.Sleep(time.Duration(rand.Intn(2000)) * time.Millisecond)
}
func tester(q *queue.Queue) {
   for {
       test()
       q.StartT()
       fmt.Println("Tester starts")
       pingPong()
       fmt.Println("Tester ends")
       q.EndT()
   }
}
func programmer(q *queue.Queue) {
   for {
       code()
       q.StartP()
       fmt.Println("Programmer starts")
       pingPong()
       fmt.Println("Programmer ends")
       q.EndP()
  }
}
func main() {
   q := queue.New()
   for i := 0; i < 10; i++ {
       go programmer(q)
  }
   for i := 0; i < 5; i++ {
       go tester(q)
  }
   select {}
}

// 用互斥锁的方式实现 queue
package queue

import "sync"

type Queue struct {
   mut                   sync.Mutex
   numP, numT            int
   queueP, queueT, doneP chan int
}

func New() *Queue {
   q := Queue{
       queueP: make(chan int),
       queueT: make(chan int),
       doneP:  make(chan int),
  }
   return &q
}

func (q *Queue) StartT() {
   q.mut.Lock()
   if q.numP > 0 {
       q.numP -= 1
       q.queueP <- 1
  } else {
       q.numT += 1
       q.mut.Unlock()
       <-q.queueT
  }
}

func (q *Queue) EndT() {
   <-q.doneP
   q.mut.Unlock()
}

func (q *Queue) StartP() {
   q.mut.Lock()
   if q.numT > 0 {
       q.numT -= 1
       q.queueT <- 1
  } else {
       q.numP += 1
       q.mut.Unlock()
       <-q.queueP
  }
}

func (q *Queue) EndP() {
   q.doneP <- 1
}
```

自己手绘的结构图：

![1c78261d5d6eb698190276eb32eeb54](assets/1c78261d5d6eb698190276eb32eeb54.jpg)





# channel

 无缓冲chan中包含的GO并发内存模型

1. 发送数据前接收方必须准备好，如果没有准备好会出现死锁； 
2. 接收数据完成之前，接收端必须发送已经结束信号，保证接收的数据完整； 

<font color=red>上述两点是并发模型的重要保证</font>



 channel的使用

## 使用时需要遵守的原则

1. 不要在接收端关闭channel

   > 在接收端关闭channel，很容易出现发送端往已关闭的channel发送数据的错误

2. 不要关闭有多个并发sender的channel

3. 只有在确保是最后一个sender的情况下，才可以关闭channel

**判断使用sync还是channel**

1. 

2. 是否转移数据的所有权
3. 您是否试图保护结构体的内部状态
4. 你是否试图协调多个逻辑片段?

## 使用技巧

1. **判断chan是否关闭：**从该 chan 接收数据会立刻返回，同时可以加入第二个参数，判断是关闭了还是正常数据返回，即：`x, ok :=<-c` ，这时候 ok 是 false。因为此特性，发送方可 close 一个 chan 用于向接收方广播（广播通道关闭的信号）； 
   **注意：**只有接收方才可以使用该方法判断channel是否关闭，发送端没有判断channel是否关闭的方法

2. 如果业务逻辑上，必须要在 receiver 端关闭channel，可以使用recover来保全程序不被down掉，例码：

   ```go
   callback := func(data *tools.EventData) bool {
       defer func() {
           if recover()!=nil{
               log.Error("write to closed channel!")
           }
       }()
       candidatePubKey := data.Data.([]byte)
       //	用来避免往已经关闭的channel写数据
       if needData {
           RePubKey <- candidatePubKey
       }
       return false
   }
   ```
   
   
    **还有一种更加优雅的方式**：启动一个专门通知关闭信号的channel，用来让receiver通知sender关闭channel；这里实现起来不难，例码就不写了。
   
3. **避免多次关闭channel的技巧：**使用`sync.Once`来关闭channel，例码：

   ```go
   type MyChannel struct {
       C    chan T
       once sync.Once  // 这里
   }
   
   func NewMyChannel() *MyChannel {
       return &MyChannel{C: make(chan T)}
   }
   
   func (mc *MyChannel) SafeClose() {
       mc.once.Do(func(){
           close(mc.C)
       })
   }
   ```

   

## 使用注意事项

1. 往一个 nil chan 发送数据会永远阻塞

2. close一个channel的时候，会往channel发送一个 nil值信号(如果是内置类型 int bool string等，发送编译器默认初始值)，看例码：

   ```go
   package main
   
   import "fmt"
   
   type T int
   
   func IsClosed(ch <-chan T) bool {
   	select {
   	case a:=<-ch:
   		fmt.Println(a)
   		return true
   	default:
   	}
   	return false
   }
   
   func main() {
   	c := make(chan T)
   	fmt.Println(IsClosed(c)) // false
   	close(c)
   	fmt.Println(IsClosed(c)) // true
   }
   ```

   此例码不适合用于判断通道是否关闭，因为`IsClosed`也可能接收到正常的数据，并不能保证一定是close函数的关闭信号。
   
3. chan 关闭后，往该 chan 发送数据会导致 runtime panic；

4. 关闭已经关闭的channel会导致panic

以上内容大部分采自：[优雅关闭channel的文章在这里](https://www.jianshu.com/p/d24dfbb33781)



**使用Goroutine和channel实现Js中的Eventloop**
结构图：

![64a43dc70aa95d876526d79f0aa8881](assets/64a43dc70aa95d876526d79f0aa8881.jpg)





# Interface

 源码：

```go
type iface struct {
    tab  *itab // 代表类型
    data unsafe.Pointer  // 代表数据
}
```

接口本质上是一个结构体，只有两个字段都为nil的时候，该接口才能为nil。我们平时的错误处理error类型也是一个接口，而且经常用 `==nil` 来判断是否有出错，所以在这点上要格外注意。


 设计接口需要考虑的事项

1. 在设计接口时，接口的定义应该放在使用该接口的包中，而不是实现了该接口的包中；
2. 实现包应该返回具体的类型（通常是指针或结构体值），这样，实现包能够方便地增加新方法；
   比如，os 包中的 File 类型实现了 io 包中的 io.Writer，Open 等方法返回的是具体的 File 指针，而不是 Writer 等接口；
3. 应该有真实的某接口使用场景，才定义接口，在没有具体的使用场景情况下，是否有必要定义接口很难判断，更难决定接口中应该包含什么方法。这里的意思是，一般来说，先定义具体的类型，在此基础上才能够更好地抽象出接口，尤其是没有具体的接口使用场景下，别凭空想象出一个接口。



 判断对象是否实现了某接口

例码：

```go
type MyWriter struct{}
func (m *MyWriter) Write(p []byte) (n int, err error) {
	return 0, nil
}
// 声明一个匿名变量
var _ io.Writer = (*MyWriter)(nil)
```

**解释：**

1. 检查 *MyWriter 是否实现了 io.Writer 接口
2. (*MyWriter)(nil) 是将nil强转为 *MyWriter 类型
3. 若*MyWriter没有实现 io.Writer 接口，编译器会直接报错

 接口的类型断言

```go
i := x.(int)
//	为了避免panic，可加上第二个参数判断是否断言成功
v, ok := x.(T)
```

**注意：**

1. 断言时，x 必须是接口（只要是接口就可以，不在乎是不是空接口）；
2. T 可以是类型或**接口**；

## 断言在switch中的使用（类型选择）

```go
switch x.(type) {
    // case
}
switch i := x.(type) {
    // case
}
```

1. x.(type) 中的 type 是固定的，只能是 type 这个关键词；
2. 不允许使用 fallthrough 语句；
3. x 必须是接口，每一个 case 中的类型必须实现了 x 接口；
4.  当匹配到具体某个 case 时，i 即为 x 中该类型的值。



# array

## 特点

- 一组特定长度的连续存储空间
- 值传递

## 从数组中切割出切片

以下对数组的操作返回的是**切片**，例码：

```go
func TestArrayTraverse(t *testing.T) {
	arr := [...]int{2, 4, 6, 8, 10}
	//	arr01是 切片不是数组
	arr01 := arr[2:4]
	fmt.Println(arr01)
}
```



# slice
## 源码

```go
type slice struct {
	array unsafe.Pointer
	len   int
	cap   int
}
```

切片本质上是一个结构体，底层由长度、容量 和 底层数组 组成。

切片本质上是值传递，因为结构体就是值传递。但是因为array字段不会改变，所以会造成引用传递的假象。

看如下例子：

```go
func main() {
	arr := []int{2, 3, 4}
	fmt.Printf("函数前：%p\n", arr)
	printSlice(arr)
	fmt.Println(arr)
}
func printSlice(arr []int) {
	fmt.Printf("函数中：%p\n", arr)
	arr= append(arr, 5)
	fmt.Printf("函数中：%p\n", arr)
}
```

- 三次打印的结果中，前两次是一样的，第三次是不一样的
- 子函数里边虽然改变了arr的值，但是main函数里边打印的arr仍然是{2, 3, 4}

有这两点足以说明，切片是值传递，并非引用传递。



## 空slice

1. `var t []string` 为 nil slice，编码json时 nil slice 会编码为 null；
2. ` t := []string{}` 为 non-nil slice，长度为0，会编码为 JSON 的数组 []；
3. `a := []int(nil)` 也为 nil slice；



## slice的扩容

1. 首先由编译器判断slice是否需要扩容
2. 若编译器无法判断，在runtime时用len和cap判断



## 常见坑

1. 以下代码输出什么：

   ```go
   func main() {
       s := make([]int, 5)
       s = append(s, 1, 2, 3)
       fmt.Println(s)
   }
   ```

   输出： [0,0,0,0,0,1,2,3]

   - make后会给每个元素赋上初始值0，append往后追加
   - 所以用make进行初始化的时候往往长度设为0；或者提前设置好长度，用下标操作各元素进行赋值
   - 使用new初始化的时候，返回的是一个指针类型变量



# append





# make

 源码：

```go
func make(t Type, size ...IntegerType) Type
```

 使用细节：

1. 输入的是什么类型，返回的就是什么类型
2. 当size不为0时，会默认给每个元素赋值(类型默认值，如：int是0，bool是false...)



# new

 源码：

```go
func new(Type) *Type
```

 使用细节：

1. 返回一个输入类型的指针类型
2. new出来的变量有地址，但是地址存储的值为nil，可以直接赋值



# reflect

1. 对于无法直接通过 `==` 进行比较的，可以通过 reflect.DeepEqual 进行比较；slice、map 等都可以通过该方法进行比较；reflect.DeepEqual 不会认为空的 slice 与 `nil` 的 slice 相等，这和 bytes.Equal() 的行为相反。
2. 反射是一种检测存储在 interface 中的类型和值的机制。这可以通过 reflect.TypeOf 函数和 reflect.ValueOf 函数得到；
3. 将 reflect.ValueOf 的返回值通过 Interface() 函数反向转变成 interface 变量；
4. 如果需要修改一个反射对象，反射对象里边存储的必须是原对象的指针，不然修改对原对象无效。
5. interface、reflect.Type 和 reflect.Value 三者的关系图。
   ![Fih7EdexaDpZjEziIZmAubKS-oU7](assets/Fih7EdexaDpZjEziIZmAubKS-oU7.jpg)



# range

 底层源码

```go
// 变量提前被赋值到一个变量
for_temp := range
// 遍历之前，就获取切片的长度
len_temp := len(for_temp)
for index_temp = 0; index_temp < len_temp; index_temp++ {
    // 还是按照++的逻辑把元素一个一个取出来
    value_temp = for_temp[index_temp]
    // 将index和value赋值
    index = index_temp
    value = value_temp
    original body
}
```



 常见坑

1. 下面代码会死循环吗？

   ```go
   func main() {
   	v := []int{1, 2, 3}
   	for i := range v {
   		v = append(v, i)
   	}
   }
   ```

   **答案: **不会，因为在遍历之前，len就已经被取出，遍历的次数不会因为 遍历时的长度改变 而改变。

2. 下面代码输出什么：

   ```go
   slice := []int{0, 1, 2, 3}
   myMap := make(map[int]*int)
   
   for index, value := range slice {
   	myMap[index] = &value
   }
   fmt.Println("=====new map=====")
   for k, v := range myMap {
   	fmt.Printf("%d => %d\n", k, *v)
   }
   ```

   **答案：**

   ```go
   =====new map=====
   0 => 3
   1 => 3
   2 => 3
   3 => 3
   ```

   注意第5行将value的地址存在map中，而value在底层 相对于for循环 是一个全局变量，不会遍历一次声明一次，所以每次遍历 value的地址 是一样的，这样当遍历map的时候，输出的都是同一个地址的值，所以都是3。



# 类型

## 类型别名与类型定义

### 类型别名

以下代码使用的就是类型别名，例码：

```go
type MyInt2 = int
```



### 类型定义

以下代码使用的就是类型定义，例码：

```go
type MyInt1 int
```



### 两者区别

使用**类型别名**时，上例中的 MyInt2 就相当于 int，只是换了个名字，所以以下代码是不会报错的：

```go
type MyInt2 = int
func main() {
    var i int =0
    var i1 MyInt2 = i 
}
```

但是使用**类型定义**时，就需要注意，看例码：

```go
type MyInt1 int
func main() {
    var i int =0
    var i1 MyInt1 = i // 这里编译不通过，需要进行强转，像下边行一样
    var i1 MyInt1 = MyInt1(i)
}
```





## 命名类型 和 未命名类型

Go 中的类型可以分为命名类型（named type） 和 未命名类型（unnamed type）。

### 问题提出

由上边的知识点我们可以知道，下边代码编译是不通过的：

```go
type MyInt1 int
func main() {
    var i int =0
    var i1 MyInt1 = i // 这里编译不通过
}
```

但是下边的这种情况又是可以通过编译的：

```go
type MyMap1 map[string]string
func main() {
    var myMap = map[string]string{"name": "shalom"}
    var myMap1 MyMap1 = myMap // 注意这一行
    fmt.Println(myMap1)
}
```

这个原因就和 命令类型 未命名类型 有关。



### 命名类型

命名类型包括 bool、int、string 等

### 未命名类型

array、slice、map 等和 **具体元素类型**、**长度** 等有关的，属于未命名类型。



### 上述问题的答案

具有 相同声明的未命名类型 被视为同一类型。如：

- 具有相同基类型的指针。
- 具有相同元素类型和⻓度的 array。
- 具有相同元素类型的 slice（注意slice没有包含长度）。
- ……

命名类型 和 非命名类型 可以相互赋值，只要基础类型一样就可以。

`map[string]string` 属于未命名类型，他可以和 `MyMap1` 相互赋值，因为他们的基础类型一样。



## 常见坑

下边这道题包含了上边两部分的知识：

```go
package main
import (
    "fmt"
)
func main() {
    type MyMap1 map[string]string
    type MyMap2 map[string]string
    var myMap = map[string]string{"name": "polaris"}
    var myMap1 MyMap1 = myMap
    var myMap2 MyMap2 = myMap1  // 此处会报错
    fmt.Println(myMap2)
}
```

1. `type MyMap map[string]string`  规定了元素的类型必须是string，不能再更改，所以就属于命名类型；
2. 既然属于命名类型，那么第二题中的 `MyMap1  MyMap2`就是两个不同类型，不能够相互赋值；
3. 命名类型 和 非命名类型 可以相互赋值，只要基础类型一样就可以。





# 函数

 绑定在类型上的方法

1. 绑定在指针类型变量的方法，当变量无法取地址时，不能调用；**注意：**临时值不能够取地址

   ```go
type duration int
   
   func (d *duration) pretty() string {
       return fmt.Sprintf("Duration: %d", *d)
   }
   
   func main() {
       // duration(42)无法取地址，所以不可以调用pretty的方法
       duration(42).pretty()
   }
   ```
   
2. 绑定在某一类型的方法，只要声明了该类型的变量，就可以调用绑定的方法；但是不能够操作变量的字段，必须要初始化后，才能够操作字段，因为没有初始化，该变量就没有内存地址，没有地址就无法操作；
3. 值类型的变量可以调绑定在指针类型的方法，但是在判断是否实现某一接口的时候，会出现：指针类型实现了，值类型没有实现的情况；





1. Go 语言规范规定：`append cap complex imag len make new real unsafe.Alignof unsafe.Offsetof unsafe.Sizeof ` 以上这些 builtin 函数必须接收返回值；而自定义的函数可以不用接收返回值。
2. 
3. 




 函数的各种使用场景

## 最普通的包级别函数

```go
package lib

func Sum(a, b int) int {
    return a + b
}
```

## 在函数内定义函数

```go
package main

func main() {
    // 注意定义方式，不能 func sum(a, b int) int {} 这种形式
    sum := func(a, b int) int {
        return a + b
    }
}
```

这种形式，一般用于函数内部重复代码的抽取，而这些逻辑在其他函数不会用到，没必要提升为包级别函数。可以在标准库源码中搜索：`:= func` 查到相关使用示例。

## 匿名函数

```go
// 保留两个 b 之间（包括 b）的字符串
strings.TrimFunc("abcfbd", func(r rune) bool {
    if r != 'b' {
        return true
    }
    return false
})
```

匿名函数常用于回调函数，函数返回值是函数的场景。比如 HTTP 中间件经常见到类似这样的代码：

```go
return func(resp http.ResponseWriter, req *http.Request) {
    // TODO:
}
```

匿名函数另外常见的场景是用于 go 语句和用于 defer 语句。

## 定义函数类型

函数作为参数和返回值，一般来说，为了增强可读性（可能还有其他考虑），在这两种场景下，我们经常会定义函数类型，典型的是 net/http 包的 HandleFunc 类型：

```go
type HandlerFunc func(ResponseWriter, *Request)
```

然后让 HandlerFunc 实现 Handler 接口，也就是实现 `ServeHTTP(ResponseWriter, *Request)` 方法，它的实现只需要简单的调用自己即可：

```go
func (f HandlerFunc) ServeHTTP(w ResponseWriter, r *Request) {
    f(w, r)
}
```

这样，任何符合 HandlerFunc 类型的函数，都可以通过强制类型转换为 HandlerFunc，进而满足 Handler 接口，**这是一个很好地技巧**。

## 函数表达式

```go
myPrintln := fmt.Println
myPrintln("Hello World!")
```

**注意: **下列一些内置的函数不能当做表达式使用：

```go
append cap complex imag len make new real unsafe.Alignof unsafe.Offsetof unsafe.Sizeof
```

## 函数作为 数组、slice、map 或 chan 的元素

**注意**：根据 map 对 key 的要求，函数不能用作 map 的 key。

这下面种场景下，为了可读性，一般也会定义函数类型。如：

```go
func main() {
    type myfunc func()

    m := make(map[string]myfunc)
    m["abc"] = func(){
        fmt.Println("abc")
    }

    for _, f := range m {
        f()
    }
}
```





# 可寻址

1. 直接值（临时值）不能取地址；
   如：&true、&"abc"、&math.Int() 等都是非法；

2. 字符串字节元素不能取地址；
   如：

   ```go
   s: = "Hello World"
   _ = &(s[5])
   ```

3. map 元素不能取地址；
   如：

   ```go
   m := map[int]int{99:1}
   _ = &(m[99])
   ```

4. 编译器只会自动对 **变量** 取地址，而不会自动对 直接值 取地址；
   如：

   ```go
   type T struct{}
   func (t *T) f() {}
   func main() {
       t := T{}
       (&t).f() // ok ，和下一句等价 
       t.f()  // ok ，将自动取地址
   
       (&T{}).f() // ok
       // T{}.f() // error
       // 不会自动取地址
   }
   ```
   那么为什么经常见到 `&T{}` 这种写法？`&T{}` 是为了编程方便，添加的一个语法糖 ，是下面形式的缩写，而不是临时值不能取地址的一个例外。
   ```go
   temp := T{}
   &temp
   ```

5. new(T) 相当于取T的地址，等价于&T{}。



# 闭包

```go
func Closure() func() int {
    var x int
    return func() int {
        x++
        return x
    }
}
```

调用这个函数会返回一个函数变量。`i := Closure()`：通过把这个函数变量赋值给 `i`，`i` 就成为了一个**闭包**。

**注意：** `i` 保存着对 `x` 的引用，可以理解 `i` 中有着一个指针指向 x 或 **i 中有 x 的地址**。由于 `i` 有着指向 `x` 的指针，所以可以修改 `x`。



# 垃圾回收

[看这篇文章，后边再总结](https://www.jianshu.com/p/8b0c0f7772da)

