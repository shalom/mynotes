# 集合类

**如何选用集合类？**

1、要求线程安全，使用Vector、Hashtable

2、不要求线程安全，使用ArrayList,LinkedList,HashMap

3、要求key和value键值，则使用HashMap,Hashtable

4、数据量很大，又要线程安全，则使用Vector



## 遍历

遍历选择迭代器 Iterator





# 文件操作（io）

java是通过流的概念来操作文件的。Java中的流分为两种：

- **字节流**：可以用于读写二进制文件（图片，exe文件，mp3）及任何类型文件
- **字符流**：可以用于读写文本文件，不能操作二进制文件

> 文件大体可分为两类：文本文件 和 二进制文件。



而两种流对应的顶级输入输出对象看下表：

|      |    字节流    | 字符流 |
| :--: | :----------: | :----: |
| 输入 | InputStream  | Reader |
| 输出 | OutputStream | Writer |



## File对象

File对象代表的是一个文件数据源，文件流对象中最为重要的File类，对File了解后对子类理解会更加容易。

```java
package com.shalom.javase;

import java.io.File;

public class FileObject {
    public static void main(String[] args) {
        //  创建一个文件对象"C:\\Users\\Administrator\\Desktop\\temNote.txt"
        File f = new File("C:\\Users\\Administrator\\Desktop\\temNote.txt");

        //  得到文件的路径
        String path = f.getAbsolutePath();
        System.out.println("the file path: "+path);
        //  得到文件的大小,字节数
//        long totalSpace = f.getTotalSpace();totalSpace:%d
        long length = f.length();
        System.out.printf(" length:%d\n",length);
        //  创建文件夹
        File dir = new File("C:\\Users\\Administrator\\Desktop\\hahahahahahahaha");
        if(dir.isDirectory())
        {
            System.out.println("the directory has existed!");
        }else {
            dir.mkdir();
        }

        //  列出一个文件夹下面的所有文件
        File desktop = new File("C:\\Users\\Administrator\\Desktop");
        if(desktop.isDirectory()) {
            String[] list = desktop.list();
            for (int i = 0; i < list.length; i++)
            {
                System.out.println(list[i]);
            }

            File[] files = desktop.listFiles();
            System.out.println(files.length);
        }else {
            System.out.println("this file is not a directory.");
        }
    }
}
```

**注意：**

- File 对象没有读写的功能，只单纯表示一个文件对象；

