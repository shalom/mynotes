# Java笔记

## IDEA的使用

#### 鼠标移动显示文档

鼠标移动到使用的api上边，就可以显示出对应的官方文档的信息。

Generate里边设置

## 底层基础

### 枚举类

只暴露若干个常量对象的类

#### 自定义的枚举类

```java
//自定义枚举类
class Season{
    //1.声明Season对象的属性:private final修饰
    private final String seasonName;
    private final String seasonDesc;

    //2.私化类的构造器,并给对象属性赋值
    private Season(String seasonName,String seasonDesc){
        this.seasonName = seasonName;
        this.seasonDesc = seasonDesc;
    }

    //3.提供当前枚举类的多个对象：public static final的
    public static final Season SPRING = new Season("春天","春暖花开");
    public static final Season SUMMER = new Season("夏天","夏日炎炎");
    public static final Season AUTUMN = new Season("秋天","秋高气爽");
    public static final Season WINTER = new Season("冬天","冰天雪地");
}
```

以上代码只暴露4个常量对象

#### 使用enum定义枚举类

```java
//使用enum关键字枚举类
enum Season1 {
    //1.提供当前枚举类的对象，多个对象之间用","隔开，末尾对象";"结束
    SPRING("春天","春暖花开"){},
    SUMMER("夏天","夏日炎炎"){},
    AUTUMN("秋天","秋高气爽"){},
    WINTER("冬天","冰天雪地"){};

    //2.声明Season对象的属性:private final修饰
    private final String seasonName;
    private final String seasonDesc;

    //2.私化类的构造器,并给对象属性赋值
    private Season1(String seasonName,String seasonDesc){
        this.seasonName = seasonName;
        this.seasonDesc = seasonDesc;
    }
}
```







# java开发环境安装

[java开发环境安装](https://www.runoob.com/java/java-environment-setup.html)









## SSM

### 创建Maven工程

创建后的工程目录如下（eclipse中）：

![1564929814248](assets/1564929814248.png)

其中有添加XML配置文件在 前端页面文件夹中。 



####   Maven 的配置文件问题
配置文件中的镜像设置，需要改成国内可以使用的镜像。<mirrors>

还有profiles中的jdk版本需要修改。



### 引入项目依赖的jar包

在项目中 pom.xml 文件中引入依赖包

打开[Maven的仓库](https://mvnrepository.com/)

#### Spring的依赖包

1. 搜索 spring jdbc ，与下边的 spring webmvc 的版本一致，同样复制里边的xml

2. 搜索 spring aspects ,版本一致



#### SpringMVC的依赖包

搜索 spring webmvc ，选定版本，**注意**：这里版本需要选择3个框架相互兼容的版本。

点进去复制里边的XML到pom.xml中

![1564963538558](assets/1564963538558.png)



#### Mybatis的依赖包

1. 搜索 Mybatis ，选定版本，复制XML；
2. 搜索 Mybatis spring，这是与Spring的适配包，这里的版本号不需要一致；



#### 数据库的依赖包

1. 数据库连接池：搜索 c3p0 ,选择下边这个，选版本，复制XML
   ![1564964131323](assets/1564964131323.png)

2. 搜索 MySQL ，选第一个



#### 其他

先引入：jstl , servlet-api , junit 

添加 servlet-api 的时候，记得添加一行，标记为人家提供的

```xml
<scope> provided </scope>
```



### 使用bootstrap

1. 下载bootstrap，解压复制到项目目录中，可以单独创建一个文件夹
2. 然后再在index.jsp文件中引入，看bootstrap的官方文档





## IDEA构建SSM项目（Maven）

要提前下载好Maven的压缩包

### 创建父工程

1. 先创建一个空白项目，**注意是空白项目**

2. 创建父工程（**注意：**这里一定要点创建module，如果选择project，那么空白项目就白创建了）
   ![1564979469265](assets/1564979469265.png)

   ![1564979705028](assets/1564979705028.png)
   
3. 因为他是父工程，在 pom.xml 文件中，添加如下行（项目打包方式）：

   ```xml
   <packaging>pom</packaging>
   ```

   父工程中不写代码，可以将src删除



### 配置Maven

setting -> 搜索Maven -> 设置：User setting file 到自己下载好的Maven的配置文件中，仓库的位置应该会自动匹配到Maven压缩包解压后的Respository



### 创建子工程

一共有 5 个子工程，分别是：edu-model，edu-dao , edu-service , edu-manager，edu-api

1. 右击父工程，创建一个module，将复选框勾上，并选择quickstart组件，若创建的是web项目，则选择下边的 webapp；**注意：**这里必须要提前将Maven配置好

   ![1564980478517](assets/1564980478517.png)

   ![1564981265743](assets/1564981265743.png)


   ![1564981361340](assets/1564981361340.png)

   

2. 确定子工程的目录位置，点击finish，就会看到IDEA自动在下载和执行一些东西；

3. 继续创建edu-dao , edu-service , edu-manager(这是web工程)，edu-api(同样是web工程，提供数据给安卓和IOS的)

4. 在 edu-model , edu-dao , edu-service 的配置文件（pom.xml）中 项目id下方一行 添加：

   ```xml
   <packaging>jar</packaging>  # 最终希望打包成jar包的意思
   ```

   在 edu-manager，edu-api 中添加：

   ```xml
   <packaging>war</packaging>	# 最终希望打包成war包
   ```



#### 管理公共依赖

公共依赖放在父工程的 pom文件中

- 在子项目中的pom配置文件中，都有对 junit 的依赖，所以可以在父工程配置文件中统一配置，子工程中就可以删除；

  ```xml
  <dependencies>
      <dependency>
          <groupId>junit</groupId>
          <artifactId>junit</artifactId>
          <version>4.11</version>
          <scope>test</scope>
      </dependency>
  </dependencies>
  ```



- 公共版本号管理

  ```xml
  <properties>
      <junit.version>4.11</junit.version>
  </properties>
  ```

  使用 properties 字段，设定好后，上边的version就可以写成  

  ```xml
  <version>${junit.version}</version>
  ```

  







### 子工程edu-manager

#### 完善目录结构

#### 依赖管理

这个子工程其实是Controller层，所以需要依赖 SpringMVC 框架，而其他子项目并不需要依赖 SpringMVC ，所以不添加到父工程中，所以我们就在此项目的 pom文件中添加 springMVC 的依赖。需要添加如下依赖包，主要是spring和springMVC的依赖包（SpringMVC 同时依赖 Spring），添加后Maven会自动下载。

```xml
<dependencies>
    <!--  1. 添加spring/springmvc的依赖  start =======================-->
    <!-- aop -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-aop</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- aspects -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-aspects</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- beans -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-beans</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- context -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-context</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- core -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-core</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- expression -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-expression</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- jdbc -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-jdbc</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- orm -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-orm</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- tx -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-tx</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- test -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-test</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- web -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-web</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- webmvc -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-webmvc</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <!--  添加spring/springmvc的依赖  end =======================-->
</dependencies>
```



对应的版本号：

```xml
<spring.version>4.3.13.RELEASE</spring.version>
```





#### 配置manager的Tomcat

这个我清楚，就不做笔记了，配好后启动一下，浏览器出现一个 hello world



#### 编写配置文件

1. 将源码文件夹和配置文件夹创建好

   ![1570865160957](assets/1570865160957.png)

2. 在 resource 文件夹中创建 springmvc.xml 文件，文件内容如下：

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <beans xmlns="http://www.springframework.org/schema/beans"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xmlns:mvc="http://www.springframework.org/schema/mvc"
          xmlns:context="http://www.springframework.org/schema/context"
          xmlns:aop="http://www.springframework.org/schema/aop" xmlns:tx="http://www.springframework.org/schema/tx"
          xsi:schemaLocation="http://www.springframework.org/schema/beans
                       <!-- 这里的版本要一致，目前是4.3版本 -->        
         http://www.springframework.org/schema/beans/spring-beans-4.3.xsd
         http://www.springframework.org/schema/mvc
         http://www.springframework.org/schema/mvc/spring-mvc-4.3.xsd
         http://www.springframework.org/schema/context
         http://www.springframework.org/schema/context/spring-context-4.3.xsd
         http://www.springframework.org/schema/aop
         http://www.springframework.org/schema/aop/spring-aop-4.3.xsd
         http://www.springframework.org/schema/tx
         http://www.springframework.org/schema/tx/spring-tx-4.3.xsd">
   
       <!-- 1.注解扫描位置-->
       <context:component-scan base-package="com.shalom.edu.web.controller"/>
   
       <!-- 2.配置映射处理和适配器-->
       <bean class="org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping"/>
       <bean class="org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter"/>
   
       <!-- 3.视图的解析器-->
       <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
           <property name="prefix" value="/WEB-INF/views/"/>
           <property name="suffix" value=".jsp"/>
       </bean>
   </beans>
   ```

   **注意：**

   - 配置文件中 spring 的版本要和项目依赖的一致；
   - 注解扫描的包要根据项目做更改；



3. 编写 Controller 包中的 UserController 类；

4. 在 web.xml 文件中配置SpringMVC的入口

   ```xml
    <!-- 配置springmvc-->
   <servlet>
       <servlet-name>DispatcherServlet</servlet-name>
       <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
       <init-param>
           <param-name>contextConfigLocation</param-name>
           <param-value>classpath:springmvc.xml</param-value>
       </init-param>
       <load-on-startup>1</load-on-startup>
   </servlet>
   <servlet-mapping>
       <servlet-name>DispatcherServlet</servlet-name>
       <!--  注意这里加了 .do  -->
       <url-pattern>*.do</url-pattern>
   </servlet-mapping>
   ```

   加上后，启动服务器，浏览器访问：http://localhost:8080/edu-manager/user/login.do 注意这里有 `.do`，看看能否访问成功。



### 子工程edu-dao

#### 依赖管理

dao 层需要依赖数据模型 model 层，所以在pom文件中添加依赖：

```xml
<dependencies>
    <dependency>
        <groupId>com.shalom.edu</groupId>
        <artifactId>edu-model</artifactId>
        <version>1.0-SNAPSHOT</version>
    </dependency>
</dependencies>
```

同时，dao层后边会用到 Mybatis 的映射文件，所以需要添加 Mybatis的依赖：

```xml
<!-- 依赖Mybatis -->
<dependency>
    <groupId>org.mybatis</groupId>
    <artifactId>mybatis</artifactId>
    <version>${mybatis.version}</version>
</dependency>

<!-- 父工程的pom文件中添加版本号 -->
<mybatis.version>3.5.0</mybatis.version>
```



#### 添加映射接口和映射文件

base 映射接口

```xml
public interface BaseMapper<T> {

    public T findByID(Integer id);
    public T findByUUID(String uuid);

    public void deleteByID(Integer id);
    public void deleteByUUID(String uuid);

    public void update(T t);

    public void insert(T t);

}
```



User 映射接口，继承 BaseMapper

```xml
public interface UserMapper extends BaseMapper<User> {

}
```



User 映射文件

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.shalom.edu.mapper.UserMapper" >

    <select id="findByID" resultType="user" parameterType="int">
        SELECT * FROM t_user WHERE id = #{id}
    </select>
</mapper>
```



### 子工程edu-service

#### 依赖管理

service依赖dao层

```xml
<dependencies>
    <dependency>
        <artifactId>edu-dao</artifactId>
        <groupId>com.shalom.edu</groupId>
        <version>1.0-SNAPSHOT</version>
    </dependency>
</dependencies>
```



依赖 Spring 的三个包

```xml
<!-- beans -->
<!-- @Autowired 注解需要用到此包 -->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-beans</artifactId>
    <version>${spring.version}</version>
</dependency>

<!-- context -->
<!-- @Service 注解需要用到此包 -->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-context</artifactId>
    <version>${spring.version}</version>
</dependency>

<!-- tx -->
<!-- 事务需要用到此包 -->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-tx</artifactId>
    <version>${spring.version}</version>
</dependency>
```



#### 代码编写





### 完善子工程 edu-manager

#### 添加剩余依赖



注意：编译项目的时候，默认不会将xml配置文件编译到jar包中，需要手动设置









## 整合ssm遇到的坑

Mybatis扫描不上

没有在spring配置文件上加上映射文件的配置，需要注意的点：

- spring配置文件配置 Mybatis 核心配置文件 和 映射文件
- spring配置文件要指定 生成代理实现类的接口所在的包
- 映射文件的 namespace 是指向具体的接口



Mybatis错误：NumberFormatException: For input string:"xx"

- 因为在动态SQL中，没有加 .toString()

  ```xml
  <where>
      <if test="custName!=null and custName!=''.toString()">
          cust_name like #{custName}
      </if>
  </where>
  ```

  

## 项目

### 需求分析

#### 前台展示

##### 用户注册

- 系统默认给用户权限等级为7
- 只有一个用户等级为2，等级为2和3的用户都可以给比自己的等级的用户降级和升级，但是不能大于等于自己的等级
- 邮箱验证码后边再加，应该没有这个必要

##### 用户登录

##### 展示文件

- 按日期分类

- 按主题分类
- 按权限筛选
- 不登录无法下载

##### 下载文件

- 判断权限

##### 查找文件

##### 在线播放



#### 后台管理

##### 权限管理

- 给用户分配权限

##### 文件发布

- 设定文件权限等级



### 表的设计

#### 用户表

用户ID							int  自增  主键

用户名							varchar(20)  非空

密码								varchar(30)  非空

所在地区						varchar(30)

年龄								tinyint  unsigned

教训火炬群昵称			varchar(30)  

权限等级						tinyint  unsigned  非空  默认7  最高等级1



#### 文件表

文件ID							int  自增  主键

文件名称						varchar(30)  非空

文件日期						date  非空

文件大小						float  

文件主题						varchar(30)  

Parshah						 varchar(50)

文件需要的最低权限	 tinyint  unsigned  非空



### 业务代码

#### 实现登录

##### 页面编写

用户名  密码  登录  注册

##### 请求处理

查询数据库，正确则登录成功，错误提示登录失败









## Maven

### Maven约定的目录结构

- 根目录：工程名

- src目录：源码

- pom.xml文件：Maven工程的核心配置文件

- main目录：存放主程序

- test目录：存放试程序

- java目录：存放 java 源代码文件

- resource目录：存放框架或其他工具的配置文件



父工程要安装了，子工程才能够相互依赖。





## Maven & SSM

### 创建项目

#### 项目结构

![1567944559585](assets/1567944559585.png)

- 一个项目群中，只能有一个项目打包的形式为war；
- 各个项目的依赖关系如上图箭头所示，Common为公共实现类；

- Parent是父工程



### 集成SSM

将以上项目创建好后，接下来就来集成SSM框架。集成用到的配置文件都是在Web项目中编写。

#### 添加依赖

首先在父工程的pom文件中添加 Spring SpringMVC Mybatis 的依赖

TODO：这些依赖需要总结一下。

由于继承关系，里边所有的子工程对 SSM 的依赖就都设置好了。



#### 集成Spring

##### web.xml编写

最终我们要将项目全部集成到Web项目中，所以在Web项目中集成 Spring。

编辑Web中的 web.xml 文件，里边填写的内容和 我们不使用Maven时候 一样，TODO：需要再总结。

web.xml 相当于 SSM 的入口。



##### spring配置文件编写

TODO：同样需要再总结

```xml
<context:component-scan base-package="com.shalom.*" >
        <context:exclude-filter type="annotation" expression="org.springframework.stereotype.Controller"/>
    </context:component-scan>
```

中间一行是排除 Controller 注解的意思，那扫描配置中为什么要排除`Controller`注解？

答：`Controller`注解的的作用是声明控制器（处理器）类。从数据流转的角度，这个类应该是由`SpringMVC`框架进行管理和组织的，所以不需要由`Spring`框架扫描。



#### 集成SpringMVC

##### web.xml编写



##### springmvc配置文件编写



#### 集成Mybatis

##### 在spring配置文件中加上Mybatis的配置





##### Mybatis配置文件编写





### 