# Spring

## 依赖

- spring-aop ——Spring的面向切面编程,提供AOP(面向切面编程)实现
- spring-aspects —— Spring提供对AspectJ框架的整合
- spring-beans —— SpringIoC(依赖注入)的基础实现
- spring-context —— Spring提供在基础IoC功能上的扩展服务，此外还提供许多企业级服务的支持，如邮件服务、任务调度、JNDI定位、EJB集成、远程访问、缓存以及各种视图层框架的封装等
- spring-context-support —— Spring-context的扩展支持,用于MVC方面
- spring-core —— Spring的核心组件
- spring-expression —— Spring表达式语言
- spring-instrument —— Spring对服务器的代理接口
- spring-instrument-tomcat —— Spring对Tomcat的连接池的集成
- spring-jdbc —— JDBC支持包，包括数据源设置和JDBC访问支持
- spring-jms —— JMS支持包，包括辅助类来发送和接收JMS消息
- spring-messaging —— 信息体系结构和协议的支持
- spring-orm —— 对象/关系映射，整合第三方的ORM框架，如hibernate,ibatis,jdo，以及spring的JPA实现
- spring-oxm —— 对象的XML映射，可以让Java与XML之间来回切换
- spring-test —— 对于单元测试和集成测试的简单封装
- spring-tx —— 为JDBC、Hibernate、JDO、JPA等提供的一致的声明式和编程式事务管理
- spring-web —— SpringMVC支持WEB端应用部署架构
- spring-webmvc —— REST Web服务和Web应用的视图控制器的实现
- spring-webmvc-portlet —— SpringMVC的增强--MVC的实现作为一个Portlet的环境
- spring-websocket —— sockjs WebSocket的实现,包括对 STOMP 的支持
  

| jar文件名            | 作用                                                         |
| -------------------- | ------------------------------------------------------------ |
| spring-webmvc        | 这是SpringMVC的jar包                                         |
| spring-jdbc          | SpringJDBC，我们需要用到，属于依赖的依赖                     |
| spring-test          | Spring的单元测试整合jar，在单元测试的时候会用到              |
| spring-aspects       | Spring的APO模块jar文件                                       |
| mybatis              | mybatis的核心                                                |
| mybatis-spring       | mybatis-Spring的整合适配器                                   |
| c3p0                 | 为了保证项目的效率，引入数据库连接池，同样可以替换为DBPC或者阿里的连接池 |
| mysql-connector-java | mysql的连接器，这个版本号要看你装的mysql的版本，因为我的是8，所以版本可能和你们的有些区别 |
| jstl                 | 引入jsp的JSTL支持                                            |
| javax.servlet-api    | Servlet的支持jar文件                                         |
| junit                | 单元测试需要的jar文件，与spring-test整合在一起               |
| log4j                | 日志jar文件，这个jar在配置后，可以在控制台打印运行时日志，有必要还可以存入文件 |









# SpringMVC





# Mybatis





# SSM





# Maven

## 整合ssm用到的依赖

在父工程的 pom 文件中添加如下依赖

```xml
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

  <dependencies>
    <!-- SpringMVC -->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-webmvc</artifactId>
      <version>4.3.7.RELEASE</version>
    </dependency>

    <!-- Spring-Jdbc -->
    <!-- https://mvnrepository.com/artifact/org.springframework/spring-jdbc -->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-jdbc</artifactId>
      <version>4.3.7.RELEASE</version>
    </dependency>

    <!--Spring-test -->
    <!-- https://mvnrepository.com/artifact/org.springframework/spring-test -->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-test</artifactId>
      <version>4.3.7.RELEASE</version>
    </dependency>

    <!-- Spring面向切面编程 -->
    <!-- https://mvnrepository.com/artifact/org.springframework/spring-aspects -->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-aspects</artifactId>
      <version>4.3.7.RELEASE</version>
    </dependency>


    <!--MyBatis -->
    <!-- https://mvnrepository.com/artifact/org.mybatis/mybatis -->
    <dependency>
      <groupId>org.mybatis</groupId>
      <artifactId>mybatis</artifactId>
      <version>3.4.2</version>
    </dependency>
    <!-- MyBatis整合Spring的适配包 -->
    <!-- https://mvnrepository.com/artifact/org.mybatis/mybatis-spring -->
    <dependency>
      <groupId>org.mybatis</groupId>
      <artifactId>mybatis-spring</artifactId>
      <version>1.3.1</version>
    </dependency>

    <!-- 数据库连接池、驱动 -->
    <!-- https://mvnrepository.com/artifact/c3p0/c3p0 -->
    <dependency>
      <groupId>c3p0</groupId>
      <artifactId>c3p0</artifactId>
      <version>0.9.1</version>
    </dependency>


    <!-- https://mvnrepository.com/artifact/mysql/mysql-connector-java -->
    <dependency>
      <groupId>mysql</groupId>
      <artifactId>mysql-connector-java</artifactId>
      <version>5.1.41</version>
    </dependency>

    
    <!-- （jstl，servlet-api，junit） -->
    <!-- https://mvnrepository.com/artifact/jstl/jstl -->
    <dependency>
      <groupId>jstl</groupId>
      <artifactId>jstl</artifactId>
      <version>1.2</version>
    </dependency>

    <!-- https://mvnrepository.com/artifact/javax.servlet/javax.servlet-api -->
    <dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>javax.servlet-api</artifactId>
      <version>3.0.1</version>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.11</version>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <!-- 这一段暂时不知道是做什么的 -->
  <build>
    <finalName>ssm</finalName>
    <pluginManagement><!-- lock down plugins versions to avoid using Maven defaults (may be moved to parent pom) -->
      <plugins>
        <plugin>
          <artifactId>maven-clean-plugin</artifactId>
          <version>3.0.0</version>
        </plugin>
        <!-- see http://maven.apache.org/ref/current/maven-core/default-bindings.html#Plugin_bindings_for_war_packaging -->
        <plugin>
          <artifactId>maven-resources-plugin</artifactId>
          <version>3.0.2</version>
        </plugin>
        <plugin>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>3.7.0</version>
        </plugin>
        <plugin>
          <artifactId>maven-surefire-plugin</artifactId>
          <version>2.20.1</version>
        </plugin>
        <plugin>
          <artifactId>maven-war-plugin</artifactId>
          <version>3.2.0</version>
        </plugin>
        <plugin>
          <artifactId>maven-install-plugin</artifactId>
          <version>2.5.2</version>
        </plugin>
        <plugin>
          <artifactId>maven-deploy-plugin</artifactId>
          <version>2.8.2</version>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
</project>
```



## 编写配置文件

### 配置web项目的web.xml文件

web.xml 文件相当于是ssm的入口，他里边需要配置 spring 和 springMVC，spring是Mybatis的入口

下面是web.xml的内容：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://java.sun.com/xml/ns/javaee"
         xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd"
         id="WebApp_ID" version="2.5">


  <display-name>Archetype Created Web Application</display-name>

  <welcome-file-list>
    <welcome-file>index.jsp</welcome-file>
  </welcome-file-list>

  <!-- 配置spring -->
  <context-param>
    <param-name>contextConfigLocation</param-name>
    <param-value>classpath:spring/applicationContext-*.xml</param-value>
  </context-param>

  <!-- 配置监听器加载spring -->
  <listener>
    <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
  </listener>

  <!-- 配置过滤器，解决post的乱码问题 放在这个位置不影响 -->
  <filter>
    <filter-name>encoding</filter-name>
    <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
    <init-param>
      <param-name>encoding</param-name>
      <param-value>UTF-8</param-value>
    </init-param>
  </filter>
  <filter-mapping>
    <filter-name>encoding</filter-name>
    <url-pattern>/*</url-pattern>
  </filter-mapping>

  <!-- 配置SpringMVC -->
  <servlet>
    <servlet-name>boot-crm</servlet-name>
    <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
    <init-param>
      <param-name>contextConfigLocation</param-name>
      <param-value>classpath:spring/springmvc.xml</param-value>
    </init-param>
    <!-- 配置springmvc什么时候启动，参数必须为整数 -->
    <!-- 如果为0或者大于0，则springMVC随着容器启动而启动 -->
    <!-- 如果小于0，则在第一次请求进来的时候启动 -->
    <load-on-startup>1</load-on-startup>
  </servlet>
  <servlet-mapping>
    <servlet-name>boot-crm</servlet-name>
    <!-- 所有的请求都进入springMVC -->
    <url-pattern>/</url-pattern>
  </servlet-mapping>
  
</web-app>
```



注意上边配置文件中的classpath，在resource文件夹中，设置对应的文件路径就可以。最终的配置文件的目录结构如下图：

![1568007132906](assets/1568007132906.png)



### 编写Spring配置文件

这里将整合Mybatis的配置文件和spring自身的配置分成两个配置文件：spring/applicationContext-dao.xml 和 spring/applicationContext-service.xml。

#### applicationContext-dao.xml 

这里是整合Mybatis相关配置

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:context="http://www.springframework.org/schema/context" xmlns:p="http://www.springframework.org/schema/p"
       xmlns:aop="http://www.springframework.org/schema/aop" xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
	http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-4.0.xsd
	http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop-4.0.xsd http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-4.0.xsd
	http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util-4.0.xsd">

    <!-- 配置 读取properties文件 jdbc.properties -->
    <context:property-placeholder location="classpath:properties/jdbc.properties" />

    <!-- 配置 数据源 -->
       <bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource" >
        <property name="driverClass" value="${jdbc.driver}"/>
        <property name="jdbcUrl" value="${jdbc.url}"/>
        <property name="user" value="${jdbc.username}"/>
        <property name="password" value="${jdbc.password}"/>
    </bean>

    <!-- 配置SqlSessionFactory -->
    <bean class="org.mybatis.spring.SqlSessionFactoryBean">
        <!-- 设置MyBatis核心配置文件 -->
        <property name="configLocation" value="classpath:mybatis/sqlMapConfig.xml" />
        <!-- 设置数据源 -->
        <property name="dataSource" ref="dataSource" />

        <!-- 配置Mybatis映射文件 -->
        <property name="mapperLocations" value="classpath:com/shalom/boshalom/dao/mapper/*.xml"></property>

    </bean>

    <!-- 配置Mapper扫描 -->
    <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
        <!-- 设置Mapper扫描包 -->
        <!-- 为此包中的所有Mapper接口生成代理实现类 -->
        <property name="basePackage" value="com.shalom.boshalom.dao" />
    </bean>

</beans>
```



#### applicationContext-service.xml

spring自身的配置：对service的注解扫描

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:context="http://www.springframework.org/schema/context" xmlns:p="http://www.springframework.org/schema/p"
       xmlns:aop="http://www.springframework.org/schema/aop" xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
	http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-4.0.xsd
	http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop-4.0.xsd http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-4.0.xsd
	http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util-4.0.xsd">

    <!-- 配置Service扫描 -->
    <context:component-scan base-package="com.shalom.boshalom.service" />
</beans>
```



### 编写SpringMVC配置文件

SpringMVC只处理Controller层，所以这里只配置扫描controller包

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:p="http://www.springframework.org/schema/p"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
        http://www.springframework.org/schema/mvc http://www.springframework.org/schema/mvc/spring-mvc-4.0.xsd
        http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-4.0.xsd">

    <!-- 配置Controller扫描 -->
    <context:component-scan base-package="com.shalom.boshalom.controller" />

    <!-- 配置注解驱动 -->
    <mvc:annotation-driven />
    <!-- 给静态资源放行 -->
    <mvc:default-servlet-handler/>

    <!-- 配置视图解析器 -->
    <bean	class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <!-- 前缀 -->
        <property name="prefix" value="/WEB-INF/views/" />
        <!-- 后缀 -->
        <property name="suffix" value=".jsp" />
    </bean>

</beans>
```





### 编写Mybatis配置文件

这里是 Mybatis 相关配置

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>

    <settings>
        <!-- 映射下划线到驼峰命名 -->
        <setting name="mapUnderscoreToCamelCase" value="true"/>
        <!-- 设置Mybatis对null值的默认处理 -->
        <setting name="jdbcTypeForNull" value="NULL"/>
        <!-- 开启延迟加载 -->
        <setting name="lazyLoadingEnabled" value="true"/>
        <!-- 设置加载的数据是按需还是全部 -->
        <setting name="aggressiveLazyLoading" value="false"/>
        <!-- 配置开启二级缓存 -->
        <setting name="cacheEnabled" value="true"/>
    </settings>
    
    <!-- 给当前包和所有子包起别名，例：包名就只需要boshalom，不需要前面的一长串 -->
    <typeAliases>
        <package name="com.shalom.boshalom"></package>
    </typeAliases>

</configuration>
```



## 业务逻辑

Mapper映射文件的编写

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.shalom.boshalom.dao.UserDao">



</mapper>
```





