# Spring框架笔记

## 快速入门

### 将spring的依赖包准备好

创建的时候需要将这个文件夹关联上，点击出 new project 后

![1565148530393](assets/1565148530393.png)

创建好后，项目目录中会显示自己设置好的spring的依赖包，结构如下：

![1565148668560](assets/1565148668560.png)





### 将IOC容器配置文件写好

创建一个 XML 文件，例码：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="
       http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

    <!-- 配置一个bean [对象]-->
    <bean id="user" class="com.shalom.spring.bean.User">
        <!-- 依赖注入数据，调用属性的set方法 -->
        <property name="name" value="zhangsan"></property>
    </bean>
</beans>
```

在IDEA中，有快速创建该配置文件的地方：点击new的时候，找到如下选项：
![1565157230348](assets/1565157230348.png)

这样就会自动填充XML约束，也就是上面例码中的一堆URL。



### 使用代码

```java
@Test
public void test01(){
    //	获得容器
    ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
	//	从容器中获取对象
    IUserService user1 = (IUserService) context.getBean("user");
    //	调用该对象的方法
    user1.add();
}
```

**注意：**User对象的 name 字段必须要有set方法，这样才可以通过配置文件中的`<property name="name" value="zhangsan"></property>`给name字段赋值。



### 文件的解释

#### xml配置文件

`<bean>`：是受spring管理的一个 javaBean 对象

- id：在IOC容器中，是`<bean>`的唯一标识；
- class：指定 JavaBean 的全类名，目的是通过反射创建对象，所以对象必须要提供无参构造器；



`<property>`：通过使用set方法，给对象的成员变量赋值



`<constructor-arg>`：使用构造器给对象的成员变量赋值

```xml
<bean id="constructortest" class="com.shalom.spring.bean.ConstructorTest">
    <constructor-arg value="Tesla" index="0" type="java.lang.String"></constructor-arg>
    <constructor-arg value="red"></constructor-arg>
    <constructor-arg value="30000"></constructor-arg>
</bean>
```

- 首先类中必须要有带参的构造器；
- index 用来指定给哪个参数赋值，解决参数赋值顺序错乱问题；
- type 用来指定赋值参数的类型，用来解决多个构造器重载，选择哪个构造器问题；



##### p命名空间

后期再补全……



##### 还有许多细节后面有时间再补... ...

TODO



#### getBean方法

getBean方法是在顶层的接口中就被定义出来的，他有多个重载，其中最常用的是：

```java
IUserService user = context.getBean("user",IUserService.class);
```

- 既指定id，也指定类型，这样就不需要再强转



### 使用beanFactory的方法

先创建一个类，实现接口 FactoryBean 

需要重写3个方法；例码：

```java
public class UserBeanFactory implements FactoryBean<User> {
    @Override
    public User getObject() throws Exception {
        return new User("shalomHu");
    }

    @Override
    public Class<?> getObjectType() {
        return User.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
```

然后在bean配置文件里写如下代码：

```xml
<bean id="user01" class="com.shalom.spring.bean.UserBeanFactory"></bean>
```



## bean

### bean的生命周期

1. 通过构造器或工厂方法创建bean实例

2. 为bean的属性设置值和对其他bean的引用

3. 调用bean的初始化方法

4. 使用bean

5. 当容器关闭时，调用bean的销毁方法

**注意：**添加上bean的后置处理器后，bean的生命周期就变为7个阶段，细节看后边...

#### 代码实现

在配置bean时，通过 init-method 和 destroy-method 属性为bean指定初始化和销毁方法

```java
/**
	* 初始化方法
	* 需要通过 init-method来指定初始化方法
	*/
public void init() {
    System.out.println("===>3. 调用初始化方法");
}

/**
	* 销毁方法： IOC容器关闭， bean对象被销毁.
	*/
public void destroy() {
    System.out.println("===>5. 调用销毁方法");
}

//	测试方法
@Test
public void testLifeCycle() {
	//	因为要调用close方法，所以不能够再使用ApplicationContext接口 
    ConfigurableApplicationContext ctx = 
        new ClassPathXmlApplicationContext("spring-lifecycle.xml");
    Car car = ctx.getBean("car",Car.class);

    System.out.println("===>4. 使用bean对象" + car);
    
    //关闭容器
    ctx.close();
}

//	配置文件；通过关键字 init-method 和 destroy-method 指定初始化和销毁方法
<bean id="car" class="com.atguigu.spring.lifecycle.Car" init-method="init"  destroy-method="destroy">
	<property name="brand" value="宝马"></property>
	<property name="price" value="450000"></property>
</bean>
```



### bean的后置处理器

bean后置处理器允许在调用**初始化方法前后**对bean进行额外的处理，而在额外处理中具体要做什么，是根据业务逻辑来的。

#### 代码实现

- 首先创建一个类，这个类必须要实现接口 BeanPostProcessor ；
  - 在类中重写初始化前和初始化后的方法，在这两个方法中实现额外的业务逻辑；
  - 两个函数返回都要返回一个对象，后续的操作都是基于这两个函数返回的对象来处理的；
- 然后在配置文件加上说明
  - **注意：**在配置文件中指定后，对IOC容器中所有的bean都起作用。

```java
/**
 * bean的后置处理器 : 对IOC容器中所有的bean都起作用. 
 */
public class MyBeanPostProcessor implements BeanPostProcessor {
	/**
	 * 在bean的生命周期的初始化方法之前执行
	 * Object bean: 正在被创建的bean对象. 
	 * String beanName: bena对象的id值. 
	 */
	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		System.out.println("postProcessBeforeInitialization");
		return bean;
	}
	/**
	 * 在bean的生命周期的初始化方法之后执行
	 */
	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		System.out.println("postProcessAfterInitialization");
		return bean;
	}
}

//	配置文件
1. 不需要指定id
<!-- 配置后置处理器 : Spring能自动识别是一个后置处理器 -->
<bean class="com.atguigu.spring.lifecycle.MyBeanPostProcessor"></bean>
```



### 读取外部配置文件

先将 property 文件写好，bean中读取的时候：

```xml
<bean class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
    <property name="location" value="classpath:db.properties"></property>
</bean>
```

- classpath 关键字指定：从类路径中读取此配置文件；

- 上边的逻辑是：给 PropertyPlaceholderConfigurer 类的 location 字段赋值；



另一种写法：

```xml
<context:property-placeholder location="classpath:db.properties"/>
```

原理和上边的一模一样，只是更加简洁；**注意：**这里需要更改XML文件的约束。



配置文件：

```properties
jdbc.driver=com.mysql.jdbc.Driver
jdbc.url=jdbc:mysql://localhost:3306/mysql
jdbc.username=root
jdbc.password=1234
```

**注意：**这里最好加上一个前缀，因为可能读出的数据和系统数据搞混；比如 username 不加上前缀，就可能读取的是系统的username。





## 自动装配

Person类，其中两个字段分别是对象 Car 和 Address

```java
public class Person {
    //  car 和 addr 可以自动装配，name 不可以自动装配
    private String name;
    private Car car;
    private Address address1;
    //	省略get set方法 ……
}
```



XML配置文件

```xml
<bean id="car" class="com.shalom.spring.autowire.Car">
    <property name="brand" value="奔驰"></property>
    <property name="price" value="50000"></property>
</bean>

<bean id="address" class="com.shalom.spring.autowire.Address">
    <property name="province" value="江西"></property>
    <property name="city" value="吉安"></property>
</bean>

<bean id="person" class="com.shalom.spring.autowire.Person" autowire="byType">
    <property name="name" value="shalom"></property>
</bean>

<bean id="person" class="com.shalom.spring.autowire.Person" autowire="byName">
    <property name="name" value="shalom"></property>
</bean>
```



**关键字：**

`autowire`



**两种类型：**

- byName：

  用 get 和 set 方法来识别配置文件中的 id，比如这里用 getAddress 中的 address 来匹配 XML文件中的 id 为 address 的bean，若匹配不上自动装配为 null

- byType：

  不匹配 id ，匹配 class 



## 注解

**XML文件中打开注解开关**

在配置文件头中 添加有关 context 的约束：

```xml
xmlns:context="http://www.springframework.org/schema/context"
xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                    http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd"
```

然后再配置文件中加上这么一段：

```xml
<context:component-scan base-package="com.shalom.spring.annotation"></context:component-scan>
```

- base-package：指定基础包，效果就是 它包括它的子包的注解 都会被扫描到；



**指定扫描和排除扫描**

**指定扫描**

可以指定只扫描其中的一类注解，和一个类

```xml
<context:component-scan base-package="com.shalom.spring.annotation" use-default-filters="false">
    <context:include-filter type="annotation" expression="org.springframework.stereotype.Controller"></context:include-filter>
    <context:include-filter type="assignable" expression="com.shalom.spring.annotation.dao.UserDaoJdbcImpl"></context:include-filter>
</context:component-scan>
```

- 根据注解来区分，type后边就赋值为 type="annotation" ，expression就等于注解对应的类
- 根据类来区分，type后边 type="assignable"，expression后边就写全类名，表示只扫描该类的注解

**注意：**一定要将使用默认过滤器的选项关掉：`use-default-filters="false"`



**排除扫描**

同样的，也可以通过 注解 和 类 来区分要 排除扫描哪些注解。

```xml
<context:component-scan base-package="com.shalom.spring.annotation" use-default-filters="true">
    <context:exclude-filter type="assignable" expression="com.shalom.spring.annotation.dao.UserDaoJdbcImpl"></context:exclude-filter>
    <context:exclude-filter type="annotation" expression="org.springframework.stereotype.Controller"></context:exclude-filter>
</context:component-scan>
```

**注意：**确认 use-default-filters = "true" ，也可以省略不写，因为默认就是true，但是一定要注意不能是 false。





**在类上添加注解**

```java
@Service
public class UserServiceImpl implements UserService {}
```

就相当于在配置文件中写了一行 bean配置

```xml
<bean id="userServiceImpl" class="com.shalom.spring.annotation.service.UserServiceImpl"></bean>
```

**注意：** id为类名的首字母小写



####　自动装配的注解

只需要使用　＠Autowired　就可以

```java
@Controller
public class UserControllar {
    @Autowired
    private UserService userService;
	//	这样可以自动填充一个实现类给 接口UserService
    public void regist(){
        userService.handleAddUser();
    }
}
```



##### 工作机制

先通过 byType 的方式匹配，若匹配出来有多个，在通过 byName 的方式匹配，上个例子中name就为 userService；

所以我们写代码的时候，通常要在注解上自己制定id；也可以在 ＠Autowired 一端主动指定要装配的对象：

```java
    @Autowired(required = false)
    @Qualifier("userServiceImpl") // 通过这一行进行主动装配
    private UserService userService;
```



标记了 ＠Autowired 的必须被装配，若希望不被装配也不报错，就需要加上required = false：

```java
    @Autowired(required = false)
    private UserService userService;
```



@Autowired 和 @Qualifier 同样可以加到成员变量对应的set方法上。原本都是写在set方法上边，后边提供了加在成员变量上的功能，为了省事，就都写在成员变量上了。



## AOP

### 动态代理

**将动态生成的代理类保存下来 ，然后再通过反编译插件反编译出源码**

```java
//将动态生成的代理类保存下来 
Properties properties = System.getProperties(); properties.put("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");
```



JDK的动态代理：

1. Proxy : 是所有动态代理类的父类， 专门用于生成代理类或者是代理对象，有两个方法用来生成：
   - `public static Class<?> getProxyClass(ClassLoader loader,Class<?>... interfaces)   `                        用于生成代理类的Class对象.                                         *      
   - `public static Object newProxyInstance(ClassLoader loader,  Class<?>[] interfaces, InvocationHandler h) `  用于生成代理对象                                                                    *   
2. InvocationHandler : 用来完成动态代理的整个过程，其中有一个最重要的方法invoke，动态代理需要添加的额外逻辑都是在invoke里边
   `public Object invoke(Object proxy, Method method, Object[] args) throws Throwable; `



基于继承的动态代理：



#### 动态代理的底层实现

```java
class $Proxy0  extends Proxy  implements Calculator{
    //	这里会通过反射对方法进行封装
    
	//    将我们创建好的 InvocationHandler 给到了父类 Proxy 中
    protected $Proxy0(InvocationHandler h) {
        super(h);
    }
    @Override
    public int add(int i, int j) {
        //	再在这里通过父类调用 invoke 方法
        //	return  super.h.invoke(this,方法对象,方法参数);
        return 0 ;
    }
    @Override
    public int sub(int i, int j) {
        return 0;
    }
    @Override
    public int mul(int i, int j) {
        return 0;
    }
    @Override
    public int div(int i, int j) {
        return 0;
    }
}
```









