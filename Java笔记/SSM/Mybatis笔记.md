# Mybatis笔记

## 简单的使用案例

1. 创建数据库表，将需要操作的表创建出来

   - 执行外部SQL脚本（SQLyog）
     ![1565057657251](../assets/1565057657251.png)

   - 将与表数据对应的 javabean 编写好；

     

2. 下载Mybatis（下载其中的压缩包即可）：[下载链接](https://github.com/mybatis/mybatis-3/releases)

   解压后的目录结构如下：
   ![1565057535721](../assets/1565057535721.png)

3. 创建项目

   如果是简单的练习，创建一个简单的项目就可以了。
   
4. 导入需要的依赖包

   - 需要导入的包有：Mybatis的驱动包 和 依赖包（主要是一些日志包），MySQL的驱动包，junit包不需要导（IDEA会自动导入）

   - 在项目下创建lib目录，将需要的依赖包复制到其中。

   - 右击lib目录，选择 Add as Library(只有做到这一步，依赖包才可以被项目识别)

     ![1565060042970](../assets/1565060042970.png)
     

5. 在src目录下创建一个日志的配置文件 log4j.properties

   ```properties
   # Global logging configuration；stdout是控制台输出的意思，如果想输出到文件，可以在后面再加上file
   log4j.rootLogger=DEBUG, stdout
   # Console output...
   log4j.appender.stdout=org.apache.log4j.ConsoleAppender
   log4j.appender.stdout.layout=org.apache.log4j.PatternLayout
   log4j.appender.stdout.layout.ConversionPattern=%5p [%t] - %m%n
   ```

   

6. 编写Mybatis的全局配置文件SqlMapConfig.xml

   Mybatis通过这个全局的配置文件创建一个 SqlSessionFactory ，每一个Mybatis的应用都是围绕SqlSessionFactory展开的；以下可以在Mybatis官方文档中找到模板。

   ```xml
   <?xml version="1.0" encoding="UTF-8" ?>
   <!DOCTYPE configuration
           PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
           "http://mybatis.org/dtd/mybatis-3-config.dtd">
   <configuration>
       <!-- 配置mybatis的环境信息 -->
       <environments default="development">
           <environment id="development">
               <!-- 配置JDBC事务控制，由mybatis进行管理 -->
               <transactionManager type="JDBC"/>
               <!-- 配置数据源，采用dbcp连接池 -->
               <dataSource type="POOLED">
                   <property name="driver" value="com.mysql.jdbc.Driver"/>
                   <property name="url" value="jdbc:mysql://localhost:3306/mybatis001?useUnicode=true&amp;characterEncoding=utf8"/>
                   <property name="username" value="root"/>
                   <property name="password" value="123"/>
               </dataSource>
           </environment>
       </environments>
       <mappers>
           <mapper resource="org/mybatis/example/BlogMapper.xml"/>
       </mappers>
   </configuration>
   ```
   
   利用以上XML文件创建 SqlSessionFactory 的例码：
   
   ```java
   //  a)读取全局配置文件；
   InputStream resourceAsStream = Resources.getResourceAsStream("SqlMapConfig.xml");
   
   //  b)通过SqlSessionFactoryBuilder创建SqlSessionFactory会话工厂。
   SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);
   ```

   

7. 创建映射文件 User.xml

   ```xml
   <?xml version="1.0" encoding="UTF-8" ?>
   <!DOCTYPE mapper
           PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
           "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
   <!-- 
   	namespace：命名空间，它的作用就是对SQL进行分类化管理，可以理解为SQL隔离
   	注意：使用mapper代理开发时，namespace有特殊且重要的作用
    -->
   <mapper namespace="test">
       <!-- 
           [id]：statement的id，要求在命名空间内唯一，标识一条SQL语句；
           [parameterType]：入参的java类型，执行SQL语句要传入的参数类型；
           [resultType]：查询出的单条结果集对应的java类型；
           [#{}]： 表示一个占位符?
           [#{id}]：表示该占位符待接收参数的名称为id。注意：如果参数为简单类型时，#{}里面的参数名称可以是任意定义
        -->
       <select id="findUserById" parameterType="int" resultType="com.shalom.bean.User">
        	<!-- sql语句可以取别名 -->   
   		SELECT * FROM USER WHERE id = #{id}
   	</select>
   </mapper>
   ```

   

8. 在全局配置文件中加载 映射文件User.xml

   ```xml
   <mappers>
       <mapper resource="com/shalom/sqlmap/User.xml"/>
   </mappers>
   ```

   

9. 编写测试代码

   ```java
   public class Demo01 {
       @Test
       public void test01() throws IOException {
           //  a)读取全局配置文件；
           InputStream resourceAsStream = Resources.getResourceAsStream("SqlMapConfig.xml");
   
           //  b)通过SqlSessionFactoryBuilder创建SqlSessionFactory会话工厂。
           SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);
   
           //  c)通过SqlSessionFactory创建SqlSession。
           SqlSession sqlSession = sessionFactory.openSession();
   
           //  d)调用SqlSession的操作数据库方法。这里会找到映射文件中的内容，第一个参数是SQL语句的唯一标识（这里建议使用 namespqce.id 以保证与其他配置文件不会有重复），第二个参数是执行SQL语句要用到的参数
           //	而SQL语句就是通过映射文件来映射的
           User user = sqlSession.selectOne("test.findUserById", 10);
           System.out.println(user);
   
           //   关闭SqlSession。
           sqlSession.commit();
       }
   }
   
   ```



### 使用接口绑定SQL

1. 创建一个接口，用来专门操作数据库中该对象

   ```java
   //	比如这里专门操作User对象，这里只简单写了个查询功能
   public interface UserMapper {
       public User getUserByID(Integer id);
   }
   ```

   

2. 因为此接口的 getUserByID 方法与我们之前在映射文件里写的 SQL 语句完全吻合，所以我们可以在映射文件里更改标识，以表明该语句和该方法的关系；

   - namespace 改成接口的全类名

   - SQL语句的 id 改成方法名“getUserByID”

     

3. 编写测试代码

   ```java
   @Test
   public void test02() throws IOException {
       //   这里对获取SqlSessionFactory做了一步封装
       SqlSessionFactory sessionFactory = getSessionFactory();
   
       //  获取数据库会话,SqlSession代表和数据库的一次会话
       SqlSession session = sessionFactory.openSession();
   
      try {
          //  调用次方法获取 EmployeeMapper 的实现对象，这里我们并没有编写实现类，Mybatis自动创建了一个代理类实现了 EmployeeMapper接口
          EmployeeMapper mapper = session.getMapper(UserMapper.class);
   
          //  调用接口的方法
          User user = mapper.getUserByID(16);
   
          System.out.println(user);
          //	使用不带参的session，需要手动提交数据
          session.commit();
      }finally {
          session.close();
      }
}
   ```
   
   

### 注意点

1. SqlSession 不是线程安全的，用完必须关闭，每次使用应该重新获取；



## 全局配置文件详解

例码：

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <!-- 配置mybatis的环境信息 -->
    <environments default="development">
        <environment id="development">
            <!-- 配置JDBC事务控制，由mybatis进行管理 -->
            <transactionManager type="JDBC"/>
            <!-- 配置数据源，采用dbcp连接池 -->
            <dataSource type="POOLED">
                <property name="driver" value="com.mysql.jdbc.Driver"/>
                <property name="url" value="jdbc:mysql://localhost:3306/mybatis001?useUnicode=true&amp;characterEncoding=utf8"/>
                <property name="username" value="root"/>
                <property name="password" value="123"/>
            </dataSource>
        </environment>
    </environments>
    
    <mappers>
        <mapper resource="org/mybatis/example/BlogMapper.xml"/>
    </mappers>
</configuration>
```

1. 第4行引入的 dtd 文件是XML的约束文件，只要该文件导入正确，编写就会有提示；
   - 该文件的路径在：`mybatis-3.5.0.jar!/org/apache/ibatis/builder/xml/mybatis-3-config.dtd`;导入的Mybatis驱动里边。
   - Eclipse里面，可以将第4行中的URL地址和本地的dtd文件绑定。IDEA中还不太清楚，IDEA一般可以直接索引到Mybatis中的驱动包里。

2. properties标签

   ```xml
   <!-- resource 指从项目中获取 而要从网络中或是磁盘中获取，需要将resource改成url -->
   <properties resource="dbconfig.properties"></properties>
   ```

   用来读取properties配置文件，经常在获取数据源（db）的时候使用
   **注意：**因为spring框架对读取数据源优化的非常方便，所以后期和spring整合的时候，不会使用该种方式进行读取数据源。

3. settings标签

   - settings标签很重要，可以设置许多选项

   - 其中一个就是是否开启驼峰命名规则，例码：

     ```xml
     <settings>
         <setting name="mapUnderscoreToCamelCase" value="true"/>
     </settings>
     ```

     这样就可以将数据库中 a_bc 格式转换为java中的 aBc
     

4. typeAliases标签

   ```xml
   <typeAliases>
       <typeAlias type="com.shalom.bean.User" alias="ur"/>
   </typeAliases>
   ```

   - 用于给类取别名，不用写那么长了

   - type关键字后边接类名，alias后边接希望改成的名

   - **注意：**alias可以省略，省略后别名就为类名的小写，上边例码中就为user

   - 批量起别名

     - 例码：

       ```xml
       <typeAliases>
           <!-- 2、package:为某个包下的所有类批量起别名 -->
           <package name="com.atguigu.mybatis.bean"/>
       
           <!-- 3、批量起别名的情况下，使用@Alias注解为某个类型指定新的别名 -->
       </typeAliases>
       ```

     - 作用是：为当前包以及下面所有的后代包的每一个类都起一个默认别名（类名小写）

     - 当包内部有类名冲突时（子包的类名和当前的相同），使用@Alias注解为某个类型指定新的别名，例码：

       ```java
       @Alias("ur")
       public class User { ... }
       ```

   - **注意：**Mybatis中有内置的别名，不要与他们重复，具体哪些看文档

5. 后边几个稍后再补上……



## 映射文件详解

### 增删改查

#### 增

```java
<insert id="addUser" parameterType="com.shalom.bean.User">
    insert into user(username,birthday,sex,address) values (#{username},#{birthday},#{sex},#{address})
</insert>

//	接口中的方法
public void addUser(User user);
```

- 参数类型可以省略；
- values中的占位符代表传入参数的字段值。
- 注意书写的格式



#### 删

```java
<delete id="deleteUserByID">
    delete from user where id=#{id}
</delete>

public void deleteUserByID(Integer id);
```



#### 改

```java
<update id="updateUser" >
    update user set username=#{username},birthday=#{birthday},sex=#{sex},address=#{address} where id=#{id}
</update>

public void updateUser(User user);
```



#### 查

##### 简单查询

```java
<select id="getUserByID" parameterType="int" resultType="com.shalom.bean.User">
	SELECT * FROM USER WHERE id = #{id}
</select>

public User getUserByID(Integer id);
```

**注意：** 返回值类型 resultType 不能省略。



##### 查询出多条数据





##### 模糊查询

如果返回的是一个集合，resultType要写集合中元素的类型，而不是写list

```java
<select id="getEmpsByLastNameLike" resultType="com.atguigu.mybatis.bean.Employee">
	select * from tbl_employee where last_name like #{lastName}
</select>

public List<Employee> getEmpsByLastNameLike(String lastName);
```

TODO：SQL中的模糊查询后边需要掌握



###### 返回一个map

- map中只包含一条记录：

  resultType 中直接填写 map，取出的map中只有一条数据记录

	```java
  <select id="getEmpByIdReturnMap" resultType="map">
        select * from tbl_employee where id=#{id}
  </select>

  //返回一条记录的map；key就是列名，值就是对应的值
  public Map<String, Object> getEmpByIdReturnMap(Integer id);

  //	测试代码
  Map<String, Object> map = mapper.getEmpByIdReturnMap(1);
  System.out.println(map);
	```



- map中包含多条记录

  resultType 中直接填写 map 的value类型，通过注解@MapKey()的方式，指定key的类型

  ```java
  <select id="getEmpByLastNameLikeReturnMap" resultType="com.atguigu.mybatis.bean.Employee">
  	select * from tbl_employee where last_name like #{lastName}
  </select>
  
  //多条记录封装一个map：Map<Integer,Employee>:键是这条记录的主键，值是记录封装后的javaBean
  //@MapKey:告诉mybatis封装这个map的时候使用哪个属性作为map的key
  @MapKey("lastName")
  public Map<String, Employee> getEmpByLastNameLikeReturnMap(String lastName);
  
  //	测试代码，利用模糊查询查询到多条记录
  Map<String, Employee> map = mapper.getEmpByLastNameLikeReturnMap("%r%");
  System.out.println(map);
  ```

  

### 获取自增主键的值

```xml
<insert id="addUser" parameterType="com.shalom.bean.User" useGeneratedKeys="true" keyProperty="id">
    insert into user(username,birthday,sex,address) values (#{username},#{birthday},#{sex},#{address})
</insert>
```

- 将 useGeneratedKeys 改为true，则打开了获取自增主键的开关；
- keyProperty指定的是：把获取的值 赋值给对象的哪个字段；
- 这样便可以获取自增主键的value；

**注意：**oracle数据库不支持自增，这个时候也有获取非自增主键的方法，搜索 selectKey 应该可以找到



### 映射文件中的参数处理

#### 单个参数：

mybatis不会做特殊处理，直接`#{参数名/任意名}`取出参数值。**任意名称都可以**



#### 多个参数：

- 多个参数时，mybatis会将多个参数封装成一个map，

  key：param1...paramN,或者参数的索引也可以

  value：传入的参数值

  所以取值的时候，就必须在 `#{...}`里边填入key值，不然会包找不到参数的错误

  **正确的写法：**`#{param1} #{param2}`

- 上面的写法不太方便，可以在方法中直接指定key

  使用@Param()注解指定key的值，例码：

  ```java
  public Employee getEmpByIdAndLastName(@Param("id")Integer id,@Param("lastName")String lastName);
  ```

- 如果参数比较多的话，上边的写法也过于繁琐，还有一种取巧方法是：

  ​		如果传入的参数刚好满足代码中的某一个类，则只需将参数封装到该类的对象中，然后传入；在映射文件读取参数的时候，可以直接使用 `#{字段名}`读取参数。
  
- 如果参数不满足代码中的任何一个类，那么直接可以将这些参数封装成一个map，映射文件取值的时候就可以直接用key的值；例码：

  ```java
  public Employee getEmpByMap(Map<String, Object> map);
  ```



<font color=red>若传入的多个参数使用的频率很高，建议专门创建一个传输类，采用传入对象的方式。</font>



##### 几种特殊情况：

```java
public Employee getEmp(@Param("id")Integer id,String lastName);
	取值：id==>#{id/param1}   lastName==>#{param2}

public Employee getEmp(Integer id,@Param("e")Employee emp);
	取值：id==>#{param1}    lastName===>#{param2.lastName/e.lastName}

##特别注意：如果是Collection（List、Set）类型或者是数组，
		 也会特殊处理。也是把传入的list或者数组封装在map中。
			key：Collection（collection）,如果是List还可以使用这个key(list)
				数组(array)
public Employee getEmpById(List<Integer> ids);
	取值：取出第一个id的值：   #{list[0]}
```



#### 参数的获取

#{} 和 ${} 都可以获取map中的值或者pojo对象属性的值；

##### 区别：

#{}:是以预编译的形式，将参数设置到sql语句中；PreparedStatement；防止sql注入
		${}:取出的值直接拼装在sql语句中；会有安全问题；
		大多情况下，我们取参数的值都应该使用 #{}；
		

原生jdbc不支持占位符的地方我们就可以使用${}进行取值
	比如分表、排序。。。；按照年份分表拆分；例码：

```sql
select * from ${year}_salary where xxx;
select * from tbl_employee order by ${f_name} ${order}
```



### 自动映射（resultType）

Mybatis默认是将自动映射开启的，可以自动的将数据库中取出的数据封装成一个JavaBean。

#### 需要满足的条件

表中的列名 和 JavaBean的属性名 **一致**。

#### 功能细节

1. autoMappingBehavior 设置为null 则会取消自动映射，最好不要取消；
2. 我们可以开启 自动驼峰命名规则映射功能 (将mapUnderscoreToCamelCase = true)，将表中的 A_COLUMN 的字段自动转换为 aColumn ；

例码：

```xml
// 在映射文件中正常使用
<typeAlias alias="Blog" type="com.tiantian.mybatis.model.Blog"/>
<select id="selectBlog" parameterType="int" resultType="Blog">
	select * from t_blog where id = #{id}
</select>

// autoMappingBehavior 和 mapUnderscoreToCamelCase 都需要在 Mybatis 的全局配置文件中设置
<settings>
    <setting name="mapUnderscoreToCamelCase" value="true"/>
    <setting name="autoMappingBehavior" value="null"/>
</settings>
```





### 自定义映射(resultMap)

当从多个表中查询数据时，就需要用到自定义映射。

例码：

```java
<resultMap id="myUserDep" type="com.shalom.bean.User">
	<id column="id" property="id" />
    <result column="name" property="username"/>
</resultMap>

<select id="getUserAndDepByID" resultMap="myUserDep">
    select username from tbl_user 
</select>
```

- resuleMap 是一个标签；

- 第一行里的 id 意思是：此 resultMap 的引用 id，用来标识当前的resultMap；

- 第一行中的 type 指定此 resultMap 将会封装成哪个类；

- 第二行中的 id 是专门给 数据库表中的主键 使用的关键字；

- 除了主键，其余字段都使用 result；

- column 是结果集的列名，结果集是指从数据库中查询出的数据；

  **为什么不是表列名？**因为SQL语句中，可能会给列名取别名。

- property 是指定JavaBean的属性名；



#### 从多个表中查数据

表示通过内联和外联的方式建立关系的，而java就是通过类与类之间的关系对应表的关系。

##### 级联

一个对象是另一个对象的字段，resultMap中将数据库中取出的数据指定为字段的字段

（超抽象，嘿嘿）可以看下边的例码



#### association

```xml
<resultMap type="com.atguigu.mybatis.beans.Employee" id="myEmpAndDept">
    <id column="eid" property="id"/>
    <result column="last_name" property="lastName"/>
    <result column="email" property="email"/>
    <result column="gender" property="gender"/>
    <!-- 级联 -->
   <result column="did" property="dept.id"/>
   <result column="dept_name" property="dept.departmentName"/>
   
    <!-- 
    association: 完成关联、联合属性的映射
     property: 指定联合属性
     javaType: 指定联合属性的类型
    -->
    <association property="dept" javaType="com.atguigu.mybatis.beans.Department">
        <id column="did" property="id" />
        <result column="dept_name" property="departmentName"/>
    </association>
</resultMap>
```

要么使用级联，要么使用association



##### association中的分布查询和延迟加载

###### 分布查询

就是当查询多表数据的时候，将从表的数据再次执行一次SQL进行查询，比如：

需求:  查询员工信息并且查询员工所在的部门信息.     

1. 先根据员工的id查询员工信息     
2. 使用外键 d_id查询部门信息

主表的映射文件 例码：

```xml
<select id="getEmpAndDeptStep" resultMap="myEmpAndDeptStep">
    select id, last_name, email,gender ,d_id  from tbl_employee where id = #{id}
</select>
<resultMap type="com.atguigu.mybatis.beans.Employee" id="myEmpAndDeptStep">
    <id column="id" property="id"/>
    <result column="last_name" property="lastName"/>
    <result column="email" property="email"/>
    <result column="gender" property="gender"/>
    <!-- 分步查询 调用另一个类的查询映射-->
    <association property="dept" 
         select="com.atguigu.mybatis.dao.DepartmentMapperResultMap.getDeptById"
         column="d_id">
    </association>

</resultMap>
```

- 通过 select 指定要调用的查询SQL
- column 指定传入的参数，对应当前SQL中的字段(第二行)



###### 延迟加载

又称懒加载，只有在使用分布查询的时候才可以使用延迟加载，延迟第二步的操作。

在全局配置文件中，将延迟加载的开关和按需加载打开：

```xml
<!-- 2. settings:  包含了很多重要的设置项 -->
<settings>
    <!-- 映射下划线到驼峰命名 -->
    <setting name="mapUnderscoreToCamelCase" value="true"/>
    <!-- 开启延迟加载 -->
    <setting name="lazyLoadingEnabled" value="true"/>
    <!-- 配置按需加载-->
    <setting name="aggressiveLazyLoading" value="false"/>
</settings>
```

在项目中经常会使用到



#### collection

当要查询关联表的一个集合数据时，比如查询部门里的多个员工，就要使用collection

例码：

```xml
<select id="getDeptAndEmps" resultMap="myDeptAndEmps">
    SELECT d.id did ,d.dept_name, e.id eid, e.last_name, e.email,e.gender
    FROM tbl_dept d  LEFT OUTER JOIN  tbl_employee  e
    ON d.id = e.d_id  WHERE d.id = #{id}
</select>
<resultMap type="com.atguigu.mybatis.beans.Department" id="myDeptAndEmps">
    <id column="did" property="id"/>
    <result column="dept_name" property="departmentName"/>
    
    <!-- 
   collection: 完成集合类型的联合属性的映射
    property: 指定联合属性
    ofType: 指定集合中元素的类型
   -->
    <collection property="emps" ofType="com.atguigu.mybatis.beans.Employee" >
        <id column="eid" property="id"/>
        <result column="last_name" property="lastName"/>
        <result column="email" property="email"/>
        <result column="gender" property="gender"/>
    </collection>
</resultMap>
```



collection 同时支持分布查询和延迟加载



### 扩展

##### 分步查询多列值的传递



##### association 或 collection的 fetchType属性 



### 抽取SQL片段

可以重复使用

例码：

```
<select id="getEmpsByDid" resultType="com.atguigu.mybatis.beans.Employee">  
	<include refid="selectEmployeeSQL"></include> from tbl_employee where d_id = #{did}   
</select>
<!-- 抽取可重用的SQL片段 -->
<sql id="selectEmployeeSQL">   select id ,last_name,email,gender </sql>
```



### 动态SQL

#### where if

##### 使用场景

我们传入一个对象，但是只通过对象中一个字段来从数据库中查询数据，所以需要使用 where if 进行判断

##### 例码：

```xml
<select id="getEmpsByConditionIfWhere" resultType="com.atguigu.mybatis.beans.Employee">	
    select id, last_name, email, gender 
    from tbl_employee 
    <!-- where 1=1 -->
    <where> <!-- 在SQL语句中提供WHERE关键字，  并且要解决第一个出现的and 或者是 or的问题 -->
        <if test="id!=null">
            and id = #{id }
        </if>
        <if test="lastName!=null&amp;&amp;lastName!=&quot;&quot;">
            and  last_name = #{lastName}
        </if>
        <if test="email!=null and email.trim()!=''">
            and  email = #{email}
        </if>
        <if test="gender==0 or gender==1">
            and  gender = #{gender}
        </if>
    </where>
</select>
```

- 用 and 替换 &&；
- email.trim() 表示 去空格；
- 注意不要忘记 and ，直接拼接，用where标签解决SQL中 where 后边不能够直接接 and和or 的问题。



#### trim 

##### 使用场景

上边使用where时，感觉and有点冗余，所以可以使用trim来解决。

##### 关键字：

是在整个trim标签的前后添加前缀或者后缀

prefix: 					添加前缀

prefixOverrides:   去掉前缀

suffix: 					添加后缀

suffixOverrides:   去掉后缀

##### 例码：

```xml
<select id="getEmpsByConditionTrim" resultType="com.atguigu.mybatis.beans.Employee">
    select id, last_name, email, gender from tbl_employee 
    <trim prefix="where"   suffixOverrides="and|or">
        <if test="id!=null">
            id = #{id } and 
        </if>
        <if test="lastName!=null&amp;&amp;lastName!=&quot;&quot;">
            last_name = #{lastName} and
        </if>
        <if test="email!=null and email.trim()!=''">
            email = #{email} and
        </if>
        <if test="gender==0 or gender==1">
            gender = #{gender}
        </if>
    </trim>
</select>
```

在trim标签的整体添加where，去掉后缀 and 或者 or



#### set

去逗号



#### choose(when、otherwise)

类似于 select case

满足其中一个，就不会再选择其他

##### 例码：

```xml
<select id="getEmpsByConditionChoose" resultType="com.atguigu.mybatis.beans.Employee">
    select id ,last_name, email, gender  
    from tbl_employee
    where 
    <choose>
        <when test="id!=null">
            id= #{id}
        </when>
        <when test="lastName!=null">
            last_name = #{lastName}
        </when>
        <when test="email!=null">
            email = #{email}
        </when>
        <otherwise>
            gender = 0 
        </otherwise>
    </choose>
</select>
```

##### 关键字

otherwise 类似defout



#### foreach 

##### 使用场景

当需要用多个id从数据库中查询到多个对象时

##### 关键字

collection: 	要迭代的集合

item: 			 当前从集合中迭代出的元素，自定义的临时变量

open: 			开始字符

close:			 结束字符

separator: 	元素与元素之间的分隔符

##### 例码：

```xml
<select id="getEmpsByIds" resultType="com.atguigu.mybatis.beans.Employee">
   <!-- 
   select * from tbl_employee where id in(?,?,?);
   select * from tbl_employee where id = ?  or id = ?  or id = ? ...
   -->

    select id ,last_name ,email, gender from  tbl_employee 
    where id in
    <foreach collection="ids" item="currId" open=" (" close=")" separator=",">
        #{currId}
    </foreach>
</select>
```

其中 ids 是通过 `@Param()` 起的别名。

​		

