# SpringMVC框架笔记

## 入门案例

1. 新建一个工程后，导入如下依赖包：

   ![1566221211857](assets/1566221211857.png)

2. 在 WEB-INF/web.xml 里边配置要用到的 servlet 

   ```xml
   <servlet>
       <servlet-name>springDispatcherServlet</servlet-name>
       <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
       <!-- 配置DispatcherServlet的初始化參數：设置文件的路径和文件名称 -->
       <init-param>
           <param-name>contextConfigLocation</param-name>
           <!-- 注意这里指定的路径 -->
           <param-value>classpath:springmvc.xml</param-value>
       </init-param>
   
       <!--
               load-on-startup:用来指定此servlet在服务器启动的时候加载
               其中的1代表加载优先级，有多个servlet同时使用load-on-startup的时候发挥作用，1代表最高级
           -->
       <load-on-startup>1</load-on-startup>
   </servlet>
   <servlet-mapping>
       <servlet-name>springDispatcherServlet</servlet-name>
       <url-pattern>/</url-pattern>
   </servlet-mapping>
   ```

3. 在src目录下创建 springmvc.xml 配置文件，因为上面指定的路径，所以直接在src下创建。

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <beans xmlns="http://www.springframework.org/schema/beans"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xmlns:context="http://www.springframework.org/schema/context"
          xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">
       <!-- 扫瞄注解 -->
       <context:component-scan base-package="com.shalom.springMVC"></context:component-scan>
   
       <!--  设置拼接物理路径：/WEB-INF/views/+returnString+.jsp  -->
       <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
           <property name="prefix" value="/WEB-INF/views/"></property>
           <property name="suffix" value=".jsp"></property>
       </bean>
   </beans>
   ```

4. 然后编写HelloWorld类，并加上相应的注解，为了确保HelloWorld能够被扫描到，一定要将它放在`com.shalom.springMVC`包的里边，也可以是里边的子包的里边。

   ```java
   package com.shalom.springMVC;
   
   import org.springframework.stereotype.Controller;
   import org.springframework.web.bind.annotation.RequestMapping;
   
   //  这里必须是Controller
   @Controller
   public class Helloworld {
       //    这样所有的hello请求都会映射到这个函数
       @RequestMapping(value = "/hello")
       public String helloWorldHandler(){
           System.out.println("helloWorldHandler running");
           return "success";
       }
   }
   ```

5. 再编写 index.jsp，服务器已启动就会进入到该页面，在里边编写发动hello请求的标签。

   ```jsp
   <%@ page contentType="text/html;charset=UTF-8" language="java" %>
   <html>
     <head>
       <title>$Title$</title>
     </head>
     <body>
       <!-- 注意这里有相对路径和绝对路径之分 -->  
       <a href="hello">hello stringMVC !</a>
     </body>
   </html>
   ```

6. 大功告成，所以从客户开始的操作流程是：

   - 客户点击 index.jsp 中的标签，发起 hello 请求；
   - 映射自定匹配到 helloWorldHandler() 方法，返回 success ；
   - springmvc.xml 再将物理路径拼接出来：/WEB-INF/views/success.jsp ，然后就在浏览器中显示出该页面



## @RequestMapping注解

### 用method指定请求方式

```java
@Controller
@RequestMapping("/testRequestMapping")
public class RequestMappingTest {
    @RequestMapping(value = "/handle",method = RequestMethod.POST)
    public String handlerRequestMapping(){
        return "success";
    }
}
```

- 上边指定了 POST ，那么就不能够用 GET 请求访问该方法



### 用params指定请求必须要有的参数

#### 只要求有参数，不要求值

```java
@Controller
@RequestMapping("/testRequestMapping")
public class RequestMappingTest {
    @RequestMapping(value = "/handle",method = RequestMethod.GET,params = {"username","password"})
    public String handlerRequestMapping(){
        return "success";
    }
}
```

请求中必须包含 "username"和"password" 两个参数，对应的前端页面：

```jsp
<a href="testRequestMapping/handle?username=shalom&password=123">Test RequestMapping</a>
```



#### 对值同样有要求

```java
@Controller
@RequestMapping("/testRequestMapping")
public class RequestMappingTest {
    @RequestMapping(value = "/handle",method = RequestMethod.GET,params = {"username=shalomhu","password"})
    public String handlerRequestMapping(){
        return "success";
    }
}
```

上边要求 username 必须等于 shalomhu ，不然请求失败；对应的前端页面：

```jsp
<a href="testRequestMapping/handle?username=shalomhu&password=123">Test RequestMapping</a>
```

username 必须为 shalomhu ，password 可以随意



#### 其他点

若希望请求中不包含某个参数，则可以加感叹号，同样的也可以使用 != ：

```java
@RequestMapping(value = "/handle",method = RequestMethod.GET,params = {"!username","password!=123"})
public String handlerRequestMapping(){
    return "success";
}
```



### 用header对请求头进行约束



### 占位符@PathVariable





## REST相关的过滤器

HTML中，能够发送请求的有3个元素：<a>标签，form表单，ajax。其中<a>标签只可以发送GET请求，其余两个可以发送GET和POST请求。



HiddenHTTPMethodFilter 过滤器只能够将POST请求转换成其他请求。







## IDEA中设置XML模板

[IDEA中设置XML模板](https://blog.csdn.net/unclezh0730/article/details/79352181)

IDEA中配置 jstl 

setting -> 搜索DTDS -> 添加URL和本地 c.tld 文件
![1566375612973](assets/1566375612973.png)

c.tld文件下载地址：http://archive.apache.org/dist/jakarta/taglibs/standard/binaries/

解压后，目标文件在tld文件夹里。





## companySystem项目流程

### 配置文件的编写

#### 编写web.xml

1. 配置核心 servlet

   ```xml
   <servlet>
       <servlet-name>dispatcherServlet</servlet-name>
       <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
       <init-param>
           <param-name>contextConfigLocation</param-name>
           <param-value>classpath:springMVC.xml</param-value>
       </init-param>
       <load-on-startup>1</load-on-startup>
   </servlet>
   <servlet-mapping>
       <servlet-name>dispatcherServlet</servlet-name>
       <url-pattern>/</url-pattern>
   </servlet-mapping>
   ```

   - `<init-param>`是配置初始化参数；

   - `<load-on-startup>`是配置服务器启动的时候就加载此 servlet；

     

2. 配置过滤器，用来将post请求转换为其他请求，如delete等

   ```xml
   <filter>
       <filter-name>hiddenHttpMethodFilter</filter-name>
       <filter-class>org.springframework.web.filter.HiddenHttpMethodFilter</filter-class>
   </filter>
   <filter-mapping>
       <filter-name>hiddenHttpMethodFilter</filter-name>
       <url-pattern>/*</url-pattern>
   </filter-mapping>
   ```

   - 记住类名
   
3. 配置字符编码过滤器

   ```xml
   <filter>
       <filter-name>characterEncodingFilter</filter-name>
       <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
       <init-param>
           <param-name>encoding</param-name>
           <param-value>UTF-8</param-value>
       </init-param>
   </filter>
   <filter-mapping>
       <filter-name>characterEncodingFilter</filter-name>
       <url-pattern>/*</url-pattern>
   </filter-mapping>
   ```

   字符编码过滤器必须放在最前边，不然不生效

4. 配置 SpringIOC 容器的监听器，整合Spring

   ```xml
   <context-param>
       <param-name>contextConfigLocation</param-name>
       <param-value>classpath:applicationContext.xml</param-value>
   </context-param>
   <listener>
       <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
   </listener>
   ```

   

#### 编写SpringMVC.XML

1. 配置扫描注解

   ```xml
   <context:component-scan base-package="com.shalom.company">
   	<context:include-filter type="annotation" expression="org.springframework.stereotype.Controller"/>
   </context:component-scan>
   ```

   - base-package的意思是：扫瞄此包下的所有注解，包括里边的子包

   - 要使用context，需要在文件顶端添加相关约束：

     ```xml
     <beans xmlns:context="http://www.springframework.org/schema/context"
            xsi:schemaLocation="http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">
     ```

2. 配置视图解析器

   ```xml
   <bean id="internalResourceViewResolver" class="org.springframework.web.servlet.view.InternalResourceViewResolver">
       <property name="prefix" value="/WEB-INF/views/"/>
       <property name="suffix" value=".jsp"/>
   </bean>
   ```

   - 记住类名
   - 会将视图路径拼接：/WEB-INF/views/ + ${returnPath} + .jsp

3. 配置处理静态资源

   ```xml
   <mvc:default-servlet-handler/>
   <mvc:annotation-driven/>
   ```

   - 要想正常读取静态资源，就必须加上这两行配置，一般做项目，这两行是必加的；
   - 第一行作用是：让读取静态资源的请求由 Tomcat 默认的servlet来处理；
   - 的二行作用是：确保 RequestMapping 能够正常执行；



#### 编写applicationContext.xml

1. 配置扫描注解

   ```xml
   <context:component-scan base-package="com.shalom.company">
   	<context:include-filter type="annotation" expression="org.springframework.stereotype.Controller"/>
   </context:component-scan>
   ```

   

2. 配置数据库连接

   ```xml
   <context:property-placeholder location="classpath:db.properties"/>
   <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
       <property name="driverClassName">
           <value>${jdbc.driver}</value>
       </property>
       <property name="url">
           <value>${jdbc.url}</value>
   
       </property>
       <property name="username">
           <value>${jdbc.username}</value>
       </property>
       <property name="password">
           <value>${jdbc.password}</value>
       </property>
   </bean>
   ```

   
   

3. 配置事务

   ```xml
   <bean id="dataSourceTransactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
       <property name="dataSource" ref="dataSource"></property>
   </bean>
   <tx:annotation-driven transaction-manager="dataSourceTransactionManager"/>
   ```

   

4. 整合 Mybatis

   - 配置用来生成SQLSession的FactoryBean

     ```xml
     <bean class="org.mybatis.spring.SqlSessionFactoryBean">
         <!-- 数据源 -->
         <property name="dataSource" ref="dataSource"></property>
         <!-- 指定Mybatis全局配置文件 -->
         <property name="configLocation" value="classpath:mybatis-comfig.xml"></property>
         <!-- 配置Mybatis映射文件 -->
         <property name="mapperLocations" value="classpath:com/shalom/company/mapper/*.xml"></property>
         <!-- 别名处理 不建议放到这里 -->
     </bean>
     ```

   - Mapper接口的代理实现类的创建，管理

     ```xml
     <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
         <!-- 为此包中的所有Mapper接口生成代理实现类 -->
         <property name="basePackage" value="com.shalom.company.dao"></property>
     </bean>
     
     <!-- 另一种方法是使用 Mybatis-Spring 实现 -->
     <!--<mybatis-spring:scan base-package="com.shalom.company.dao"></mybatis-spring:scan>-->
     ```

     

#### 编写mybatis-comfig.xml

因为 数据库连接 和 Mapper接口 已经在spring配置文件中配置了，所以这里只需要配置下边几项：

```xml
<settings>
    <!-- 映射下划线到驼峰命名 -->
    <setting name="mapUnderscoreToCamelCase" value="true"/>
    <!-- 设置Mybatis对null值的默认处理 -->
    <setting name="jdbcTypeForNull" value="NULL"/>
    <!-- 开启延迟加载 -->
    <setting name="lazyLoadingEnabled" value="true"/>
    <!-- 设置加载的数据是按需还是全部 -->
    <setting name="aggressiveLazyLoading" value="false"/>
    <!-- 配置开启二级缓存 -->
    <setting name="cacheEnabled" value="true"/>
</settings>
```



### 前端页面的编写

#### 编写index.jsp

index.jsp 是服务器启动时候默认打开的页面，可以当做页面的入口。

这里写个显示公司简介列表的请求，以这个请求作为入口：

```jsp
<a href="companies">公司简介列表</a>
```

- 由于这里是获取已经存在的数据，是GET请求，所以使用 a 标签就可以；
- 点击后，发送 companies 请求，就要显示公司列表页面了；





