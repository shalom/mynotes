 # IDEA构建SSM项目（Maven）

要提前下载好Maven的压缩包

# 创建父工程

1. 先创建一个空白项目，**注意是空白项目**

2. 创建父工程（**注意：**这里一定要点创建module，如果选择project，那么空白项目就白创建了）
   ![1564979469265](C:/Users/Administrator/Desktop/myNotes/03_Java笔记/assets/1564979469265.png)

   ![1564979705028](C:/Users/Administrator/Desktop/myNotes/03_Java笔记/assets/1564979705028.png)

3. 因为他是父工程，在 pom.xml 文件中，添加如下行（项目打包方式）：

   ```xml
   <packaging>pom</packaging>
   ```

   父工程中不写代码，可以将src删除



# 配置Maven

setting -> 搜索Maven -> 设置：User setting file 到自己下载好的Maven的配置文件中，仓库的位置应该会自动匹配到Maven压缩包解压后的Respository



# 创建子工程

一共有 5 个子工程，分别是：edu-model，edu-dao , edu-service , edu-manager，edu-api

1. 右击父工程，创建一个module，将复选框勾上，并选择quickstart组件，若创建的是web项目，则选择下边的 webapp；**注意：**这里必须要提前将Maven配置好

   ![1564980478517](C:/Users/Administrator/Desktop/myNotes/03_Java笔记/assets/1564980478517.png)

   ![1564981265743](C:/Users/Administrator/Desktop/myNotes/03_Java笔记/assets/1564981265743.png)

   ![1564981361340](C:/Users/Administrator/Desktop/myNotes/03_Java笔记/assets/1564981361340.png)

   

2. 确定子工程的目录位置，点击 finish，就会看到IDEA自动在下载和执行一些东西；

3. 继续创建edu-dao , edu-service , edu-manager(这是web工程)，edu-api(同样是web工程，提供数据给安卓和IOS的)

4. 在 edu-model , edu-dao , edu-service 的配置文件（pom.xml）中 项目id下方一行 添加：

   ```xml
   <packaging>jar</packaging>  # 最终希望打包成jar包的意思
   ```

   在 edu-manager，edu-api 中添加：

   ```xml
   <packaging>war</packaging>	# 最终希望打包成war包
   ```



## 管理公共依赖

公共依赖放在父工程的 pom文件中

- 在子项目中的pom配置文件中，都有对 junit 的依赖，所以可以在父工程配置文件中统一配置，子工程中就可以删除；

  ```xml
  <dependencies>
      <dependency>
          <groupId>junit</groupId>
          <artifactId>junit</artifactId>
          <version>4.11</version>
          <scope>test</scope>
      </dependency>
  </dependencies>
  ```



- 公共版本号管理

  ```xml
  <properties>
      <junit.version>4.11</junit.version>
  </properties>
  ```

  使用 properties 字段，设定好后，上边的version就可以写成  

  ```xml
  <version>${junit.version}</version>
  ```

  





# 子工程edu-manager

## 依赖管理

这个子工程其实是Controller层，所以需要依赖 SpringMVC 框架，而其他子项目并不需要依赖 SpringMVC ，所以不添加到父工程中，所以我们就在此项目的 pom文件中添加 springMVC 的依赖。需要添加如下依赖包，主要是spring和springMVC的依赖包（SpringMVC 同时依赖 Spring），添加后Maven会自动下载。

```xml
<dependencies>
    <!--  1. 添加spring/springmvc的依赖  start =======================-->
    <!-- aop -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-aop</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- aspects -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-aspects</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- beans -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-beans</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- context -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-context</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- core -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-core</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- expression -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-expression</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- jdbc -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-jdbc</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- orm -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-orm</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- tx -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-tx</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- test -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-test</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- web -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-web</artifactId>
        <version>${spring.version}</version>
    </dependency>
    
    <!-- webmvc -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-webmvc</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <!--  添加spring/springmvc的依赖  end =======================-->
</dependencies>
```



对应的版本号：

```xml
<spring.version>4.3.13.RELEASE</spring.version>
```





## 配置manager的Tomcat

这个我清楚，就不做笔记了，配好后启动一下，浏览器出现一个 hello world



## 编写配置文件

1. 将源码文件夹和配置文件夹创建好

   ![1570865160957](C:/Users/Administrator/Desktop/myNotes/03_Java笔记/assets/1570865160957.png)

2. 在 resource 文件夹中创建 springmvc.xml 文件，文件内容如下：

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <beans xmlns="http://www.springframework.org/schema/beans"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xmlns:mvc="http://www.springframework.org/schema/mvc"
          xmlns:context="http://www.springframework.org/schema/context"
          xmlns:aop="http://www.springframework.org/schema/aop" xmlns:tx="http://www.springframework.org/schema/tx"
          xsi:schemaLocation="http://www.springframework.org/schema/beans
                       <!-- 这里的版本要一致，目前是4.3版本 -->        
         http://www.springframework.org/schema/beans/spring-beans-4.3.xsd
         http://www.springframework.org/schema/mvc
         http://www.springframework.org/schema/mvc/spring-mvc-4.3.xsd
         http://www.springframework.org/schema/context
         http://www.springframework.org/schema/context/spring-context-4.3.xsd
         http://www.springframework.org/schema/aop
         http://www.springframework.org/schema/aop/spring-aop-4.3.xsd
         http://www.springframework.org/schema/tx
         http://www.springframework.org/schema/tx/spring-tx-4.3.xsd">
   
       <!-- 1.注解扫描位置-->
       <context:component-scan base-package="com.shalom.edu.web.controller"/>
   
       <!-- 2.配置映射处理和适配器-->
       <bean class="org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping"/>
       <bean class="org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter"/>
   
       <!-- 3.视图的解析器-->
       <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
           <property name="prefix" value="/WEB-INF/views/"/>
           <property name="suffix" value=".jsp"/>
       </bean>
   </beans>
   ```

   **注意：**

   - 配置文件中 spring 的版本要和项目依赖的一致；
   - 注解扫描的包要根据项目做更改；



3. 编写 Controller 包中的 UserController 类；

4. 在 web.xml 文件中配置SpringMVC的入口

   ```xml
    <!-- 配置springmvc-->
   <servlet>
       <servlet-name>DispatcherServlet</servlet-name>
       <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
       <init-param>
           <param-name>contextConfigLocation</param-name>
           <param-value>classpath:springmvc.xml</param-value>
       </init-param>
       <load-on-startup>1</load-on-startup>
   </servlet>
   <servlet-mapping>
       <servlet-name>DispatcherServlet</servlet-name>
       <!--  注意这里加了 .do  -->
       <url-pattern>*.do</url-pattern>
   </servlet-mapping>
   ```

   加上后，启动服务器，浏览器访问：http://localhost:8080/edu-manager/user/login.do 注意这里有 `.do`，看看能否访问成功。



# 子工程edu-dao

## 依赖管理

dao 层需要依赖数据模型 model 层，所以在pom文件中添加依赖：

```xml
<dependencies>
    <dependency>
        <groupId>com.shalom.edu</groupId>
        <artifactId>edu-model</artifactId>
        <version>1.0-SNAPSHOT</version>
    </dependency>
</dependencies>
```

同时，dao层后边会用到 Mybatis 的映射文件，所以需要添加 Mybatis的依赖：

```xml
<!-- 依赖Mybatis -->
<dependency>
    <groupId>org.mybatis</groupId>
    <artifactId>mybatis</artifactId>
    <version>${mybatis.version}</version>
</dependency>

<!-- 父工程的pom文件中添加版本号 -->
<mybatis.version>3.5.0</mybatis.version>
```



## 添加映射接口和映射文件

base 映射接口

```xml
public interface BaseMapper<T> {

    public T findByID(Integer id);
    public T findByUUID(String uuid);

    public void deleteByID(Integer id);
    public void deleteByUUID(String uuid);

    public void update(T t);

    public void insert(T t);

}
```



User 映射接口，继承 BaseMapper

```xml
public interface UserMapper extends BaseMapper<User> {

}
```



User 映射文件

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.shalom.edu.mapper.UserMapper" >

    <select id="findByID" resultType="user" parameterType="int">
        SELECT * FROM t_user WHERE id = #{id}
    </select>
</mapper>
```



# 子工程edu-service

## 依赖管理

service依赖dao层

```xml
<dependencies>
    <dependency>
        <artifactId>edu-dao</artifactId>
        <groupId>com.shalom.edu</groupId>
        <version>1.0-SNAPSHOT</version>
    </dependency>
</dependencies>
```



依赖 Spring 的三个包

```xml
<!-- beans -->
<!-- @Autowired 注解需要用到此包 -->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-beans</artifactId>
    <version>${spring.version}</version>
</dependency>

<!-- context -->
<!-- @Service 注解需要用到此包 -->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-context</artifactId>
    <version>${spring.version}</version>
</dependency>

<!-- tx -->
<!-- 事务需要用到此包 -->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-tx</artifactId>
    <version>${spring.version}</version>
</dependency>
```



## 代码编写





# 完善子工程 edu-manager

## 添加剩余依赖

```xml
<!-- 数据库驱动 -->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>${mysql.version}</version>
</dependency>
<!-- 数据连接池 -->
<dependency>
    <groupId>com.mchange</groupId>
    <artifactId>c3p0</artifactId>
    <version>${c3p0.version}</version>
</dependency>

<!-- jstl标签类 -->
<dependency>
    <groupId>jstl</groupId>
    <artifactId>jstl</artifactId>
    <version>${jstl.version}</version>
</dependency>
<dependency>
    <groupId>taglibs</groupId>
    <artifactId>standard</artifactId>
    <version>${taglibs.version}</version>
</dependency>
<!-- log start -->
<dependency>
    <groupId>log4j</groupId>
    <artifactId>log4j</artifactId>
    <version>${log4j.version}</version>
</dependency>
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-api</artifactId>
    <version>${slf4j.version}</version>
</dependency>
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-log4j12</artifactId>
    <version>${slf4j.version}</version>
</dependency>

<!-- spring和Mybatis整合包 -->
<dependency>
    <groupId>org.mybatis</groupId>
    <artifactId>mybatis-spring</artifactId>
    <version>${mybatis-spring.version}</version>
</dependency>
```







注意：编译项目的时候，默认不会将xml配置文件编译到jar包中，需要手动设置





