# flex 索引

flex容器一共有6个属性：

- flex-direction
- flex-wrap
- flex-flow
- justify-content
- align-items
- align-content



## flex-direction

决定主轴方向：

```shell
row（默认值）：	 主轴为水平方向，起点在左端。  从左往右
row-reverse：	主轴为水平方向，起点在右端。	从右往左
column：			主轴为垂直方向，起点在上沿。	从上往下
column-reverse：	主轴为垂直方向，起点在下沿。	从下往上
```



## flex-wrap

`flex-wrap`属性定义：如果一条轴线排不下，如何换行。

（1）`nowrap`（默认）：不换行。

![img](assets/bg2015071007.png)

（2）`wrap`：换行，第一行在上方。

![img](assets/bg2015071008.jpg)

（3）`wrap-reverse`：换行，第一行在下方。

![img](assets/bg2015071009.jpg)



## flex-flow

是上两个属性的简化形式，默认值为 `row nowrap`。



## justify-content

`justify-content`属性定义了项目在主轴上的对齐方式。

![img](assets/bg2015071010.png)

- `flex-start`（默认值）：左对齐，子元素对齐主轴起始点；
- `flex-end`：右对齐
- `center`： 居中
- `space-between`：两端对齐，项目之间的间隔都相等。
- `space-around`：每个项目两侧的间隔相等。所以，项目之间的间隔比项目与边框的间隔大一倍。



## align-items

`align-items`属性定义项目在交叉轴上如何对齐。

![img](assets/bg2015071011.png)

- `flex-start`：交叉轴的起点对齐。
- `flex-end`：交叉轴的终点对齐。
- `center`：交叉轴的中点对齐。
- `baseline`: 项目的第一行文字的基线对齐。
- `stretch`（默认值）：如果项目未设置高度或设为auto，将占满整个容器的高度。



## align-content

`align-content`属性定义了多根轴线的对齐方式。如果项目只有一根轴线，该属性不起作用。

![img](assets/bg2015071012.png)

- `flex-start`：与交叉轴的起点对齐。
- `flex-end`：与交叉轴的终点对齐。
- `center`：与交叉轴的中点对齐。
- `space-between`：与交叉轴两端对齐，轴线之间的间隔平均分布。
- `space-around`：每根轴线两侧的间隔都相等。所以，轴线之间的间隔比轴线与边框的间隔大一倍。
- `stretch`（默认值）：轴线占满整个交叉轴。



# 子元素属性

## order

`order`属性定义子元素的排列顺序。数值越小，排列越靠前，默认为0。



## flex-grow

`flex-grow`属性定义项目的放大比例，默认为`0`，即如果存在剩余空间，也不放大。

![img](assets/bg2015071014.png)

如果所有项目的`flex-grow`属性都为1，则它们将等分剩余空间（如果有的话）。如果一个项目的`flex-grow`属性为2，其他项目都为1，则前者占据的剩余空间将比其他项多一倍。



## flex-shrink

`flex-shrink`属性定义了项目的缩小比例，默认为1，即如果空间不足，该项目将缩小。

![img](assets/bg2015071015.jpg)

如果所有项目的`flex-shrink`属性都为1，当空间不足时，都将等比例缩小。如果一个项目的`flex-shrink`属性为0，其他项目都为1，则空间不足时，前者不缩小。



## flex-basis

`flex-basis`属性定义了在分配多余空间之前，项目占据的主轴的空间。它的默认值为`auto`，即项目的本来大小。



## flex

`flex`属性是`flex-grow`, `flex-shrink` 和 `flex-basis`的简写，默认值为`0 1 auto`。后两个属性可选。

该属性有两个快捷值：`auto` (`1 1 auto`) 和 none (`0 0 auto`)。

建议优先使用这个属性，而不是单独写三个分离的属性。



## align-self

`align-self`属性允许单个项目有与其他项目不一样的对齐方式，可覆盖`align-items`属性。默认值为`auto`，表示继承父元素的`align-items`属性，如果没有父元素，则等同于`stretch`。

![img](assets/bg2015071016.png)

```
.item {
  align-self: auto | flex-start | flex-end | center | baseline | stretch;
}
```

各属性的意思另找。



# flex实战

- [Flex 布局教程：实战](http://www.ruanyifeng.com/blog/2015/07/flex-examples.html)

后面学习。。。