package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

type Trainer struct {
	Name string
	AgeAge  int
	City string
}

func main() {

	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB!")

	trainer := Trainer{
		Name: "shalom",
		AgeAge:  19,
		City: "shanghai",
	}
	collection := client.Database("shalom").Collection("Trainer")
	result, err := collection.InsertOne(context.Background(), trainer)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Inserted a single document: ", result.InsertedID)

	t := Trainer{}
	filter := bson.D{{"name", "shalom"}}
	err = collection.FindOne(context.Background(), filter).Decode(&t)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(t)

	update := bson.D{
		{"$max", bson.D{
			{"Ageage", 4},
		}},
	}

	updateResult, err := collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("updateResult ::=> ",updateResult)

	err = collection.FindOne(context.Background(), filter).Decode(&t)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(t)
}
