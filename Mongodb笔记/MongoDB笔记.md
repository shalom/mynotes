# 安装

1.下载安装包

```
curl -O https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-4.0.6.tgz
```

2.解压安装包

```
tar -zxvf mongodb-linux-x86_64-4.0.6.tgz
```

3.将安装包重命名为 mongodb 并拷贝到 /usr/local/ 目录

```
mv mongodb-linux-x86_64-4.0.6 /usr/local/mongodb
```

4.配置环境变量

```shell
vim /etc/profile or ~/.bashrc
将 /usr/local/mongodb/bin 添加到 PATH 中
source /etc/profile or ~/.bashrc
```

5.创建MongoDB数据存储目录和日志目录，最后是存放配置文件目录

```shell
mkdir -p /data/mongodb/data /data/mongodb/logs /data/mongodb/conf
```

6.编辑配置文件

```shell
vim /data/mongodb/conf/mongodb.conf

port=27017
fork=true # 以创建子进程的方式运行
dbpath=/data/mongodb/data/db #日志输出方式数据库路径
logappend=true #日志输出方式，日志append而不是overwrite
logpath=/data/mongodb/logs/mongo.log #日志路径
auth=true #开启安全验证（可以不开启）
```

7.创建MongoDB服务

```shell
vim /usr/lib/systemd/system/mongod.service

[Unit]
Description=mongoDB service
After=network.target
Wants=network.target  

[Service]
Type=forking
Restart=always
RestartSec=5
ExecStart=/usr/local/mongodb/bin/mongod --config /data/mongodb/conf/mongodb.conf 
[Install]
WantedBy=multi-user.target
```

8.启动&设置自启动

```shell
systemctl daemon-reload
systemctl start mongod
systemctl enable mongod
```

