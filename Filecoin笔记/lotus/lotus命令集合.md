# lotus命令集合

Lotus 是 Filecoin 分布式存储网络的客户端（的 golang 实现）。

本文介绍 Lotus 的命令，在终端中执行命令用例格式如下：

`lotus [全局选项] 命令 [命令选项] [参数…]` 其中，[] 内代表可选项。

全局选项有两个 :

| [全局选项]    | 说明                   |
| ------------- | ---------------------- |
| --help, -h    | 显示帮助 (默认 :false) |
| --version, -v | 打印版本 (默认 :false) |

下面介绍命令和对应的命令选项：



### **lotus daemon 命令**

作用是启动 Lotus 守护进程，在终端中执行命令用例格式如下：

```
lotus daemon [命令选项] [参数…]
```

| [命令选项] [参数…]   | 说明                                   |
| -------------------- | -------------------------------------- |
| --api value          | (默认值 :“1234”)                       |
| --genesis value      | 用于运行第一个节点的 genesis 文件      |
| --bootstrap          | (默认值 :true)                         |
| --import-chain value | 在第一次运行时，从给定文件加载链       |
| --halt-after-import  | 从文件中导入链后暂停进程 (默认 :false) |
| --pprof value        | 指定写入 cpu 性能测试文件的文件名      |



### **lotus auth 命令**

作用是管理 RPC 权限，在终端中执行命令用例格式如下：

```
lotus auth create-token [命令选项] [参数…]
```

| [命令选项] [参数…] | 说明                                             |
| ------------------ | ------------------------------------------------ |
| --perm value       | 给令牌分配权限中的一种 :read, write, sign, admin |

### **lotus chain 命令**

作用是与 filecoin 区块链交互，在终端中执行命令用例格式如下：

```
lotus chain 命令 [命令选项] [参数…]
```

| 命令     | [命令选项] [参数…]       | 说明 |
| -------- | ------------------------ | ---- |
| head     |                          |      |
|          | 打印链头                 |      |
| getblock |                          |      |
|          | 获取一个块并打印它的细节 |      |

| --raw| 只打印原始块头 (默认 :false)
read-obj|
| 读取对象的原始字节
getmessage|
| 获取并通过其 cid 打印消息
sethead|
| 手动设置本地节点头 tipset(注意 : 通常仅用于恢复)

| --genesis| 将链头重置为 genesis(默认为 false)

| --epoch value| 重置链头至指定世代（epoch）(默认值 :0)
list|
| 查看链的一部分

| --height value| （默认值：0）

| --count value| （默认值：30）

| --format value| 指定打印出 tipset 的格式 (默认 :":() ")
get|
| 通过路径获取链的 DAG 节点

```
    描述 : 在指定路径下获取 ipld 节点  
    lotus chain get /ipfs/[cid]/some/path  
    注意 : 你可以使用特殊的路径元素来遍历一些数据结构 :  
    - /ipfs/[cid]/@H:elem - get 'elem' from hamt  
    - /ipfs/[cid]/@Ha:t01 - get element under Addr(t01).Bytes  
    - /ipfs/[cid]/@A:10 - get 10th amt element
```



| 命令   | [命令选项] [参数…]  | 说明 |
| ------ | ------------------- | ---- |
| export |                     |      |
|        | 将链导出到 car 文件 |      |

| --tipset value|

slash-consensus|
| 报告共识错误
bisect|
| 因事件而二分链

```
描述：对链状态树进行二分
lotus chain bisect [min height] [max height] ``'1/2/3/state/path' 'shell command' 'args'
返回第一个条件为 true 的提示集
v
[开始]FFFFFFFTTT[结束]
示例 : 查找交易 ID 为 100,000 时出现的高度
- lotus chain bisect 1 32000 ``'@Ha:t03/1'`` jq -e '.``[2] > 100000'
有关特殊路径元素，请参见 ``'chain get'`` 帮助

```



### **lotus client 命令**

作用是进行交易，存储数据，检索数据，在终端中执行命令用例格式如下：

```
lotus client 命令 [命令选项] [参数…]
```

| 命令     | [命令选项] [参数…]                   | 说明 |
| -------- | ------------------------------------ | ---- |
| import   |                                      |      |
|          | 导入数据                             |      |
| local    |                                      |      |
|          | 列出本地导入的数据列表               |      |
| deal     |                                      |      |
|          | Initialize storage deal with a miner |      |
| find     |                                      |      |
|          | 在网络中查找数据                     |      |
| retrieve |                                      |      |
|          | 从网络中检索数据                     |      |

| --address value| transactions 的地址
query-ask|
| find a miners ask

| --peerid value| specify peer ID of node to make query against

| --size value| 数据字节大小 (默认值 :0)

| --duration value| 交易持续时间 (默认值 :0)
list-deals|
| 列出存储市场交易清单



### **lotus fetch-params 命令**

作用是获取验证参数，在终端中执行命令用例格式如下：

```
lotus fetch-params [命令选项] [参数…]
```

| [命令选项] [参数…]     | 说明                                       |
| ---------------------- | ------------------------------------------ |
| --proving-params value | 下载给定大小的参数用于创建证明，例如 32GiB |

### **lotus mpool 命令**

作用是管理消息池，在终端中执行命令用例格式如下：

```
lotus mpool 命令 [命令选项] [参数…]
```

| 命令    | [命令选项] [参数…]    | 说明 |
| ------- | --------------------- | ---- |
| pending |                       |      |
|         | 得到未决消息          |      |
| sub     |                       |      |
|         | 订阅 mpool 的改变     |      |
| stat    |                       |      |
|         | 打印 mempool 统计数据 |      |



### **lotus msig 命令**

作用是与多重签名钱包交互，在终端中执行命令用例格式如下：

```
lotus msig 命令 [命令选项] [参数…]
```

| 命令   | [命令选项] [参数…]       | 说明 |
| ------ | ------------------------ | ---- |
| create |                          |      |
|        | 创建一个新的多重签名钱包 |      |

| --required value| (默认值 :0)

| --value value| 给多重签名的初始资金 (默认值 :“0”)
inspect|
| 检查一个多重签名钱包
propose|
| 计划一个多签名事务
approve|
| 批准一个多签名事务



### **lotus net 命令**

作用是管理 P2P 网络，在终端中执行命令用例格式如下：

```
lotus net 命令 [命令选项] [参数…]
```

| 命令    | [命令选项] [参数…] | 说明 |
| ------- | ------------------ | ---- |
| peers   |                    |      |
|         | 打印节点集         |      |
| connect |                    |      |
|         | 连接到一个节点     |      |
| listen  |                    |      |
|         | 列出监听地址列表   |      |
| id      |                    |      |
|         | 得到节点的身份标识 |      |



### **lotus paych 命令**

作用是与 filecoin 链状态的交互和查询，在终端中执行命令用例格式如下：

```
lotus paych 命令 [命令选项] [参数…]
```

| 命令    | [命令选项] [参数…]                   | 说明 |
| ------- | ------------------------------------ | ---- |
| get     |                                      |      |
|         | 创建一个新的支付管道或获得一个现有的 |      |
| list    |                                      |      |
|         | 列出所有本地注册的付款管道           |      |
| voucher |                                      |      |
|         | 与支付管道凭证进行交互               |      |

| create| 创建已签名的支付管道凭证

| create --lane value| 指定要使用的支付管道通路 (默认值 :0)

| check| 检查付款管道凭证的有效性

| add| 将支付管道凭证添加到本地数据存储中

| list| 列出指定支付管道的已存储的凭证

| list --export| 输出导出字符串 (默认值 :false)

| best-spendable| 打印当前可消费的价值最高的凭证

| submit| 提交凭证到链以更新支付管道状态

### **lotus send 命令**

作用是账户间转账，在终端中执行命令用例格式如下：

```
lotus send [命令选项]
```

| [命令选项] [参数…] | 说明                       |
| ------------------ | -------------------------- |
| --source value     | 可选的指定要发送资金的帐户 |



### **lotus state 命令**

作用是与 filecoin 链状态的交互和查询，在终端中执行命令用例格式如下：

```
lotus state 命令 [命令选项] [参数…]
```

| 命令              | [命令选项] [参数…]             | 说明 |
| ----------------- | ------------------------------ | ---- |
| power             |                                |      |
|                   | 查询全网算力或矿机算力         |      |
| sectors           |                                |      |
|                   | 查询矿机的扇区集               |      |
| proving           |                                |      |
|                   | 查询一个矿机的证明集           |      |
| pledge-collateral |                                |      |
|                   | 获得最低限度的矿机抵押         |      |
| list-actors       |                                |      |
|                   | 列出网络中的所有角色           |      |
| list-miners       |                                |      |
|                   | 列出网络中的所有矿工           |      |
| get-actor         |                                |      |
|                   | 打印角色信息                   |      |
| lookup            |                                |      |
|                   | 查找对应的 ID 地址             |      |
| replay            |                                |      |
|                   | 在 tipset 中重放特定消息       |      |
| sector-size       |                                |      |
|                   | 查看矿机的扇区规格             |      |
| read-state        |                                |      |
|                   | 查看角色状态的 json 表示       |      |
| list-messages     |                                |      |
|                   | 列出与给定条件匹配的链上的消息 |      |

| --to value| 消息返回到给定地址

| --from value| 从给定地址返回消息消息

| --toheight value| 给定块高度 (默认值 :0) 之前不要查看

| --cids| 打印消息 cid 而不是消息 (默认 :false)
compute-state|
| 进行状态计算

| --height value| 将高度设置为计算状态 (默认值 :0)

| --apply-mpool-messages| 将来自 mempool 的消息应用到计算状态 (默认 :false)
call|
| Invoke a method on an actor locally

| --from value| (默认值 : "t01")

| --value value| 指定调用的值 (默认值：“0”)

| --ret value| 指定如何解析输出 (auto, raw, addr, big)(默认值 :"auto")
get-deal|
| 查看链上交易信息
wait-msg|
| 等待消息出现在链上

| --timeout value| (default: "10m")

| --tipset value| 指定 tipset 调用方法 (传递逗号分隔的 cids 数组)

### **lotus sync 命令**

作用是检查或与链同步器交互，在终端中执行命令用例格式如下：

```
lotus sync 命令 [命令选项] [参数…]
```

| 命令      | [命令选项] [参数…]                                  | 说明 |
| --------- | --------------------------------------------------- | ---- |
| status    |                                                     |      |
|           | 检查同步状态                                        |      |
| wait      |                                                     |      |
|           | 等待同步完成                                        |      |
| mark-bad  |                                                     |      |
|           | 将给定的块标记为 bad，将阻止包含此块的 xxx 同步到链 |      |
| check-bad |                                                     |      |
|           | 检查给定块是否被标记为坏块，原因是什么              |      |



### **lotus version 命令**

作用是打印版本号



### **lotus wallet 命令**

作用是管理钱包，在终端中执行命令用例格式如下：

```
lotus wallet 命令 [命令选项] [参数…]
```

| 命令        | [命令选项] [参数…]       | 说明 |
| ----------- | ------------------------ | ---- |
| new         |                          |      |
|             | 生成一个给定类型的新 key |      |
| list        |                          |      |
|             | 列出钱包列表             |      |
| balance     |                          |      |
|             | 获取账户余额             |      |
| export      |                          |      |
|             | 导出 key                 |      |
| import      |                          |      |
|             | 导入 key                 |      |
| default     |                          |      |
|             | 获取默认钱包地址         |      |
| set-default |                          |      |
|             | 设置默认钱包地址         |      |



### **lotus log 命令**

作用是管理日志，在终端中执行命令用例格式如下：

```
lotus log 命令 [命令选项] [参数…]
```

| 命令      | [命令选项] [参数…]                           | 说明 |
| --------- | -------------------------------------------- | ---- |
| list      |                                              |      |
|           | 列出日志系统                                 |      |
| set-level |                                              |      |
|           | 设置日志系统的日志级别，系统标志可被多次指定 |      |

| --system value| 限定具体日志系统

```
    格式：lotus log set-level [command options]   
    例如：lotus log set-level --system chain --system blocksync debug  

    Available Levels: debug，info，warn，error  
    Environment Variables:  
    GOLOG_LOG_LEVEL - 所有日志系统，默认日志级别  
    GOLOG_LOG_FMT   - 更改输出日志格式 (json, nocolor)  
    GOLOG_FILE      - 将日志以追加形式写入 stderr 文件
```



### **help 命令**

作用是显示命令列表或单个命令的帮助