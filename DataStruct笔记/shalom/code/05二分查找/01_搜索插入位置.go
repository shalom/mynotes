package main

import "fmt"

/*
给定一个有序无重复元素数组和一个目标值，在数组中找到目标值，并返回其索引。
如果目标值不存在于数组中，返回它被按顺序插入后的位置(这里不要求一定要插入)。
 */

func searchInsert(nums []int, target int) int {
	left := 0
	right := len(nums)

	var result int
	for {
		i := (left + right) / 2

		if nums[i] > target {
			right = i
			fmt.Printf("left:%d right:%d\n",left,right)
		} else if nums[i] < target {
			left = i + 1
			fmt.Printf("left:%d right:%d\n",left,right)
		} else {
			result = i
			break
		}
		if left >= right {
			result = left
			break
		}
	}
	return result
}

func main() {
	arr:=make([]int,10)
	for i := 0; i < 10; i++ {
		arr[i] = 10+2*i
	}
	fmt.Println(arr)

	result:=searchInsert(arr,9)
	fmt.Println("result: ",result)
}
