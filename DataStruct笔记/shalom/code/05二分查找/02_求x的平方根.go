package main

import "fmt"

/*
	计算并返回 x 的平方根，其中 x 是非负整数。
	由于返回类型是整数，结果只保留整数的部分，小数部分将被舍去。
 */

func mySqrt(x int) int {
	left := 0
	right := x
	var n, num, result int
	for {
		num = (left + right) / 2

		n = num * num

		if n < x {
			left = num + 1
		} else if n > x {
			right = num - 1
		} else {
			result = num
			break
		}
		fmt.Println(left, right)
		if left >= right {
			if left*left > x {
				result = left - 1
			} else {
				result = left
			}

			break
		}
	}
	return result
}

/*
	时间复杂度: O(log(n))
 */

func main() {
	sqrt := mySqrt(0)
	fmt.Println(sqrt)
}
