package main

import (
	"bytes"
	"container/list"
	"crypto/sha256"
	"fmt"
)

//创建结构体
type MerkleTree struct {
	RootNode *MerkleNode
}

type MerkleNode struct {
	Left 	*MerkleNode
	Right 	*MerkleNode
	Data 	[]byte
}

//创建一个新的节点
func NewMerkleNode(left,right *MerkleNode,data []byte) *MerkleNode {
	mNode := MerkleNode{}

	if left == nil && right == nil {
		//叶子节点
		hash := sha256.Sum256(data)
		mNode.Data = hash[:]
	} else {
		prevHashes := append(left.Data,right.Data...)
		hash := sha256.Sum256(prevHashes)
		mNode.Data = hash[:]
	}

	mNode.Left = left
	mNode.Right = right

	return &mNode
}

//生成一颗新树
func NewMerkleTree(data [][]byte) *MerkleTree {
	var nodes []MerkleNode

	if len(data) % 2 != 0 {
		data = append(data,data[len(data) - 1])
	}

	for _,datum := range data {
		node := NewMerkleNode(nil,nil,datum)
		nodes = append(nodes,*node)
	}

	//循环一层一层的生成节点，知道到最上面的根节点为止
	for i := 0; i < len(data)/2; i++ {
		var newLevel []MerkleNode

		for j := 0; j < len(nodes); j += 2 {
			node := NewMerkleNode(&nodes[j],&nodes[j+1],nil)
			newLevel = append(newLevel,*node)
		}

		nodes = newLevel
	}

	mTree := MerkleTree{&nodes[0]}

	return &mTree
}
func(tree *MerkleTree) UpdateMerkleTree(data,newData []byte){
	if tree.RootNode == nil {
		return
	}
	oldhash := sha256.Sum256(data)
	newhash := sha256.Sum256(newData)
	s := NewStack()
	s.Push(tree.RootNode)
	
	for !s.Empty() {
	
		cur := s.Pop().(*MerkleNode)
		
		if bytes.Equal(cur.Data, oldhash[:]){
			cur.Data = newhash[:]
		}
		if cur.Right != nil {
			s.Push(cur.Right)
		}
		if cur.Left != nil {
			s.Push(cur.Left)
		}
	}
	s = NewStack()
    out := NewStack()
    s.Push(tree.RootNode)
    
    for !s.Empty() {
        cur := s.Pop().(*MerkleNode)
        out.Push(cur)
        
        if cur.Left != nil {
            s.Push(cur.Left)
        }
        
        if cur.Right != nil {
            s.Push(cur.Right)
        }
    }
    
    for !out.Empty() {
        cur := out.Pop().(*MerkleNode)
       if cur.Right != nil && cur.Left!=nil{
			prevHashes := append(cur.Left.Data,cur.Right.Data...)
			hash := sha256.Sum256(prevHashes)
			if !bytes.Equal(cur.Data, hash[:]){
				cur.Data = hash[:]
			}
		}
    }
	return
}
type Stack struct {
	list *list.List
}

func NewStack() *Stack {
	list := list.New()
	return &Stack{list}
}

func (stack *Stack) Push(value interface{}) {
	stack.list.PushBack(value)
}

func (stack *Stack) Pop() interface{} {
	if e := stack.list.Back(); e!= nil {
		stack.list.Remove(e)
		return e.Value
	}

	return nil
}

func (stack *Stack) Len() int {
	return stack.list.Len()
}

func (stack *Stack) Empty() bool {
	return stack.Len() == 0
}
func main()  {
	tree := NewMerkleTree([][]byte{[]byte("0x1111"), []byte("0x1111"),[]byte("0x1112"),[]byte("0x1112")})
	fmt.Println(tree.RootNode.Data)

	//更新
	tree.UpdateMerkleTree([]byte("0x1112"),[]byte("0x1111"))
	fmt.Println(tree.RootNode.Data)
}