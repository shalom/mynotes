# Ubuntu 16.04 安装MySQL

Linux安装软件有多种方式，像Ubuntu就有3中安装软件的方式：

1. 通过 APT 方式安装

   在 Centos 系统中，类似的方式是 Yum

2. 通过下载离线安装包 DEB Bundle 进行安装

3. 通过 tar.gz 压缩包进行安装

   不同版本的 Linux 服务器通用的一种方式

以上3种方式各有利弊，我用的比较多的是第一种和第三种。

**APT方式安装的特点：**

使用 APT 方式安装软件，APT会为你把软件的设置都设置好，包括环境变量、开机自启动等；比较麻烦的一点是，它会将软件依赖的文件分散存储在不同的目录下，这样你需要改某些配置文件的时候，就需要花些精力去找这些配置文件。

**tar.gz方式安装的特点：**

使用tar.gz安装软件，所有的配置都需要程序员手动配置，同时所有的文件位置也是在程序员的掌握之中；这样配置相对比较繁琐，需要知道软件的每个配置的作用，并且还需要进行服务管理等操作，比如开机自启动。

这里先给出一篇优秀博客：[Ubuntu 16.04安装MySQL](https://www.cnblogs.com/EasonJim/p/7147787.html) 本文只记录这篇文章以外的内容，并且主要以 APT 方式为主。

## APT 方式安装细节

### 指定要安装软件的版本号

找出 apt 软件源中，mysql-server 所有可安装的版本号：

```sh
apt-cache madison <<package name>>
apt-cache madison mysql-server
```

在我的虚拟机中，列出的结果是这样的：

```
mysql-server | 5.7.31-0ubuntu0.16.04.1 | http://cn.archive.ubuntu.com/ubuntu xenial-updates/main amd64 Packages
mysql-server | 5.7.31-0ubuntu0.16.04.1 | http://cn.archive.ubuntu.com/ubuntu xenial-updates/main i386 Packages
mysql-server | 5.7.31-0ubuntu0.16.04.1 | http://security.ubuntu.com/ubuntu xenial-security/main amd64 Packages
mysql-server | 5.7.31-0ubuntu0.16.04.1 | http://security.ubuntu.com/ubuntu xenial-security/main i386 Packages
mysql-server | 5.7.11-0ubuntu6 | http://cn.archive.ubuntu.com/ubuntu xenial/main amd64 Packages
mysql-server | 5.7.11-0ubuntu6 | http://cn.archive.ubuntu.com/ubuntu xenial/main i386 Packages
```

表格的第二列就是版本号，如果要指定版本号，必须复制版本号全称

```sh
apt-get install mysql-server=5.7.31-0ubuntu0.16.04.1
apt-get install <<package name>>=<<version>>
```

然后会出现设置root用户密码的提示，设置完就可以使用。

使用 APT 安装的一个好处是，自动设置好开机自启。当你输入命令：

```sh
systemctl status mysql.service
```

会发现 mysql 服务处于 active 的状态，并且已经设置了开启自启

```sh
● mysql.service - MySQL Community Server
   Loaded: loaded (/lib/systemd/system/mysql.service; enabled(这里表明设置了开机自启); vendor preset: enabled)
   Active: active (running) since Mon 2020-08-17 16:38:09 CST; 51s ago
 Main PID: 10101 (mysqld)
   CGroup: /system.slice/mysql.service
           └─10101 /usr/sbin/mysqld

Aug 17 16:38:08 shalom systemd[1]: Starting MySQL Community Server...
Aug 17 16:38:09 shalom systemd[1]: Started MySQL Community Server.
```



### 软件被安装到了哪些目录中

安装好之后会创建如下目录：

数据库目录：/var/lib/mysql/ 

配置文件：/usr/share/mysql（命令及配置文件） ，/etc/mysql（如：my.cnf）

相关命令：/usr/bin(mysqladmin mysqldump等命令) 和/usr/sbin

启动脚本：/etc/init.d/mysql（启动脚本文件mysql的目录）