# 操作系统信息

Ubuntu 16.04 TLS

VMWare 虚拟机客户端

# 问题

在虚拟机上进行操作时，总是提示没有剩余空间：

```
no space left on device
```

而通过VMWare增加虚拟机的磁盘空间后，并没有生效，请看`/dev/sda5`：

```
root@shalom:/# df -h
df: /mnt/hgfs: Transport endpoint is not connected
Filesystem      Size  Used Avail Use% Mounted on
udev            467M     0  467M   0% /dev
tmpfs            98M  4.7M   93M   5% /run
/dev/sda5       9.1G  9.1G     0 100% /
tmpfs           488M     0  488M   0% /dev/shm
tmpfs           5.0M     0  5.0M   0% /run/lock
tmpfs           488M     0  488M   0% /sys/fs/cgroup
/dev/sda3       6.3G  2.9G  3.1G  49% /home
/dev/sda1       1.9G  161M  1.6G  10% /boot
tmpfs            98M     0   98M   0% /run/user/1000
```

如何扩展 `/dev/sda5` 成为了一个迫在眉睫的问题。

# 解决方法

## VMware中的操作

1. 关闭虚拟机

   只有关闭了虚拟机，才可以对虚拟机的磁盘进行扩展

2. 增加磁盘容量

   ![1585303675967](assets/1585303675967.png)

## Ubuntu系统内的操作

1. 查看磁盘现状

   ```
   root@shalom:/home/shalom# fdisk -l
   Disk /dev/sda: 100 GiB, 107374182400 bytes, 209715200 sectors
   Units: sectors of 1 * 512 = 512 bytes
   Sector size (logical/physical): 512 bytes / 512 bytes
   I/O size (minimum/optimal): 512 bytes / 512 bytes
   Disklabel type: dos
   Disk identifier: 0xe4c0db48
   
   Device     Boot    Start       End   Sectors  Size Id Type
   /dev/sda1  *        2048   3905535   3903488  1.9G 83 Linux
   /dev/sda2        3905536   8787967   4882432  2.3G 82 Linux swap / Solaris
   /dev/sda3        8787968  22460415  13672448  6.5G 83 Linux
   /dev/sda4       22460416  20971519  18725478  9.3G  5 Extended
   /dev/sda5       22462464  20971519  18725273  9.3G 83 Linux
   ```

   fdisk 命令弹出来的信息的解读，自行百度。

2. 通过fdisk工具，将 `/dev/sda4 /dev/sda5` 删除

   ```
   root@shalom:/home/shalom# fdisk /dev/sda
   
   Welcome to fdisk (util-linux 2.27.1).
   Changes will remain in memory only, until you decide to write them.
   Be careful before using the write command.
   
   
   Command (m for help): d
   Partition number (1-5, default 5): 5
   
   Partition 5 has been deleted.
   
   Command (m for help): d
   Partition number (1-4, default 4): 4
   
   Partition 4 has been deleted.
   ```

3. 重新创建扩展分区 `/dev/sda4` 和逻辑分区 `/dev/sda5` 

   - 起始和终止都选择默认

4. 重启虚拟机

5. 重新调整分区大小

   ```sh
   resize2fs -p /dev/sda4
   ```

6. 完成



# 理论知识

## 磁盘

在linux系统中所有的设备都会以文件的形式存储。设备一般保存在`/dev`目录下面，以`sda、sda1、sda2 ...，sdb、sdb1...，hda，hdb`为名。现在的设备一般都是 sd 命名，以前的很老的硬盘是以 ha 命名。

sda：第一块硬盘，如果对磁盘进行了分区会有sda1(第一个分区)，sda2等。

sdb：第二个硬盘，同样对硬盘分区后有sdb1，sdb2等。

## 分区

分区的目的就是便于管理，比如在windows系统我们一般会分C盘，D盘，E盘等。

linux只能创建4个主分区，如果需要创建更多的分区，那么就必须创建逻辑分区，其中逻辑分区需要占用一个主分区。

## fdisk磁盘管理工具

fdisk 是 Linux 的磁盘分区表操作工具。

```sh
fdisk [-l] ${装置名称}
```

选项与参数：

- `-l`：输出后面接的装置所有的分区内容。若仅有 `fdisk -l` 时， 则系统将会把整个系统内能够搜寻到的装置的分区均列出来。

### 实例一

列出所有分区信息

```sh
root@shalom:/home/shalom# fdisk -l
Disk /dev/sda: 100 GiB, 107374182400 bytes, 209715200 sectors # 磁盘的总容量，总柱面数
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xe4c0db48

Device     Boot    Start       End   Sectors  Size Id Type
/dev/sda1  *        2048   3905535   3903488  1.9G 83 Linux
/dev/sda2        3905536   8787967   4882432  2.3G 82 Linux swap / Solaris
/dev/sda3        8787968  22460415  13672448  6.5G 83 Linux
/dev/sda4       22460416 209715199 187254784 89.3G  5 Extended	# 这是扩展分期
/dev/sda5       22462464 209715199 187252736 89.3G 83 Linux		# 这是逻辑分区
```



### 实例 2

找出你系统中的根目录所在磁盘，并查阅该硬盘内的相关信息

```sh
[root@www ~]# df /            <==注意：重点在找出磁盘文件名而已
Filesystem           1K-blocks      Used Available Use% Mounted on
/dev/hdc2              9920624   3823168   5585388  41% /

[root@www ~]# fdisk /dev/hdc  <==仔细看，不要加上数字喔！
The number of cylinders for this disk is set to 5005.
There is nothing wrong with that, but this is larger than 1024,
and could in certain setups cause problems with:
1) software that runs at boot time (e.g., old versions of LILO)
2) booting and partitioning software from other OSs
   (e.g., DOS FDISK, OS/2 FDISK)

Command (m for help):     <==等待你的输入！
```

输入 m 后，就会看到底下这些命令介绍

```sh
Command (m for help): m   <== 输入 m 后，就会看到底下这些命令介绍
Command action
    a   toggle a bootable flag
    b   edit bsd disklabel
    c   toggle the dos compatibility flag
    d   delete a partition            <==删除一个partition
    l   list known partition types
    m   print this menu
    n   add a new partition           <==新增一个partition
    o   create a new empty DOS partition table
    p   print the partition table     <==在屏幕上显示分割表
    q   quit without saving changes   <==不储存离开fdisk程序
    s   create a new empty Sun disklabel
    t   change a partition's system id
    u   change display/entry units
    v   verify the partition table
    w   write table to disk and exit  <==将刚刚的动作写入分割表
    x   extra functionality (experts only)

    a :设置可引导标记
    b :修改bsd的磁盘标签
    c :设置DOS操作系统兼容标记
    d :删除一个分区
    l :显示已知的分区类型，其中82为Linux swap分区，83为Linux分区
    m :显示帮助信息
    n :增加一个新的分区
    o :创建一个新的空白的DOS分区表
    p :显示磁盘当前的分区表
    q :退出fdisk程序，不保存任何修改
    s :创建一个新的空白的Sun磁盘标签
    t :改变一个分区的系统号码（比如把Linux Swap分区改为Linux分区）
    u :改变显示记录单位
    v :对磁盘分区表进行验证
    w :保存修改结果并退出fdisk程序
    x :特殊功能
```

离开 fdisk 时按下 `q`，那么所有的动作都不会生效！相反的， 按下`w`就是动作生效的意思。

```sh
Command (m for help): p  <== 这里可以输出目前磁盘的状态

Disk /dev/hdc: 41.1 GB, 41174138880 bytes        <==这个磁盘的文件名与容量
255 heads, 63 sectors/track, 5005 cylinders      <==磁头、扇区与磁柱大小
Units = cylinders of 16065 * 512 = 8225280 bytes <==每个磁柱的大小

   Device Boot      Start         End      Blocks   Id  System
/dev/hdc1   *           1          13      104391   83  Linux
/dev/hdc2              14        1288    10241437+  83  Linux
/dev/hdc3            1289        1925     5116702+  83  Linux
/dev/hdc4            1926        5005    24740100    5  Extended
/dev/hdc5            1926        2052     1020096   82  Linux swap / Solaris
# 装置文件名 启动区否 开始磁柱    结束磁柱  1K大小容量 磁盘分区槽内的系统

Command (m for help): q
```

想要不储存离开吗？按下 q 就对了，不要随便按 w 。

## df 命令

df命令参数功能：检查文件系统的磁盘空间占用情况。可以利用该命令来获取硬盘被占用了多少空间，目前还剩下多少空间等信息。

语法：

```sh
df [-ahikHTm] [目录或文件名]
```

选项与参数：

- -a ：列出所有的文件系统，包括系统特有的 /proc 等文件系统；
- -k ：以 KBytes 的容量显示各文件系统；
- -m ：以 MBytes 的容量显示各文件系统；
- -h ：以人们较易阅读的 GBytes, MBytes, KBytes 等格式自行显示；
- -H ：以 M=1000K 取代 M=1024K 的进位方式；
- -T ：显示文件系统类型, 连同该 partition 的 filesystem 名称 (例如 ext3) 也列出；
- -i ：不用硬盘容量，而以 inode 的数量来显示

### 实例 1

将系统内所有的文件系统列出来！

```sh
[root@www ~]# df
Filesystem      1K-blocks      Used Available Use% Mounted on
/dev/hdc2         9920624   3823112   5585444  41% /
/dev/hdc3         4956316    141376   4559108   4% /home
/dev/hdc1          101086     11126     84741  12% /boot
tmpfs              371332         0    371332   0% /dev/shm
```

在 Linux 底下如果 df 没有加任何选项，那么默认会将系统内所有的 (不含特殊内存内的文件系统与 swap) 都以 1 Kbytes 的容量来列出来！

### 实例 2

将容量结果以易读的容量格式显示出来

```sh
[root@www ~]# df -h
Filesystem            Size  Used Avail Use% Mounted on
/dev/hdc2             9.5G  3.7G  5.4G  41% /
/dev/hdc3             4.8G  139M  4.4G   4% /home
/dev/hdc1              99M   11M   83M  12% /boot
tmpfs                 363M     0  363M   0% /dev/shm
```

### 实例 3

将系统内的所有特殊文件格式及名称都列出来

```sh
[root@www ~]# df -aT
Filesystem    Type 1K-blocks    Used Available Use% Mounted on
/dev/hdc2     ext3   9920624 3823112   5585444  41% /
proc          proc         0       0         0   -  /proc
sysfs        sysfs         0       0         0   -  /sys
devpts      devpts         0       0         0   -  /dev/pts
/dev/hdc3     ext3   4956316  141376   4559108   4% /home
/dev/hdc1     ext3    101086   11126     84741  12% /boot
tmpfs        tmpfs    371332       0    371332   0% /dev/shm
none   binfmt_misc         0       0         0   -  /proc/sys/fs/binfmt_misc
sunrpc  rpc_pipefs         0       0         0   -  /var/lib/nfs/rpc_pipefs
```

### 实例 4

将 /etc 底下的可用的磁盘容量以易读的容量格式显示

```sh
[root@www ~]# df -h /etc
Filesystem            Size  Used Avail Use% Mounted on
/dev/hdc2             9.5G  3.7G  5.4G  41% /
```

------

## du 命令

Linux du命令也是查看使用空间的，但是与df命令不同的是Linux du命令是对文件和目录磁盘使用的空间的查看，还是和df命令有一些区别的，这里介绍Linux du命令。

语法：

```sh
du [-ahskm] 文件或目录名称
```

选项与参数：

- -a ：列出所有的文件与目录容量，因为默认仅统计目录底下的文件量而已。
- -h ：以人们较易读的容量格式 (G/M) 显示；
- -s ：列出总量而已，而不列出每个各别的目录占用容量；
- -S ：不包括子目录下的总计，与 -s 有点差别。
- -k ：以 KBytes 列出容量显示；
- -m ：以 MBytes 列出容量显示；

### 实例 1

只列出当前目录下的所有文件夹容量（包括隐藏文件夹）:

```sh
[root@www ~]# du
8       ./test4     <==每个目录都会列出来
8       ./test2
....中间省略....
12      ./.gconfd   <==包括隐藏文件的目录
220     .           <==这个目录(.)所占用的总量
```

直接输入 du 没有加任何选项时，则 du 会分析当前所在目录的文件与目录所占用的硬盘空间。

### 实例 2

将文件的容量也列出来

```sh
[root@www ~]# du -a
12      ./install.log.syslog   <==有文件的列表了
8       ./.bash_logout
8       ./test4
8       ./test2
....中间省略....
12      ./.gconfd
220     .
```

### 实例 3

检查根目录底下每个目录所占用的容量

```sh
[root@www ~]# du -sm /*
7       /bin
6       /boot
.....中间省略....
0       /proc
.....中间省略....
1       /tmp
3859    /usr     <==系统初期最大就是他了啦！
77      /var
```

通配符 * 来代表每个目录。

与 df 不一样的是，du 这个命令其实会直接到文件系统内去搜寻所有的文件数据。



# 更多资讯

[linux 使用fdisk分区扩容](https://www.cnblogs.com/chenmh/p/5096592.html)

[Linux下使用fdisk扩展分区容量](https://www.linuxprobe.com/linux-fdisk-size.html)

[Linux 磁盘管理](https://www.runoob.com/linux/linux-filesystem.html)



