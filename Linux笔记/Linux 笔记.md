# Linux 笔记

## Ubuntu

### Ubuntu16.04 Server

#### 安装Ubuntu16.04 Server 版

[链接一](https://blog.csdn.net/gongxifacai_believe/article/details/52454814)     [链接二](https://blog.csdn.net/zhengchaooo/article/details/79500209)

#### 安装VMTools

1. 开启ubuntu server虚拟机

2. vmware workstation菜单项，选取虚拟机(M) --> 安装VMware Tools

3. mkdir /mnt/cdrom  #创建一个文件夹，以挂载cdrom

4. mount /dev/cdrom /mnt/cdrom  #你可以先去/dev目录下查看有没有cdrom这个设备，这一步是挂载cdrom到/mnt/cdrom

5. cd /mnt/cdrom

6. cp VMwareTools-10.0.5-3228253.tar.gz /mnt/VMwareTools-10.0.5-3228253.tar.gz #因为在/mnt/cdrom为挂载点。我们连root权限下也不能操作，所以复制出挂载点再操作

7. cd /mnt

8. tar -zxvf  VMwareTools-10.0.5-3228253.tar.gz #解压操作不多说

9. cd  vmware-tools-distrib  #解压之后多出 vmware-tools-distrib这个文件夹，进去

10. ./vmware-install.pl #安装



### 安装

- 下载Ubuntu16.4 ios文件
- 直接通过VMWare 新建虚拟机 就可以安装

#### Ubuntu安装VMTools

1. 将VMTools光碟上的 `VMwareTools-10.0.5-3228253.tar.gz` 复制并解压(提取)到本地的任何目录；
2. <font color=red>注意：</font>必须将光碟窗口关闭；
3. 执行提取后目录`vmware-tools-distrib`中的脚本：`vmware-install.pl`，一路回车通过
4. success！



#### 更改软件源

进入 /etc/apt 目录，修改 sources.list 文件

找到可用源，最好将先前的源文件备份





#### Ubuntu安装go，goland

##### go

```shell
# 1. 使用wget工具下载安装包(或者可以先下载好安装包，复制到虚拟机中)
$ wget https://dl.google.com/go/go1.11.linux-amd64.tar.gz
# 2. 解压tar包到/usr/local 会多出一个go文件夹
$ sudo tar -zxvf go1.11.linux-amd64.tar.gz -C /usr/local
# 3. 创建Go目录
$ mkdir $HOME/go
# 4. 用vi打开~./bashrc，配置环境变量
$ vim ~/.bashrc
# 5. 增加下面的环境变量，保存退出
export GOROOT=/usr/local/go
export GOPATH=/go
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin
# 6. 使环境变量立即生效, 一些命令二选一
$ source ~/.bashrc
$ . ~/.bashrc
# 7. 检测go是否安装好
$ go version
```



##### goland

1. 下载安装包：<https://www.jetbrains.com/go/> 

2. 解压后，执行bin目录下的 goland.sh 文件，进行第一次打开

3. 配置 goland 在 Ubuntu 中的环境变量

   1. ```$ vi ~/.bashrc``` 
   2. 在最后补充： ```export PATH=$PATH:goland.sh所在目录的绝对路径 ```
   3. 完成后，在终端中就可以输入goland.sh命令打开Goland，不带./

4. 创建桌面快捷方式

   1. 在 /usr/share/applications 目录中使用 vim 编辑器创建文件 goland.desktop 

   2. 将如下代码写入该文件：

      ```shell
      [Desktop Entry]
      
      Name=GoLand
      
      Comment=GoLand
      # Exec指定启动的文件(根据自己电脑指定)
      Exec=/home/ubuntu/GoLand-2017.3/bin/goland.sh
      # Icon指定启动软件的图标(根据自己电脑指定)
      Icon=/home/ubuntu/GoLand-2017.3/bin/goland.png
      
      Terminal=false
      
      Type=Application
      
      Categories=Developer;
      ```

      

   3. 将生成的文件 goland.desktop 拖入启动器或者复制到桌面



#### 设置root密码

`sudo passwd`



#### Ubuntu重启后数据会还原

- 检查快照是否设置了自动恢复
- 检查磁盘是否设置为永久模式
- 检查是否是 guest 模式登录



#### Ubuntu切换输入法

- `Shift+Win+Space` 三个键；
- 若无法切换，可能是没有添加输入源，在设置里添加。



#### Ubuntu安装vim编辑器

​	可以联网使用命令快捷安装： `sudo apt-get install vim`



#### Ubuntu安装docker

1. [docker安装](https://www.runoob.com/docker/ubuntu-docker-install.html)

2. 将当前用户添加到docker组，因为docker安装好后，只能由root用户操作，所以要将当前用户添加到docker组中

   ```shell
   # 如果没有 docker group，则添加，有就不用
   $ sudo groupadd docker
   # 将用户加入该 group 内。然后退出并重新登录就生效啦。
   $ sudo gpasswd -a ${USER} docker
   # 重启docker服务
   $ systemctl restart docker
   # 当前用户切换到docker群组
   $ newgrp - docker
   $ docker version
       Client:
           Version: 18.06.1-ce
           API version: 1.38
           Go version: go1.10.3
           Git commit: e68fc7a
           Built: Tue Aug 21 17:24:51 2018
           OS/Arch: linux/amd64
           Experimental: false
       Server:
           Engine:
           Version: 18.06.1-ce
           API version: 1.38 (minimum version 1.12)
           Go version: go1.10.3
           Git commit: e68fc7a
           Built: Tue Aug 21 17:23:15 2018
           OS/Arch: linux/amd64
           Experimental: false
   ```

   

5. 安装docker-compose

   ```shell
   #安装依赖工具
   $ sudo apt-get install python-pip -y
   #安装编排工具
   $ sudo pip install docker-compose
   #查看版本
   $ sudo docker-compose version
   ```

   这里有一个坑：`No module named ssl_match_hostname`

   **解决方案：**

   ```py
   sudo pip uninstall backports.ssl-match-hostname
   sudo apt-get install python-backports.ssl-match-hostname
   ```



#### Ubuntu安装nodejs

1. 官方下载地址 ：https://nodejs.org/en/download/ 

2. 下载二进制源码包 

   ```shell
   $ wget https://nodejs.org/dist/v8.11.4/node-v8.11.4-linux-x64.tar.xz
   ```

3. 解压并安装 

   ```shell
   # 指定压缩包解压到/opt目录下
   $ sudo tar xvf node-v8.11.4-linux-x64.tar.xz -C /opt
   	- 在/opt目录下得到 node-v8.11.4-linux-x64 目录
   ```

4. 将node.js设置为全局可用 

   ```shell
   # 打开系统级别的配置文件 /etc/profile
   $ sudo vim /etc/profile
   # 添加如下配置项, 保存退出
       export NODEJS_HOME=/opt/node-v8.11.4-linux-x64
       export PATH=$PATH:$NODEJS_HOME/bin
   # 重新加载配置文件
   $ . /etc/profile
   ```

5. 测试

   ```shell
   $ node -v
   ```



#### 设置虚拟机和Windows系统的共享目录

在VMware的设置中的选项栏里，将共享目录打开，Ubuntu的路径在 `/mnt/hgfs/  ` 



#### Ubuntu安装 openJDK

1. 更新软件包列表：

   ```shell
   sudo apt-get update
   ```

   

2. 安装openjdk-8-jdk：

   ```shell
   sudo apt-get install openjdk-8-jdk
   ```

   

3. 查看java版本，看看是否安装成功：

   ```shell
   java -version
   ```



### 网络

#### 三种网络模式

- **桥接模式：**相当于在物理主机与虚拟机网卡之间架设了一座桥梁，从而可以<font color=red>通过物理主机的网卡访问外网</font>。

- **NAT模式：**让VM虚拟机的网络服务发挥路由器的作用，使得通过虚拟机软件模拟的主机可以通过物理主机访问外网，在真机中NAT虚拟机网卡对应的物理网卡是VMnet8。

- **仅主机模式：**仅让虚拟机内的主机与物理主机通信，不能访问外网，在真机中仅主机模式模拟网卡对应的物理网卡是VMnet1。



#### 手动设置静态IP

1. 将 VMware中的 编辑选项 的 虚拟网络编辑器 中的 VMare0 设置成 桥接模式/自动

2. 将虚拟机的网络模式设置成桥接模式，不复制物理连接状态，**注意：**这里若不设置桥接模式，下边的重启网络将会失败；

3. 命令 `ifconfig` 查看自己的网卡编号，这里以 ens33 为例

4. 输入命令 `sudo vi /etc/network/interfaces` 来编辑网络的配置文件

5. 在文件中添加：

   ```shell
   auto ens33					# 网卡号：ens33
   iface ens33 inet static		# static表示固定IP，dhcp表示自动获取IP
   address 192.168.43.13		# IP地址	
   netmask 255.255.255.0		# 子网掩码
   gateway 192.168.43.1		# 网关	如果是路由器上的话gateway设置为192.168.0.1即可
   ```

6. 输入以下命令重启网络：

   ```shell
   sudo /etc/init.d/networking force-reload  	# 重新加载网络配置文件
   sudo /etc/init.d/networking restart			# 重启网络
   ```

   

7. ping一下局域网内的ip试试能不能ping的通，ping通了就说明内网没问题了，但是如果你尝试ping一下`www.baidu.com`，你会发现报错了。原因很简单，设置为静态IP后缺少DNS服务器，因此接下来我们要设置一个永久的dns服务器。网上有最多的使用 `vim /etc/resolvconf/resolv.conf.d/base` 来配置dns的方法在Ubuntu18.04中已经行不通了，另外使用netplan的那个是针对Ubuntu Server18.04的。我们要按如下步骤配置：

   ```shell
   # 输入命令修改该文件
   sudo vi /etc/systemd/resolved.conf
   # 将DNS的#号去掉，并添加
   DNS=8.8.8.8 225.5.5.5
   ```

8. 重启客户机 `sudo reboot`

9. 再尝试 `ping www.baidu.com`

10. 若还不行尝试如下操作

    - 停止并重新启以太网卡：

       ```shell
      sudo ifconfig ens33 down
      sudo ifconfig ens33 up
      ```

    - 添加默认网关：

      ```shell
      sudo route add default gw 192.168.1.1
      ```

**原理：**

1. 桥接模式使用的是VMware0网卡，通过桥梁连接物理机的网卡，通过物理主机的网卡访问外网；
2. `/etc/network/interfaces`文件是网络的配置文件；
3. 重新加载该文件，并重启网络，网络就会按照该配置文件启动；
4. 最后再配置域名解析器DNS 。



   

上边的方法不好使，用这个：

6. 刷新ip

   单纯使用断开连接再重新连接，并不是正确的方式，正确的方式是，使用以下命令行。

   ```shell
   sudo ip addr flush ens33
   sudo systemctl restart networking.service
   ```

   

   注意，第一条代码中，你要修改成本机的网络接口。

7. 重启系统

   重新启动系统，重启后，会提示未找到合法连接，打开浏览器也是无法使用网络的，还要进行最后一步。



一般到这里就成功了，如果还没有成功，继续下边的操作：

8. 打开命令行，输入以下代码

   ```undefined
   sudo gedit /etc/NetworkManager/NetworkManager.conf
   ```

   类似于上面的操作，打开该文件，将“managed=false”修改为“managed=true”。意思是，将网络连接设置为自定义或手动。

9. 重启network manager：

   ```undefined
   sudo service network-manager restart
   ```

10. 重启系统





### apt命令（……）

- apt-key list
- sudo apt-get update 
- cat /etc/apt/sources.list
- apt-get remove docker
- sudo apt-get remove docker
- sudo apt autoremove
- sudo apt-get update
- sudo apt-get clean
- add-apt-repository 



### source命令

在当前bash环境下读取并执行FileName中的命令。该命令通常被 "." 替代，例码：

```shell
source filename 
. filename（中间有空格）
```

### vim编辑器批量添加注释和取消注释

添加注释：

```shell
# :起始行号,结束行号s/^/注释符/g
:14,19s/^/#/g
从14行到19行添加注释符： #
```

取消注释：

```shell
# :起始行号,结束行号s/^注释符//g
:14,19s/^#//g
从14行到19行取消注释符： #
```



### vim编辑器显示行号

#### 临时显示

​	在末行模式下输入命令 `set number` 即可；

#### 永久显示

在用户根目录下编写配置文件 .vimrc `vim ~/.vimrc`

然后再该文件的最后一行加上`set number`，保存退出。

**原理：**vim打开时，会首先读取该配置文件。



### 查看端口号被那个进程占用

```shell
lsof -i:8028 	# 查询8028端口号使用情况
```



### 查看bash版本

```shell
bash -version
```



### 重启

```shell
reboot
```





### 翻墙

1. 网络模式将net模式设置成 自定义vmnet8模式；
2. 从Windows查看vmnet8的IP，Linux系统里边设置网络代理 IP就填它；
3. 端口号填写Windows翻墙软件的端口





### 黑窗体显示git分支号

- 在 `~/.bashrc` 文件中添加如下代码：

  ```shell
  function git_branch {
      branch="`git branch 2>/dev/null | grep "^\*" | sed -e "s/^\*\ //"`"
      if [ "${branch}" != "" ];then
          if [ "${branch}" = "(no branch)" ];then
          	branch="(`git rev-parse --short HEAD`...)"
          fi
          echo "->$branch"
      fi
  }
  ```

- 重载 .bashrc 文件

  ```shell
  source ~/.bashrc
  ```

  

### 更改显示屏大小

设置中找到显示，然后设置分辨率，推荐1440*900 也可以根据自己屏幕大小进行调整



### 解决ifconfig命令ens33网卡不见了的问题

```shell
sudo /sbin/dhclient
```



### xshell将用户名设置为彩色

更改 .bashrc 文件，将 `# force_color_prompt=yes` 注释取消

再执行 `source ~/.bashrc` 使更改生效



### 将\r\n替换为\n

```shell
sed -i 's/\r$//' test.sh
```







### 常见错误

#### Could not get lock /var/lib/dpkg/lock

出现这个问题可能是有另外一个程序正在运行，导致资源被锁不可用。而导致资源被锁的原因可能是上次运行安装或更新时没有正常完成，进而出现此状况，解决的办法其实很简单：

在终端中敲入以下两句

```shell
sudo rm /var/cache/apt/archives/lock
sudo rm /var/lib/dpkg/lock
```



#### 执行shell脚本报错：No such file or directory

原因：shell脚本的 format格式 不正确

查看 format格式 ：

- 使用vi编辑器打开脚本

- 末行模式下输入：set ff 就会显示当前脚本的 format格式

  我出错的原因是 set ff=dos ，格式是dos

- 将文件 format格式改为 unix

  末行模式下输入：set ff=unix

- 最后再确认一下，然后保存退出。



## Centos

### yum

[yum命令](https://www.cnblogs.com/sopost/p/3245477.html)

