安装ssh服务

```sh
sudo apt update
sudo apt install openssh-server
```



安装好后，会自动启动，用system出台了查看状态

```sh
sudo systemctl status ssh
```



防火墙开启，下面的命令是Ubuntu20.4版本的防火墙开启命令：

```sh
sudo ufw allow ssh
```

