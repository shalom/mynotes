# 阿里云Centos7服务器环境搭建

## JDK 安装

### 下载和安装

Centos7 自带有 OpenJDK runtime environment  (openjdk)。它是一个在linux上实现的开源的 java 平台。

我们可以输入以下命令，以查看可用的JDK软件包列表：



```shell
yum search java | grep -i --color JDK
```

就会得到很长的一串列表。

在列表中选择一个合适版本的 JDK，通过如下命令安装：



```shell
yum install java-1.8.0-openjdk
```

### 配置

#### 配置环境变量 `JAVA_HOME` 

通过 yum 安装 JDK，默认安装的路径是 `/usr/lib/jvm/`，cd 到这个目录中你会看到类似如下的文件列表：



![img](https:////upload-images.jianshu.io/upload_images/12104622-0e22f075f73324b6.png?imageMogr2/auto-orient/strip|imageView2/2/w/929/format/webp)

1571386407588.png

所以应该设置为：`JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.222.b10-1.el7_7.x86_64`

编辑 `/etc/profile` 配置全局的 `JAVA_HOME`：



```shell
vim /etc/profile
```

再将 `export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.222.b10-1.el7_7.x86_64` 添加到最后一行；同理，将如下两行也添加到末尾：



```shell
export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
export PATH=$PATH:$JAVA_HOME/bin
```

执行 `source /etc/profile` 命令让配置文件的更改立即生效。然后就可以执行 `echo $JAVA_HOME` `echo $CLASSPATH` `echo $PATH` 来验证环境变量是否配置成功。

### 验证 java 是否安装成功

执行命令 `java -version` 查看 java 的版本

创建一个简单的 java 文件，编译执行：



```java
public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello, World! This is a test code by nixCraft!");
    }
}
```

执行命令：



```shell
//  编译
javac HelloWorld.java
//  运行
java HelloWorld
```

输出下边这句话代表 java 安装成功



```csharp
Hello, World! This is a test code by nixCraft!
```

## MySQL 安装

### 清理环境

Centos 是自带 MariaDB 数据库的，安装 MySQL 前最好先将 MariaDB 清理，以免出现错误；



```shell
rpm -qa | grep -i mariadb                           //查看有没有安装mariadb 
rpm -e --nodeps mariadb-libs-5.5.56-2.el7.x86_64    //如果有，卸载MariaDB 
```

如果之前有安装过 MySQL ，那么最好也将原来的清理干净



```shell
rpm -qa | grep -i mysql                   //查看有没有安装mysql
rpm -e MySQL-client-5.6.38-1.el7.x86_64   //如果有，卸载旧版本Mysql

//删除服务
chkconfig --list | grep -i mysql          //查看服务
chkconfig --del mysql                     //删除服务

//删除mysql分散的文件夹
whereis mysql                             //查出相应的mysql文件夹
rm -rf /use/lib/mysql                     //删除
```

### 安装依赖

MySQL 依赖两个组建：perl 和 autoconf 。安装 MySQL 前需要将这两个安装好：



```shell
yum install perl
yum -y install autoconf //此包安装时会安装Data:Dumper模块 
```

### 下载安装 MySQL

#### 下载

点击链接[下载MySQL](https://dev.mysql.com/downloads/mysql/5.6.html#downloads)

注意选择正确的版本：

- centos内核基于Red Hat，所以下载的时候需要下载Red Hat版本
- 注意是32位的还是64位的，如果不清楚自己系统的位数，可以通过 `file /bin/ls` 查看

下载好后，想办法将压缩包传到服务器上，这里有个工具 rz ，可以搜索 rz 找到相关的信息。

#### 增加 mysql 用户组



```shell
cat /etc/group |grep mysql  //检查mysql用户及组是否存在，如果没有执行下面命令
groupadd mysql  //创建组
useradd -r -g mysql mysql //创建用户并把该用户加入到组mysql，这里的 -r是指该用户是内部用户，不允许外部登录
passwd mysql  //给用户mysql设置密码，需要输入2次
```

#### 安装



```shell
// 解压mysql
tar -xvf MySQL-5.6.38-1.el7.x86_64.rpm-bundle.tar  //注意，是-xvf不是-zxvf
 
// 安装mysql
rpm -ivh MySQL-client-5.6.38-1.el7.x86_64.rpm      //-i是安装，-v是列出更多详细信息，-h是在安装时列出hash标记
rpm -ivh MySQL-devel-5.6.38-1.el7.x86_64.rpm
rpm -ivh MySQL-server-5.6.38-1.el7.x86_64.rpm 
```

### 配置

#### 修改密码



```shell
service mysql status                                               //查看mysql服务状态
//如果是开启服务状态，用service mysql status关闭服务
mysqld_safe --user=mysql --skip-grant-tables --skip-networking &  //绕过密码登录
mysql -u root -p                                                  //登录 
use mysql                                                         //切换数据库
select Host,User,Password from user;                              //查询用户
UPDATE user SET password=password("root") WHERE user='root';      //修改密码
quit                                                              //退出
```

重启服务，登陆试试密码是否修改成功



```shell
service mysql restart                                             //重启mysql服务
mysql -u root -proot                                              // -proot,p代表password,root是密码
use mysql                                                         //切换数据库
set password = password('root');                                  //第一次登陆必须修改mysql密码
flush privileges;                                                 //刷新权限
```

#### 赋予远程连接权限



```shell
// 赋予任何主机访问数据的权限（远程访问）
grant all privileges on *.* to 'root'@'%' identified by 'root' with grant option; 
flush privileges;                                                  //刷新权限
quit                                                              //退出 mysql 
service mysql restart                                             //重启mysql
```

#### 防火墙开放开3306端口

Centos7 及以上是由 firewall 来管理防火墙



```shell
//查看firewall状态（runing:运行，not runing:没有运行），如果没有运行，用systemctl start firewalld启动
firewall-cmd --state 
firewall-cmd --permanent --zone=public --add-port=3306/tcp      //添加3306端口
firewall-cmd --reload                                           //重新加载firewall
```

#### 在服务器控制台开放 3306 端口

#### 远程用图形化界面连接数据库

连接成功说明 MySQL 已经配置成功！

#### 设置mysql开机启动



```shell
chkconfig --list mysql                    //查看mysql服务
chkconfig mysqld on                       //开启MySQL服务自动开启命令
chkconfig mysql on                        //开启MySQL服务自动开启命令
```

### mysql 组装后的重要目录

- /var/lib/mysql 数据库文件
- /usr/share/mysql 命令及配置文件
- /usr/bin mysqladmin、mysqldump等命令

## Tomcat 安装

### 防火墙开放对应端口

Tomcat默认使用的是 8080 端口，开放该端口：



```shell
firewall-cmd --zone=public --add-port=8080/tcp --permanent    #(--permanent永久生效，没有此参数重启后.效)
firewall-cmd --reload  #刷新防火墙 使其生效
firewall-cmd --zone=public --list-ports #查看防火墙放行端口列表
```

### 下载

执行下载命令：



```shell
wget http://mirrors.shuosc.org/apache/tomcat/tomcat-8/v8.5.24/bin/apache-tomcat-8.5.24.tar.gz
```

解压



```shell
tar -zxvf apache-tomcat-8.5.24.tar.gz
```

### 配置

有些地方会讲到配置 java 环境变量，如果在安装 java 的时候已经配置好了，这里就不需要再配置。

#### 配置tomcat8开机启动

在`/usr/lib/systemd/system`目录下增加`tomcat.service`文件



```shell
vim /usr/lib/systemd/system/tomcat.service
```

写入如下内容：



```bash
[Unit]
Description=Tomcat  # 服务名
After=syslog.target network.target remote-fs.target nss-lookup.target # 表示在什么服务之后

[Service]
Type=oneshot
ExecStart=/usr/local/tomcat8/bin/startup.sh   # 这里需要根据自己的文件位置更改路径
ExecStop=/usr/local/tomcat8/bin/shutdown.sh   # 这里需要根据自己的文件位置更改路径
ExecReload=/bin/kill -s HUP $MAINPID
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
```

允许开机自启动：



```shell
systemctl enable Tomcat
```

配置好后就可以使用如下命令：



```shell
systemctl start tomcat8.service    //启动tomcat
systemctl stop tomcat8.service    //关闭tomcat   
systemctl restart tomcat8.service   //重启tomcat 
systemctl status tomcat8.service   //查看状态tomcat 
```

#### 更改端口号

根据自己的需求更改，默认是8080

找到自己的Tomcat安装目录，里边有一个 conf 文件夹



```shell
cd /tomcat/apache-tomcat-8.5.47/conf
```

更改里边的 server.xml 文件，在如下的地方更改端口号：



![img](assets/12104622-25d2d88d2c77cd09.webp)



更改后再重启Tomcat

## Nginx 安装

### 安装依赖

Nginx有三个依赖：**PCRE**，**zlib**  和 **OpenSSL**

- perl 是一个正则表达式库，nginx 的 http 模块需要使用 pcre 来解析正则表达式；
- zlib 库提供了很多种压缩和解压缩的方式， nginx 使用 zlib 对 http 包的内容进行 gzip；
- OpenSSL 是一个强大的安全套接字层密码库，囊括主要的密码算法、常用的密钥和证书封装管理功能及 SSL 协议，并提供丰富的应用程序供测试或其它目的使用。nginx 不仅支持 http 协议，还支持 https（即在ssl协议上传输http），所以需要在 Centos 安装 OpenSSL 库。

三个依赖库执行如下命令安装：



```shell
yum install -y pcre pcre-devel
yum install -y zlib zlib-devel
yum install -y openssl openssl-devel
```

### 下载并解压

使用`wget`命令下载



```shell
wget -c https://nginx.org/download/nginx-1.10.1.tar.gz
```

解压



```shell
tar -zxvf nginx-1.10.1.tar.gz
```

### 配置

使用默认配置就可以

cd 到解压后的目录里边，有一个 `configure` 可执行文件，输入命令 `./configure` 执行。

### 编译安装



```shell
make
make install
```

查找安装路径：



```shell
whereis nginx
```

### 启动和停止Nginx

在Nginx的安装路径中有个 sbin 文件夹，里边是 Nginx的可执行文件



```shell
./nginx 
./nginx -s stop
./nginx -s quit
./nginx -s reload
```

`./nginx -s quit`：此方式停止步骤是待nginx进程处理任务完毕进行停止。
 `./nginx -s stop`：此方式相当于先查出nginx进程id再使用kill命令强制杀掉进程。

查询Nginx的进程



```shell
ps aux|grep nginx
```

启动后，浏览器访问80端口，会得到Nginx的页面，这样Nginx就安装成功了！

## Redis 安装

下载并解压



```shell
wget http://download.redis.io/releases/redis-4.0.6.tar.gz
tar -zxvf redis-4.0.6.tar.gz
```

在解压目录下执行 make 命令进行编译，然后 make install 安装

安装完成后，执行`redis-server -v`查看版本，然后启动Redis



```shell
[root@localhost redis-4.0.6]# redis-server 
9961:C 12 Dec 05:14:58.101 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
9961:C 12 Dec 05:14:58.101 # Redis version=4.0.6, bits=64, commit=00000000, modified=0, pid=9961, just started
9961:C 12 Dec 05:14:58.101 # Warning: no config file specified, using the default config. In order to specify a config file use redis-server /path/to/redis.conf
                _._                                                  
           _.-``__ ''-._                                             
      _.-``    `.  `_.  ''-._           Redis 4.0.6 (00000000/0) 64 bit
  .-`` .-```.  ```\/    _.,_ ''-._                                   
 (    '      ,       .-`  | `,    )     Running in standalone mode
 |`-._`-...-` __...-.``-._|'` _.-'|     Port: 6379
 |    `-._   `._    /     _.-'    |     PID: 9961
  `-._    `-._  `-./  _.-'    _.-'                                   
 |`-._`-._    `-.__.-'    _.-'_.-'|                                  
 |    `-._`-._        _.-'_.-'    |           http://redis.io        
  `-._    `-._`-.__.-'_.-'    _.-'                                   
 |`-._`-._    `-.__.-'    _.-'_.-'|                                  
 |    `-._`-._        _.-'_.-'    |                                  
  `-._    `-._`-.__.-'_.-'    _.-'                                   
      `-._    `-.__.-'    _.-'                                       
          `-._        _.-'                                           
              `-.__.-'                                               
```

另起一个终端来连接Redis服务端



```shell
redis-cli
```

这样Redis就安装成功啦！