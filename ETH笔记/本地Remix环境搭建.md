离线版的remix，[文档](<https://github.com/ethereum/remix-project>)

```sh
npm install -g @nrwl/cli
git clone https://github.com/ethereum/remix-project.git

cd remix-project
npm install
nx build remix-ide --with-deps
nx serve
```

这样就可以使用浏览器访问 localhost:8080 进入Remix页面。

安装remixd来使remix可以操作本地文件夹：

```sh
# 全局安装remixd
npm install -g remixd
```

**如何使用：**

remix 需要激活 REMIXD 插件

然后使用如下命令：

```sh
remixd -s <absolute-path-to-the-shared-folder> --remix-ide <your-remix-ide-URL-instance>

# 例如，会自动将./转化为绝对路径
remixd -s ./ --remix-ide https://remix.ethereum.org
```

