# Become a validator Process

**准备参数**

要调用deposit合约的质押方法，需要准备好调用的参数。需要准备的参数有：

- #### BLS public key

- #### BLS withdrawal key

具体在合约代码中的提现：

```js
function deposit(
    bytes calldata pubkey,
    bytes calldata withdrawal_credentials,
    bytes calldata signature,
    bytes32 deposit_data_root
) external payable;
```

以上参数在后面生成的`deposit_data.json`文件中获取。

**初始化validator key**

validator key是由[这个秘钥生成器](https://github.com/ethereum/eth2.0-deposit-cli/releases/)生[成]()的，下载好可执行文件后，执行如下命令：

```bash
./deposit new-mnemonic --num_validators 3 --chain medalla
# --num_validators 指定validator数量
# --chain 指定网络，这里是medalla测试网，主网填mainnet
```

选择助记词语言（居然还有中文）：

![image-20201110185124517](assets/image-20201110185124517.png)

需要设置密码，这个密码将用来证明这个秘钥是否是属于你：

![image-20201110185355016](assets/image-20201110185355016.png)

我这里输入的是：`xhl19951005`

设置好后，就会将助记词显示出来，这个助记词一定要保管好，不要丢失也不要被人盗取：

```
夏 姚 薯 饱 卢 佳 临 鸡 验 乔 坝 徒 印 贷 砍 半 锭 响 削 呵 革 熊 争 飘
```

确认输入助记词后，就会显示生成秘钥文件的进度条：

![image-20201110185827938](assets/image-20201110185827938.png)

我们用tree命令查看生成的文件（这里面不包含withdraw key，这个可以使用助记词来生成）：

```
validator_keys/
├── deposit_data-1605005798.json					# 1605005798是生成此文件的时间戳
├── keystore-m_12381_3600_0_0_0-1605005795.json
├── keystore-m_12381_3600_1_0_0-1605005796.json
└── keystore-m_12381_3600_2_0_0-1605005797.json
```

仔细查看`deposit_data-1605005798.json`的内容：

```json
# 会发现 pubkey withdrawal_credentials amount signature deposit_data_root 等参数
因为这里json格式会打乱，就不粘贴了
```

以上参数就刚好是deposit contract的质押函数所需的参数。在lanchpad里，会要求你上传这个文件，来生成调用deposit合约所需的参数。

![image-20201110191933599](assets/image-20201110191933599.png)



如何使用ETH2客户端，docker

