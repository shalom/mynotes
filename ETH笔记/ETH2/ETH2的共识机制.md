# 

## 四个阶段

### Phase0

主要内容是信标链，而**信标链是eth2的核心**，能够管理validator并且协调分片链。这个阶段包含的是ETH2的POS共识机制，主要的参与角色是validator，并且他们的余额也会在这个阶段提现

### Phase1

处理ETH2的分片数据的添加，存储和检索

### Phase1.5

将ETH1作为ETH2的一条分片链

### Phase2

添加合约执行功能，让所有的分片链都能够执行智能合约。简单来说就是将eth2从强大的数据库升级成为**完全去中心化的计算平台。**

## 节点的 action

### Slot 和 Epoch

一个 Slot 是12 secounds，validator可以在这个时间内 propose 一个block，这个 validator 是被随机选出来的。每个 Slot 可能有一个 block，也可能没有。

全网所有的 Validator 被划分为许多个 committees，committee之间相互独立，每个 slot 都有至少一个 committee 来验证（并不是所有的 committee 都会验证每个slot）。<font color=red>(committee 如何验证 solt?验证的是什么？)</font> 。committee中的某一个 validator 会被选为代表者，与此同时其他的127个validator就会验证 slot。每个 committee 中至少有128个 validator。当前 Epoch 结束后，现有的 committees 会被打乱，所有的节点会被重新分配到新的随机的 committee 中。

一个 Epoch 由32个 Slots 组成，大约为 6.4 minutes

![img](assets/19c4273ffa5e44a8a5b69ce4b48d4006.png)

​							Slot Epoch Validator 和 Committee



## Staking & Hardware

最理想的设置和实践是用一个专门的电脑来 Staking。尽量限制其他程序在你的电脑中运行，尤其是它在连接到外网的时候。

使用Linux系统，它会得到客户端更好的支持，并且不会强制你重新启动。

在参与 Staking 的过程中，你将质押价值约为 $7000 的以太币，质押时间为 1-2 年，所以尽量买一个好一点的硬件设施来参与 Staking。

强烈建议备份电池，将路由器和调制解调器也加上，保证在断点的情况下节点也能够继续工作。



## ETH2.0的Keys

预览：

![img](assets/key.png)

ETH 1.0 和 ETH 2.0 的密钥都是以椭圆曲线加密算法为基础生成的，然而 ETH 2.0 有附加的功效，它使用 BLS 签名方案，当创建 keys 的时候，需要不同的参数。

ETH 1.0 只需要提供一个私钥就可以获得他们的资产，ETH 2.0则需要提供两个不同的私钥：The **validator** **private** key 和 the **withdrawal** **private** key。

### The validator key

The validator key 同样是由公私钥组成的。Validator **private** key 用来在 Beacon Chain 上 propose和 attest 区块，因此这个私钥应该存储在热钱包中。

这样做的好处是可以方便快捷的将 validator keys 从一个设备转移到另一个设备中。然而，如果它被偷了，小偷将可以用如下两种方式进行作恶：

- 获得验证着失信标签，有如下方式获得失信标签：
  - 成为一个 proposer 并且在一个 Slot 中发出两个不同的block提案；
  - 成为验证者时，对同一个验证对象发出两个不同的attestations
- 强制自愿退出，并且使 the withdrawal key 所有者获得 ETH 余额的所有权。

The validator public key 包含在 deposit data 中，deposit data 使得ETH2 能够识别 validator。



### The withdrawal key

The withdrawal key 在阶段一或者阶段二才可能出现，当validator想要挪移他的余额的时候，就需要提供 The withdrawal key。

丢失了 The withdrawal key ，就意味着丢失了validator的资产所有权，不过 validator 依然可以进行签名验证和提案block，但因为获得不到奖励，一般大家都不会这么做。

要想提现，需要通过 The validator key 来使 validator 的状态变为 exited。



## Genesis Event

下面要用到的一些关键词：

- `Seconds_Per_Eth1_Block` = 14 seconds

- `Eth1_Follow_Distance =` 1024 blocks * 14 seconds

- `Min_Genesis_Time` = a unix timestamp (=this has yet to be decided)

- `Min_Genesis_Active_Validator_Count` = 16,384

- `Genesis_Delay `= 7 days 

要让 ETH 2.0 启动，需要满足以下两个条件：

1. 至少需要 **16,384 validators** 
2. ETH2的启动时间不能早于 `min_genesis_time` 

因为第一个条件满足的时间可能在 `min_genesis_time` 之前，也可能在`min_genesis_time`之后，所以 ETH2 的Genesis就会存在两个不同的场景：

场景一，第一个条件满足在`min_genesis_time` 之前：

ETH2的启动时间将为 `min_genesis_time`，成为Genesis validator的时限为 `min_genesis_time` - `Genesis_Delay `

![img](assets/event1.png)

场景二，第一个条件满足在`min_genesis_time` 之后：

ETH2的启动时间将是在满足条件一的时刻加上`Genesis_Delay`

![img](assets/event2.png)



## Deposit Process

![img](assets/deposit.png)

### Mempool - Status: Unknown

这个状态是ETH1的合约执行状态。当调用 deposit 合约进行质押时，发起的交易就会处于pending状态，矿工选取 gas price 高的交易打包，而 gas price 低的交易将会延后。当网络情况比较拥堵时，有许多交易都在排队等候，这时候就会出现后提交交易的 gas price 高于先提交的，这会形成一个 gas price涨价的趋势，而之前的交易需要等待的时间就是个未知数。

### Deposit contract - Status: Deposited

合约会检查交易的 input data 是否正确，如果不正确交易会被拒绝，质押的ETH也会被退回（文档中说的是1ETH，但是目前合约中的数字是32ETH，1ETH应该是为validator充值是用的，现在这个功能还未开放）。

> 用户创建的 input data 是 validator 和 withdrawal keys 的反射（这个反射暂时理解为生成keys的solt）

#### 为何需要7.5小时

首先，需要等待ETH1的1024个区块，确保此交易不会被回滚，这个时间就需要大约 4hours。

除此之外，还需要等待 ETH2 的32个 Epoch，这里需要3.5hours。<font color=red>（为什么需要等待32个Epoch？）</font>

如果ETH1节点提供了错误的deposit logs，这将会导致更长的等待时间。所以建议自己启动 ETH1 节点。

大约7.5hours后，Beacon Chain 意识到了质押金，就可以在[ETH2浏览器上](https://beaconcha.in)看到validator的状态为deposited。

### Validator Queue - Status: Pending

进入了 Validator Queue 后，将进入一个漫长的等待期，因为每个 Epoch 只有3个validator通过 pending 状态，也就是每天900个名额。

在Genesis的时候加入的validator降不需要在队列中等待。

### Staking - Status: Active

这个状态代表validator将可以进行 propose blocks 和 sign attestations，这样就可以挣得奖励。

![img](assets/active.png)

### 其他状态

#### Deposit Invalid

质押失败

![image-20201103235436273](assets/image-20201103235436273.png)

#### Active Offline

一个Active状态的节点连续两个 Epoch 没有参与验证

![image-20201103235545076](assets/image-20201103235545076.png)

#### Exiting Online

validator处于在线状态，但即将离开ETH2网络，可能是因为余额已经不足16ETH，也可能是validator主动退出

![image-20201103235804095](assets/image-20201103235804095.png)

#### Exiting Offline

与上一个状态相对应，只是validator处于离线状态

![image-20201103235858395](assets/image-20201103235858395.png)

#### Slashing Online

validator在线，并且被惩罚即将被迫退出网络

![image-20201104000055524](assets/image-20201104000055524.png)

#### Slashing Offline

validator离线，并且被惩罚即将被迫退出网络

![image-20201104000129533](assets/image-20201104000129533.png)

#### Slashed

validator已经被踢出网络，资产将在36天后才可以提取

![image-20201104000302012](assets/image-20201104000302012.png)

#### Exited

validator正常退出网络，资产将在1天后可以被提取

![image-20201104000411319](assets/image-20201104000411319.png)



## Attestation

每个Epoch，validator将会被随机分配到一个committee中，committee会被指定去验证某一个分片链的block，网络条件好的committee将获得前面的slot，并将自己验证的block放入其中，slot越靠前奖励越高。每个validator都会提出一个 attestation(attestation相当于vote)，这个 vote 由一下几部分组成： 

- Committee
- Validator Index
- Finality vote
- Signature
- Chain head vote(vote on what the validator believes is the head of the chain，chain head就是当前Epoch的block0的attestation，由两个部分组成) 
  - Slot
  - Hash

尽管每个validator的attestation的数据很小，但由于validator的数量非常多，并且数据产生的速度非常快，那么区块链存储这些数据就需要花费大量的资源，因此减少数据非常重要，而这个减少数据的过程被称为“聚合”。

聚合是以committee为单位进行的，启动Ethereum 2.0至少需要16,384个validator。

### 聚合的认证（Aggregated Attestation）

每个block都会有一个或者多个committee来认证。每个committee最少有128个validator，其中将会随机选出16个validator作为聚合者(aggregator).

在committee中，每个validator将自己的、未聚合的认证(Attestation)广播给聚合者。16个聚合者要合并所有的attestation并且最终向着一个单一的、已经聚合的**attestation**达成一致，然后再发给block提案者。任何还未接收聚合Attestation的proposer收到后，就会将聚合的Attestation添加到block中。如下图：

![image-20201104000411319](assets/testing.png)

committee的聚合后的attestation：

- Committee
- Validators
- Chain head vote
- Finality vote
- Aggregated signature

要检查信标链的有效性，只需要对部分聚合签名进行验证，就可以评估大多数验证者的投票。



### Rewards

认证奖励取决于两个变量：the base reward 和 the inclusion delay

认证奖励(Attester incentives)=7/8 * base reward / inclusion delay

#### Base reward

(Validator effective balance * 2^6) **/ SQRT(**Effective balance of **all** active validators**)** 

SQRT是平方根

#### Inclusion delay

在一个Epoch内，当validators在为Block0进行vote时(这个时候Block1还没有被proposed)，所有的认证都将会被包含在Block0中，所以这些认证就会成为Block1的 chain head，在这个Epoch内的所有后续的Block的chain head都是Block0的attestations。Block1的Inclusion delay就为1，Block2的Inclusion delay就为2……

![image](assets/inclusion.png)

注意：上图中的Block编号是从1开始算，不是0

（<font color=red>上图和上面的那段话应该有错误，chain head应该是上一个Epoch的最后一个block的Attestation。</font>）

由此我们就可以看出 inclusion delay 对于reward的影响：

1 -> 1/2 -> 1/3 -> ……

![image](assets/inclusionEffect.png)



**注意：** 以上的顺序是由validators的网络状况竞争得来的，这就意味着：在一个epoch内，slot之间相互没有依赖，但是epoch之间是有依赖的，可以将epoch理解为传统意义下的block，而slot应该是和各个分片相关。



### 影响Inclusion delay的因素

#### Attestation Generation Delay

在committee中，有些节点会有过期的状态数据，或者因为机器的性能不足导致在生成签名花费过多的时间等情况，都会造成生成Attestation延迟。

#### Attestation Propagation（广播） Delay

这个网络延迟是一定存在的，aggregator越早接收到所有的Attestation越好。所以validator应该确保自己连接到足够多的、不同的对等节点，以确保网络畅通。

#### Aggregate Generation Delay

Aggregator因为机器性能问题，在生成聚合认证的时候发生延迟。

#### Aggregate Propagation Delay

与attestation类似，这一步也会有网络延迟



（<font color=red>以上的情况会不会导致系统功能失效？暂时的理解是所有的延迟都会有一个超时等待时间</font>）



### Attestation scenarios

#### validator错过认证

validator有1Epoch的时间去提交他们的认证，如果在当前Epoch没有提交，validator还可以在下一个Epoch之内提交。

#### Aggregator Missing

每个Epoch总共有16个Aggregator，另外，随机的validators会监听两个子网<font color=red>(两个子网是什么意思？)</font>，持续256个Epochs，并且作为Aggregators的备份

![image](assets/aggregatorBackup.png)



#### Missing block proposer

注意：aggregator也可能同时成为 block proposer。

如果一个validator被选中为proposer，提出block后下线了，这样block就不包含认证，这样下一个block的proposer将会捡起上一个block的认证，并且将它打包到下一个block中，奖励计算的 inclusion delay 将会增加1



## 奖惩机制

### 惩罚机制

惩罚机制的设立主要出于两个目的：

​		(1) 使得攻击eth2的成本难以负担

​		(2) 查核验证者是否真正履行了职责，防止验证者消极怠惰。

#### 罚没

如果有验证者被证明作恶，他们的部分或全部权益就会被销毁。遭到罚没的验证者无法继续参与网络的共识机制，会被强制退出。

#### 两种罚没场景

##### 双重投票（double voting）

这个场景可以理解为POS协议的双花攻击，validator在同一个epoch中对两个不同的区块进行投票，同时这两个区块中花费的ETH来源相同。（<font color=red>**目前的理解是validator在同一个epoch内只会被分配到一个Slot内进行投票**</font>）

另一个版本是：**Proposer slashing**，validator在一个epoch中的同一个slot发出两个不同的block提案。

##### 环绕投票（surround voting）

validator在对同一个Slot的block进行投票的时候，发出了两次投票，并且两次投票的版本不同，并且没有表明他们不再信任第一个版本的block，出现这样的情况就视为环绕投票。



当前阶段，有且仅当一下两种场景会导致validator被罚没。后续阶段会增加其他规则。当validator遵循协议，并没有蓄意作恶的行为时，出现了罚没的情况，这个能使程序出现了bug，那么这个时候应该怎么办呢？

为了将这种以为损失降低到最小，被销毁的质押金会与同时段（epoch）被罚没的validator数量成一定比例。因为只有数量很少的validator出现罚没时，不太可能是对ETH2发起的攻击，因为要攻击成功是需要花费大量的validator的。所以在这种情况，系统会认为validator是无心之过，会惩罚，但是力度不会很大（最低的罚没金额为1ETH，但是在Phase0，这个值为0.25ETH）。相反的，如果大量的validator几乎同时产生不当的行为，那么他们将会损失很大一部分的质押金（最高罚没金额为全部余额），因为这种情况会被视为对ETH2发动的网络攻击。

被罚没的验证着将被迫停止继续参与协议，会被强制退出；如果validator是无心之过，呢么这种小额惩罚的措施可以防止犯错的validator一错再错。（<font color=red>这里有一个风险是：当因为程序bug出现小额罚没的情况，自己将会承受损失</font>）

#### 离线的验证者

在正常的情况下，离线的validator会失去他们本可以通过验证工作所获得的奖励，并且会收到一定的惩罚。总的来说，如果validator有50%以上的时间在线，他们的质押金就会随着时间的推移增加。

由此可见，离线的惩罚并不是很重，当validator client进行维护等工作时，最好的选择是短时间离线，而不是采取退出协议再重新加入的方式（因为退出和进入都存在相关延迟性）。

实际上，因为一些维护的原因，系统中可能出现两个实体是同一个validator，因为主客户端和备份客户端同时在线（ETH2并不会排除同时在线的情况）。这个时候就可能导致validator出现罚没的情况：同一个validator由两个不同的validator client发起了两个不同版本的vote。测试网中就出现过这种情况。

这种离线惩罚机制能够成立的前提是区块正在被最终确认，即有2/3的验证者（按权益加权）在线并且其投票正在被计数。这是eth2在正常情况下应该达到的预期状态。**如果少于2/3的验证者在线，就说明eth2中发生了灾难性的错误。**在此类情况下，以太坊的共识协议（包括Casper）则无法达成共识。

#### ETH2如何在战争中存活

战争会导致大量的validator离线，ETH2面对这样子的情况时，会让这些离线的validator面临 inactivity leak 惩罚（消极惩罚）。随着离线时间的拉长，收到inactivity leak 惩罚的validator将会被一点点的扣减余额，当余额减至某个程度（16ETH）时，ETH2 将不再需要这些validator的参与，这部分节点就被剔除。这将导致ETH2的总validator数量减少，比例就会回到2/3以上。

随着战争的不断蔓延，可能会不断的循环上演上面的过程。ETH2的生命力也将在这种极端的情况下得到考验。

如果这种情况发生了，里显得离线的validator client在 21 天内损失的ETH可高达 50% (16 ETH)。21 天之后，这些验证者就会被逐出验证者池。



通过惩罚机制的分析，我们可以预测：如果验证者都依赖于相同的现实来源（如Infura）或是都借助AWS来托管validator client，一旦这些托管方发生什么错误，验证者们会面临更为严厉的惩罚。这也是风险之一。



### 激励机制

#### 检举者奖励

通过提供证明，发起其他验证者罚没行为的警告，这些验证者能够因其为eth2扫除不利而获得奖励。

#### **区块提议者奖励**

验证者会被随机地分配产生区块的任务，被选中的验证者就是“区块提议者”（proposer）。区块提议者能够通过以下方式获得奖励：

▫ 打包一份来自检举者的证明，且该证明证实某个验证者被罚没；

▫ 打包来自其他验证者的证明（attestations）

当验证者被选中提议区块时，奖励能够鼓励其向区块链提供有用信息。

#### **证明者奖励**

证明（attestations）是表明验证者同意eth2中某个决定的投票。这类信息构成了eth2的共识基础，主要通过以下5种途径获得奖励：

- 获取链上证明
- 与验证者就区块链历史记录达成共识
- 与验证者就区块链最前部分达成共识
- 快速使证明上链
- 在指定分片中指向正确区块

**开拓验证者收益**

在PoS系统中，有两种常见的验证者奖励模式：**固定奖励**（fixed rewards）和**固定通胀**（fixed inflation）。

在固定奖励模式中，验证者通过履行职责获得固定数额的奖励，而通胀率取决于验证者数量。这种模式需要解决的问题在于如何设置回报率（reward rate）。如果回报率过低。验证者数量会过少；如果回报率太高，则会衍生出安全性需求之外的验证行为，并且浪费资金。

因此，拥有固定通胀率的模式更受欢迎。在这种模式中，既定数额的奖励由所有的活跃验证者瓜分。这种模式的好处在于利用市场的力量来寻求合适的奖励数额，因为验证者可以根据当下的收益自行选择是否参与验证。

但这种模式也存在一定缺陷。验证者奖励有可能是不稳定的，这使得对于个人验证者来说，很难做出盈利性决策；这种模式还使得协议容易暴露在“泄气攻击”（discouragement attack）[3]之下，受到这种攻击时，验证者会试图阻止其他验证者参与进来，以此提高自己的奖励（即便自己会受到暂时的损失）。

Eth2旨在结合两种模式的优点：**验证者的奖励与ETH质押总量的平方根成正比。**这种混合奖励模式的好处是在抑制通胀变化和验证者回报率的同时，依然能够借助市场力量来决定验证者的奖励金额。

### 提现情况

你可以用你的验证者节点签名一条自愿退出信息以示意你想要停止验证。但是请记住，在阶段0里，一旦你退出了就回不去了。你不能再激活你的验证者节点了，在阶段1.5之前你也不能转移或提取你的资金(这意味着在阶段1.5之前你都无法访问你的资金)。

## 共识机制

Greedy Heaviest Observed Subtree (GHOST，贪婪最重可观察子树协议) ，在GHOST协议中，获得投票数最高的分叉将被选为链头（将每个分叉区块及其各子区块的所有票数计入）。也就是说，每当发生分叉时，GHOST会选择拥有更多votes的区块子树，votes会一直统计到没有子区块的区块。

准确来说，Eth2在其PoS机制中采用了GHOST的一个变体，即**Latest Message Driven GHOST (LMD-GHOST，由最新消息驱动的GHOST)**。



Casper the Friendly Finality Gadget (Casper FFG)

**Casper FFG机制在做出决策的时候更倾向于保障安全性而非活性。** 这就意味着虽然Casper FFG能做出最终性决策，但如果网络状态不理想，它可能无法就任何事情做出决策。

Eth2并不会在每个slot（一个区块生成的预计时间）都进行证明和最终确认，而是每32个slots进行证明和最终确认，每32个slots就称为一个epoch。首先，验证者要对一个epoch中的所有32个区块进行签名，表示就此达成一致。如果2/3的验证者进行了签名，那么这些区块就得到了证明。在后一个epoch中，验证者们会进行一次投票，表明他们看见了之前被证明的epoch，如果有2/3的验证者投票，那么前一个epoch就被最终确定，并且永久地成为Eth2链的一部分。

FFG这种方式十分高明。**投票实际上由两个子投票构成，一个用于证明epoch，另一个用于对证明过后的epoch进行最终确认。**这节省了很多节点之间额外的通信需求，有助于实现将网络规模扩大至数百万验证者的目标。

Eth2的共识既仰赖于LMD-GHOST机制（增加新区块，决定区块链链头），也仰赖于Casper FFG机制（最终决定哪些区块成为区块链的一部分）。GHOST强调活性，能够快速高效地将区块添加进区块链，而FFG则与GHOST相辅相成，通过对epochs进行最终确定，弥补了安全性的缺失。



## validator client和node之间的关系

ETH2区分了node（信标节点）和validator client（验证者客户端），一个validator需要这两者产能正常履行职责。

node负责维护整个信标链和所以的分片。volidator client负责处理**单个**validator的逻辑。validator client需要从 node 哪里获取信标链的当前状态，attest和propose block的时候，都需要与 node 交互才能完成。

简单来说，node负责存储信标链的数据，node与node之间相互广播通信，来同步数据；而validator client则是根据共识机制来生成数据，validator clients达成的共识数据将由 nodes 来存储。

![img](assets/0f18d366663449d1a6ed7d4de39306ca.jpg)

### node的职责

- 与其他node同步数据
- 处理来自 validator client 或者 validator committee 的 block attestations
- validator client需要利用node来进行 propose 和 attest，通过RPC
- 存储并且更新世界状态（每个baecon block的数据）
- validator和shard的随机性提供者
- 监听来自ETH1的质押金
- 维护baecon chain的validator注册表，包括新增的和删除的validators
- 维护全网时钟，与其他node有一个同步的时钟，时钟正确才能够正常执行奖惩规则







补充：

在进行调用 deposit 合约进行质押时，消耗36000gas，建议设置 gas limit 为 400,000 - 500,000



参考链接：

[ETH2 Knowledge Base](https://kb.beaconcha.in/attestation)

[Attestant Attestation](https://www.attestant.io/posts/defining-attestation-effectiveness/)

[github specifications](https://github.com/ethereum/eth2.0-specs/tree/dev/specs)

[ETHhub](https://eth2.ethereum.cn/)

[ETH Blog](https://blog.ethereum.org/)

[验证者宝典](https://mp.weixin.qq.com/s?__biz=MzU2NDcyOTc2NA==&mid=2247486342&idx=1&sn=4ba98e0fa6647a8d8642084411ce5af3&chksm=fc47c94dcb30405bcd67169c5392f7fbb0ab14f3cf214ad4ee3754d77a6da5fb6529f317757d&mpshare=1&scene=1&srcid=1109GvtA1YgRLiiusMU5reLr&sharer_sharetime=1604907861464&sharer_shareid=11082f13401cd07be2c9441692f59fbb&key=d5497f8bd75b2dfe0199d8d893e88977b4e40f07f65d9da481abb1e68d59fa8a7fcc786450928ea5ae47f21c5cb25ba3514a5a37ee73fbe5dffe29de797ef8a7388268c52b911c960e87d88f816a9b38e5ecd3ad07d684dc77154988b20937fc51222e1dd04114bd14f17f09e271ac2a175b649d4d507e8fad176703ccd2f27a&ascene=1&uin=MjQ3Mjk5ODMyNA%3D%3D&devicetype=Windows+10+x64&version=6300002f&lang=zh_CN&exportkey=AbjnOQWZAtx4msN0gze02Gg%3D&pass_ticket=PwFM6tc4wCNfj%2BJ9Ai3I0dVsmOsIyW6Nm9F7jLj4%2BRRq35Onyu8Xhl3cjsG1DuYm&wx_header=0)

[ETH中文网](https://eth2.ethereum.cn/)

