# sidecar 文档

sidecar项目是启动ETH2节点的项目。使用golang版本的客户端，并且使用prysm.sh脚本来启动节点。

**注意：**以下文档内容来自develop分支

## prysm.sh的使用

prysm.sh是用来下载和启动golang版本的ETH2.0客户端（包括beacon node，validator和slasher）的脚本，由Prysmatic实验室提供。

### sidecar程序使用prysm.sh的命令

#### beacon node

在 beacon.StartBeacon() 中，操作系统这里就不做区分了

```bash
./prysh.sh beacon-chain --config-file=${utils.PrysmConfigPath} --datadir=${utils.DataDir} --http-web3provider=${eth1Url}
```

其中，${utils.PrysmConfigPath} 和 ${utils.DataDir} 也就是可执行文件运行时的目录和特定文件名拼接而成的，对应项目根目录下的 prysm-config.yaml 和 datadir目录；${eth1Url}可以从代码中看到，是从配置文件sidecar.json中读取的。

`--datadir: ` 指定 beacon node 的数据存放的目录；

`--http-web3provider: ` 给出ETH1节点的URL；

`--config-file: ` 指定启动beacon node的配置文件，文件内容如下：

```yaml
accept-terms-of-use: true
rpc-host: localhost
rpc-port: 4000
monitoring-host: localhost
monitoring-port: 8000
grpc-gateway-host: localhost
grpc-gateway-port: 3500
pyrmont: true
datadir: ./datadir
wallet-password-file: ./password.txt
disable-monitoring: true
```

参数的意思：

```yaml
accept-terms-of-use: 是否同意团队的条款信息(类似免责声明)
rpc-host: RPC服务应该监听的host，默认是127.0.0.1
rpc-port: RPC服务应该监听的端口，默认是4000
monitoring-host: prometheus监控用的地址，默认127.0.0.1
monitoring-port: prometheus监控用的端口，三个程序的默认值（Beacon:8080, Validator:8081, Slasher: 8082）
grpc-gateway-host: grpc使用的地址，默认127.0.0.1
grpc-gateway-port: grpc使用的端口，Beacon:3500，Validator:7500
pyrmont: 未知，文档中没有
datadir: 文件存放目录
wallet-password-file: 包含钱包密码的文件
disable-monitoring: 禁用所有监视服务
```

奇怪的是，最后禁用了监控功能，为什么还要配置 monitoring-host 和 monitoring-port。



#### validator client

在validator.StartValidatorNode()中

```bash
./prysh.sh 	validator --wallet-dir=${utils.WalletDir} --datadir=${utils.DataDir} --wallet-password-file=${utils.WalletPasswordPath}
```

其中， ${utils.DataDir} 和beacon node里的一样，对应项目根目录下的datadir目录；${utils.WalletDir} 是由[秘钥生成器](https://github.com/ethereum/eth2.0-deposit-cli/releases/)生[成]()的目录文件，具体可以看 `Become a validator process.md`，在sidecar中对应wallet目录；${utils.WalletPasswordPath} 是一个普通文档文件，里面保存了初始化validator时设置的密钥。



### prysm.sh下载

在[github仓库](https://github.com/prysmaticlabs/prysm)中可以找到

### prysm.sh的参数

[参数文档](https://docs.prylabs.network/docs/prysm-usage/parameters#shared-flags)

