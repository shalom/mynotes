# 启动 ETH2.0 验证节点

[链接](https://learnblockchain.cn/article/1364)

## 环境

Linux系统：Ubuntu20.04

硬件要求：测试网可能只需要 100GB 的 SSD 固态硬盘就够了。对于主网来说，接下来的几年，使用 1TB 的 SSD 固态硬盘（信标链和分片链各占一半）可能更好。总的来说，最好做好在必要时扩展硬盘容量的准备。

## 大体流程

1. 启动一个 Eth1 节点并同步 Eth1 Goerli 测试网
2. 生成并激活验证者密钥对
3. 配置信标链节点和验证者客户端
4. 让信标链节点在验证者（签名功能）的帮助下施展魔法（处理区块、见证消息和罚没事件）



## 确保端口开放

```sh
# ssh端口
ufw allow 22/tcp
# Go Erhereum 端口
ufw allow 30303/tcp & ufw allow 30303/udp
# Grafana 端口
ufw allow 3000/tcp
# Prysm 端口
ufw allow 13000/tcp & ufw allow 12000/udp
# Prometheus 端口
ufw allow 9090/tcp
```

以上开放的都是程序的默认端口。

启用 ufw 防火墙和查看防火墙开放端口状态：

```sh
ufw enable
ufw status numbered
```



## 升到最新

```sh
apt update && apt upgrade
apt dist-upgrade && apt autoremove
```



## 安装并运行 ETH1 节点 Go Ethereum

Go Ethereum 建議使用 PPA 的（Personal Package Archives）

```sh
add-apt-repository -y ppa:ethereum/ethereum
```

安装

```sh
apt-get install ethereum
```

### 将Go Ethereum作为后台服务

开启一个新用户专门启动 ethereum

```
# 此类用户无法登录服务器
useradd --no-create-home --shell /bin/false goeth
```

创建文件夹存储 ethereum 的数据

```sh
mkdir -p /var/lib/goethereum
```

给新用户添加文件夹操作权限

```sh
chown -R goeth:goeth /var/lib/goethereum
```

配置systemd服务文件

```sh
vim /etc/systemd/system/geth.service
```

服务文件内容：

```
[Unit]
Description=Ethereum1 go client
After=network.target
Wants=network.target  

[Service]
User=goeth
Group=goeth
Type=simple
Restart=always
RestartSec=5
ExecStart=/usr/bin/geth --goerli --http --datadir /var/lib/goethereum  
# 命令注释：
# --goerli 指定使用 Goerli 测试网
# --http 暴露信标链节点连接的端口 8545
# --datadir 指定数据存放目录

[Install]
WantedBy=multi-user.target
```

刷新systemd服务文件

```sh
systemctl daemon-reload
```

运行服务，并查看状态，确保状态为active

```sh
systemctl start geth
systemctl status geth
```

给服务创建软连接，实现开启自启

```sh
systemctl enable geth
```

GO Ethereum 会自动开始同步测试网中的区块，使用日志 journal 命令查看程序运行状况

```sh
journalctl -f -u geth.service
```

同步测试网 Goerli 需要很长的时间，如果出现停滞的情况，可以手动连接节点来帮助同步。停止服务，并修改服务文件的启动命令：

```sh
ExecStart=geth --goerli --http --datadir /var/lib/goethereum --bootnodes "enode://46add44b9f13965f7b9875ac6b85f016f341012d84f975377573800a863526f4da19ae2c620ec73d11591fa9510e992ecc03ad0751f53cc02f7c7ed6d55c7291@94.237.54.114:30313,enode://119f66b04772e8d2e9d352b81a15aa49d565590bfc9a80fe732706919f8ccd00a471cf8433e398c55c4862aadb4aadf3a010201483b87e8358951698aa0b6f07@13.250.50.139:30303"
# --bootnodes 主动要连接的节点列表，用逗号分隔
```

刷新服务文件，重新启动服务



## 安装并运行 Prysm 

官方文档可以看到有3种方法来安装 Prysm

[Prysm Medalla 测试网文档](<https://docs.prylabs.network/docs/install/install-with-script>)

这里使用编译二进制文件的方式来安装 Prysm（比较耗时）

### 安装 Bazel

Bazel是 Prysm 官方文档里写到的构建 Prysm 的开源工具。

执行如下命令：

```sh
$ apt install curl gnupg
$ curl https://bazel.build/bazel-release.pub.gpg | sudo apt-key add -
$ echo "deb [arch=amd64] https://storage.googleapis.com/bazel-apt stable jdk1.8" | sudo tee /etc/apt/sources.list.d/bazel.list
```

安装

```sh
apt update
apt install bazel
apt install bazel-3.2.0
```

### 构建Prysm

clone Prysm 的代码

```sh
git clone https://github.com/prysmaticlabs/prysm
cd prysm
```

构建 信标链 和 验证者 可执行文件

```sh
bazel build //beacon-chain:beacon-chain
bazel build //validator:validator
```

**注意：**我在网络状况良好的情况下，购进啊 beacon-chain 的时候花了将近 2 小时……

### 将信标链节点作为后台服务

开启一个新用户专门启动 beacon-chain

```
# 此类用户无法登录服务器
useradd --no-create-home --shell /bin/false beaconchain
```

创建文件夹存储 ethereum 的数据

```sh
mkdir -p /var/lib/prysm/beaconchain
```

给新用户添加文件夹操作权限

```sh
chown -R beaconchain:beaconchain /var/lib/prysm/beaconchain
```

将编译好的二进制文件复制到 `/usr/local/bin` 目录下

```sh
cp bazel-bin/beacon-chain/linux_amd64_stripped/beacon-chain /usr/local/bin
```

给新用户添加 beacon-chain 操作权限

```sh
chown -R beaconchain:beaconchain /usr/local/bin/beacon-chain
```

配置systemd服务文件

```sh
vim /etc/systemd/system/beaconchain.service
```

服务文件内容：

```
[Unit]
Description=Beaconchain
Wants=network-online.target
After=network-online.target  
[Service]
Type=simple
User=beaconchain
Group=beaconchain
Restart=always
RestartSec=5
Environment="ClientIP=$(curl -s v4.ident.me)"
ExecStart=/bin/bash -c '/usr/local/bin/beacon-chain --p2p-host-ip=${ClientIP} --datadir=/var/lib/prysm/beaconchain --http-web3provider=http://127.0.0.1:8545'  
# --p2p-host-ip 应对故障用的Ip
# --http-web3provider 指定Eth1节点的IP+port，这里是本地的节点

[Install]
WantedBy=multi-user.target
```

刷新systemd服务文件

```sh
systemctl daemon-reload
```

你需要等待 Go Ethereum 同步区块完成才可以运行信标链（beacon-chain）服务，运行服务，并查看状态，确保状态为active

```sh
systemctl start beaconchain
systemctl status beaconchain
```

给服务创建软连接，实现开启自启

```sh
systemctl enable beaconchain
```

使用日志 journal 命令查看程序运行状况

```sh
journalctl -f -u beaconchain.service
```



## 获得 Goerli 的以太币

想成为ETH2测试网络的验证者（即测试网络的 PoS 矿工），您必须先获得 32 个 Goerli 测试网 ETH

有如下3种方法获得 Goerli 的测试币：

- 访问 <https://prylabs.net/participate> 这个网站并连接自己的 Metamask 钱包可一次性领到 32 个测试网 ETH。这个网站是 Prysm Labs 为便利大家参加 Tapoz 测试网而提供的引导性网站，也是目前为止能一次性领到最多 Goerli ETH 的方法。请珍惜使用 : )
- <https://goerli-faucet.slock.it/> 这个网站是 Goerli 测试网运行者提供的水龙头，输入地址并执行一次人机身份验证可领到 0.05 Goerli ETH。是的，你没看错，是 0.05 个，如果你要用这种办法来拿到 32 个 Goerli ETH，你需要做 160 次人机身份认证 : )
- <https://faucet.goerli.mudit.blog/> 这个网站是由社区提供的。可按时间段获得不同数量的测试币。9 天总共可获得 37.5 个 Goerli ETH （不过笔者并未亲手测试过，似乎需要社交媒体转发才能获得）

第一种方法无疑是最优选择（**第三个链接**是最直接的），大致流程：

1. 进入网页，按提示点击，直到需要生成验证者keys

2. 有两种方式生成 `validator_keys`，这里同样使用最简单的方式：

   - 下载[ethereum/eth2.0-deposit-cli](https://github.com/ethereum/eth2.0-deposit-cli/releases/)最新分支的压缩包

     ```sh
     wget https://github.com/ethereum/eth2.0-deposit-cli/releases/download/v0.2.1/eth2deposit-cli-v0.2.1-linux-amd64.tar.gz
     ```

     **注意：**由于代码的更新，上面的url会发生比变化

   - 执行生成 `validator_keys` 的命令：

     ```sh
     ./deposit --num_validators 1 --chain medalla
     # 一个验证者，测试网为medalla
     ```

     这里会需要你输入一个密码，密码输入后，**会生成一串助记词，这个就是提现的凭证**，然后生成了如下文件：

     ```sh
     validator_keys/
     ├── deposit_data-1597829039.json
     └── keystore-m_12381_3600_0_0_0-1597829038.json
     ```

     其中 `deposit_data-1597829039.json` 包含了验证者的公钥，在接下来的步骤中会需要上传。

3. 上传公钥，连接现有的 Metamask 钱包地址，然后用twitter发送一个只包含钱包地址的推文，将推文的链接复制 粘贴到水龙头输入框中，就会获得32个Goerli的以太币，然后在转到 validator_keys 对应的账户地址中，这样验证者就拥有了32个ETH，可以成为验证节点（Pos矿工）

   **注意：** 上述的描述很简陋，需要很具提示来进行详细的操作，其中会生成 两套 密码，其中一个是验证者钱包密钥，后面需要用到

## 创建验证者钱包

```sh
cd prysm
bazel run //validator:validator -- accounts-v2 import --keys-dir=$HOME/eth2.0-deposit-cli/validator_keys
# --keys-dir 指定在目录中生成验证者密钥的路径
```

输入 Eth2 Launch Pad 网站上创建的密码，这时就会将账户导入到新钱包中

确认验证者账户创建成功：

```sh
bazel run //validator:validator -- accounts-v2 list
```

## 启动验证者客户端

开启一个新用户专门启动 validator

```
# 此类用户无法登录服务器
useradd --no-create-home --shell /bin/false validator
```

创建文件夹存储 validator的数据

```sh
mkdir -p /var/lib/prysm/validator
```

给新用户添加文件夹操作权限

```sh
chown -R validator:validator /var/lib/prysm/validator
```

将编译好的二进制文件复制到 `/usr/local/bin` 目录下

```sh
cd prysm
cp bazel-bin/validator/linux_amd64_stripped/validator /usr/local/bin
```

配置systemd服务文件

```sh
vim /etc/systemd/system/validator.service
```

服务文件内容：

```
[Unit]
Description=ETH2 Validator
Wants=network-online.target
After=network-online.target  

[Service]   
Type=simple
User=validator
Group=validator
Restart=always
RestartSec=5
ExecStart=/usr/local/bin/validator --datadir /var/lib/prysm/validator --wallet-dir /home//.eth2validators/prysm-wallet-v2 --wallet-password-file /home//.eth2validators/prysm-wallet-v2/password.txt --graffiti ""  
# --wallet-dir 指定钱包路径

[Install]
WantedBy=multi-user.target
```

将钱包密码写在password.txt中

刷新systemd服务文件

```sh
systemctl daemon-reload
```

运行服务，并查看状态，确保状态为active

```sh
systemctl start validator
systemctl status validator
```

给服务创建软连接，实现开启自启

```sh
systemctl enable validator
```

GO Ethereum 会自动开始同步测试网中的区块，使用日志 journal 命令查看程序运行状况

```sh
journalctl -f -u validator.service
```

可以通過 beaconcha.in 檢查自己的驗證者的狀態，只需搜索你的驗證者公鑰即可。





