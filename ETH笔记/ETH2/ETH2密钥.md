# 流程图

![img](assets/image.png)



# 总体

依据相同的思路和算法 [elliptic-curve cryptography](https://en.wikipedia.org/wiki/Elliptic-curve_cryptography) 生成的。

ETH2需要参数不同，并且使用  [**B**oneh-**L**ynn-**S**hacham](https://en.wikipedia.org/wiki/Boneh–Lynn–Shacham) 签名方案。

# ETH2密钥

ETH1只需要一个私钥文件，ETH2需要两个私钥文件：

-  The **validator** **private** key 
- The **withdrawal** **private** key.



## The validator key

用来主动的在ETH2上进行签名操作，例如：块的提案和验证。因此它必须要在热钱包里。

如果被偷了，攻击者会有如下攻击方式：



## The withdrawal key

要使用 withdrawal ，验证者必须进入到”自愿退出状态“。

被称为提现密钥，可以用来提取验证者的余额。



# 要点

**validator 的唯一标识是什么？**

不是私钥，一个validator可能拥有多个key，validator的唯一标识是 beaconchain 的 ”unique deposit data“ 。

**如何获得 unique deposit data？**

这个数据是存储在链中的，可以通过链上数据获取……

**如何为我的validator充值？**

需要向 deposit contract 发起一笔交易，交易金额需要大于 1Ether ，并且要以 unique deposit data 为参数传入。



# ETH2中validator的助记词



