# Ethereum 2.0 Phase 0 -- The Beacon Chain

注意：这篇文档中的内容还在被研究人员开发中……

## Introduction

这份文档代表了ETH2的Phase 0的规范，也就是 Beacon Chain。

ETH2的核心是一个被称为“beacon chain”的系统链，baecon chain存储和管理了validators的注册表。在当前这种ETH2最原始的阶段，成为validator的唯一机制就是：在ETH1上向deposit合约发起交易。当ETH1的质押收据被beacon chain审核，激活金额到账并且完成排队，就会激活一个validator。退出既可以是自愿的，也可以是强迫的，这样作为不当行为的惩罚。beacon chain的主要负载是“attestations”。Attestations是同时并且可用的votes，阶段0为POS block投票，阶段1为分片block投票。

## Notation提醒

以`这种格式` 出现的代码片段为 Python 3 代码。

## Custom Types自定义类型

为了类型的可读性，我们定义了如下的Python类型：

| Name             | SSZ equivalent | Description                       |
| ---------------- | -------------- | --------------------------------- |
| `Slot`           | `uint64`       | a slot number                     |
| `Epoch`          | `uint64`       | an epoch number                   |
| `CommitteeIndex` | `uint64`       | a committee index **at a slot**   |
| `ValidatorIndex` | `uint64`       | a validator registry index        |
| `Gwei`           | `uint64`       | an amount in Gwei                 |
| `Root`           | `Bytes32`      | a Merkle root                     |
| `Version`        | `Bytes4`       | a fork version number             |
| `DomainType`     | `Bytes4`       | a domain type                     |
| `ForkDigest`     | `Bytes4`       | a digest of the current fork data |
| `Domain`         | `Bytes32`      | a signature domain                |
| `BLSPubkey`      | `Bytes48`      | a BLS12-381 public key            |
| `BLSSignature`   | `Bytes96`      | a BLS12-381 signature             |



## Constants常量

以下是在全篇规范中使用的不可配置的常量：

| Name                          | Value                 |
| ----------------------------- | --------------------- |
| `GENESIS_SLOT`                | `Slot(0)`             |
| `GENESIS_EPOCH`               | `Epoch(0)`            |
| `FAR_FUTURE_EPOCH`            | `Epoch(2**64 - 1)`    |
| `BASE_REWARDS_PER_EPOCH`      | `uint64(4)`           |
| `DEPOSIT_CONTRACT_TREE_DEPTH` | `uint64(2**5)` (= 32) |
| `JUSTIFICATION_BITS_LENGTH`   | `uint64(4)`           |
| `ENDIANNESS`                  | `'little'`            |



## Configuration配置

注意：为了设计规范，默认将主网配置的值包括在内。主网，测试网和 YAML-based 测试的配置的不同可以在文件夹 [`configs/constant_presets`](https://github.com/ethereum/eth2.0-specs/blob/dev/configs) 中找到。这些配置会在不同的发布版本中更新，而且可能会在开发的时候移除。

### 杂项

| Name                                 | Value                              |
| ------------------------------------ | ---------------------------------- |
| `ETH1_FOLLOW_DISTANCE`               | `uint64(2**10)` (= 1,024)          |
| `MAX_COMMITTEES_PER_SLOT`            | `uint64(2**6)` (= 64)              |
| `TARGET_COMMITTEE_SIZE`              | `uint64(2**7)` (= 128)             |
| `MAX_VALIDATORS_PER_COMMITTEE`       | `uint64(2**11)` (= 2,048)          |
| `MIN_PER_EPOCH_CHURN_LIMIT`          | `uint64(2**2)` (= 4)               |
| `CHURN_LIMIT_QUOTIENT`               | `uint64(2**16)` (= 65,536)         |
| `SHUFFLE_ROUND_COUNT`                | `uint64(90)`                       |
| `MIN_GENESIS_ACTIVE_VALIDATOR_COUNT` | `uint64(2**14)` (= 16,384)         |
| `MIN_GENESIS_TIME`                   | `uint64(1578009600)` (Jan 3, 2020) |
| `HYSTERESIS_QUOTIENT`                | `uint64(4)`                        |
| `HYSTERESIS_DOWNWARD_MULTIPLIER`     | `uint64(1)`                        |
| `HYSTERESIS_UPWARD_MULTIPLIER`       | `uint64(5)`                        |
| `PROPORTIONAL_SLASHING_MULTIPLIER`   | `uint64(3)`                        |

- 为了committees的安全，`TARGET_COMMITTEE_SIZE` 需要超过建议的最小committee数量111；有足够的validator时(至少`SLOTS_PER_EPOCH * TARGET_COMMITTEE_SIZE`个=4,096)，调整算法能够保证committee的数量不少于`TARGET_COMMITTEE_SIZE`。(具有可验证延迟函数(VDF)的不可偏随机性将提高committee的鲁棒性，并降低安全的最小委员会规模。)



### Gwei values

| Name                          | Value                                   |
| ----------------------------- | --------------------------------------- |
| `MIN_DEPOSIT_AMOUNT`          | `Gwei(2**0 * 10**9)` (= 1,000,000,000)  |
| `MAX_EFFECTIVE_BALANCE`       | `Gwei(2**5 * 10**9)` (= 32,000,000,000) |
| `EJECTION_BALANCE`            | `Gwei(2**4 * 10**9)` (= 16,000,000,000) |
| `EFFECTIVE_BALANCE_INCREMENT` | `Gwei(2**0 * 10**9)` (= 1,000,000,000)  |



### Initial values

| Name                    | Value                   |
| ----------------------- | ----------------------- |
| `GENESIS_FORK_VERSION`  | `Version('0x00000000')` |
| `BLS_WITHDRAWAL_PREFIX` | `Bytes1('0x00')`        |



### 时间参数

| Name                                  | Value                     | Unit    | Duration     |
| ------------------------------------- | ------------------------- | ------- | ------------ |
| `GENESIS_DELAY`                       | `uint64(172800)`          | seconds | 2 days       |
| `SECONDS_PER_SLOT`                    | `uint64(12)`              | seconds | 12 seconds   |
| `SECONDS_PER_ETH1_BLOCK`              | `uint64(14)`              | seconds | 14 seconds   |
| `MIN_ATTESTATION_INCLUSION_DELAY`     | `uint64(2**0)` (= 1)      | slots   | 12 seconds   |
| `SLOTS_PER_EPOCH`                     | `uint64(2**5)` (= 32)     | slots   | 6.4 minutes  |
| `MIN_SEED_LOOKAHEAD`                  | `uint64(2**0)` (= 1)      | epochs  | 6.4 minutes  |
| `MAX_SEED_LOOKAHEAD`                  | `uint64(2**2)` (= 4)      | epochs  | 25.6 minutes |
| `MIN_EPOCHS_TO_INACTIVITY_PENALTY`    | `uint64(2**2)` (= 4)      | epochs  | 25.6 minutes |
| `EPOCHS_PER_ETH1_VOTING_PERIOD`       | `uint64(2**5)` (= 32)     | epochs  | ~3.4 hours   |
| `SLOTS_PER_HISTORICAL_ROOT`           | `uint64(2**13)` (= 8,192) | slots   | ~27 hours    |
| `MIN_VALIDATOR_WITHDRAWABILITY_DELAY` | `uint64(2**8)` (= 256)    | epochs  | ~27 hours    |
| `SHARD_COMMITTEE_PERIOD`              | `uint64(2**8)` (= 256)    | epochs  | ~27 hours    |

### State list lengths

|                                |                                       |                  |               |
| ------------------------------ | ------------------------------------- | ---------------- | ------------- |
| Name                           | Value                                 | Unit             | Duration      |
| `EPOCHS_PER_HISTORICAL_VECTOR` | `uint64(2**16)` (= 65,536)            | epochs           | ~0.8 years    |
| `EPOCHS_PER_SLASHINGS_VECTOR`  | `uint64(2**13)` (= 8,192)             | epochs           | ~36 days      |
| `HISTORICAL_ROOTS_LIMIT`       | `uint64(2**24)` (= 16,777,216)        | historical roots | ~52,262 years |
| `VALIDATOR_REGISTRY_LIMIT`     | `uint64(2**40)` (= 1,099,511,627,776) | validators       |               |



### 奖励和惩罚

| Name                            | Value                          |
| ------------------------------- | ------------------------------ |
| `BASE_REWARD_FACTOR`            | `uint64(2**6)` (= 64)          |
| `WHISTLEBLOWER_REWARD_QUOTIENT` | `uint64(2**9)` (= 512)         |
| `PROPOSER_REWARD_QUOTIENT`      | `uint64(2**3)` (= 8)           |
| `INACTIVITY_PENALTY_QUOTIENT`   | `uint64(2**24)` (= 16,777,216) |
| `MIN_SLASHING_PENALTY_QUOTIENT` | `uint64(2**5)` (= 32)          |

- The `INACTIVITY_PENALTY_QUOTIENT` equals `INVERSE_SQRT_E_DROP_TIME**2` where `INVERSE_SQRT_E_DROP_TIME := 2**12` epochs (about 18 days) is the time it takes the inactivity penalty to reduce the balance of non-participating validators to about `1/sqrt(e) ~= 60.6%`. Indeed, the balance retained by offline validators after `n` epochs is about `(1 - 1/INACTIVITY_PENALTY_QUOTIENT)**(n**2/2)`; so after `INVERSE_SQRT_E_DROP_TIME` epochs, it is roughly `(1 - 1/INACTIVITY_PENALTY_QUOTIENT)**(INACTIVITY_PENALTY_QUOTIENT/2) ~= 1/sqrt(e)`.



### Max operations per block

| Name                     | Value          |
| ------------------------ | -------------- |
| `MAX_PROPOSER_SLASHINGS` | `2**4` (= 16)  |
| `MAX_ATTESTER_SLASHINGS` | `2**1` (= 2)   |
| `MAX_ATTESTATIONS`       | `2**7` (= 128) |
| `MAX_DEPOSITS`           | `2**4` (= 16)  |
| `MAX_VOLUNTARY_EXITS`    | `2**4` (= 16)  |



### Domain types

| Name                         | Value                      |
| ---------------------------- | -------------------------- |
| `DOMAIN_BEACON_PROPOSER`     | `DomainType('0x00000000')` |
| `DOMAIN_BEACON_ATTESTER`     | `DomainType('0x01000000')` |
| `DOMAIN_RANDAO`              | `DomainType('0x02000000')` |
| `DOMAIN_DEPOSIT`             | `DomainType('0x03000000')` |
| `DOMAIN_VOLUNTARY_EXIT`      | `DomainType('0x04000000')` |
| `DOMAIN_SELECTION_PROOF`     | `DomainType('0x05000000')` |
| `DOMAIN_AGGREGATE_AND_PROOF` | `DomainType('0x06000000')` |

