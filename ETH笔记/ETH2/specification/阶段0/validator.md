# Ethereum 2.0 Phase 0 -- Honest Validator

注意：这篇文档中的内容还在被研究人员开发中…… 这篇文档和 baecon-chain.md 是兄弟文档，描述了参与ETH2协议的validator的预期行为。

## Introduction

这篇文档代表了ETH2的阶段0中的“诚实validator”应该有的行为。这篇文档不区分node和validator client，node负责：跟踪和读取beacon chain，validator client 参与共识(consensus)。这两个软件之间的关系还在设计决策中，不在这个文档的讨论范围内。

validator是参与ETH2协议共识机制的一个实体。对于用户来说，validator是一个可选角色，在这个角色中，用户将ETH作为抵押，同时验证和证明block的有效性，以此来建立和保护协议，并从中获取经济利益。这个和POW很像的地方：矿工提供硬件和算例作为抵押，来建立和保护协议，并从中获利。

## 预备的知识

所有定义在 beacon-chain.md 和 deposit.md 中的术语、常量、函数和协议机制，这篇文档都需要用到。请提前观看这两篇文档，并且在后续的阅读中作为参考。

## 常量

### 杂项

| Name                                    | Value          | Unit                                                         | Duration  |
| --------------------------------------- | -------------- | ------------------------------------------------------------ | --------- |
| `TARGET_AGGREGATORS_PER_COMMITTEE`      | `2**4` (= 16)  | validators                                                   |           |
| `RANDOM_SUBNETS_PER_VALIDATOR`          | `2**0` (= 1)   | subnets                                                      |           |
| `EPOCHS_PER_RANDOM_SUBNET_SUBSCRIPTION` | `2**8` (= 256) | epochs                                                       | ~27 hours |
| `ATTESTATION_SUBNET_COUNT`              | `64`           | The number of attestation subnets used in the gossipsub protocol. |           |



## 成为一个validator

### 初始化

在提交存款和加入validator注册表之前，验证器必须在本地初始化许多参数。

#### BLS public key

validator公钥是BLS12-381曲线上的G1点。私钥(privkey)必须与公钥一起安全地生成。私钥必须是“热的”（或者说在热钱包里），因为在validator的整个生命周期中，必须始终保证可以用来对数据进行签名。

#### BLS withdrawal key

withdrawal private key同样要与公钥一起安全的生成。withdrawal private key不需要在validator正常的生命周期中保持可用，所以可以保存在冷钱包里。

The validator 通过如下选项来构件他们的 `withdrawal_credentials` :

- Set `withdrawal_credentials[:1] == BLS_WITHDRAWAL_PREFIX`.
- Set `withdrawal_credentials[1:] == hash(withdrawal_pubkey)[1:]`.

### 提交质押金

在阶段0中，所有validator的质押金来自于由`DEPOSIT_CHAIN_ID`和`DEPOSIT_NETWORK_ID`定义的Eth1（现阶段是测试网Goerli）。质押金将会存入地址为` DEPOSIT_CONTRACT_ADDRESS` 的合约。

提交步骤：

- 将validator的初始化参数打包进`deposit_data` 中，对应 beacon-chain.md 的[`DepositData`](https://github.com/ethereum/eth2.0-specs/blob/dev/specs/phase0/beacon-chain.md#depositdata) 对象
- 将质押金的数额转换为 Gwei ，并且 `amount >= MIN_DEPOSIT_AMOUNT`.
- Set `deposit_data.pubkey` to validator's `pubkey`.
- Set `deposit_data.withdrawal_credentials` to `withdrawal_credentials`.
- Set `deposit_data.amount` to `amount`.
- Let `deposit_message` be a `DepositMessage` with all the `DepositData` contents except the `signature`.
- Let `signature` be the result of `bls.Sign` of the `compute_signing_root(deposit_message, domain)` with `domain=compute_domain(DOMAIN_DEPOSIT)`. (*Warning*: Deposits *must* be signed with `GENESIS_FORK_VERSION`, calling `compute_domain` without a second argument defaults to the correct version).
- Let `deposit_data_root` be `hash_tree_root(deposit_data)`.
- Send a transaction on the Ethereum 1.0 chain to `DEPOSIT_CONTRACT_ADDRESS` executing `def deposit(pubkey: bytes[48], withdrawal_credentials: bytes[32], signature: bytes[96], deposit_data_root: bytes32)` along with a deposit of `amount` Gwei.

*Note*: Deposits made for the same `pubkey` are treated as for the same validator. A singular `Validator` will be added to `state.validators` with each additional deposit amount added to the validator's balance. A validator can only be activated when total deposits for the validator pubkey meet or exceed `MAX_EFFECTIVE_BALANCE`.由于上述步骤包含许多的 baecon-chain.md 中的术语，暂时不翻译。

### Process deposit（质押过程）

直到他们被存储或者任何子数据被添加到beacon chain的 `state.eth1_data` 中，deposit才会被beacon chain执行。这将会花费最小 `ETH1_FOLLOW_DISTANCE` 数量的 ETH1 block 时间(大约4 hours) 和 `EPOCHS_PER_ETH1_VOTING_PERIOD` 数量的epochs(大约3.4hours)。一旦这个必要的ETH1数据被添加，deposit将会自动的被添加到一个beacon chain的block中，并且会进入到 `state.validators` 中，这些将在一个或者两个Epoch内完成。然后这个validator就会在等待激活队列中排队。

### Validator index

一旦validator进入到 `state.validators` 中，the validator's `validator_index` 就会

