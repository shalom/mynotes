# Uniswap笔记

一个去中心化的交易所，[文档地址](https://uniswap.org/docs/v2/)

## 基本概念

中心化交易所只是个撮合平台，双方可以接受的成交价格一样，就撮合达成交易。但是去中心化的交易所要实现这个撮合功能，成本特别高，因为每次挂单和撤单的操作都需要上链。

为了实现低成本的去中心化交易所，就产生了AMM模式的去中心化交易所。

- A：Automated 

- M：market

- M：makers

maker是中间商，也被称为“做市商”。假如有两个资产A和B需要进行交换，makers需要同时拥有A和B两种资产，并且将这两种资产同时质押到market中，所有需要在A和B两种资产之间进行交换的用户都是直接和makers进行交易，makers通过收取交易手续费获利。

特点：

- makers可以很方便的收取手续费，在uniswap中奖励给makers的费率是0.3%
- 对项目方来说，makers投入的质押量越多越好，质押量也被称为**流动性**，质押量越多则代表项目被更多的人认可和信任，并且币值也更加的稳定

**LP Token**

LP Token是提供流动性的证明，在文档中也被称为 LP Shares。

- LP：Liquidity Provider 流动性提供者

makers将两个token同时质押到uniswap中时，uniswap会生成一个一揽子集合资产（LP Token），并且按照质押量计算 LP Token 的数量发放给makers。

当用户不想继续为项目方提供流动性时，LP Token就是他们的提现证明。

**流动性挖矿**

每一个项目方都希望自己发行的token能够拥有更多的流动性，所以就会制定一些奖励机制来激励用户进行质押，成为流动性提供者。比如在Bounce项目中，每天发行的的 128个BOT中，就有20%是奖励给流动性提供者的，这被称为流动性挖矿。

流动性提供者需要将自己在uniswap上获得的 LP Token质押在Bounce项目的流动性挖矿合约中，合约根据LP Token的总质押量和用户LP Token质押的时间，来计算奖励。



## 协议内容

![img](assets/anatomy.jpg)

1. 流动性提供者将 TokenA 和 TokenB 质押再Uniswap资金池中，并获得质押凭证 LP Shares；
   - 任何人都可以通过质押成为流动性提供者，并且可以随时赎回资产
2. 交易者若想用TokenA换取TokenB，需额外支付0.3%手续费，手续费将加入总资金池，Uniswap资金池返回相应价值的TokenB。

### 价格计算

当用户发起交换交易时，以amount(A) * amount(B)保持不变的原则，来计算对应的提币数量。因为这个不变常量的原因，交易量越大的交易失败的可能性就越高。

![img](assets/lp.jpg)



随着交易费率的加入，A B的总量之积只会越来越大，而两个总量的比值就是A B两个币之间的价格。

![img](assets/trade.jpg)

具体计算公式和推导过程在 `Uniswap-contract-code\periphery\UniswapV2Library.sol` 中 `getAmountOut` 函数中，设计很精妙。

双方token数量的计量单位都为双方最小单位。





## 在本地测试环境中使用uniswap合约

在 Bounce 项目中，质押奖励由之前发放ETH改成了奖励BOT，目前的做法是将ETH通过uniswap合约库中的v2版本的router，也就是`UniswapV2Router02.sol`，来将ETH转换为BOT，然后再发放。

实现这个转换很简单，只需要调用 `UniswapV2Router02.sol` 中的 `swapExactETHForTokens` 方法。但是在本地测试环境中，要运行起来可能需要踩几个坑。

1. 首先，要想调用 `swapExactETHForTokens` 方法，需要给交易的两个资产添加流动性，没有流动性将会报错；
2. 添加流动性的方法是 `addLiquidityETH` ，然而这个方法在本地是运行不起来的，因为里面有些参数直接被写死，需要修改源代码；
3. 添加流动性`addLiquidityETH` 传入的参数，token A 和 token B 的值的乘积的算术平方根不能低于 1000。