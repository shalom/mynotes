# Aragon提供的模板

[模板创建入口](https://rinkeby.client.aragon.org/#/)

Aragon提供了 6 个模板，让普通用户做一些简单的配置就能够快速创建DAOs 。

使用这些模板创建出来的组织有几个共同的功能：

1. Token：拥有组织内的token人员，数量展示，token主要用来投票。上面6个模板中有些模板的token是可交易的，有些模板是不可交易的，有些模板每个用户只能拥有一个token。
2. Voting：用于发起propose和vote；
3. Finance：组织的金库，展示资金历史使用记录。

每个新成员要加入组织，都需要进行vote，并且通过，才能加入成功。



# Aragon提供开发者的组件

[开发文档](https://hack.aragon.org/docs/getting-started)



其中最重要的部分是 aragonOS 模块，它是Aragon团队提供的开发智能合约的框架。这个框架提供的主要的几个功能是：

- 可以方便的升级合约
- 多权限控制
- 合约间的交互

以上3个功能在 OpenZeppelin 中都能找到。

在Guides一栏中，可以看到框架提供的一个[空白项目](https://hack.aragon.org/docs/tutorial)

项目的大体目录：

```bash
root
├── app 
├ ├── src
├ ├ ├── App.js
├ ├ ├── index.js
├ ├ └── script.js
├ └── package.json
├── contracts
├ └── CounterApp.sol
├── scripts
├ └── buidler-hooks.js
├── arapp.json
├── manifest.json
├── buidler.config.js
└── package.json
```

- app: 前端文件夹
  - src
  - package.json: Frontend npm configuration file.
- contracts: 只能合约文件夹.
  - `CounterApp.sol`: Aragon app contract.
- scripts: 
  - `buidler-hooks.js`: Buidler 插件要用到的 script hook.
- arapp.json: Aragon configuration file. 元数据相关的配置。
- manifest.json: Aragon configuration file. 网络相关的配置。
- buidler.config.js: Buidler 插件配置文件.
- package.json: Main npm configuration file.



## 可升级

1. 和OpenZeppelin一样，合约部署的时候，也分两个合约，一个是代理合约，一个是逻辑合约。升级的时候更改逻辑合约，代理合约不变。

2. 因为代理合约的构造函数无法执行（老油条的青春一去不复返），所以合约编写不能有构造函数。与此同时，用户需要自定义一个最先执行、并且只会执行一次的函数来替代构造函数。
   使用修饰符 onlyInit 修饰的函数为初始函数，而其余的能够改变状态的函数要使用修饰符 isInitialized 来修饰，来保证它不会再初始化函数之前被执行。

   ```js
   import "@aragon/os/contracts/apps/AragonApp.sol";
   
   contract MyApp is AragonApp {
       address receiver;
   
       function initialize(address _receiver) public onlyInit {
           initialized();
           receiver = _receiver;
       }
   
       function sendFunds() payable external isInitialized {
           receiver.transfer(msg.value);
       }
   }
   ```

3. 访问控制功能（ACL）将在初始化完成之后生效。

## 角色访问控制

使用案例如下：

```js
import "@aragon/os/contracts/apps/AragonApp.sol";

contract MyApp is AragonApp {
    bytes32 public constant SET_RECEIVER_ROLE = keccak256("SET_RECEIVER_ROLE");
    
	function setReceiver(address _newReceiver) external auth(SET_RECEIVER_ROLE) {
        receiver = _newReceiver;
    }
}
```

这个和OpenZeppelin中的角色控制library相似，auth(SET_RECEIVER_ROLE) 标识符来控制

额外需要做的是，还需要将角色添加到[`arapp.json`](https://hack.aragon.org/docs/cli-global-confg#the-arappjson-file)文件中，like this：

```json
{
  "roles": [
    {
      "name": "Set the receiver of funds",
      "id": "SET_RECEIVER_ROLE",
      "params": []
    }
  ]
}
```

在name字段中描述这个角色的功能。

## 转发

所谓转发，其实就是合约交互，类似于合约A调用合约B方法。EVMScripts则是执行过程中的一个重要参数。

就像模板例子中的：我们要使用 Token APP 添加一个 tokenholder ，那么就需要经过 voter APP 进行投票，投票通过后， voter APP 就可以自动从 Finance app 中提取资金给新来的 tokenholder。这里就涉及到3个合约的相互调用。

要想使合约具有转发功能，需要实现如下3和接口：

```js
// 1. 导入相应的合约
import "@aragon/os/contracts/apps/AragonApp.sol";
import "@aragon/os/contracts/common/IForwarder.sol";

// 2. 继承
contract MyApp is IForwarder, AragonApp {
    address receiver;

    function initialize(address _receiver) onlyInit public {
        initialized();
        receiver = _receiver;
    }
	//	3. 固定写法
    function isForwarder() public pure returns (bool) {
        return true;
    }
	//	4. 这里自定义在另一个合约中执行 _evmScript 的条件，只有这里返回 true ，才可以执行
    function canForward(address _sender, bytes _evmCallScript) public view returns (bool) {
        // Arbitrary logic for deciding whether to forward a given intent
        return _sender == receiver;
    }
	//	5. 这里使用了canForward进行判断，如果为true，那么就可以继续执行
    function forward(bytes _evmScript) public {
        require(canForward(msg.sender, _evmScript));

        // Input is unused at the moment
        bytes memory input = new bytes(0);

        // 6. 黑名单地址，这里边的地址将不能完成调用
        // An array of addresses that cannot be called from the script
        address[] memory blacklist = new address[](0);

        // Either immediately run the script or save it for later execution
        runScript(_evmScript, input, blacklist); // actually executes script
    }
}
```

转发在模板中 voter APP 中的实现：

```js
contract Voting is IForwarder, AragonApp {
    /**
    * @notice Creates a vote to execute the desired action, and casts a support vote
    * @dev IForwarder interface conformance
    * @param _evmScript Start vote with script
    */
    function forward(bytes _evmScript) public {
        require(canForward(msg.sender, _evmScript));
        _newVote(_evmScript, "", true);
    }

    function canForward(address _sender, bytes _evmCallScript) public view returns (bool) {
        return canPerform(_sender, CREATE_VOTES_ROLE, arr());
    }

    function isForwarder() public pure returns (bool) {
        return true;
    }
}
```

更多的细节可以看[参考文档](https://hack.aragon.org/docs/aragonos-ref)

[前端js的API](https://hack.aragon.org/docs/api-intro)

