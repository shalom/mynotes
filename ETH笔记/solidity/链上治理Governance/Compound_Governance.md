# Compound 的链上治理解析

[合约链接](https://etherscan.io/address/0xc0dA01a04C3f3E0be433606045bB7017A7323E38)

[COMP代币](https://etherscan.io/address/0xc00e94cb662c3520282e6f5717214004a7f26888)

[TimeLock](https://etherscan.io/address/0x6d903f6003cca6255d85cca4d3b5e5146dc33925)

## 提案状态和周期 overview

**状态：**

- pending：提案成功发布，将进入pending状态；
- active：发布提案后，默认会有一个block的延迟，发布提案交易所在的区块的下一个区块开始投票，提案进入active状态；
- defeated：提案被驳回状态，当判断当前block的时间已经超过了提案的结束时间，赞成票 <= 反对票 或者 赞成票小于最低票数；
- succeeded：提案通过投票，判断不为defeated状态，并且还未做排队处理，则处于successed状态；
- queued：调用 queue() 使提案进入queued状态；
- executed：调用 execute() 执行提案中的所有交易，更改提案为executed状态；
- expired：默认14天内执行queued状态的提案，否则提案进入超时状态，超时状态的提案无法被执行。

**周期：**

1. 创建提案
2. 投票（3day）
3. 投票结束
4. 进入队列等待执行

![Image for post](https://miro.medium.com/max/3720/1*z_pNFVOKwHEzpl8KW9bfHA.png)



## 提案内容

每个提案最多可以拥有10个action，每个action由4个主要参数组成：

- target：要调用的合约地址
- value：transaction的value
- signature：要调用的函数签名
- calldata：传参

执行提案就是遍历执行所有的action。

反映到具体的事务：包括调整质押率、调整市场利率模型、修改协议中的一些变量参数等



## 代币COMP发放规则

总量 1000 万枚，约 423 万枚，将会被分发给在 Compound 平台进行借贷的用户，而且完全免费。每个 ETH Block 的生成都会转出 0.5 枚 COMP ，但是2020 年 6 月 23 日通过治理提案修改为每个区块转出 0.44 枚 COMP，这样每天约发放 2500 枚 COMP，意味着需要 4 年多的时间才会全部分发完。

Compound 是一个借贷平台，发放的COMP，50% 的 COMP 会分配给资产提供者，50% 的 COMP 分配给借款人，用户可以根据自己资产在所在市场内占比获得。

另 577 万枚发放方式（总量的百分比）：

- 23.96% 已被分发给 Compound Labs 的股东

- 22.26% 将分 4 年分配给创始人和团队成员 

- 3.73% 将分配给未来的团队成员 

- 其余未定



## 治理参与规则

**提案发布门槛：** 拥有COMP总量的 1% ，才有资格发布提案（也就是10万枚COMP）。

**票数统计规则：** 

- 任何持有 COMP 治理代币的用户都可以参与 （1 COMP 代表 1 票）；
- 提案最低的赞成票数不能低于 COMP 的4%（40万枚）。



Compound链接：

https://compound.finance/docs#getting-started

https://medium.com/compound-finance/compound-governance-5531f524cf68



## TimeLock

当前合约使用到了时间锁，从时间锁可以引申到**哈希时间锁协议**，简称HTLAs。





