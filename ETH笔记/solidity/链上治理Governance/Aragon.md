一个可以让任何组织高效安全的协作，并且没有技术漏洞和恶意节点的生态系统。

# 功能

发布一个Dao中包含的元素：身份，所有权，投票，资本，人们，外联，支付处理，会计，保险

身份，所有权，投票，资本，人们 五个元素就类似与现在公司的：头衔，股权，股权的作用，风险投资，员工。元素构成了一个依赖关系，组成了一个公司。

以上元素在代码中的实现：

- 身份：用秘钥来证明自己的身份
- 权限：分红权限，操作权限
- 声誉：组织间进行交易后，组织者之间可以进行评分，并且用产生的交易数据进行审计考核
- 所有权：每个人的股权都是公开的，有限制的进行股权转让
- 资本：提供快速融资通道，组织可以直接发行股份
- 奖励：可以给员工发放 ETH上的任何代币作为工作奖励，可以自定义奖励条件，金额，发放频率等
- 支付：收款渠道
- 会计：可视化的数据展示模块

## 最基础的DAO：Aragon Core

应用层主要的四个部分：

- 规章系统：权限管理，不同角色拥有不同权限
- 治理系统：也就是一个Propose如何被决定是否采用
- 资本系统：发行管理代币(ERC20)
- 会计系统：可视化的资金使用情况系统



# 去中心化法庭

解决纠纷的方案

ANT在仲裁中的作用：

- 投票权
- 参与仲裁的押金（发起仲裁，成为法官）

## 仲裁机制

法官

1. 发起仲裁，申请人需要交纳押金
2. 冻结仲裁相关的合约（比如一个正在被攻击者利用漏洞攻击的一个合约），发起冻结的人需要是当事人
3. 法官进行裁判，需要将裁判的判定隐私化，不影响到其他法官的决策
   1. 生成一个秘密的随机数（随机数与自己的判决结果对应），提交判决结果的摘要给法庭，保存好随机数；
   2. 裁判期限过后，法官需公开他们的判决结果和随机数，任何人都可以验证这个判决结果和随机数是否对应，如果不对应，则惩罚该法官缴纳的押金；
   3. 任何一个法官提前披露了法官A的随机数，法官A就会被惩罚，押金的一部分奖励给披露人。

以上仲裁机制主要就是为了杜绝法官串通。

法庭初审流程：

1. 从缴纳了押金的法官中，随机选出5个。如果被选中者拒绝参加，会被轻微处罚，然后再选一个；
2. 法官查看相关规则，参与方发送的加密资料；
3. 法官进行判决，最后取票数多的一方；
4. 投正确票的法官会奖励声誉代币（不可交易），投错票的法官会被严重处罚。

若当事人不满足法官的判决，可以缴纳更多的押金用以发起上诉，这时网络中所有的法官都可以参与二审：

1. 法官查看相关规则，参与方发送的加密资料；
2. 法官进行判决，最后取票数多的一方；
3. 押金会退给这些法官，外加把败诉一方的处罚作为奖励；
4. 如果和一审判决不一样，所有一审的投错票的法官都会被严厉惩罚。

若当事人还是不满足前两轮的判决，可以缴纳非常多的押金继续上诉至最高法院，然后由司法机关声誉最高的9个法官组成最高法院进行判决：

1. 法官查看相关规则，参与方发送的加密资料；
2. 法官进行判决，最后取票数多的一方；
3. 押金会退给这些法官，外加把败诉一方的处罚作为奖励；
4. 如果之前的审判决和当前判决不一样，所有之前投错票的法官都会被严厉惩罚。

这9个法官会收到由ATN持有者决定的薪水(ANF)





create-aragon-app



# 使用AragonOS开发
	1. 首先必须继承合约 AragonApp
		这个合约中有两个非常重要的状态变量：
	  - kernel：负责管理应用程序的升级和访问控制（这两个功能如何使用？）
		- appId ：应用程序的标识符，也就是ENS域名
	2. 和OpenZeppelin一样，合约部署的时候，也分两个合约，一个是代理合约，一个是逻辑合约。
  升级的时候更改逻辑合约，代理合约不变。
	3. 因为代理合约的构造函数无法执行（老油条的青春一去不复返），所以合约编写不能有构造函数。
  与此同时，用户需要自定义一个最先执行，并且只会执行一次的函数来替代构造函数
  使用修饰符 onlyInit 来修饰，而其余的能够改变状态的函数要使用修饰符 isInitialized 来修饰，
  来保证它不会再初始化函数之前被执行。
	4. 访问控制功能（ACL）将在初始化完成之后生效。

### 角色访问控制

使用案例如下：

```js
import "@aragon/os/contracts/apps/AragonApp.sol";

contract MyApp is AragonApp {
    bytes32 public constant SET_RECEIVER_ROLE = keccak256("SET_RECEIVER_ROLE");
    
	function setReceiver(address _newReceiver) external auth(SET_RECEIVER_ROLE) {
        receiver = _newReceiver;
    }
}
```

这个和OpenZeppelin中的角色控制library相似，auth(SET_RECEIVER_ROLE) 标识符来控制

额外需要做的是，还需要将角色添加到[`arapp.json`](https://hack.aragon.org/docs/cli-global-confg#the-arappjson-file)文件中，like this：

```json
{
  "roles": [
    {
      "name": "Set the receiver of funds",
      "id": "SET_RECEIVER_ROLE",
      "params": []
    }
  ]
}
```

在name字段中描述这个角色的功能。



### 转发和EVMScripts

所谓转发，其实就是合约交互，类似于合约A调用合约B方法。EVMScripts则是执行过程中的一个重要参数。

就像模板例子中的：我们要使用 Token APP 添加一个 tokenholder ，那么就需要经过 voter APP 进行投票，投票通过后， voter APP 就可以自动从 Finance app 中提取资金给新来的 tokenholder。这里就涉及到3个合约的相互调用。

要想使合约具有转发功能，需要实现如下3和接口：

```js
// 1. 导入相应的合约
import "@aragon/os/contracts/apps/AragonApp.sol";
import "@aragon/os/contracts/common/IForwarder.sol";

// 2. 继承
contract MyApp is IForwarder, AragonApp {
    address receiver;

    function initialize(address _receiver) onlyInit public {
        initialized();
        receiver = _receiver;
    }
	//	3. 固定写法
    function isForwarder() public pure returns (bool) {
        return true;
    }
	//	4. 这里自定义在另一个合约中执行 _evmScript 的条件，只有这里返回 true ，才可以执行
    function canForward(address _sender, bytes _evmCallScript) public view returns (bool) {
        // Arbitrary logic for deciding whether to forward a given intent
        return _sender == receiver;
    }
	//	5. 这里使用了canForward进行判断，如果为true，那么就可以继续执行
    function forward(bytes _evmScript) public {
        require(canForward(msg.sender, _evmScript));

        // Input is unused at the moment
        bytes memory input = new bytes(0);

        // 6. 黑名单地址，这里边的地址将不能完成调用
        // An array of addresses that cannot be called from the script
        address[] memory blacklist = new address[](0);

        // Either immediately run the script or save it for later execution
        runScript(_evmScript, input, blacklist); // actually executes script
    }
}
```

转发在voter APP 中的实现：

```js
contract Voting is IForwarder, AragonApp {
    /**
    * @notice Creates a vote to execute the desired action, and casts a support vote
    * @dev IForwarder interface conformance
    * @param _evmScript Start vote with script
    */
    function forward(bytes _evmScript) public {
        require(canForward(msg.sender, _evmScript));
        _newVote(_evmScript, "", true);
    }

    function canForward(address _sender, bytes _evmCallScript) public view returns (bool) {
        return canPerform(_sender, CREATE_VOTES_ROLE, arr());
    }

    function isForwarder() public pure returns (bool) {
        return true;
    }
}
```

问题：_evmScript从哪里来？





官网：https://aragon.org/

