# ERC20 API

**TransferFrom**

```js
function transferFrom(address _from, address _to, uint256 _value)
returns (bool success)
```

发送代币到另一个地址

**approve**

```js
function approve(address _spender, uint256 _value)
returns (bool success)
```

允许 _spender 使用 _value 数量的代币，TransferFrom 并不赋予使用权

**注意：**

approve两次调用后，第二次将覆盖第一次，无论调用前 `_spender `已经使用了多少。所以就存在这样一个漏洞：当用户第二次调用approve时，`_spender `得知消息后，抢在第二次调用前将token全部花光（因为ETH交易确认需要时间，所以存在这样的操作空间），这样`_spender `原本为0的可用额度将再次变为第二次调用approve的_value。

**解决方案**

为了减少可能带来的损失，可以先调用approve，参数为0，确保成功后，再次调用approve，参数为目标值。

[参考链接](https://docs.google.com/document/d/1YLPtQxZu1UAvO9cZ1O2RPXBbT0mooh4DYKjA_jp-RLM/edit#)