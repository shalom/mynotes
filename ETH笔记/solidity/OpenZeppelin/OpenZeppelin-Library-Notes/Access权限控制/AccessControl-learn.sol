
/**
    Ownable.sol 是拥有者权限控制的提供者。提供的功能有：
        - 记录拥有者
        - 提供判断是否是拥有者的modifier    onlyOwner()
        - 拥有者能够将所有权移交其他地址    transferOwnership(address newOwner) public
        - 销毁拥有权，将没有任何地址符合是拥有者    renounceOwnership() public
    AccessControl.sol   提供更加丰富的权限控制功能，可以自定义角色，实现多维度的权限控制
        # 简单使用
        ## 注册角色
            使用bytes32作为角色的标识符，标识符应该为：唯一的，公开的，不可变的，所以最好使用 public constant 修饰
            就如下面的 bytes32 public constant MINER = keccak256("MINER_ROLL"); 
        ## 添加角色成员
            使用 _setupRole(rollIdentifier,address) 方法添加角色
        ## 判断是否为某一角色
            使用 hasRole(MINER,person) 方法判断，返回true代表person是MINER角色
        # call函数
        - hasRole(bytes32 role, address account) → bool
            返回true,account已经被授予role角色
        - getRoleMemberCount(bytes32 role) → uint256
            获得拥有role的成员数量
        - getRoleMember(bytes32 role, uint256 index) → address
            获得role中下标为index的成员地址，index必须在0和getRoleMemberCount返回值之间(不包含，前闭后开)
        - getRoleAdmin(bytes32 role) → bytes32
            获得role的AdminRole的 Identifier
        # send-tx函数
        - grantRole(bytes32 role, address account)
            给account授权角色。会判断account是否拥有role的adminRole权限，没有给role设置adminRole同样也不能执行成功
        - revokeRole(bytes32 role, address account)
            给account取消授权角色。会判断account是否拥有role的adminRole权限，没有给role设置adminRole同样也不能执行成功
        - renounceRole(bytes32 role, address account)
            自我取消授权角色。要求 account == msg.sender
        - _setupRole(bytes32 role, address account)         internal
            给account授权角色。因为这里授权不会进行权限判断，所以最好只放在构造函数中，并且只执行一次
        - _setRoleAdmin(bytes32 role, bytes32 adminRole)    internal
            设置一个角色的管理角色，拥有管理角色的account则拥有更多的权限，比如能够调用grantRole来授权

    @notice OwnableTest：Ownable和AccessControl使用案例，结合ERC721.sol
 */
pragma solidity ^0.6.0;

import '@openzeppelin/contracts/token/ERC721/ERC721.sol';
import '@openzeppelin/contracts/utils/Counters.sol';
import '@openzeppelin/contracts/access/Ownable.sol';
import "@openzeppelin/contracts/access/AccessControl.sol";

contract ERC721Learn is ERC721,Ownable,AccessControl{
    using Counters for Counters.Counter;

    Counters.Counter private id;
    bytes32 public constant ADMIN = keccak256("ADMIN");
    bytes32 public constant MINER = keccak256("MINER_ROLL");

    constructor (string memory name,string memory symbol) public ERC721(name,symbol) {
        _setRoleAdmin(MINER,ADMIN);
        _setupRole(ADMIN,msg.sender);
        setMiner(msg.sender);
    }

    modifier isMiner(address person) {
        require(hasRole(MINER,person));
        _;
    }

    function setAdmin(address person)public onlyOwner{
        _setupRole(ADMIN,person);
    }
    // 只有ADMIN成员才能够执行成功
    function setMiner(address person)public{
        grantRole(MINER,person);
    }
    function rewardToken(address user,string memory tokenURI) public isMiner(msg.sender){
        id.increment();

        uint256 _id = id.current();
        _safeMint(user,_id);
        _setTokenURI(_id,tokenURI);
    }
}
