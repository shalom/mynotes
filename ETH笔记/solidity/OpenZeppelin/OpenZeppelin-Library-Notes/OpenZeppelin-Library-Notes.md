[doc](https://docs.openzeppelin.com/contracts/3.x/)

# Ownable.sol

合约所有权库

## 方法

- 返回合约拥有者地址

```
function owner() public view returns(address)
```

- 返回合约调用地址是否是合约拥有者

```
function isOwner() public view returns(bool)
```

- 放弃合约所有权(合约不属于任何地址) **注意：** 放弃后，合约将不能再被任何地址所拥有，同时导致函数修饰器`onlyOwner`无法使用。

```
function renounceOwnership() public onlyOwner
```

- 转移合约所有权

```
function transferOwnership(address newOwner) public onlyOwner
```

## 修饰器

只允许合约拥有者执行

```
modifier onlyOwner() {
  require(isOwner());
  _;
}
```

## 事件

合约所有权转移事件，合约创建时也会调用此事件 `emit OwnershipTransferred(address(0),msg.sender)`

```
event OwnershipTransferred(
  address indexed previousOwner,
  address indexed newOwner
);
```



# 多角色管理

用于比较细分的权限管理 AccessControl.sol 。 AccessControl 有一个超级管理员角色：DEFAULT_ADMIN_ROLE，他可以管理所有的角色，但同时也可以调用 `_setRoleAdmin` 来更改这个角色。

```js
pragma solidity ^0.6.0;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract MyAccess is Ownable,AccessControl{
    //	矿工，发币者
    bytes32 public constant MINER = keccak256("MINER_ROLL");
    //	消币者
    bytes32 public constant BURNER = keccak256("BURN_ROLL");
    
    constructor() internal{
        _setRole(DEFAULT_ADMIN_ROLE,msg.sender);
    }
    //	两个方法使用不同的鉴权方式
    function giveMiner(address p)public{
        require(hasRole(DEFAULT_ADMIN_ROLE,msg.sender));
        _setupRole(MINER,p);
    }
    function giveBurner(address p)public onlyOwner{
        _setupRole(BURNER,p);
    }
    
    function minning()public{
        require(hasRole(MINER,msg.sender));
        // ...
    }
    function burning()public{
        require(hasRole(BURNER,msg.sender));
    }
}
```



# Token

## ERC20

`_mint(address,uint)` internal的发行代币函数

- 确保数量不为0，
- 确保总发行量和用户金额同步
- 触发Transfer事件

用例，给矿工奖励代币(同时增加了代币总发行)：

```js
function mintMinerReward()public{
    _mint(block.coinbase,1000);
}
```

源码：

```js
function _mint(address account, uint256 value) internal {
    require(account != 0);
            _beforeTokenTransfer(address(0), account, amount);
    _totalSupply = _totalSupply.add(value);
    _balances[account] = _balances[account].add(value);
    emit Transfer(address(0), account, value);
}
```



`_beforeTokenTransfer(address from, address to, uint256 amount)` 钩子，内置在 `_mint()` `_burn()` `_transfer()` 方法中，是官方提供的一个可重载的方法(使用了 virtual 标记，使用 override 重载)

用例，当发行代币时，不能超过上限：

```js
function _beforeTokenTransfer(address from, address to, uint256 amount) internal virtual override {
    super._beforeTokenTransfer(from, to, amount);

    // require(from.balance > amount, "not enough balance");
    if (from == address(0)) { // When minting tokens
        require(totalSupply().add(amount) <= _cap, "Cap exceeded.");
    }
}
```

源码，没有提供实质内容：

```js
function _beforeTokenTransfer(address from, address to, uint256 amount) internal virtual { }
```



 # Math

`add()` 判断了是否溢出 overflow

```js
function add(uint256 a, uint256 b) internal pure returns (uint256) {
    uint256 c = a + b;
    require(c >= a, "SafeMath: addition overflow");
    return c;
}
```



`sub()` 相减之前判断大小



`mul()` 节省Gas，判断是否overflow

```js
function mul(uint256 a, uint256 b) internal pure returns (uint256) {
    if (a == 0 || b == 0) {
        return 0;
    }

    uint256 c = a * b;
    require(c / a == b, "SafeMath: multiplication overflow");

    return c;
}
```



`div()` 判断除数是否 >0 ，因为是无符号整型

 ```js
function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
    require(b > 0, errorMessage);
    uint256 c = a / b;

    return c;
}
 ```



`mod()` 取余 ,同样除数不能为0

```js
function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
    require(b != 0, errorMessage);
    return a % b;
}
```



# Initializable

因为OpenZeppelin中的可升级的合约中的 代理合约 是不可以有构造函数的，所以需要有一个函数来专门做初始化的工作，与此同时需要保证这个参数只能被执行一次，并且最先执行。

修饰器 `initializer()` 可以实现以上判断：

```js
modifier initializer() {
    // 正在初始化过程中（上一次初始化还未完成），函数在构造函数中被调用，未被初始化 以上三种情况都可以继续执行
    require(_initializing || _isConstructor() || !_initialized, "Initializable: contract is already initialized");
	// 执行初始化函数前，将状态改为已初始化
    bool isTopLevelCall = !_initializing;
    if (isTopLevelCall) {
        _initializing = true;
        _initialized = true;
    }

    _;
	// 将正在初始化改为false
    if (isTopLevelCall) {
        _initializing = false;
    }
}
```





