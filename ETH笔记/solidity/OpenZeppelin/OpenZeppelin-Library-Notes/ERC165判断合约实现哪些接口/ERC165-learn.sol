pragma solidity ^0.6.0;
/**
    # 要实现功能：判断合约实现了哪些接口；需要解决一下几个问题：
        - 如何标识一个接口
        - 合约如何公开自己已经实现的接口
        - 外部如何察觉合约公开的并且已经实现的接口
 */

 /**
    ## 如何标识一个接口
    使用接口中所有方法的selector的异或(XOR)距离作为接口的 Identifier。
    @notice 以下代码展示了接口中有1个函数、2个函数和3个以上函数时，计算接口Identifer例子
  */
interface Interface1 {
    function world(int) external pure;
}
interface Interface2 {
    function hello() external pure;
    function world(int) external pure;
}
interface Interface3 {
    function hello() external pure;
    function world(int) external pure;
    function andYou(string memory str) external view;
}
contract Caculator {
    function caculate1() returns(bytes4){
        Interface1 i;
        return i.world.selector;
    }
    function caculate2() returns(bytes4){
        Interface2 i;
        return i.hello.selector ^ i.world.selector;
    }
    function caculate3()returns(bytes4) {
        Interface3 i;
        return i.hello.selector ^ i.world.selector ^ i.andYou.selector;
    }
}

/**
    ## 外部如何察觉合约公开的并且已经实现的接口
    合约需要实现了ERC165接口，并且实现相关功能，才能使外部察觉自己实现的接口。所以，第一步是：如何判断一个合约是否实现了 ERC165 。
    ### 判断一个合约是否实现了ERC165
    @notice 判断的几个步骤：
    - 让目标合约发起 STATICCALL 调用，输入的参数为：0x01ffc9a701ffc9a700000000000000000000000000000000000000000000000000000000
        - STATICCALL调用：<address>.staticcall(bytes memory) returns (bool, bytes memory)；返回调用状态和调用返回的数据
    - 若调用失败，或者返回的数据为false，那么目标合约没有实现 ERC165。对应：ok1 == false 或者 data1 == 0x0000000000000000000000000000000000000000000000000000000000000000
    - 再次发起 STATICCALL 调用，输入的参数为：0x01ffc9a7ffffffff00000000000000000000000000000000000000000000000000000000
    - 若调用失败，或者返回的数据为true，那么目标合约没有实现 ERC165。对应：ok2 == false 或者 data1 == 0x0000000000000000000000000000000000000000000000000000000000000001
    - 否则，目标合约实现了ERC165

    实践：
        - 在remix上部署 ./ERC165.sol 和 Test；./ERC165.sol是OpenZeppelin合约库中的合约，已经实现了接口ERC165
        - 将合约ERC165地址作为testERC165的第一个参数
        - 调用后，返回的数据：
            0:bool: ok1 true
            1:bool: ok2 true
            2:bytes: dd1 0x0000000000000000000000000000000000000000000000000000000000000001
            3:bytes: dd2 0x0000000000000000000000000000000000000000000000000000000000000000
    ### 判断一个合约是否实现了其他接口
    当一个合约实现了ERC165接口后，就可以调用supportsInterface函数来判断
        function supportsInterface(bytes4 interfaceId) external view returns (bool); 
        其中 interfaceId 就是接口的唯一标识
        如果返回true，则代表已经实现。
 */
contract Test_{
    function testERC165(address contractAddr,bytes memory data1,bytes memory data2) public view returns(bool ok1,bool ok2,bytes memory data1,bytes memory data2){
        
        (ok1,data1)= contractAddr.staticcall{gas:30000}(data1);
        
        (ok2,data2)= contractAddr.staticcall{gas:30000}(data2);
    }
}
