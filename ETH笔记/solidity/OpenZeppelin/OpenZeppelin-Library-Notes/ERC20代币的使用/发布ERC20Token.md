# 发布ERC20代币

可分割，可自定义精度的代币。

## 使用openzeppelin

**最简单的代币合约**

```solidity
pragma solidity ^0.6.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

// initialSupply：总发行量 ；weis：精度（默认是18）
contract GLDToken is ERC20 {
    constructor(uint256 initialSupply,uint8 weis,string memory name ,string memory name_) public ERC20(name, name_) {
    	//	设置精度函数
    	_setupDecimals(weis);
        _mint(msg.sender, initialSupply);
    }
}
```

部署后可以查询余额和转账：

```bash
> GLDToken.balanceOf(deployerAddress)
1000000000000000000000
> GLDToken.transfer(otherAddress, 300000000000000000000)
> GLDToken.balanceOf(otherAddress)
300000000000000000000
> GLDToken.balanceOf(deployerAddress)
700000000000000000000
```



`_setupDecimals()` 是设置精度方法，更多 [ERC20API](https://docs.openzeppelin.com/contracts/3.x/api/token/erc20#ERC20-_setupDecimals-uint8-)