pragma solidity >=0.6.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract LearnToken is ERC20,Ownable{
    uint _cap;

    // ERC20 的构造函数 ERC20(string,string) 初始化Token的名称和标识
    constructor(string memory name,string memory symbol,uint8 precision,uint cap)public ERC20(name,symbol){
        _setupDecimals(precision);
        _cap = cap;
    }
    

    // 发行,铸造；单位是最小单位，默认精度是18，但是在MetaMask钱包中转账填的单位是10e18
    function mint(address receiver,uint amount) public onlyOwner{
        _mint(receiver,amount);
    }

    // 转账 在 IERC20.sol 中已经实现
    // function transfer(address receiver ,uint amount)public{
    //     _transfer(msg.sender,receiver,amount)
    // }

    // transferFrom

    
    /**
    IERC20接口中的方法（一下内容都为ERC20实现）：
        transfer(recipient, amount): 和MetaMask转账一样，recipient收到同时拥有代币的使用权

        approve 和 transferFrom：
            approve(address spender, uint256 amount) bool
            transferFrom(address sender, address recipient, uint256 amount) bool
                transferFrom 的 msg.sender 对应 approve 的 spender；transferFrom 的 sender 对应 approve 的 msg.sender.
                transferFrom是将sender中的token转到recipient中，但是这个金额只能够从津贴里面扣，这个津贴就是sender通过approve函数授权给msg.sender的；
                approve函数中的spender对应transferFrom中的msg.sender，当我们要通过transferFrom给账户A转账时，首先需要拥有sender给msg.sender授权的金额N，然后转账的金额不能超过N。
        allowance：
            allowance(address owner, address spender)uint256
                查看spender在owner那里的金额，金额实际还是在owner账户中

        totalSupply() uint256 ：
            总发行量

        balanceOf(address account) uint256：
            查看account的余额
     */

    /**
    send-tx:
        constructor(name, symbol)
            构造函数，代币的名字和标识
        increaseAllowance(address spender, uint256 addedValue) → bool
            增加spender可消费的msg.sender的津贴，addedValue为增量
        decreaseAllowance(address spender, uint256 subtractedValue) → bool
            增加spender可消费的msg.sender的津贴，subtractedValue为减量

        内部函数：
            _mint(address account, uint256 amount) 铸币，将新发行的币发到account中，并更新发行总量
            _burn(address account, uint256 amount) 烧币，减少amount数量代币，代币从account的余额中扣
            _setupDecimals(uint8 decimals_) 设置精度，此函数应在构造函数中设置，并且后期不能更改，若更改将发生错误

    call:
        decimals() → uint8
            获得Token的精度
        
    钩子：
        _beforeTokenTransfer(address from, address to, uint256 amount) internal virtual
            在_mint，_burn，_transfer交易发生前中都有调用这个钩子，但是本身并没有内容，需要用户自己实现。当用户override了_beforeTokenTransfer后，
            _mint，_burn，_transfer调用的将是重写后的_beforeTokenTransfer
     */
    
    // 限制总发行量 
    //     在出现增加代币的情况，确保代币现有总量加上增发的数量不超过设定的上限_cap。仅当from为address(0)且to不为address(0)时，合约处于增发状态
    function _beforeTokenTransfer(address from, address to, uint256 amount) internal override(ERC20){
        if(from == address(0)){
            require(totalSupply().add(amount) <= _cap,"totalSupply exceeds cap");
        }
        
    }




    
}
