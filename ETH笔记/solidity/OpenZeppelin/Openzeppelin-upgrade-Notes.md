# openzeppelin upgrades 笔记

## 安装

```bash
npm install @openzeppelin/upgrades
```

## 初始化函数

可升级合约不能使用原生的 constructor ，所以需要自定义一个函数来替代 constructor。这个函数需要保证：

1. 只能够执行一次
2. 在所有函数中，最先被执行

OpenZeppelin提供了一个基础合约`Initializable` 来实现以上的功能：

```js
import "@openzeppelin/upgrades/contracts/Initializable.sol";


contract MyContract is Initializable {
    uint256 public x;

    function initialize(uint256 _x) public initializer {
        x = _x;
    }
}
```

`Initializable` 中有个修饰符： `initializer` 

### 继承合约中使用初始化函数

合约出现继承关系时，原生的 constructor 会自动执行父合约的 构造函数 ，但是OpenZeppelin的不会，所以需要在初始化函数中调用父合约的初始化函数：

```js
import "@openzeppelin/upgrades/contracts/Initializable.sol";


contract BaseContract is Initializable {
    uint256 public y;

    function initialize() public initializer {
        y = 42;
    }
}


contract MyContract is BaseContract {
    uint256 public x;

    function initialize(uint256 _x) public initializer {
        BaseContract.initialize(); // Do not forget this call!
        // or super.initialize();
        x = _x;
    }
}
```



### 使用合约库时需要注意的事项

在 `OpenZeppelin/contracts` 库中，ERC20等一系列库合约并不是可升级合约，因此在可升级合约中不能够使用。我们需要使用 `OpenZeppelin/upgrades` 中的相对应的库合约，例如：ERC20UpgradeSafe 

*@openzeppelin/contracts-ethereum-package/contracts/token/ERC20/ERC20.sol*

### storage变量

声明 storage 变量时，若直接赋值，相当于在constructor 中调用 此赋值指令，所以不适用在 OpenZeppelin 的可升级合约中，像这样：

```js
contract MyContract is Initializable {
    //	这里不要赋值
    uint256 public hasInitialValue /*= 10*/;

    function initialize() public initializer {
        hasInitialValue = 42;
    }
}
```

常量是可以赋值的：

```js
contract MyContract is Initializable {
    //	这里不要赋值
    uint256 public constant hasInitialValue = 10;

    function initialize() public initializer {
        
    }
}
```



## 潜在的漏洞

我们知道，可升级合约是由两部分构成的：代理合约和逻辑合约。与用户交互的合约是代理合约，我们也不会提供前端接口来让用户直接调用逻辑合约。

当然，会有恶意节点来调用逻辑合约，逻辑合约的状态是不会影响到代理合约的，除非逻辑合约中有 `selfdestruct` 和 `delegatecall` 。

若逻辑合约自毁，代理合约的所有调用将委托给一个没有任何代码的地址，因此会破坏项目中所有的实例，所以要避免使用 `selfdistruct` 。`delegatecall` 则可以间接造成以上效果。



## 修改可升级合同的限制

1. 无法修改变量的类型

2. 无法修改变量的声明顺序

3. 要引入新变量是，一定要排在最后

   ```js
   contract MyContract {
       uint256 private x;
       string private y;
       bytes private a; // 新变量放在最后
   }
   ```

4. 重命名变量，升级后将保持之前相同的值

5. 若删除某一变量，存储中并不会删除，若有其他变量在这个被删除的变量的位置，这个新变量将读取剩余的值

### 父合约的修改限制

1. 不可修改父合约的继承顺序
3. 父合约不能增加新的storage变量

所以，需要根据未来可能发生的变化，在父合约中预留一些变量。





