# mocha

测试文件命名规范：add.js -> add.test.js

测试文件结构：

```js
describe('',function(){
    it('',function(){
        
    })
    it('',function(){
        
    })
})
```

其中，describe相当于一个测试组，it 就是组中的测试用例。

## 断言库 chai

我们常常会结合 `chai` 断言库来编写测试用例，断言库 `chai` 的 `expect` 风格的用法：

```js
// 相等或不相等
expect(4 + 5).to.be.equal(9);
expect(4 + 5).to.be.not.equal(10);
expect(foo).to.be.deep.equal({ bar: 'baz' });

// 布尔值为true
expect('everthing').to.be.ok;
expect(false).to.not.be.ok;

// typeof
expect('test').to.be.a('string');
expect({ foo: 'bar' }).to.be.an('object');
expect(foo).to.be.an.instanceof(Foo);

// include
expect([1,2,3]).to.include(2);
expect('foobar').to.contain('foo');
expect({ foo: 'bar', hello: 'universe' }).to.include.keys('foo');

// empty
expect([]).to.be.empty;
expect('').to.be.empty;
expect({}).to.be.empty;

// match
expect('foobar').to.match(/^foo/);
```

使用案例：

```js
const expect = require('chai').expect;

describe('mocha和chai使用案例',function(){
    it('测试equal',function(){
        expect(99+1).to.be.equal(100);
    })
    it('测试typeof',function(){
        expect('hello').to.be.a('string');
    })
})
```



## 测试用例的钩子

```js
describe('mocha hooks',function(){
    before(function(){
        // 在本组所有测试用例（it）之前执行
    })
    beforeEach(function(){
        // 在每个测试用例（it）之前执行一次
    })
    after(function(){
        // 在本组所有测试用例（it）之后执行
    })
    afterEach(function(){
        // 在每个测试用例（it）之后执行一次
    })
    it('',function(){
        
    })
})
```



## 测试用例（it）管理

使用 `only` 和 `skip` 方法进行管理。

`only` 使用后，只有带有 only 的测试用例才会被执行

```js
it.only('1 加 1 应该等于 2', function() {
  expect(add(1, 1)).to.be.equal(2);
});

it('任何数加0应该等于自身', function() {
  expect(add(1, 0)).to.be.equal(1);
});
```

上面例子中，只有 '1 加 1 应该等于 2' 测试用例会被执行。

`skip` 作用刚好相反，被指定的 it 会被跳过不执行。

```js
it.skip('任何数加0应该等于自身', function() {
  expect(add(1, 0)).to.be.equal(1);
});
```



## mocha常用的命令

在项目中，mocha默认会自动执行当前目录下的test的目录的所有测试文件，所以我们通常会将测试文件放在test目录中，这样输入命令 `mocha` 就会自动执行 test 目录下的所有测试文件。

### 执行所有子文件的测试文件

但是子目录里面的测试文件不会自动执行，这时候需要加上参数 `--recursive`

```bash
mocha --recursive
```

### 异步测试

用例不会立马得到结果，需要等待的时间，这时候需要设置超时时间。mocha默认是2000毫秒超时时间，可以使用 `--timeout` 或者 `-t` 更改。**注意：**更改了超时时间后，即使测试用例全部都执行完，程序也会等待超市时间结束。

```bash
mocha --timeout 5000
mocha --recursive -t 5000
```

