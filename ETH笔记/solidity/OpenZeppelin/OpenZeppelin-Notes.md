# OpenZeppelin笔记

## 环境

node，npm

## 功能体验

### 1. 安装openzeppelin/cli

因为各个项目依赖的版本不相同，所以选择本地安装

```bash
npm install --save-dev @openzeppelin/cli
```

### 2. 初始化项目

生成 .openzeppelin文件夹（包含项目信息）、contracts 文件夹和 network.js 文件（网络配置，默认配置了Ganache的网络）

```bash
npx oz init
# or
npx openzeppelin init
```

contracts 文件夹存放合约代码

### 3. 添加第一个合约代码

使用一个简单地合约案例：

```sol
pragma solidity ^0.6.0;

contract Box {
    uint256 private value;

    // Emitted when the stored value changes
    event ValueChanged(uint256 newValue);

    // Stores a new value in the contract
    function store(uint256 newValue) public {
        value = newValue;
        emit ValueChanged(newValue);
    }

    // Reads the last stored value
    function retrieve() public view returns (uint256) {
        return value;
    }
}
```

### 4. 编译合约

编译命令会自动遍历 contracts 和里面的子文件夹的所有文件，将编译后的合约 abi 存放在  `build/contracts` 目录中

```bash
npx oz compile 
```

还可以指定编译器版本

```bash
npx oz compile --solc-version=0.6.7
```

更多的关于[openzeppelin/cli命令](https://docs.openzeppelin.com/cli/2.8/commands)的信息

### 5. 添加更多的合约

在contracts中添加一个子文件夹 access-control，在里面添加一个权限管理的合约 `Auth.sol`

```sol
pragma solidity ^0.6.0;

contract Auth {
    address private administrator;

    constructor() public {
        // Make the deployer of the contract the administrator
        administrator = msg.sender;
    }

    function isAdministrator(address user) public view returns (bool) {
        return user == administrator;
    }
}
```

然后我们可以在Box合约中使用 Auth.sol 中的方法：

```sol
pragma solidity ^0.6.0;

import "./access-control/Auth.sol"
contract Box {
	Auth private auth;
	constructor(Auth _auth)public{
		auth = _auth
	}
	function store(uint256 newValue) public {
		require(auth.isAdministrator(msg.sender));
        value = newValue;
        emit ValueChanged(newValue);
    }
    ...
}
```

部署的时候，将Auth合约的地址传入Box的构造函数中。

### 6. 使用openzeppelin的合约库

openzeppelin 库内的合约都经过了许多考验，被认为是安全的。

安装 openzeppelin 的合约库：

```bash
npm install --save-dev @openzeppelin/contracts
```

导入时，添加前缀 `@openzeppelin/contracts` ，库中有一个合约 `Ownable.sol` 可以代替 `Auth.sol`

所以可以在 Box 这样使用Ownable：

```sol
pragma solidity ^0.6.0;
import "@openzeppelin/contracts/access/Ownable.sol";
contract Box is Ownable{
	...
	//	继承了Ownable中的onlyOwner函数修饰器
	function store(uint256 newValue) public onlyOwner {
        value = newValue;
        emit ValueChanged(newValue);
    }
    ...
}
```

可以在[合约库文档](https://docs.openzeppelin.com/contracts/3.x/)找到更多库的信息，也可以了解到如何开发安全的智能合约。[openzeppelin/contract的GitHub仓库](https://github.com/OpenZeppelin/openzeppelin-contracts)



### 7. 启动本地blockchain Ganache

```bash
npm install --save-dev ganache-cli
```

以 deterministic 模式 启动 ganache-cli 

```bash
npx ganache-cli --deterministic
```

由于我们在初始化项目的时候，已经配置好了连接Ganache网络，所以openzeppelin命令可以看到Ganache的网络信息：

```bash
npx oz accountes
? Pick a network development # 会提示你选择一个网络
Accounts for dev-1598942408008:
Default: 0x90F8bf6A479f320ead074411a4B0e7944Ea8c9C1
All:
- 0: 0x90F8bf6A479f320ead074411a4B0e7944Ea8c9C1
- 1: 0xFFcf8FDEE72ac11b5c542428B35EEF5769C409f0
...
```

查看余额

```bash
npx oz balance
? Enter an address to query its balance 0x90F8bf6A479f320ead074411a4B0e7944Ea8c9C1
? Pick a network development
Balance: 100 ETH
100000000000000000000
```

### 8. 部署合约

如果还未编译，部署命令也会自动为你编译 contracts 里的合约。

```bash
npx oz deploy
✓ Compiled contracts with solc 0.6.7 (commit.b8d736ae)
? Choose the kind of deployment regular
? Pick a network development
? Pick a contract to deploy Box
✓ Deployed instance of Box
0xe78A0F7E598Cc8b0Bb87894B0F60dD2a88d6a8Ab
```

### 9. 与合约交互

#### 9.1 命令行方式交互

发起交易调用 store()

```bash
npx oz send-tx
? Pick a network development
? Pick an instance Box at 0xe78A0F7E598Cc8b0Bb87894B0F60dD2a88d6a8Ab
? Select which function store(newValue: uint256)
? newValue: uint256: 10
✓ Transaction successful. Transaction hash: 0x4664f5c5148ddf0c1b6a2ce64b51b06bb4ef48be4c0f85e18279295643ddcfe9
Events emitted: 
 - ValueChanged(10)
```

查询状态，不消耗Gas

```bash
npx oz call
? Pick a network development
? Pick an instance Box at 0xe78A0F7E598Cc8b0Bb87894B0F60dD2a88d6a8Ab
? Select which function retrieve()
✓ Method 'retrieve()' returned: 10
10
```

#### 9.2 编程式交互

需要安装 web3 和 @openzeppelin/contract-loader

```
npm install web3 @openzeppelin/contract-loader
```

**导入和查看账户信息**

编写代码：src/index.js

```js
const Web3 = require('web3');
const {setupLoader} = require('@openzeppelin/contract-loader');

async function main(){
    const web3 = new Web3('http://localhost:8545');
    const accounts = await web3.eth.getAccounts();
    console.log(accounts);
}

main();
```

运行

```bash
node src/index.js 
[ '0x90F8bf6A479f320ead074411a4B0e7944Ea8c9C1',
  '0xFFcf8FDEE72ac11b5c542428B35EEF5769C409f0',
  ...
]
```

**获取状态数据**

更改如下：

```js
const Web3 = require('web3');
const {setupLoader} = require('@openzeppelin/contract-loader');

async function main(){
    const web3 = new Web3('http://localhost:8545');
    //	contract's address
    const address = '0xe78A0F7E598Cc8b0Bb87894B0F60dD2a88d6a8Ab';
    const box = loader.fromArtifact('Box',address);
    
    const value = await box.methods.retrieve().call();
    console.log("01 Box value is",value);
}

main();
```

运行：

```bash
node src/index.js
01 Box value is 10
```

**发起交易**

```js
onst Web3 = require('web3');
const {setupLoader} = require('@openzeppelin/contract-loader');

async function main(){
    const web3 = new Web3('http://localhost:8545');
    const loader = setupLoader({provider:web3}).web3;
    const accounts = await web3.eth.getAccounts();

    const address = '0xe78A0F7E598Cc8b0Bb87894B0F60dD2a88d6a8Ab';
    const box = loader.fromArtifact('Box',address);

    const value = await box.methods.retrieve().call();
    console.log("01 Box value is",value);

    // send a transaction
    await box.methods.store(20)
        .send({from:accounts[0],gas:50000,gasPrice:1e6});

    const value_ = await box.methods.retrieve().call();
    console.log("02 Box value is",value_)
}

main();
```

运行：

```bash
node src/index.js
01 Box value is 10
02 Box value is 20
```



### 10. 测试

安装 @openzeppelin/test-environment

```bash
npm install --save-dev @openzeppelin/test-environment
```

@openzeppelin/test-environment 被 require 的时候，会自动启动一个本地的 ETH 测试网来供测试使用，就像Ganache。我们可以通过如下方式获得测试网的账户：

```js
const {accounts} = require('@openzeppelin/test-environment');
const [admin,deployer,user] = accounts;
```

@openzeppelin/test-environment 也会从 `build/contracts` 中获取已经编译好的合约，并返回合约实例，并没有部署

```js
const {accounts,contract} = require('@openzeppelin/test-environment');
const contractInstance = contract.fromArtifact('MyContractName');
```

其中MyContractName是合约名，不是合约文件名。

#### 结合 mocha 编写测试用例

```js
const {accounts,contract} = require("@openzeppelin/test-environment");
const [account01] = accounts;
const expect = require('chai').expect;
const box = contract.fromArtifact('Box');

decribe('Box 合约测试',function(){
    beforeEach(async function(){
        // 为每个测试用例部署一个合约
        this.contract = await box.new({from:account01});
    })

    it('测试stor方法',function(){
        await this.contract.stor(9,{from:account01});
        expect(await this.contract.retrieve()).to.be.equal(9);
    })
})
```

完成编写后，可以使用 mocha 命令来运行测试文件，最好的做法是在 package.json 的 scripts 中添加测试命令：

```json
"script":{
    "test":"macha --recursive -t 5000"
}
```

然后使用 `npm test` 就可以运行测试文件。值得注意的是：这里不会自动编译合约，当合约被修改后，不要忘记编译。



#### @openzeppelin/test-helpers

用来捕获合约返回的错误和事件 `expectEvent` `expectRevert ` ;合约返回的数据是 bignumber ,`BN`可以用来比较 bignumber。

```js
const {accounts,contract} = require("@openzeppelin/test-environment");
const { BN,expectEvent,expectRevert } = require('@openzeppelin/test-helpers');
const [account01,account02] = accounts;
const expect = require('chai').expect;
const box = contract.fromArtifact('Box');

describe('Box 合约测试',function(){
    beforeEach(async function(){
        // 为每个测试用例部署一个合约
        this.contract = await box.new({from:account01});
    })

    it('测试stor方法',async function(){
        const value = new BN('9');
        await this.contract.store(value,{from:account01});
        expect(await this.contract.retrieve()).to.be.bignumber.equal(value);
    })

    it('捕获事件',async function(){
        const value = new BN('10');
        // 判断事件EventName是否触发，若事件有参数，{newValue:value}标明
        const reciever = await this.contract.store(value,{from:account01});
        expectEvent(reciever,'ValueChanged',{newValue:value});
    })

    it('捕获错误',async function(){
        // 判断事件返回的错误是否是 'Ownable: caller is not the owner'，如果合约没有返回错误，测试也不通过
        await expectRevert(
            this.contract.store(11,{from:account02}),'Ownable: caller is not the owner'
        )
    })
})
```

**注意：**EventName是合约中的事件名，newValue 是合约事件名中的变量名





### 11. 连接测试网

目前4个ETH的测试公网可以使用：Ropsten、Rinkeby、Kovan、Goerli

一种比较便捷的方式是使用Infura提供的免费托管节点，来连接到测试网。在连接测试网之前，需要准备一个有测试币的账号。

因为Infura提供的是一个公共节点，不能够对我们产生的交易进行签名，所以还需要使用 `@truffle/hdwallet-provider` 进行签名。

```bash
npm install --save-dev @truffle/hdwallet-provider
```

**获得[Infura](https://infura.io/)的access**

注册账号，新建一个project来获得 projectId。

**更改network.js文件**

```js
const { projectId, mnemonic } = require('./secrets.json');
const HDWalletProvider = require('@truffle/hdwallet-provider');

module.exports = {
  networks: {
    development: {
      protocol: 'http',
      host: 'localhost',
      port: 8545,
      gas: 5000000,
      gasPrice: 5e9,
      networkId: '*',
    },
    goerli:{
      provider: () => new HDWalletProvider(
        mnemonic, `https://goerli.infura.io/v3/${projectId}`
      ),
      networkId: 5,
      gasPrice: 10e9
    }
  },
};
```

首先要获得账户的助记词和Infura的projectId，导入 @truffle/hdwallet-provider ；这里还需要注意的是 networkId ，主网是1，然后按照网站给出的测试网顺序 +1 ，Goerli是最后一个测试网，所以 NetworkId 是5 .

**查看账户列表**

```bash
npx oz accounts
? Pick a network goerli
Accounts for goerli:
Default: 0xBCcC2073ADfC46421308f62cfD9868dF00D339a8
All:
- 0: 0xBCcC2073ADfC46421308f62cfD9868dF00D339a8
- 1: 0x1be80e5ea99793dbA9a8bA7a8d1395f8D4c14BB1
...
```

**查看账户余额**

```bash
npx oz balance
? Enter an address to query its balance 0xBCcC2073ADfC46421308f62cfD9868dF00D339a8
? Pick a network goerli
Balance: 276.333888297762234752 ETH
276333888297762234752
```

**部署合约**

```bash
npx oz deploy
Nothing to compile, all contracts are up to date.
? Choose the kind of deployment regular
? Pick a network goerli
? Pick a contract to deploy Box
✓ Deployed instance of Box
0xc58768Aefb77757a85a570DC5b0D108025aFA076
```

**发送交易**

```bash
npx oz send-tx
? Pick a network goerli
? Pick an instance Box at 0xc58768Aefb77757a85a570DC5b0D108025aFA076
? Select which function store(newValue: uint256)
? newValue: uint256: 30
✓ Transaction successful. Transaction hash: 0x173e5a2fb4e18a15b4ea660711fade560a0a3458ae594213f04f6eadb3e22ed8
Events emitted: 
 - ValueChanged(30)
```

**查询状态**

```bash
npx oz call
? Pick a network goerli
? Pick an instance Box at 0xc58768Aefb77757a85a570DC5b0D108025aFA076
? Select which function retrieve()
✓ Method 'retrieve()' returned: 30
30
```



### 12. 合约升级

- 逻辑合约

- 代理合约（实际交互的）

代理合约只是一个简单的合约，代理所有逻辑合约的calls。与生活中的代理关系不同的是，所有的balance，contract storage都是在代理合约里面的，逻辑合约只处理逻辑，数据的读写以及交易都发生在代理合约里面。

核心思想是：解耦contract的代码和状态。

所以升级一个合约有以下两个步骤：

1. 部署一个新的逻辑合约；
2. 向逻辑合约发起一笔交易，更新他需要交互的逻辑合约地址

这样用户不需要更改交互的合约地址，还是一如既往的与代理合约进行交互，同时你可以修改bug

#### 12.1 命令行升级

首先，部署合约的时候，应该选择可升级类型(ungradeable)

```bash
npx oz deploy
Nothing to compile, all contracts are up to date.
? Choose the kind of deployment upgradeable # 注意：这里选择upgradeable
? Pick a network development
? Pick a contract to deploy Box
✓ Added contract Box
✓ Contract Box deployed
All implementations have been deployed
? Call a function to initialize the instance after creating it? No
✓ Setting everything up to create contract instances
✓ Instance created at 0xCfEB869F69431e42cdB54A4F4f105C19C080A601
To upgrade this instance run 'oz upgrade'
0xCfEB869F69431e42cdB54A4F4f105C19C080A601
```

 你可以发起一笔交易以更改contract的状态：

```bash
npx oz send-tx
? Pick a network development
? Pick an instance Box at 0xCfEB869F69431e42cdB54A4F4f105C19C080A601
? Select which function store(newValue: uint256)
? newValue: uint256: 99
✓ Transaction successful. Transaction hash: 0x848050e2718b2bf0caadc8fb8a5a1b7056773cc06984472bd42f3b5eeefd941a
Events emitted: 
 - ValueChanged(99)
```

增加一个新需求，自增函数：

```sol
contract Box {
    // ...

    // Increments the stored value by 1
    function increment() public {
        value = value + 1;
        emit ValueChanged(value);
    }
}
```

修改测试完成后，就可以执行升级命令来升级我们之前部署的合约：

```bash
npx oz upgrade
? Pick a network development
? Which instances would you like to upgrade? Choose by name
? Pick an instance to upgrade Box
? Call a function on the instance after upgrading it? No
✓ Compiled contracts with solc 0.6.12 (commit.27d51765)
✓ Contract Box deployed
All implementations have been deployed
✓ Instance upgraded at 0xCfEB869F69431e42cdB54A4F4f105C19C080A601. Transaction receipt: 0xcd85e8481c8e15f37a2822e5ac829cec315484dc8cddde37625270358ff9370f
✓ Instance at 0xCfEB869F69431e42cdB54A4F4f105C19C080A601 upgraded
```

合约升级成功，但是地址和状态都没有改变。

查看状态是否是升级前的状态：

```bash
npx oz call
? Pick a network development
? Pick an instance Box at 0xCfEB869F69431e42cdB54A4F4f105C19C080A601
? Select which function retrieve()
✓ Method 'retrieve()' returned: 99
99
```

调用新方法：

```bash
npx oz send-tx
? Pick a network development
? Pick an instance Box at 0xCfEB869F69431e42cdB54A4F4f105C19C080A601
? Select which function increment()
✓ Transaction successful. Transaction hash: 0xb3eaceb24df52c4933d868113aabb02c12a91080e0c923373e1ddedae7b46325
Events emitted: 
 - ValueChanged(100)
```



#### 12.2 编程式升级

TODO

### 13. 主网上线前的准备

TODO



## openzeppelin/cli命令

[openzeppelin/cli命令](https://docs.openzeppelin.com/cli/2.8/commands)