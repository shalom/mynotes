# 事件在日志中的格式

[文档地址](https://goethereumbook.org/zh/)

以下是ERC20标准的 Transfer 事件触发时，在remix看到的事件数据输出：

```json
[
	{
		"from": "0x6c7349879474673FDEb2de4870D7DADf15958f3f",
		"topic": "0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef",
		"event": "Transfer",
		"args": {
			"0": "0xBCcC2073ADfC46421308f62cfD9868dF00D339a8",
			"1": "0xEdFF9d58D91759438464D58d61b82BCFDFB7Bc70",
			"2": "9999999",
			"from": "0xBCcC2073ADfC46421308f62cfD9868dF00D339a8",
			"to": "0xEdFF9d58D91759438464D58d61b82BCFDFB7Bc70",
			"value": "9999999",
			"length": 3
		}
	}
]
```



获取交易 Receipt 数据：

```json
{
  blockHash: "0x7eaf6abe64592d10828e136635aa6be6f4d09da3bb5b9fddf87773ee152d657c",
  blockNumber: 4654718,
  contractAddress: null,
  cumulativeGasUsed: 52464,
  from: "0x076979a0b3c87334e5d72e3afcafaa80f7888cac",
  gasUsed: 52464,
  logs: [{
      address: "0x73c2a5b1a32fa8e33101a6ab119203f4417feae4",
      blockHash: "0x7eaf6abe64592d10828e136635aa6be6f4d09da3bb5b9fddf87773ee152d657c",
      blockNumber: 4654718,
      data: "0x0000000000000000000000000000000000000000000000056bc75e2d63100000",
      logIndex: 0,
      removed: false,
      topics: ["0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef", "0x000000000000000000000000076979a0b3c87334e5d72e3afcafaa80f7888cac", "0x000000000000000000000000cd9f286ba6a3d2df7885f4a2be267fc524d32bd3"],
      transactionHash: "0xe03fac05ff4dde83fc9267184fd8c08bd78599f950e817dbf7fa4a4d4d319ce2",
      transactionIndex: 0
  }],
  logsBloom: "0x20000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000080000008000000000400000000000000000000000000000000000000040000000000000000100000000000000000000000000010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000200000000000000000000000200000002000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000040000000000000000400",
  status: "0x1",
  to: "0x73c2a5b1a32fa8e33101a6ab119203f4417feae4",
  transactionHash: "0xe03fac05ff4dde83fc9267184fd8c08bd78599f950e817dbf7fa4a4d4d319ce2",
  transactionIndex: 0
}
```



每当事件被触发一次，就会在logs数组项中就会多一项。在 go-ethereum 中，对应日志的数据结构：

```go
type Log struct {
   // 合约地址
   Address common.Address `json:"address" gencodec:"required"`
   // Topics[0]事件签名的32位hash Topics[1]事件的第一个参数 Topics[2]第二个参数
   Topics []common.Hash `json:"topics" gencodec:"required"`
   // supplied by the contract, usually ABI-encoded
   Data []byte `json:"data" gencodec:"required"`

   // Derived fields. These fields are filled in by the node
   // but not secured by consensus.
   // block in which the transaction was included
   BlockNumber uint64 `json:"blockNumber"`
   // hash of the transaction
   TxHash common.Hash `json:"transactionHash" gencodec:"required"`
   // index of the transaction in the block
   TxIndex uint `json:"transactionIndex"`
   // hash of the block in which the transaction was included
   BlockHash common.Hash `json:"blockHash"`
   // index of the log in the block
   Index uint `json:"logIndex"`

   // The Removed field is true if this log was reverted due to a chain reorganisation.
   // You must pay attention to this field if you receive logs through a filter query.
   Removed bool `json:"removed"`
}
```



**其中最重要的两个数据是**：

- Topics

- Data



## Topics

topics 是个数组，这个数组的第一个元素就代表所触发的事件，是个 256 位的数字，用 16 进制表示。这个是事件名的签名：

```go
logTransferSig := []byte("Transfer(address,address,uint256)")
logTransferSigHash := crypto.Keccak256Hash(logTransferSig)
```

其中 logTransferSigHash 就是 Topics[0]

其余元素是事件参数中，指定了 indexed 的参数。就像 ERC20 中 Transfer 事件：

```sol
event Transfer(address indexed from, address indexed to, uint256 value);
```

其中 from ，to 指定了indexed，那么 Topics[1] Topics[2] 就分别为 from to。

 **注意：**合约事件中，最多可以指定3个参数为 indexed ，从 go-ethereum 中可以看到，Topics 的容量为 4 。如果标记 indexed 的参数为数组，那么topics只会保存他们的keccak-256 哈希值。



## Data

Data就是传入事件的参数，但是不包括 指定了indexed 的参数，所以这里的 Data 中只包含 value 。







