# 类型对应关系

| 类型             | Solidity | Go                |
| :--------------- | :------- | :---------------- |
| 字符串           | string   | string            |
| 布尔             | bool     | bool              |
| 地址             | address  | common.Address    |
| 无符号整数       | uintN    | uintN 或 *big.Int |
| 有符号整数       | intN     | intN 或 *big.Int  |
| 固定长度字节数组 | bytesN   | [N]byte           |
| 动态长度字节数组 | bytes    | []byte            |
| 固定长度数组     | T[k]     | array             |
| 动态长度数组     | T[]      | slice             |
| 枚举             | enum     | uintN             |
| 映射             | mapping  | -                 |
| 结构体           | struct   | -                 |