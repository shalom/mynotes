package main

import (
	"context"
	"fmt"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	hdwallet "github.com/miguelmota/go-ethereum-hdwallet"
	"github.com/status-im/keycard-go/hexutils"
	"go-ethereum-learn/util"
	"log"
	"math/big"
)

const TOKEN_ADDRESS = "0x6c7349879474673FDEb2de4870D7DADf15958f3f"

func main() {
	config, err := util.ParseConfig()
	if err != nil {
		log.Fatal("util.ParseConfig err: ", err)
	}

	client, err := ethclient.Dial("https://rinkeby.infura.io/v3/" + config.ProjectID)
	if err != nil {
		log.Fatal("ethclient.Dial err: ", err)
	}
	chainID, err := client.NetworkID(context.Background())
	if err != nil {
		log.Fatal("client.NetworkID err: ", err)
	}
	gasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		log.Fatal("client.SuggestGasPrice err: ", err)
	}
	fmt.Println("gasPrice ==> ",gasPrice)
	value := big.NewInt(0)

	wallet, err := hdwallet.NewFromMnemonic(config.Mnemonic)
	if err != nil {
		log.Fatal("hdwallet.NewFromMnemonic err: ", err)
	}
	senderAccount, err := util.GetAccountByIndex(wallet, "0", true)
	if err != nil {
		log.Fatal("util.GetAccountByIndex senderAccount err: ", err)
	}
	fmt.Println("senderAccount ==> ",senderAccount.Address.Hex())
	receiverAccount, err := util.GetAccountByIndex(wallet, "1", true)
	if err != nil {
		log.Fatal("util.GetAccountByIndex receiverAccount err: ", err)
	}
	tokenAddr := common.HexToAddress(TOKEN_ADDRESS)
	nonce, err := client.PendingNonceAt(context.Background(), senderAccount.Address)
	if err != nil {
		log.Fatal("client.PendingNonceAt err: ", err)
	}
	//	获得函数签名
	funcSign := util.GetFunctionSign("transfer(address,uint256)")

	paddedAddr := common.LeftPadBytes(tokenAddr.Bytes(), 32)
	fmt.Println("address ==> ", hexutils.BytesToHex(paddedAddr))
	amount := new(big.Int)
	_, b := amount.SetString("1000000000000000000000", 0)
	if !b {
		log.Fatal("amount.SetString error")
	}
	paddedAmount := common.LeftPadBytes(amount.Bytes(), 32)
	/*
		input data
	*/
	var data []byte
	data = append(data, funcSign...)
	data = append(data, paddedAddr...)
	data = append(data, paddedAmount...)

	//	获得Gas预估
	gasLimit, err := client.EstimateGas(context.Background(), ethereum.CallMsg{
		To:   &receiverAccount.Address,
		Data: data,
	})
	if err != nil {
		log.Fatal("client.EstimateGas err: ", err)
	}
	fmt.Println("gasLimit ==> ", gasLimit)

	transaction := types.NewTransaction(nonce, receiverAccount.Address, value, gasLimit, gasPrice, data)
	tx, err := wallet.SignTx(senderAccount, transaction, chainID)
	if err != nil {
		log.Fatal("wallet.SignTx err: ", err)
	}

	balanceAt, err := client.BalanceAt(context.Background(), senderAccount.Address, nil)
	if err != nil {
		log.Fatal("client.BalanceAt error: ", err)
	}
	fmt.Println("balance ==>",balanceAt)
	err = client.SendTransaction(context.Background(), tx)
	if err != nil {
		log.Fatal("client.SendTransaction err: ", err)
	}
	fmt.Printf("tx sent: %s", tx.Hash().Hex())
}
