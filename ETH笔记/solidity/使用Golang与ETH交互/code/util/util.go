package util

import (
	"github.com/ethereum/go-ethereum/accounts"
	"github.com/ethereum/go-ethereum/crypto"
	hdwallet "github.com/miguelmota/go-ethereum-hdwallet"
)

const 	PRE_PATH    = "m/44'/60'/0'/0/"

func GetAccountByIndex(wallet *hdwallet.Wallet, index string, isPin bool) (accounts.Account, error) {
	path := PRE_PATH + index
	p := hdwallet.MustParseDerivationPath(path)
	return wallet.Derive(p, isPin)
}

func GetFunctionSign(function string)[]byte {
	funcByte := []byte(function)
	funcHash := crypto.Keccak256(funcByte)
	return funcHash[:4]
}
