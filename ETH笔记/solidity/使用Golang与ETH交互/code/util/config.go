package util

import (
	"encoding/json"
	"os"
)

const 	CONFIG_PATH = "./config.json"

type ConfigData struct {
	Mnemonic  string `json:"mnemonic"`
	ProjectID string `json:"project_id"`
}

func ParseConfig() (config ConfigData, err error) {
	file, err := os.OpenFile(CONFIG_PATH, os.O_RDONLY, 0666)
	if err != nil {
		return
	}

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&config)
	if err != nil {
		return
	}
	return
}


