package util

import (
	"fmt"
	"github.com/status-im/keycard-go/hexutils"
	"testing"
)

func TestGetFunctionSign(t *testing.T) {
	funcHash01 := GetFunctionSign("transfer(address,uint256)")
	fmt.Println(hexutils.BytesToHex(funcHash01))

	//	0x5bb68afd
	funcHash02 := GetFunctionSign("fixPoolCreateV3(string,address,address,uint256,uint256,uint256,bool,uint256,uint256)")
	fmt.Println("==> ", hexutils.BytesToHex(funcHash02))

	funcHash03 := GetFunctionSign("bonusClaim()")
	fmt.Println(hexutils.BytesToHex(funcHash03))

	funcHash04 := GetFunctionSign("createErc721(string,address,uint256,uint256,uint256,uint256,bool,uint256)")
	fmt.Println(hexutils.BytesToHex(funcHash04))
}
