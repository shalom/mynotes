pragma solidity ^0.7.0;

contract Test {
    uint256 public a;

    constructor()public{
        a = 11;
    }

    function setValue(uint256 _a) public {
        a = _a;
    }

    function getValue()public view returns(uint256){
        return a;
    }
}
