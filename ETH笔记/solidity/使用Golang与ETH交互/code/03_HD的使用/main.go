package main

import (
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/miguelmota/go-ethereum-hdwallet"
	"log"
	"math/big"
)

func main() {
	mnemonic := "hood athlete copy total thank produce climb shoulder exile lizard sunny immense"

	wallet, err := hdwallet.NewFromMnemonic(mnemonic)
	if err != nil {
		log.Fatal("hdwallet.NewFromMnemonic error: ",err)
	}

	path := hdwallet.MustParseDerivationPath("m/44'/60'/0'/0/1")
	account, err := wallet.Derive(path, true)
	if err != nil {
		log.Fatal("wallet.Derive error: ",err)
	}

	fmt.Println(account.Address.Hex())

	accounts := wallet.Accounts()
	fmt.Println(len(accounts))
	for _, v := range accounts {
		fmt.Println(v.Address.Hex())
	}

	nonce := uint64(0)
	value := big.NewInt(1000000000000000000)
	toAddress := common.HexToAddress("0x0")
	gasLimit := uint64(21000)
	gasPrice := big.NewInt(21000000000)
	var data []byte

	tx := types.NewTransaction(nonce, toAddress, value, gasLimit, gasPrice, data)
	signedTx, err := wallet.SignTx(account, tx, nil)
	if err!=nil{
		log.Fatal("wallet.SignTx error: ",err)
	}

	spew.Dump(signedTx)
}
