module go-ethereum-learn

go 1.14

require (
	github.com/aristanetworks/goarista v0.0.0-20200812190859-4cb0e71f3c0e // indirect
	github.com/btcsuite/btcd v0.21.0-beta // indirect
	github.com/davecgh/go-spew v1.1.1
	github.com/ethereum/go-ethereum v1.9.22
	github.com/miguelmota/go-ethereum-hdwallet v0.0.0-20200123000308-a60dcd172b4c
	github.com/status-im/keycard-go v0.0.0-20190316090335-8537d3370df4
	github.com/tyler-smith/go-bip39 v1.0.2 // indirect
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a // indirect
	golang.org/x/sys v0.0.0-20200926100807-9d91bd62050c // indirect
)
