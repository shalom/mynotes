package main

import (
	"fmt"
	"github.com/ethereum/go-ethereum/ethclient"
	"log"
)

func main() {
	/*
		连接本地的私网
	*/
	//client,err:=ethclient.Dial("http://localhost:8545")

	/*
		连接 infura 托管节点，URL也可以为：wss://rinkeby.infura.io/ws/v3/ded95f7c60334f19a9bc49dc69dfad30
	*/
	client, err := ethclient.Dial("https://rinkeby.infura.io/v3/ded95f7c60334f19a9bc49dc69dfad30")
	if err != nil {
		log.Fatal("Dial client error: ", err)
	}

	fmt.Println(client)
}
