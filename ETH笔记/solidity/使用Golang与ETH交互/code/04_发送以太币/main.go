package main

import (
	"context"
	"fmt"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	hdwallet "github.com/miguelmota/go-ethereum-hdwallet"
	"go-ethereum-learn/util"
	"log"
	"math/big"
)

func main() {
	config, err := util.ParseConfig()
	if err != nil {
		log.Fatal("parseConfig error: ", err)
	}
	/*
		连接 infura 托管节点，URL也可以为：wss://rinkeby.infura.io/ws/v3/ded95f7c60334f19a9bc49dc69dfad30
	*/
	client, err := ethclient.Dial("https://rinkeby.infura.io/v3/" + config.ProjectID)
	if err != nil {
		log.Fatal("Dial client error: ", err)
	}
	chainID, err := client.NetworkID(context.Background())
	if err != nil {
		log.Fatal("client.NetworkID toAccount error: ", err)
	}

	/*
		使用HD钱包
	*/
	wallet, err := hdwallet.NewFromMnemonic(config.Mnemonic)
	if err != nil {
		log.Fatal("hdwallet.NewFromMnemonic error: ", err)
	}

	senderAccount, err := util.GetAccountByIndex(wallet, "0", true)
	if err != nil {
		log.Fatal("wallet.Derive error: ", err)
	}

	//	获得当前应该使用的nonce
	nonce, err := client.PendingNonceAt(context.Background(), senderAccount.Address)
	if err != nil {
		log.Fatal("client.PendingNonceAt error: ", err)
	}
	fmt.Println("==> ", nonce)

	//	获得当前网络中普遍gasPrice
	gasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		log.Fatal("client.SuggestGasPrice error: ", err)
	}
	fmt.Println("gasPrice: ", gasPrice)

	fromAccount, err := util.GetAccountByIndex(wallet, "1", true)
	if err != nil {
		log.Fatal("wallet.Derive toAccount error: ", err)
	}

	//	转账的value(1 ether)，单位为 wei，可以使用工具网站来进行各个单位的转换：https://etherconverter.netlify.app/
	value := big.NewInt(1000000000000000000)
	//	转账的gasLimit应该设置为21000
	gasLimit := uint64(21000)

	//	获得未签名的交易
	tx := types.NewTransaction(nonce, senderAccount.Address, value, gasLimit, gasPrice, nil)

	//	签名并广播
	signedTX, err := wallet.SignTx(fromAccount, tx, chainID)
	if err != nil {
		log.Fatal("wallet.SignTx toAccount error: ", err)
	}

	err = client.SendTransaction(context.Background(), signedTX)
	if err != nil {
		log.Fatal("client.SendTransaction  error: ", err)
	}

	fmt.Printf("send tx : %s", signedTX.Hash().Hex())
}
