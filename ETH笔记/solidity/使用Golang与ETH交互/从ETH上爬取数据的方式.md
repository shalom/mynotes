# 爬取ETH数据

## 通过以太坊节点开放的 json-rpc 来交互

[json-rpc文档](https://eth.wiki/json-rpc/api)

[Infura节点文档](https://infura.io/docs/ethereum/json-rpc/eth-call)

### 调用合约的call方法

其中有一个接口是用来调用合约的call方法，也就是不更改数据状态、不消耗Gas的方法：`eth_call`。

向节点发送POST请求

`URL：POST https://<network>.infura.io/v3/YOUR-PROJECT-ID`

**HEADERS**

`Content-Type: application/json`

**请求带上的数据**

在文档中给出的案例：

```json
{
    "jsonrpc":"2.0",
    "method":"eth_call",
    "params":[
        {
            "from":"0xb60e8dd61c5d32be8058bb8eb970870f07233155",
            "to":"0xd46e8dd67c5d32be8058bb8eb970870f07244567",
            "gas":"0x76c0",
            "gasPrice":"0x9184e72a000",
            "value":"0x9184e72a",
            "data":"0xd46e8dd67c5d32be8d46e8dd67c5d32be8058bb8eb970870f072445675058bb8eb970870f072445675"
        },
        "latest"
    ],
    "id":1
}
```

因为不消耗gas，params中的部分参数可以省略。不可以省略的参数为：`to` && `data` ，to为要调用的合约地址，data为交易的Input，也就是使用solidity ABI 编码及解码函数中 `abi.encodeWithSignature(string signature, ...) returns (bytes)` 生成的Input。

其次，params中的最后一个参数 "latest" 代表从最新区块中获取数据。

#### 使用Golang生成Input

Input分为两个部分：

- 要调用的函数签名
- 函数传入的参数

对应 `abi.encodeWithSignature(string signature, ...) returns (bytes)` 中的 `signature && ...` 

使用 `go-ethereum` 中的方法生成**函数签名**

```go
sign := crypto.Keccak256Hash([]byte("Transfer(address,address,uint256)"))
```

签名应该取 sign 的前4个字节。

使用 `go-ethereum` 中的方法生成**参数部分**

```go
package main

import (
    "math/big"
    "log"
    "github.com/ethereum/go-ethereum/common/hexutil"
    "github.com/ethereum/go-ethereum/accounts/abi"
    "github.com/ethereum/go-ethereum/common"
    "github.com/ethereum/go-ethereum/crypto/sha3"
)

func main() {
    uint256Ty, _ := abi.NewType("uint256")
    bytes32Ty, _ := abi.NewType("bytes32")
    addressTy, _ := abi.NewType("address")

    arguments := abi.Arguments{
        {
            Type: addressTy,
        },
        {
            Type: bytes32Ty,
        },
        {
            Type: uint256Ty,
        },
    }

    bytes, _ := arguments.Pack(
        common.HexToAddress("0x0000000000000000000000000000000000000000"),
        [32]byte{'I','D','1'},
        big.NewInt(42),
    )
}
```

对于 `eth_call` 中的data参数，需要将函数签名的前4个字节和参数拼接。

在 `Ankr-gin-ERC721` 项目中，调用合约 `balanceOf` 方法查询余额的案例：

```go
//	调用合约的balanceOf方法查询余额
func BalanceOf(contractAddr string, userAddr string, chainID int) ([]byte, error) {
	addressTy, err := abi.NewType("address", "", nil)
	if err != nil {
		return nil, err
	}

	arg := abi.Arguments{
		{
			Type: addressTy,
		},
	}
	bytes, err := arg.Pack(common.HexToAddress(userAddr))
	if err != nil {
		return nil, err
	}
	hex_ := hex.EncodeToString(bytes)

	fmt.Println("========> ", hex_)

	param := Param{
		To:   contractAddr,
		Data: BALANCEOF_SIGN + string(hex_),
	}
	rpcData := RPCData{
		JsonRPC: JSONRPC,
		Method:  METHOD,
		Id:      requestID,
		Params:  make([]interface{}, 0),
	}
	rpcData.Params = append(rpcData.Params, param)
	rpcData.Params = append(rpcData.Params, BLOCK)

	url := conf.ETHHttpURLs[chainID] + conf.ConfigMsg.ProjectID
	data, err := util.PostUrl(url, nil, rpcData, nil)
	if err != nil {
		return data, err
	}
	requestID++

	return data, nil
}
```



##  通过以太坊节点客户端来订阅合约事件

[可以查看这个电子书来学习如何订阅并解析事件](https://goethereumbook.org/zh/event-subscribe/)



## 通过 Etherscan 提供的接口来获取数据

[Etherscan API](https://learnblockchain.cn/docs/etherscan/)

