# truffle/hdwallet-provider

hdwallet-provider 可以成为 Web3 provider，可以使用助记词给交易签名。

**环境要求**

```
Node >= 7.6
Web3 ^1.2.0
```

**安装**

```bash
npm install @truffle/hdwallet-provider --save
```

**使用**

当我们写脚本来调用我们的合约，以及在 openzeppelin 项目中时，我们都会需要用到 hdwallet-provider 来获得 Web3 Instance 。

```js
// 1. 导入
const HDWalletProvider = require("@truffle/hdwallet-provider");
const Web3 = require('web3');

async function main(){
    // 2. 获得provider
    let provider = new HDWalletProvider(mnemonic, `https://rinkeby.infura.io/v3/${projectId}`);
    // 3. 获得web3实例
	const web3 = new Web3(provider);
}
```



[README.MD](https://github.com/trufflesuite/truffle/tree/master/packages/hdwallet-provider)