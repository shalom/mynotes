const Web3 = require('web3');

const web3 = new Web3(new Web3.providers.HttpProvider('HTTP://127.0.0.1:8545'));

// console.log(web3);
web3.eth.defaultAccount = '0xc783df8a850f42e7f7e57013759c285caa701eb6';

console.log(web3.eth.accounts);
console.log(web3.eth.defaultAccount);
console.log(web3.eth.Contract.defaultBlock);

console.log(" == >> ",web3.utils.fromAscii('proposes'))

const abi = [
    {
      "inputs": [],
      "name": "a",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "getValue",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "v",
          "type": "uint256"
        }
      ],
      "name": "setValue",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    }
  ];

let contract = new web3.eth.Contract(abi,'0x7c2c195cd6d34b8f845992d380aadb2730bb9c6f',{
    from:'0x26c43a1d431a4e5ee86cd55ed7ef9edf3641e901',
    gasPrice:'20000000000'
})
contract.methods.setValue(10).send({from: '0xc783df8a850f42e7f7e57013759c285caa701eb6'})
.then(function(receipt){
    console.log(receipt);
    contract.methods.getValue().call({from:'0xc783df8a850f42e7f7e57013759c285caa701eb6'},function(err,result){
        if(!err){
            console.log('result => ',result);
        }
    })
});