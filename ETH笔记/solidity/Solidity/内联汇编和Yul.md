# 内联汇编assembly

https://learnblockchain.cn/article/675
https://solidity.tryblockchain.org/blockchain-solidity-assembly.html

## 使用内联汇编的好处

1. 更少的Gas消耗
2. 拥有更多的控制权，编写库时特别有用

经典库：

- [String Utils](https://github.com/Arachnid/solidity-stringutils/blob/master/src/strings.sol) by [Nick Johnson](https://github.com/Arachnid) (Ethereum Foundation 开发)
- [Bytes Utils](https://github.com/GNSPS/solidity-bytes-utils/blob/master/contracts/BytesLib.sol) by [Gon çalo S á](https://github.com/GNSPS) (Consensys 开发)



## 基本语法

### 引入汇编

使用 `assembly{}` 引入

```js
function assemblyTest()public{
    assembly{
        // write assembly code here.
    }
}
```

可以引入多个 `assembly{}` 代码块，但是代码块之间不能通信， `assembly{}` 代码块也不能嵌套。

```js
function assemblyTest()public{
    assembly{
        let x := 9
    }
    assembly{
        let y := x // error
    }
}
```



### 变量定义和赋值

使用 `let` 关键字定义变量，赋值使用 `:=` 而不是 `=`

```js
function assemblyTest()public{
    assembly{
        let y
        y := 0xabc
        let x := "hello"
    }
}
```

 使用 `let` 定义时，会自动初始化为 0 值。

EVM 内部执行 `let` 指令的大致步骤：

- 创建一个新的堆栈槽位
- 为变量保留该槽位
- 当到达代码块结束时自动销毁该槽位

所以在 `assembly{}` 代码块以外，是无法访问内部的变量的。



### 注释

```js
function assemblyTest()public{
    assembly{
        // 单行注释
        /*
         多行注释
        */
    }
}
```



### 字面量

可以识别 10进制 和 16进制字面量，还可以识别字符串，字符串长度不能超过32字符。

```js
function assemblyTest()public{
    assembly{
        let x := 10
        let y := 0xabcdef
        let str := "hello world"
        let e := "very long string more than 32 bytes" // error
    }
}
```



### 内部代码块

两个内部代码块之间互不影响，individual code blocks.

```js
function assemblyTest()public{
    assembly{
        {
            let x := 999
        }
        {
            let x := 888
            x := y // error
        }
    }
}
```

内部代码块可以嵌套，并且与 `assembly{}` 代码块共用命名空间；子块可以访问父块的变量，父块不能访问子块变量。

```js
function assemblyTest()public{
    assembly{
        let x := 0
        {
            let x := 999 // error
            {
                let b := x
                {
                    let c := b
                }
                b := c // error
            }
        }
    }
}
```



### 循环

内联汇编中只有for循环，没有while循环。

solidity代码：

```js
function for_loop_solidity(uint n, uint value)public pure returns(uint){
    for(uint i = 0;i < n; i++){
        value *= 2;
    }
}
```

assembly代码：

```js
function for_loop_assembly(uint n, uint value)public pure returns(uint){
	assembly{
        for{let i := 0} lt(i,n) {i := add(i,1)}{
            value := mul(2,value)
        }
        mstore(0x0,value)
        return(0x0,32)
    }
}
```

同样的是3个元素：

- 初始化 `{let i := 0}`
- 执行条件 `lt(i,n)` 
- 迭代的后续步骤 `{i := add(i,1)}`



### 判断语句

#### if

```js
assembly {
    if slt(x, 0) { x := sub(0, x) }  // Ok
            
    if eq(value, 0) revert(0, 0)    // Error, 需要大括号
}
```

assembly 中的 if 是没有 else 部分的，而且必须要有大括号`{}`

#### switch

`switch` 语句将一个表达式的值与多个**常量**进行对比，同时也支持默认分支 `default` ,当表达式的值不匹配任何其他分支条件是，将执行 `default` 分支代码。

```js
function for_loop_assembly(uint n, uint value)public pure returns(uint){
	assembly{
        let x := 0
        switch calldataload(4)
        case 0 {
        	x := calldataload(0x24)        
        }
        default{
        	x := calldataload(0x44)
    	}
	    sstore(0,div(x,2))
    }
}
```

注意上面的语法不同之处：

1. 分支列表不需要大括号，但是分支的代码块需要大括号；

2. 所有的分支条件值必须：1）具有相同的类型 2）具有不同的值；

3. 如果分支条件已经涵盖所有可能的值，那么不允许再出现 default 条件。

   ```js
   assembly {        
       let x := 34     
       switch lt(x, 30)
       case true {
           // do something
       }
       case false {
           // do something els
       }
       default {
           // 不允许
       }          
   }
   ```

可以用switch实现 if else 功能。



### 函数

#### 函数的定义和返回值

```js
assembly {
    function my_assembly_function(param1, param2) -> my_result {
        // param2 - (4 * param1)
        my_result := sub(param2, mul(4, param1))
    }
    let some_value = my_assembly_function(4, 9)  // 4 - (9 * 4) = 32
}
```

标准的函数定义方式：`function` 关键字，函数名，()，{}

参数不需要指定类型

返回值用 `->` 指定

**注意：** 函数返回时，不要使用 `return`  ，`return()`是EVM内置的操作码，会结束此次交易调用。

#### 退出函数

可以使用特殊语句`leave`退出当前函数。`leave` 关键字只能在函数内使用。



## 常用操作码

### create2

`create2(v, n, p, s)`

用来创建合约的操作码。详细内容请看`./code/UniswapV2Factory.sol & ./code/UniswapV2Library.sol` 









