pragma solidity >=0.5.0;

import '@uniswap/v2-core/contracts/interfaces/IUniswapV2Pair.sol';
import '@nomiclabs/buidler/console.sol';
import "../interfaces/IUniswapV2Factory.sol";
import "./SafeMath.sol";

/**
    这是uniswap代码库中的一个片段，pairFor用来反推 create2操作码生成的合约地址，并且不需要链上操作
 */
library UniswapV2Library {
    using SafeMath for uint;

    // returns sorted token addresses, used to handle return values from pairs sorted in this order
    function sortTokens(address tokenA, address tokenB) internal pure returns (address token0, address token1) {
        require(tokenA != tokenB, 'UniswapV2Library: IDENTICAL_ADDRESSES');
        (token0, token1) = tokenA < tokenB ? (tokenA, tokenB) : (tokenB, tokenA);
        require(token0 != address(0), 'UniswapV2Library: ZERO_ADDRESS');
    }

    // calculates the CREATE2 address for a pair without making any external calls
    function pairFor(address factory, address tokenA, address tokenB) internal pure returns (address pair) {
        (address token0, address token1) = sortTokens(tokenA, tokenB);
        /** 
        ++代表字节打包操作
        keccak256 (0xff ++ fabric_addr ++ hash (user_id) ++ keccak256 (wallet_init_code)) 
            fabric_addr：   工厂方法，create2操作码调用者，在这个例子中对应的就是UniswapV2Factory合约的地址
            hash (user_id)：也就是执行create2的salt，用户自定义的随机数
            wallet_init_code：initcode，在本例中：initcode => type(UniswapV2Pair).creationCode
        */ 
        pair = address(uint(
                keccak256(abi.encodePacked(
                    hex'ff',
                    factory,
                    keccak256(abi.encodePacked(token0, token1)),
                    keccak256(IUniswapV2Factory(factory).getInitCode())
                    // hex'96e8ac4277198ff8b6f785478aa9a39f403cb768dd02cbee326c3e7da348845f' // init code hash 这个是uniswap代码库写死的一个值，在本地测试环境下跑会因为这个值报错！
                )
            )));
    }
}
