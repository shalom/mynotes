pragma solidity =0.5.16;

import './interfaces/IUniswapV2Factory.sol';
import './UniswapV2Pair.sol';

/**
    这是uniswap代码库的生成UniswapV2Pair合约片段，createPair函数被用来生成两个资产（tokenA & tokenB）的 LP token 合约，不熟悉可以去看uniswap笔记。
    uniswap的文档介绍Pair Addresses地址：https://uniswap.org/docs/v2/smart-contract-integration/getting-pair-addresses/
    uniswap的github地址：https://github.com/Uniswap/uniswap-v2-core https://github.com/Uniswap/uniswap-v2-periphery
 */
contract UniswapV2Factory is IUniswapV2Factory {
    // A是token B是WETH
    function createPair(address tokenA, address tokenB) external returns (address pair) {
        require(tokenA != tokenB, 'UniswapV2: IDENTICAL_ADDRESSES');
        // 排序
        (address token0, address token1) = tokenA < tokenB ? (tokenA, tokenB) : (tokenB, tokenA);
        require(token0 != address(0), 'UniswapV2: ZERO_ADDRESS');
        require(getPair[token0][token1] == address(0), 'UniswapV2: PAIR_EXISTS'); // single check is sufficient

         /**
            操作码：create2(v, n, p, s), 用于生成合约，并且return the new address
                v：单位是wei，一般值为0
                n：是initcode，通过solidity内置的特殊函数type(address).creationCode生成，address不能是自身，也不能是自己的子合约，因为这样会引起循环引用
                p：initcode的长度，mload(bytecode)操作码意思是：bytecode的size
                s：是开发者自定义的变量，在合约看来就是个随机数，在下面的案例中salt使用的是两个资产地址的hash
            这个操作码还有一个亮点是：
                不需要任何的链上操作，只需要获得create2的4个参数，就可以计算出操作码create2生成的地址，在 ./UniswapV2Library.sol 中可以看到应用案例。
         */
        bytes memory bytecode = type(UniswapV2Pair).creationCode;
        bytes32 salt = keccak256(abi.encodePacked(token0, token1));
        assembly {
            pair := create2(0, add(bytecode, 32), mload(bytecode), salt)
        }

        IUniswapV2Pair(pair).initialize(token0, token1);
        getPair[token0][token1] = pair;
        getPair[token1][token0] = pair; // populate mapping in the reverse direction
        allPairs.push(pair);
        emit PairCreated(token0, token1, pair, allPairs.length);
    }
}
