### 解决的问题

通过传统的中心化交易所进行资产的交易时，通常我们需要先将资产交给交易所，再由交易所进行撮合，最终促成交易的达成。但由于这样的交易所通常是中心化的交易所，因此必然会存在**对交易所的信任问题**，这就带来了一定的交易风险，还会产生较高的**手续费**。信息的发布和共享适合使用中心化平台做，资产交易不适合。

### 运行逻辑

通过该智能合约，双方约定**转账方**先冻结一笔钱，并**通过哈希锁将发起方的交易代币锁定**，如果**在规定时间内**有人能够提供之前生成支付的加密证明，并且与之前约定的哈希值一致，交易即可完成。通过哈希锁和时间锁的配合，就可以对资产的发送方和接收方形成相互制约，同时保证资产的交换要么发生，要么不发生，最终保证了该笔交易的逻辑原子性（受公链回滚制约）。

#### 哈希锁

所谓哈希锁，即通过哈希值对一次交易加锁，该锁只能由这个哈希值的原值进行解锁。发送方需要在链下将**原值**发送给接收方（这里可以使用非对称加密）。

#### 时间锁

接收方在规定的时间内解开哈希锁，才能够成功提取发送方锁定的金额。**在规定时间内**，只有接收方才可以解锁；超过规定时间后，只有发送方才可以解锁。

### 在合约中的简单实现

下面的合约提供了创建拟定交易，接收者提取款项，超时退款功能

```solidity
pragma solidity ^0.6.0;

contract HashedTimelock{
    struct LockTransaction{
        address payable sender;
        address payable receiver;
        uint amount;                // transaction value
        bytes32 hashlock;           // use sha256 hash function
        uint timelock;              // UNIX timestamp seconds of end point
        bool withdraw;              // Has withdrawn?
        bool refunded;              // Has refunded?
        // bytes32 preimage;           // it's secret, sha256(_preimage) should equal to hashlock
    }

    mapping(bytes32 => LockTransaction) LockTransactions;
    // LockTransaction[bytes32] LockTransactions;

    modifier hasValue(){
        require(msg.value > 0,"transaction value must > 0");
        _;
    }
    modifier futureTimelock(uint t){
        require(t > now,"end point must be in futrue.");
        _;
    }
    modifier tranExists(bytes32 id){
        require(hasTransaction(id),"The trancaction does not exist.");
        _;
    }
    modifier hashlockMatches(bytes32 _tID, bytes32 _originalV) {
        require(
            LockTransactions[_tID].hashlock == sha256(abi.encodePacked(_originalV)),
            "hashlock hash does not match"
        );
        _;
    }
    modifier withdrawable(bytes32 _tID){
        require(msg.sender == LockTransactions[_tID].receiver,"you arn't the receiver");
        require(!LockTransactions[_tID].withdraw,"the transaction has been completed.");
        require(now < LockTransactions[_tID].timelock,"you came late.");
        _;
    }
    modifier refundable(bytes32 _tID){
        require(now >= LockTransactions[_tID].timelock,"timelock not yet passed");
        require(msg.sender == LockTransactions[_tID].sender,"not sender");
        require(!LockTransactions[_tID].withdraw,"already withdrawn");
        require(!LockTransactions[_tID].refunded,"already refunded");
        _;
    }


    /**
    @notice create a new transaction.
    @param _hashlock Original value of the hash
    @param _timelock end point
     */
    function newLockTran(address payable _receiver, bytes32 _hashlock, uint _timelock)
        external payable hasValue futureTimelock(_timelock) returns (bytes32 tId)
    {
        tId = sha256(abi.encodePacked(msg.sender, _receiver, msg.value, _hashlock, _timelock));

        if(hasTransaction(tId))
            revert();

        LockTransactions[tId] = LockTransaction({
            sender  :msg.sender,
            receiver:_receiver,
            amount  :msg.value,
            hashlock:_hashlock,
            timelock:_timelock,
            withdraw:false,
            refunded:false
        });
    }

    /**
    @notice receiver withdraw within the given time.
    @param _preimage original value
    @return TODO
     */
    function withdraw(bytes32 _tId, bytes32 _preimage)
        external tranExists(_tId) hashlockMatches(_tId, _preimage) withdrawable(_tId) returns (bool)
    {
        LockTransaction storage t = LockTransactions[_tId];
        // t.preimage = _preimage;
        t.withdraw = true;
        t.receiver.transfer(t.amount);
        return true;
    }

    /**
    @notice sender refund after timeLock
     */
    function refund(bytes32 _tId)
        external tranExists(_tId) refundable(_tId) returns (bool)
    {
        LockTransaction storage t = LockTransactions[_tId];
        t.refunded = true;
        t.sender.transfer(t.amount);
        return true;
    }
    function hasTransaction(bytes32 tId)internal view returns (bool exists){
        exists = (LockTransactions[tId].sender != address(0));
    }
}
```

### HashTimeLock在HTLC和Interledger中的实现

TODO



参考链接：

https://zhuanlan.zhihu.com/p/112228102

https://blog.csdn.net/sinat_34070003/article/details/79748573

https://blog.csdn.net/jingzi123456789/article/details/104807107

https://www.jianshu.com/p/14ac59720c16