# git笔记

## 工作中会使用到的git流程

- 在代码托管平台创建git账号，如： www.coding.net 
- 本机生成ssh密钥；
- 在用户密钥中找到 .ssh 文件夹，找到以`.pub` 结尾的密钥文件，并复制到密钥认证页面中；
- git clone “ssh链接”；
- 获得项目代码

## Windows用SSH密钥连接GitHub

### 什么是SSH？

- 简单说，SSH是一种网络协议，用于计算机之间的加密登录。
- 如果一个用户从本地计算机，使用SSH协议登录另一台远程计算机，我们就可以认为，这种登录是安全的，即使被中途截获，密码也不会泄露。
- 最早的时候，互联网通信都是明文通信，一旦被截获，内容就暴露无疑。1995年，芬兰学者Tatu Ylonen设计了SSH协议，将登录信息全部加密，成为互联网安全的一个基本解决方案，迅速在全世界获得推广，目前已经成为Linux系统的标准配置。
- 需要指出的是，SSH只是一种协议，存在多种实现，既有商业实现，也有开源实现。本文针对的实现是[OpenSSH](http://www.openssh.com/)，它是自由软件，应用非常广泛。



### 操作步骤

#### 1. 本机生成SSH密钥对

- 打开 git bash ，输入命令`cd ~/.ssh`转到保存ssh密钥的文件夹中；

- 若提示文件夹不存在，或者文件中只有一个 known_hosts 文件，表明不存在SSH密钥，若存在则不需要生成；

- 输入命令：

  ```shell
  $ cd ~   		# 保证当前路径在 `~` 下，即 `C:/Users/用户名` 目录
  $ ssh-keygen -t rsa -C "your_email@example.com"   # 这将根据你提供的邮箱地址，创建一对密钥，该邮箱必须和远程代码托管平台一致
  ```

  就会发现在 ~/.ssh 文件夹下会有2个文件：id_rsa 和 id_rsa.pub
  
  这里会让你设置密码，设置好之后，每次操作都可能需要输入密码，输入如下命令设置全局用户和邮箱：
  
  ```shell
  git config --global user.name ""	# 设置用户名，和远程代码托管平台一致
  git config --global user.email ""	# 设置邮箱，和远程代码托管平台一致
  ```



#### 2. 将本机生成的SSH密钥添加到GitHub中

登陆Github网站，选择 `Settings` –> `SSH and GPG keys` 菜单，点击 `New SSH key` 按钮。
粘贴你的密钥到 `Key` 输入框中并设置 `Title` 信息，点击 `Add SSH key` 按钮完成。



#### 3. 连接测试

先保证本地 `Git` 已设置好git账户的 `用户名` 和 `邮箱` 信息：

```shell
$ git config --global user.name "your_username"   # 设置用户名
$ git config --global user.email "your_email@example.com"  # 设置邮箱地址
```

测试SSH keys 是否设置成功，执行如下命令：

```shell
$ ssh -T git@github.com
```



## 远程操作

### 本地仓库与远程仓库关联

```shell
$ git remote add origin <远程仓库连接>
```

### 本地删除远程分支

```shell
$ git push origin --delete <branchName>
```





## 解决的错误

1. 错误信息：`fatal: refusing to merge unrelated histories`
   - 解决方案：这个错误通常在 pull 远程分之后， merge 两个不想关的分支时出现；
   - 在命令后边加上 `--allow-unrelated-histories`；
2. 错误信息：`cannot lock ref 'HEAD': unable to resolve reference 'refs/heads/generateblock': reference broken`  