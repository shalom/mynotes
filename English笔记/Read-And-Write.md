# Slots

**32 Slots = 1 Epoch** .A time period of **12 seconds** in which a randomly chosen validator has time to propose a block. Each slot may or may not have a block in it. The total number of validators is split up in committees and one or more individual committees are responsible to attest to each slot. One validator from the committee will be chosen to be the aggregator, while the other 127 validators are attesting. After each Epoch, the validators are mixed and merged to new committees.  There is a minimum of 128 validators per committee.

![73458538-bd09eb80-4375-11ea-83a1-27b5fb1394a1 ](images/73458538-bd09eb80-4375-11ea-83a1-27b5fb1394a1 .png)





- slots  n. 插槽 
- epoch  n. 时代
- period  n. 周期，时期
  - a time period 一个时间段
- a randomly chosen validator  一个随机选出来的验证者（用动词做形容词）
- individual 个人的，独特的 
- aggregator  整合者
- attest 证明



expression:

- is split up 被分成
- are responsible to  负责做……



# Epoch

Represents the number of 32 slots and takes approximately 6.4 minutes. 
Epochs play an important role when it comes to the validator queue and finality. 



- approximately  大约
- queue 队列



# Finalization

In Ethereum 2.0 **at least two third of the validators have to be honest**, therefore if there are two competing Epochs and one third of the validators decide to be malicious, they will receive a penalty. Honest ones will be rewarded.

In order to determine if an Epoch has been finalized, validators have to agree on the latest two epochs in a row (= “justified”) then all previous Epochs can be considered as finalized.

![img](images/73467349-81761e00-4383-11ea-8733-af69fa72ebf6.png)

- two third of	三分之二
- malicious   恶意的
- penalty  罚款，罚金
- determine  v 决定
- finalized  adj  落定，敲定
- in a row  排成一排，连续
- previous 先前的



# Deposit contract

The Deposit contract is the **gateway** to Ethereum 2.0 **through a smart contract** on Ethereum 1.0.  The smart contract accepts any transaction with a minimum amount of 1 ETH and a valid input data. Ethereum 2.0 beacon-nodes listen to the deposit contract and use the input data to credit each validator.

- credit  授予信用 	eg： credit card 信用卡



# Input Data

The Input data, also called the **deposit data**, is a user generated, 842 long sequence of characters.  It represents the [validator public key and the withdrawal public key](https://kb.beaconcha.in/ethereum-2-keys), which were signed with by the validator private key. The input data needs to be added to the transaction to the [deposit contract](https://kb.beaconcha.in/glossary#deposit-contract) in order to get identified by the [beacon-chain](https://kb.beaconcha.in/glossary#beacon-chain).

- sequence  序列  



# Validator

Validators need to deposit 32 ETH into the validator deposit contract on the Ethereum 1.0 chain. Validator operators have to run a validator node. Its job is to propose blocks and sign attestations. A validator has to be online for at least 50% of the time in order to have positive returns.  

## Eligible for activation & Estimated activation

Refers to pending validators. The deposit has been recognized by the ETH2 chain at the timestamp of “Eligible for activation”. If there is a queue of [pending validators](https://www.beaconcha.in/validators), an estimated timestamp for activation is calculated. 

- Eligible  合格者
- Estimated  预估的
- recognized 意识到的
- activation  激活



You are not familiar with your phone