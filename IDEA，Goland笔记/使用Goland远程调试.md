# 使用Goland远程调试

## 远程服务器环境

安装go语言运行环境，安装go语言debug工具 delve

### 安装delve

在非 go module 目录下执行

```shell
go get github.com/go-delve/delve/cmd/dlv
```

这将会把远程代码copy到GOPATH中，执行如下命令进入到对应的目录

```shell
cd $GOPATH/src/github.com/go-delve/delve
```

编译安装

```shell
make install
```

安装好后，可执行文件应该在 $GOPATH/bin 中，这里配置一下 PATH，配置好后执行 dlv 验证是否安装成功



## 在goland中设置和远程服务器同步

### 安装插件

安装插件 RemoteSynchronizer

![image-20201019101650853](assets/image-20201019101650853.png)



### 配置连接远程服务器

重启goland后，进行如下设置：

![image-20201019103021424](assets/image-20201019103021424.png)

接下来配置就和 xshell 一样

![image-20201019103707797](assets/image-20201019103707797.png)

最后点击 Test Connection 测试是否连接成功，成功后，设置映射目录：

![image-20201019103917507](assets/image-20201019103917507-1603075163378.png)

到现在，设置完毕，将本地项目文件同步到远端。选中项目根目录，不然上传的不是整个项目。

![image-20201019104144412](assets/image-20201019104144412.png)



设置自动实时同步

![img](assets/20170907112031659)



## 配置调试选项

添加调试配置

![image-20201019104432587](assets/image-20201019104432587.png)



选择一个远程服务器开放给外网的端口

![image-20201019131305514](assets/image-20201019131305514.png)

最下面那一行将是在远端服务器执行的命令

```shell
dlv debug --headless --listen=:9105 --api-version=2 --accept-multiclient
```

这个命令应该在main.go所在的目录下执行，然后就会看到类似下边的结果：

![image-20201019132209235](assets/image-20201019132209235.png)

配置完毕，就可以执行debug了，远程服务器会输出日志











