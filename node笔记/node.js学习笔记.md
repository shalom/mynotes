# node.js学习笔记

#### web基础知识：

> - 端口号用来指定程序，所有需要联网的程序都必须具有端口号
> - IP地址用来定位计算机
> - 浏览器在中文操作系统中默认的文本格式是gby格式，服务器默认是utf-8,



## exports

exports不可以被重写，exports本质上是 module.exports 的封装



## node_modules重用模块

**导入module时，node会遵照如下流程搜寻这个模块：**

![1562552187516](assets/1562552187516.png)

详细说明：[在nodejs中引进模块要经历的步骤](https://520mwx.com/view/6982)



**如果导入的module是一个目录，会在此目录下执行如下操作：**

![1562552342732](assets/1562552342732.png)

`package.json`指定定义模块的文件的例码：

```json
{
    "main":"./test.js"
}
```

