# IPFS笔记

## 文件名中不能包含的一些符号

```go
func replace(title *string) {
   t := *title
   // 文件名中不允许包含 \/:*"<>?|
   t = strings.Replace(t, `\`, `＼`, -1)
   t = strings.Replace(t, `\`, `／`, -1)
   t = strings.Replace(t, `:`, `：`, -1)
   t = strings.Replace(t, `*`, `＊`, -1)
   t = strings.Replace(t, `"`, `＂`, -1)
   t = strings.Replace(t, `<`, `＜`, -1)
   t = strings.Replace(t, `>`, `＞`, -1)
   t = strings.Replace(t, `?`, `？`, -1)
   t = strings.Replace(t, `|`, `｜`, -1)
   title = &t
}
```



## 注释模板

### 1. 方法注释模板

```go
/**
  * @methods: $methods_name$
  * @parameter:$Parameter$
  * @return: $Return$
  * @Description: TODO
  * @author: shalom
  * @date: $date$ $time$
  * @version: V1.0
  */
```

### 3. 包注释

```go
/**
  * @Description: TODO
  * @author: jxc
  * @date: ${DATE} ${TIME}
  * @version: V1.0
  * @Software: ${PRODUCT_NAME}
  */
```

### 4. 通道接收注释

```go
/**
  * @Use: TODO
  * @Description: TODO
  * @author: jxc
  * @date: $date$ $time$
  * @version: V1.0
  *
  */
```

### 5. 通道赋值注释

```go
/**
  * @Assignment: TODO
  * @Description: TODO
  * @author: jxc
  * @date: $date$ $time$
  * @version: V1.0
  *
  */
```

### 6. 通道创建注释

```go
/**
  * @channel:  TODO
  * @Description: TODO
  * @author: jxc
  * @date: $date$ $time$
  * @version: V1.0
  *
  */
```

![1546500325200](assets/1546500325200.png)



## IPFS应用初体验

### 环境配置

1. 下载IPFS节点软件

   > 链接: <https://pan.baidu.com/s/1SVe0zEY_x4cduDOAdO2MoQ> 提取码: 7dtz

2. 解压，在文件夹中启动cmd窗口，运行安装脚本文件 install.sh

   > - **注意：**运行该文件前，需要翻墙
   > - 在windows系统中，只能使用cmd窗口打开，powershell和git都不可以

3. 运行完几乎没有提示，在当前命令行窗口执行 `ipfs version`命令

   > 看看是否显示版本信息，若正常显示，则安装成功！

4. 配置环境变量

   > 将解压的目录路径添加入环境变量，则可以在任何路径中识别 ipfs 命令

### ipfs命令使用

#### 初始化命令

​	` ipfs init`

​	会生成节点id，效果如图：![1546505166071](assets/1546505166071.png)

**图解释：**

1. 第一行：生成节点文件在路径 C:\Users\ad\.ipfs 中；
2. 第三行：节点身份 hash 
3. 最后一行：执行展示欢迎页面的命令
   效果如下：
   ![1546505879461](assets/1546505879461.png)



#### 将节点接入网络，以守护进程的方式启动ipfs节点命令

​	`ipfs daemon`

​	相当于启动了一个ipfs服务进程，可以使用命令`ipfs swarm peers`查看连接的所有节点，只有当启动监听后，节点才能够接受ipfs网络中的内容检索请求，参与内容的交换与分布。

​	效果如图：
​	![1546506055556](assets/1546506055556.png)

#### 上传下载文件命令

`ipfs add "文件路径"`	可以是绝对路径，也可以是相对路径

`ipfs get "文件Hash"`下载到了当前目录下，文件名为该文件hash值

`ipfs cat "文件Hash"`查看文件内容



## IPFS原理

### IPFS整合的技术

1. BitTorrent
   - 迅雷就是采用的这种技术
   - **BitTorrent协议**（简称**BT**，俗称**比特洪流**、**BT下载**）是用在[对等网络](https://zh.wikipedia.org/wiki/%E5%AF%B9%E7%AD%89%E7%BD%91%E7%BB%9C)中[文件分享](https://zh.wikipedia.org/wiki/%E6%96%87%E4%BB%B6%E5%88%86%E4%BA%AB)的[网络协议](https://zh.wikipedia.org/wiki/%E7%BD%91%E7%BB%9C%E5%8D%8F%E8%AE%AE)[程序](https://zh.wikipedia.org/wiki/%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%A8%8B%E5%BA%8F)。和[点对点](https://zh.wikipedia.org/wiki/%E9%BB%9E%E5%B0%8D%E9%BB%9E)（point-to-point）的协议[程序](https://zh.wikipedia.org/wiki/%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%A8%8B%E5%BA%8F)不同，它是用户群对用户群（peer-to-peer），而且用户越多，下载同一文件的人越多，下载该档案的速度越快。且下载后，继续维持上传的状态，就可以“分享”，成为其用户端节点下载的[种子文件](https://zh.wikipedia.org/wiki/%E7%A7%8D%E5%AD%90%E6%96%87%E4%BB%B6)（.torrent），同时上传及下载。
2. DHT
   - **DHT** 可以是下列意思：
     - [分布式散列表](https://zh.wikipedia.org/wiki/%E5%88%86%E6%95%A3%E5%BC%8F%E9%9B%9C%E6%B9%8A%E8%A1%A8)
     - [离散哈特利转换](https://zh.wikipedia.org/wiki/%E9%9B%A2%E6%95%A3%E5%93%88%E7%89%B9%E5%88%A9%E8%BD%89%E6%8F%9B)
3. Git
4. SFS



### 三大模块

1. 上传
2. 获取
3. 节点管理



### 梅克尔 Merkle Tree & Merkle DAG

#### Hash

##### Hash运算的特性

- 抗碰撞性

  > 两个不一样的DATA几乎不能计算出一样的Hash值，有一定的几率，但是非常小，可以忽略不计。

- 不可逆性

  > 不能够通过Hash值反推出DATA内容。

##### Hash的作用

- 验证数据的完整性

  > 由于Hash运算的两大特性，可以用于验证数据的完整性。

##### Hash的不足

- 由于数据源不稳定，若是由于网络原因，无法下载完整的数据，则达不到验证数据的效果；
- 或者多次重复下载，才能够下载完整的数据，造成效率低下；



#### Hash List 

##### Hash List解决的需求

- 由于Hash的不足，就诞生了 Hash List 的方法，解决由于网络问题造成的 Hash 不能够使用的情况；
- 点对点网络中，下载的时候，会从若干个机器中传输数据，这时候需要 Hash List。

##### Hash List的原理

- 将DATA分割成若干个小数据块，每个数据块可以很小，比如2K，这些小数据块会分散在若干个不同的机器中；
- 分别计算每个数据块的hash值，用于验证小数据块是否完整，若不完整，只需要下载2K大小的数据就够了，可以有效地解决网络不稳定的问题；
- 将所有小数据块的hash拼接，然后再对这个长字符串做一次Hash运算，得到的的hash值就是这个Hash List的根hash，用于验证整个数据的完整性。



#### Merkle Tree

##### Merkle Tree 的原理

- Hash List可以被认为是一种特殊的 Merkle Tree，树高为2的多叉 Merkle Tree；
- 和 Hash List 不一样的是：不通过小数据块的hash直接计算根hash，而是相邻两两计算，层层递进，最终计算出一个根hash，就像一个倒挂的二叉树；

##### Merkle Tree的特点

- 在用于验证数据完整性的时候，若是数据量非常大，Merkle Tree可以做到单独验证其中的一个分支，而Hash List则必须下载完整个Hash List才能够验证；

##### Merkle Tree的应用

- 数字签名
- P2P网络
- 比特币
  简化支付验证，使得轻节点只需要下载区块头80字节的数据，就可以验证交易的合法性。



#### Merkle DAG 梅克尔有向无环图

##### Merkle DAG的基本原理

- 和Merkle Tree相似，是Git、IPFS等的核心技术；
- 在IPFS中，每个block的大小限制在256K内，当你上传一个数据时，会被分成许多的小块，每个小块被称为IPFSObject；
- IPFSObject中，有两个部分，一个是Link，另一个是DATA
  - Link是DATA的引用，或者称为索引，包含三个字段：Link的名字，数据的Hash和Size；
  - DATA就是数据本身
  - 代码：**TODO：**为何是link数组？
    ![1546603788077](assets/1546603788077-1547640604488.png)

##### Merkle DAG在IPFS中的应用

- 上传文件时，文件被分割后，小块的hash与已经存储的hash一样，则不会重复存储；
- 内容寻址；**TODO：**详细的内容还需补全
- 验证数据是否完整；



### IPFS中的KAD网络







## IPFS源码解读

### IPFS的包概览

### mfs包

包mfs实现了可变IPFS文件系统的内存模型。

它包括四个主要结构:

1)文件系统

​	文件系统充当各种mfs文件系统的容器和入口点

2) Root 

​	Root表示整个mfs系统中挂载的单个文件系统

3)目录

4)文件



### 源码中的对象

- Request

  - 代表来自客户的对命令的所有调用

  - 源码：

    ```go
    type Request struct {
    	Context       context.Context
    	Root, Command *Command
    	Path      []string
    	Arguments []string
    	Options   cmdkit.OptMap
    	Files files.File
    	bodyArgs *arguments
    }
    ```


- node


  - ipfs.Node


    - IpfsNode是IPFS的核心模块，它表示一个IPFS实例。

  - ipld.Node

    - 节点是所有IPLD节点必须实现的基本接口。

  - UnixfsNode

    - 用来帮助生成DAG树的节点

    - 源码：

      ```go
      type UnixfsNode struct {
      	raw     bool
      	rawnode *dag.RawNode
      	node    *dag.ProtoNode
      	ufmt    *ft.FSNode
      	posInfo *pi.PosInfo
      }
      ```

  - FSNode

    - FSNode可以是 DAG树 中的任何对象（root，directory，file）

    - FSNode是个接口，可以被以上任意对象实现；源码：

      ```go
      type FSNode interface {
      	GetNode() (ipld.Node, error)
      	Flush() error	//	在io层面 关闭子节点
      	Type() NodeType
      }
      ```

  - RawNode

  - ProtoNode

- adder

- DagBuilderHelper

  - 将对生成DAG树有帮助的对象，组合到一起

- DAG Tree中的三个对象

  - root
    - mfs.Root
      - 表示文件系统树的根。
      - 构造函数：`adder.mfsRoot()`里边封装了`mfs.NewRoot()`函数
      - `Root.GetDirectory() ` 可以通过该方法获得根目录
  - directory
    - 目录对象，叶节点以上都是目录对象
  - file
    - 叶子结点就是file对象

- DAG Service

- Repo

  - 表示给定ipfs节点的所有持久数据。

- ……



### IPFS源码解读 - main函数入口解读

> 路径：`cmd/ipfs/main.go` 

#### main.go主要脉络

- main.go 主要执行的函数是 `mainRet()`

  ```go
  func main() {
  	os.Exit(mainRet(""))
  }
  func mainRet(repopath string) int {
      ...
  }
  ```

- `mainRet()`中主要执行的是 `cli.Run()` 函数，主要参数是：`buildEnv` 和 `makeExecutor`

  ```go
  err = cli.Run(ctx, Root, os.Args, os.Stdin, os.Stdout, os.Stderr, buildEnv, makeExecutor)
  ```

  - `buildEnv` 是一个生成 `&oldcmds.Context` 的函数

  - `makeExecutor ` 是生成命令执行器的函数

  - 执行的流程请看例码：

    ```go
    //	这是buildEnv的函数类型，&oldcmds.Context能够实现 接口cmds.Environment
    func(ctx context.Context, req *cmds.Request) (cmds.Environment, error)
    
    func Run(ctx context.Context, root *cmds.Command,
    	cmdline []string, stdin, stdout, stderr *os.File,
    	buildEnv cmds.MakeEnvironment, makeExecutor cmds.MakeExecutor) error {
        ...
        //	这里调用buildEnv函数
    	env, err := buildEnv(req.Context, req)
        ...
        //	在使用buildEnv生成的cmds.Environment去生成命令执行器：exctr
        exctr, err := makeExecutor(req, env)
        ...
        //	这里启动go程，执行主逻辑
        go func() {
    		// 启daemon时files相关命令的定义在gx/ipfs/QmNueRy.../go-ipfs-cmds/executor.go
    		// 无daemon时files相关命令的定义在gx/ipfs/QmNueRy.../go-ipfs-cmds/client.go
    		//	实现 Execute 接口中的方法在两个文件中 executor.go 和 client.go
    		err := exctr.Execute(req, re, env)
    		if err != nil {
    			errCh <- err
    		}
    	}()
        ...
    }
    ```

  - select 监听 错误 和 状态码

    ```go
    select {
    // 监控传出的错误
    case err := <-errCh:
      ...
    // 监控传出的 状态码
    case code := <-exitCh:
      ...
    }
    ```

- `exctr.Execute()`函数

  - 该函数是一个在接口中的函数，接口路径：`vendor/gx/ipfs/QmNueRy.../go-ipfs-cmds/executor.go`

    ```go
    type Executor interface {
    	Execute(req *Request, re ResponseEmitter, env Environment) error
    }
    ```

  - 实现该接口的对象有两个，分别在两个文件中：
    `vendor/gx/ipfs/QmNueRy.../go-ipfs-cmds/executor.go`
    `vender/gx/ipfs/QmNueRy.../go-ipfs-cmds/client.go`

  - 目前还没有检测到 `client.go` 中的 `Execute()` 函数有被使用，后边检测到再补上 TODO：

  - 在 `executor.go` 中会执行命令中的三个函数：`PreRun`  `PostRun`  `Run` 



#### 临时写入部分，后边整理

##### 当执行daemon命令的时候

- req 是通过 `req, errParse := Parse(ctx, cmdline[1:], stdin, root)` 生成的
  - 传入的参数 ctx 是由 `mainRet()` 中的 `intrh, ctx := setupInterruptHandler(ctx)` 生成的
- 通过req 可直接生成 env  `env, err := buildEnv(req.Context, req)`
- env 可生成获得节点









### IPFS源码解读 - 新建command

#### 触发命令入口

##### 根命令触发入口

> 根命令是命令中的第一个参数位置的命令；
>
> 如：`ipfs add <path> ` add就是一个根命令

- 在文件 core/commonds/root.go 中，`rootSubcommands`字典中，添加命令和对应的`*cmds.Commond`

- **注意：**这里是一个地址类型

- 例码：

  ```go
  var rootSubcommands = map[string]*cmds.Command{
  	"add":       AddCmd,
  	"bitswap":   BitswapCmd,
      ...
  }
  ```


##### 子命令触发入口

> 子命令是命令中的第二个参数位置的命令；
>
> 如：`ipfs config replace <file> ` replace就是一个config下的子命令，config是根命令

- 以config命令的子命令为例，在编写config对应的`*cmds.Commond`时，`rootSubcommands`字典中，添加命令和对应的`*cmds.Commond`

- **注意：**这里是一个地址类型

- 例码：

  ```go
  var ConfigCmd = &cmds.Command{
      ...
      Subcommands: map[string]*cmds.Command{
  		"show":      configShowCmd,
  		"edit":      configEditCmd,
          ...
      }
  }
  ```


### 编写命令对象 &cmds.Command

#### Arguments字段

##### 格式，作用与效果

- 规范命令输入格式

- 分为 FileArg 和 StringArg

- 以add命令和config命令为例，例码：

  ```go
  //	add命令代码：
  Arguments: []cmdkit.Argument{
      cmdkit.FileArg("path", true, true, "The path to a file to be added to ipfs.").EnableRecursive().EnableStdin(),
  },
  //	config命令代码：
  Arguments: []cmdkit.Argument{
      cmdkit.StringArg("key", true, false, "The key of the config entry (e.g. \"Addresses.API\")."),
      cmdkit.StringArg("value", false, false, "The value to set the config entry to."),
  },
  ```

  - add命令需要添加文件，所以类型为 FileArg；config命令不需要用户上传及文件路径，所以是StringArg
  - 参数一：参数名
  - 参数二：是否必须添加
  - 参数三：是否有可选参数
  - 参数四：参数的解释

- 效果

  - add命令

    `ipfs add ... <path>`，必须传入path，会有可选参，不强制

  - config命令
    `ipfs config <key> [<value>]`，必须有key，value可以为空，没有可选参数



##### Options 可选参数

###### 格式，作用与效果

- 编辑可选参数，为命令添加特殊功能
- 有 bool，string 和 int类型的，string类型需要传入一段字符串，int 同理
- 例码：

```go
//	参数一：参数全名  参数二：参数缩写  参数三：参数解释
cmdkit.BoolOption(quietOptionName, "q", "Write minimal output."),
cmdkit.StringOption(chunkerOptionName, "s", "Chunking algorithm, size-[bytes] or rabin-[min]-[avg]-[max]").WithDefault("size-262144"),
```

- 效果：
  `ipfs add -q -s "分块算法名" <path>`，-q 安静模式；-s 指定分块算法名



#### Run 函数编写

- Run函数是命令的主要逻辑实现

- 例码：

  ```go
  func(req cmds.Request, res cmds.Response) {
      ...
  }
  ```

- Request的内容

  > 请求命令，在 core/commands/ 文件夹中的 文件中，Request都是当前文件夹的 request.go 文件中的 Request 接口，例码：

  ```go
  type Request interface {
  	Path() []string
  	Option(name string) *cmdkit.OptionValue
      Options() cmdkit.OptMap	//	返回存储可选参的 map[string]interface{} 
  	Arguments() []string
  	StringArguments() []string
  	Files() files.File	//	用户定义的 fileArg 路径	
  	Context() context.Context
  	InvocContext() *Context
  	Command() *Command
  }
  ```

- Reponse的内容

  > 作用一：用于传出显示在命令行的反馈信息





### IPFS源码 - INIT模块解读

#### init命令的使用

```shell
ipfs init [--bits=<bits> | -b] [--empty-repo | -e] [--profile | -p] [--] [<default-config>]

-b, --bits       int  - 生成的RSA私钥位数，默认值：2048
-e, --empty-repo bool - 是否不在本地存储中添加、固定帮助文件。默认值：false
-p, --profile 	 string - 用户指定配置文件，默认为空
[<default-config>] - 使用该指定配置进行初始化
```

##### init命令的主要任务

> `ipfs init `命令负责 **初始化ipfs配置文件并生成新的密钥对**。

#### init命令的文件路径

`cmd/ipfs/init.go`

#### init.go 的主脉络

##### 代码模块

- `preRun` 函数

  - 验证当前节点是否在线，也就是就是是否已经执行了 ipfs daemon 命令
  - 如果已经在线，将无法执行 `ipfs init` 命令

- Run 函数

  - 通过Request获得 init 命令中用户指定的文件 **TODO:**用户指定的是文件，如何执行NextFile()函数？

    ```go
    f := req.Files
    ```

  - 再通过 目录文件f 获得下边的配置文件

    ```go
    confFile, err := f.NextFile()
    ```

  - File 接口对象

    > NextFile() 方法可以直接通过目录文件获得配置文件

  - 若用户没有指定文件存放路径，则系统会使用默认路径：~/.ipfs

    - 例码：

      ```go
      //	获取用户指定文件
      f := req.Files
      //	用户没有指定，所以 f==nil
      if f != nil {
      	...
      }
      //	获得用户指定的配置文件，多个文件路径用·,·隔开，用户一般不指定
      profile, _ := req.Options["profile"].(string)
      
      var profiles []string
      //	用户不指定 profile==nil
      if profile != "" {
          profiles = strings.Split(profile, ",")
      }
      //	doInit函数是核心，cctx.ConfigRoot 里面存的就是默认路径 ~/.ipfs
      //	在doInit函数中，执行init命令主要逻辑
      if err := doInit(os.Stdout, cctx.ConfigRoot, empty, nBitsForKeypair, profiles, conf); err != nil {
          res.SetError(err, cmdkit.ErrNormal)
          return
      }
      ```

- `doInit()`函数

  > doInit() 函数是 init 命令执行的主要函数
  - `doInit()`例码：

    ```go
    func doInit(out io.Writer, repoRoot string, nBitsForKeypair int, confProfiles []string, conf *config.Config, isSuperNode bool) error {
        ...
        //	检测目录是否具有写权限，目录默认为用户目录下的.ipfs文件夹  ~/.ipfs
        if err := checkWritable(repoRoot); err != nil {
            return err
        }
        //	检测是否已经做过初始化，如果已经做过初始化，~/.ipfs文件夹就会存在，会返回错误
    	//	若想再次初始化，手动删除~/.ipfs文件夹即可
    	if fsrepo.IsInitialized(repoRoot) {
    		return errRepoExists
    	}
        //	如果conf为空，则重新生成conf，如果用户指定了配置文件，则不会为空
    	if conf == nil {
    		var err error
    		//	这里初始化 config
    		//	config.Init主要就是构造 config 里的参数，并写入默认值
    		conf, err = config.Init(out, nBitsForKeypair)
    		if err != nil {
    			return err
    		}
    	}
    	//	设置是否是超级节点
    	conf.SuperNode = isSuperNode
        ...
        //	fsrepo.Init就是将这些配置进行持久化，也就是写入磁盘。
    	//	fsrepo.Init会在~/.ipfs下面创建 config,blocks,version, datastore,keystore,datastore_spce 等文件和文件夹。
    	if err := fsrepo.Init(repoRoot, conf); err != nil {
    		return err
    	}
        //	添加默认资产，就是IPFS默认添加的一些初始化的文件，TODO：本地路径在哪里？
    	// 	在 ipfs init 命令执行后会给个命令，执行该命令后就会执行 addDefaultAssets 函数
    	if err := addDefaultAssets(out, repoRoot); err != nil {
    		return err
    	}
    
    	//	命名ipns，默认值是self，可用命令 ipfs key list 查看
    	return initializeIpnsKeyspace(repoRoot)
    }
    ```






### IPFS源码 - Daemon模块解读

#### Daemon命令的使用

```shell
# 基本命令
ipfs daemon			- 命令用来启动一个连接网络的IPFS节点。
# 可选参数
ipfs daemon [--init] [--routing=<routing>] [--mount] [--writable] 
            [--mount-ipfs=<mount-ipfs>] [--mount-ipns=<mount-ipns>] 
            [--unrestricted-api] [--disable-transport-encryption] 
            [--enable-gc] [--manage-fdlimit=false] [--offline] [--migrate] 
            [--enable-pubsub-experiment] [--enable-mplex-experiment=false]
            
--init                         bool   - 是否使用默认设置自动初始化ipfs，默认值：false
--routing                      string - 路由选项，默认值：dht
--mount                        bool   - 是否将IPFS挂载到文件系统，默认值：false
--writable                     bool   - 是否允许使用`POST/PUT/DELETE`修改对象，默认值： false.
--mount-ipfs                   string - 当使用--mount选项时IPFS的挂接点，默认值采用配置文件中的设置
--mount-ipns                   string - 当使用--mount选项时IPNS的挂接点，默认值采用配置文件中的设置
--unrestricted-api             bool   - 是否允许API访问未列出的哈希，默认值：false
--disable-transport-encryption bool   - 是否进制传输层加密，默认值：false。当调试协议时可开启该选项
--enable-gc                    bool   - 是否启用自动定时仓库垃圾回收，默认值：false
--manage-fdlimit               bool   - 是否按需自动提高文件描述符上限，默认值：false
--offline                      bool   - 是否离线运行，即不连接到网络，仅提供本地API，默认值：false
--migrate                      bool   - true对应于mirage提示时输入yes，false对应于输入no
--enable-pubsub-experiment     bool   - 是否启用发布订阅（pubsub）特性，该特性目前尚处于实验阶段
--enable-mplex-experiment      bool   - 是否启用`go-multiplex`流多路处理器，默认值：true
```

#### Daemon命令的路径

`cmd/ipfs/daemon.go`



#### Daemon.go 的主脉络










### IPFS源码 - ADD模块解读

#### ADD命令的使用

```shell
# 基本命令
ipfs add <path>...
# 可选参数（Option）
ipfs add [--recursive | -r][--quiet | -q] [--quieter | -Q][--silent] [--progress | -p][--trickle | -t] [--only-hash | -n][--wrap-with-directory | -w] [--hidden | -H][--chunker= | -s] [--pin=false][--raw-leaves] [--nocopy][--fscache] [--] <path>...

-r,         --recursive           bool   - 递归添加目录内容 Default: false.
-q,         --quiet               bool   - 安静模式，执行过程中输出显示尽可能少的信息
-Q,         --quieter             bool   - 更安静模式，仅输出最终的结果哈希值
--silent                          bool   - 静默模式，不输出任何信息.
-p,         --progress            bool   - 流式输出过程数据，为true会显示**进度条**.
-t,         --trickle             bool   - 使用trickle-dag格式进行有向图生成.
-n,         --only-hash           bool   - 只计算hash，不写入内容到ipfs
-w,         --wrap-with-directory bool   - 使用目录对象包装文件，当同时上传多个文件时使用
-H,         --hidden              bool   - 包含隐藏文件，仅在进行递归添加时有效
-s,         --chunker             string - 使用的分块算法.
--pin                             bool   - 添加时固定对象，默认值：true
--raw-leaves                      bool   - 叶节点使用裸块. (experimental 试验的).
--nocopy                          bool   - 使用filestore添加文件. (experimental).
--fscache                         bool   - 为已有块检查filestore. (experimental).
```

#### 实现ADD命令的源码  

> **深度优先阅读源码的方法：**
>
> - 源码很长，很冗杂，阅读时，需要先将主线捋清楚，找到关键函数，将整个主线脉络疏通；
> - 然后再根据需求完善细节部分。

##### add命令的主脉络

- add.go 文件主要就是给 add 命令对象赋值，折叠下来就这一行

  ```go
  //	64行
  var AddCmd = &cmds.Command{...}
  ```

  command对象原样是：

  ```go
  type Command struct {
  	Options   []cmdkit.Option
  	Arguments []cmdkit.Argument
  	PreRun    func(req *Request, env Environment) error
  	Run      Function
  	PostRun  PostRunMap
  	Encoders EncoderMap
  	Helptext cmdkit.HelpText
  	External bool
  	Type        interface{}
  	Subcommands map[string]*Command
  }
  ```

- command对象中，需要我们首先关注的是前五个字段

  ```go
  //	可选参数
  Options   []cmdkit.Option
  //	参数格式
  Arguments []cmdkit.Argument
  //	前运行
  PreRun    func(req *Request, env Environment) error
  //	主要功能逻辑
  Run      Function
  //	后运行
  PostRun  PostRunMap
  ```

- 而真正与主脉络相关的是 `Run` 字段，类型是格式为`func(*Request, ResponseEmitter, Environment)`的函数，函数体就是我们需要分析的主要逻辑

  > 函数体中，也同样需要找到函数体的主脉络，以下就是函数体的主脉络：

  - 获取文件添加对象，或者理解为文件操作指针（fileAdder）

    ```go 
    //	317行
    //	初始化文件操作指针，用于添加新文件
    //	该文件操作指针控制了add命令所有的文件操作开关
    fileAdder, err := coreunix.NewAdder(req.Context, n.Pinning, n.Blockstore, dserv)
    ```

  - 构建 `addAllAndPin` 方法，并在394行的字go程里调用该方法

    ```go
    //	353行
    addAllAndPin := func(f files.File) error {...}
    //	394行
    go func(){
        ...
        err = addAllAndPin(req.Files)
    }()
    ```

  - 在 `addAllAndPin` 函数体中，真正产生添加功能的是 `fileAdder.AddFile(file)` 函数

    ```go
    //	367行
    if err := fileAdder.AddFile(file); err != nil {
        return err
    }
    ```

- `AddFile`方法本质上是调用了 `addFile` 方法，`addFile` 是主功能函数

- 在`addFile`方法中，又调用了`adder.add`方法，这个方法是添加普通文件的方法，其他类型文件也会在addFile方法中被转成普通文件

  ```go
  //	477行
  dagnode, err := adder.add(reader)
  ```

- `adder.add` 方法中，最终调用了两个生成 MerkleDAG Tree 的方法，来完成树的生成，具体选择哪个方法由用户自己选择（trickle.Layout  Or  balanced.Layout)

  ```go
  //	130行
  if adder.Trickle {
      return trickle.Layout(params.New(chnk))
  }
  
  return balanced.Layout(params.New(chnk))
  ```

- Layout()方法的逻辑

  > Layout() 是完成 Merkle DAG算法 的主要函数，主要作用是生成 Merkle DAG Tree ，并将数据写入到叶节点中

  - trickle.Layout的脉络

    > trickle.Layout()函数调用了一个关键函数 fillTrickleRec() ，生成树和写入叶节点的功能都是由该函数实现的，里边用到了 递归。

    trickle.Layout() 中的 fillTrickleRec() 函数：

    ```go
    //	44行
    if err := fillTrickleRec(db, root, -1); err != nil {...}
    ```

  - fillTrickleRec() 函数的脉络

    > 该函数最终实现了两个重要的功能：生成树 和 将数据写入叶节点 ；由于算法比较复杂，后期有需求再仔细研究

    fillTrickleRec() 函数体：

    ```go
    func fillTrickleRec(db *h.DagBuilderHelper, node *h.UnixfsNode, maxDepth int) error {
    	// Always do this, even in the base case
    	//	传入的是 UnixfsNode，FillNodeLayer:将 dataNodes 作为子节点添加到 UnixfsNode 中
    	if err := db.FillNodeLayer(node); err != nil {
    		return err
    	}
    
    	for depth := 1; ; depth++ {
    		// Apply depth limit only if the parameter is set (> 0).
    		if maxDepth > 0 && depth == maxDepth {
    			return nil
    		}
    		for layer := 0; layer < layerRepeat; layer++ {
    			if db.Done() {
    				return nil
    			}
    
    			nextChild := db.NewUnixfsNode()
                //	这里递归调用
    			if err := fillTrickleRec(db, nextChild, depth); err != nil {
    				return err
    			}
    
    			if err := node.AddChild(nextChild, db); err != nil {
    				return err
    			}
    		}
    	}
    }
    ```




##### add命令的分支

###### 写入本地缓存



###### postRun流程







##### core/commands/add.go文件

- 



##### importer/balanced/builder.go文件

> （平衡）





##### importer/trickle/trickledag.go文件

> （细） 





#### add.go的全部代码加注释

​	注释写在了代码中，还有许多地方需要再补充；	

```go
package commands

import (
	"errors"
	"fmt"
	"io"
	"os"
	"strings"

	//	块服务提供的接口
	blockservice "github.com/ipfs/go-ipfs/blockservice"
	//	核心api
	core "github.com/ipfs/go-ipfs/core"
	//	add的一些工具方法和结构
	"github.com/ipfs/go-ipfs/core/coreunix"
	//	文件存储接口
	filestore "github.com/ipfs/go-ipfs/filestore"
	//	DAG服务接口
	dag "github.com/ipfs/go-ipfs/merkledag"
	//	提供一个新的线程安全的DAG
	dagtest "github.com/ipfs/go-ipfs/merkledag/test"
	//	一个可变IPFS文件系统的内存模型
	mfs "github.com/ipfs/go-ipfs/mfs"
	//	一个可变IPFS文件系统的内存模型
	ft "github.com/ipfs/go-ipfs/unixfs"

	//	以下是第三方团队提供的工具包
	cmds "gx/ipfs/QmNueRyPRQiV7PUEpnP4GgGLuK1rKQLaRW7sfPvUetYig1/go-ipfs-cmds"
	mh "gx/ipfs/QmPnFwZ2JXKnXgMw8CdBPxn7FWh6LLdjUjxV1fKHuJnkr8/go-multihash"
	pb "gx/ipfs/QmPtj12fdwuAqj9sBSTNUxBNu8kCGNp8b3o8yUzMm5GHpq/pb"
	offline "gx/ipfs/QmS6mo1dPpHdYsVkm27BRZDLxpKBCiJKUH8fHX15XFfMez/go-ipfs-exchange-offline"
	bstore "gx/ipfs/QmadMhXJLHMFjpRmh85XjpmVDkEtQpNYEZNRpWRvYVLrvb/go-ipfs-blockstore"
	cmdkit "gx/ipfs/QmdE4gMduCKCGAcczM2F5ioYDfdeKuPix138wrES1YSr7f/go-ipfs-cmdkit"
	files "gx/ipfs/QmdE4gMduCKCGAcczM2F5ioYDfdeKuPix138wrES1YSr7f/go-ipfs-cmdkit/files"
)

// ErrDepthLimitExceeded indicates that the max depth has been exceeded.
//	超出ipfs深度限制的错误提示信息
var ErrDepthLimitExceeded = fmt.Errorf("depth limit exceeded")

//	可选项参数(Option)
const (
	quietOptionName       = "quiet"
	quieterOptionName     = "quieter"
	silentOptionName      = "silent"
	progressOptionName    = "progress"
	trickleOptionName     = "trickle"
	wrapOptionName        = "wrap-with-directory"
	hiddenOptionName      = "hidden"
	onlyHashOptionName    = "only-hash"
	chunkerOptionName     = "chunker"
	pinOptionName         = "pin"
	rawLeavesOptionName   = "raw-leaves"
	noCopyOptionName      = "nocopy"
	fstoreCacheOptionName = "fscache"
	cidVersionOptionName  = "cid-version"
	hashOptionName        = "hash"
)

//	阻塞管道，管道最大缓冲容量
const adderOutChanSize = 8

//	声明一个命令对象
var AddCmd = &cmds.Command{
	//	该命令帮助信息，命令提示
	Helptext: cmdkit.HelpText{
		Tagline: "Add a file or directory to ipfs.",
		ShortDescription: `
Adds contents of <path> to ipfs. Use -r to add directories (recursively).
`,
		LongDescription: `
Adds contents of <path> to ipfs. Use -r to add directories.
Note that directories are added recursively, to form the ipfs
MerkleDAG.

The wrap option, '-w', wraps the file (or files, if using the
recursive option) in a directory. This directory contains only
the files which have been added, and means that the file retains
its filename. For example:

  > ipfs add example.jpg
  added QmbFMke1KXqnYyBBWxB74N4c5SBnJMVAiMNRcGu6x1AwQH example.jpg
  > ipfs add example.jpg -w
  added QmbFMke1KXqnYyBBWxB74N4c5SBnJMVAiMNRcGu6x1AwQH example.jpg
  added QmaG4FuMqEBnQNn3C8XJ5bpW8kLs7zq2ZXgHptJHbKDDVx

You can now refer to the added file in a gateway, like so:

  /ipfs/QmaG4FuMqEBnQNn3C8XJ5bpW8kLs7zq2ZXgHptJHbKDDVx/example.jpg

The chunker option, '-s', specifies the chunking strategy that dictates
how to break files into blocks. Blocks with same content can
be deduplicated. The default is a fixed block size of
256 * 1024 bytes, 'size-262144'. Alternatively, you can use the
rabin chunker for content defined chunking by specifying
rabin-[min]-[avg]-[max] (where min/avg/max refer to the resulting
chunk sizes). Using other chunking strategies will produce
different hashes for the same file.

  > ipfs add --chunker=size-2048 ipfs-logo.svg
  added QmafrLBfzRLV4XSH1XcaMMeaXEUhDJjmtDfsYU95TrWG87 ipfs-logo.svg
  > ipfs add --chunker=rabin-512-1024-2048 ipfs-logo.svg
  added Qmf1hDN65tR55Ubh2RN1FPxr69xq3giVBz1KApsresY8Gn ipfs-logo.svg

You can now check what blocks have been created by:

  > ipfs object links QmafrLBfzRLV4XSH1XcaMMeaXEUhDJjmtDfsYU95TrWG87
  QmY6yj1GsermExDXoosVE3aSPxdMNYr6aKuw3nA8LoWPRS 2059
  Qmf7ZQeSxq2fJVJbCmgTrLLVN9tDR9Wy5k75DxQKuz5Gyt 1195
  > ipfs object links Qmf1hDN65tR55Ubh2RN1FPxr69xq3giVBz1KApsresY8Gn
  QmY6yj1GsermExDXoosVE3aSPxdMNYr6aKuw3nA8LoWPRS 2059
  QmerURi9k4XzKCaaPbsK6BL5pMEjF7PGphjDvkkjDtsVf3 868
  QmQB28iwSriSUSMqG2nXDTLtdPHgWb4rebBrU7Q1j4vxPv 338
`,
	},

	//	参数；required：是否一定要加上；variadic:是否有可变参数
	Arguments: []cmdkit.Argument{
		cmdkit.FileArg("path", true, true, "The path to a file to be added to ipfs.").EnableRecursive().EnableStdin(),
	},
	//	可选参数，有多个可选参数，添加了（experimental）的参数还在实验中……
	Options: []cmdkit.Option{
		cmds.OptionRecursivePath, // a builtin option that allows recursive paths (-r, --recursive)
		cmdkit.BoolOption(quietOptionName, "q", "Write minimal output."),
		cmdkit.BoolOption(quieterOptionName, "Q", "Write only final hash."),
		cmdkit.BoolOption(silentOptionName, "Write no output."),
		cmdkit.BoolOption(progressOptionName, "p", "Stream progress data."),
		cmdkit.BoolOption(trickleOptionName, "t", "Use trickle-dag format for dag generation."),
		cmdkit.BoolOption(onlyHashOptionName, "n", "Only chunk and hash - do not write to disk."),
		cmdkit.BoolOption(wrapOptionName, "w", "Wrap files with a directory object."),
		cmdkit.BoolOption(hiddenOptionName, "H", "Include files that are hidden. Only takes effect on recursive add."),
		cmdkit.StringOption(chunkerOptionName, "s", "Chunking algorithm, size-[bytes] or rabin-[min]-[avg]-[max]").WithDefault("size-262144"),
		cmdkit.BoolOption(pinOptionName, "Pin this object when adding.").WithDefault(true),
		cmdkit.BoolOption(rawLeavesOptionName, "Use raw blocks for leaf nodes. (experimental)"),
		cmdkit.BoolOption(noCopyOptionName, "Add the file using filestore. Implies raw-leaves. (experimental)"),
		cmdkit.BoolOption(fstoreCacheOptionName, "Check the filestore for pre-existing blocks. (experimental)"),
		cmdkit.IntOption(cidVersionOptionName, "CID version. Defaults to 0 unless an option that depends on CIDv1 is passed. (experimental)"),
		cmdkit.StringOption(hashOptionName, "Hash function to use. Implies CIDv1 if not sha2-256. (experimental)").WithDefault("sha2-256"),
	},
	//	执行前的参数设定，这个方法决定是否选择显示进度条
	PreRun: func(req *cmds.Request, env cmds.Environment) error {
		quiet, _ := req.Options[quietOptionName].(bool)
		quieter, _ := req.Options[quieterOptionName].(bool)
		quiet = quiet || quieter

		silent, _ := req.Options[silentOptionName].(bool)

		if quiet || silent {
			return nil
		}

		// ipfs cli progress bar defaults to true unless quiet or silent is used
		//	除非用户指定了 quiet 和 silent，否则默认为输出进度条； progress bar：进度条
		_, found := req.Options[progressOptionName].(bool)
		if !found {
			req.Options[progressOptionName] = true
		}

		return nil
	},
	//	执行命令，命令执行的主要逻辑
	Run: func(req *cmds.Request, res cmds.ResponseEmitter, env cmds.Environment) {
		//	获取节点，返回：节点+error
		n, err := GetNode(env)
		if err != nil {
			res.SetError(err, cmdkit.ErrNormal)
			return
		}

		//	Repo 代表给定节点的所有持久数据，Config()返回的是持久数据中的配置信息
		cfg, err := n.Repo.Config()
		if err != nil {
			res.SetError(err, cmdkit.ErrNormal)
			return
		}
		// check if repo will exceed storage limit if added
		// TODO: this doesn't handle the case if the hashed file is already in blocks (deduplicated)
		// TODO: conditional GC is disabled due to it is somehow not possible to pass the size to the daemon
		//if err := corerepo.ConditionalGC(req.Context(), n, uint64(size)); err != nil {
		//	res.SetError(err, cmdkit.ErrNormal)
		//	return
		//}

		progress, _ := req.Options[progressOptionName].(bool)
		trickle, _ := req.Options[trickleOptionName].(bool)
		wrap, _ := req.Options[wrapOptionName].(bool)
		hash, _ := req.Options[onlyHashOptionName].(bool)
		hidden, _ := req.Options[hiddenOptionName].(bool)
		silent, _ := req.Options[silentOptionName].(bool)
		chunker, _ := req.Options[chunkerOptionName].(string)
		dopin, _ := req.Options[pinOptionName].(bool)
		rawblks, rbset := req.Options[rawLeavesOptionName].(bool)
		nocopy, _ := req.Options[noCopyOptionName].(bool)
		fscache, _ := req.Options[fstoreCacheOptionName].(bool)
		cidVer, cidVerSet := req.Options[cidVersionOptionName].(int)
		hashFunStr, _ := req.Options[hashOptionName].(string)

		// The arguments are subject to the following constraints.
		//
		// nocopy -> filestoreEnabled
		// nocopy -> rawblocks
		// (hash != sha2-256) -> cidv1

		// NOTE: 'rawblocks -> cidv1' is missing. Legacy reasons.

		// nocopy -> filestoreEnabled
		//	nocopy 使用filestore添加文件
		//	FilestoreEnabled 是一个bool，必须为true保证不重复存储文件
		//	Package filestore实现了一个块存储，它可以直接从文件系统中的原始位置读取某些数据块
		//	如果 使用filestore添加文件 但是FilestoreEnabled没有开启 错误
		if nocopy && !cfg.Experimental.FilestoreEnabled {
			res.SetError(filestore.ErrFilestoreNotEnabled, cmdkit.ErrClient)
			return
		}

		// nocopy -> rawblocks
		//	如果 使用filestore添加文件 （但是没有叶节点没有使用裸块）必须使用裸块
		//	TODO：为什么要有 this constraint？
		if nocopy && !rawblks {
			// fixed?
			if rbset {
				res.SetError(
					fmt.Errorf("nocopy option requires '--raw-leaves' to be enabled as well"),
					cmdkit.ErrNormal,
				)
				return
			}
			// No, satisfy mandatory constraint.
			rawblks = true
		}

		// (hash != "sha2-256") -> CIDv1
		//	cidVer强制为1，CID版本，CID是内容寻址的ID
		if hashFunStr != "sha2-256" && cidVer == 0 {
			if cidVerSet {
				res.SetError(
					errors.New("CIDv0 only supports sha2-256"),
					cmdkit.ErrClient,
				)
				return
			}
			cidVer = 1
		}

		// cidV1 -> raw blocks (by default)
		//	版本大于0，默认使用裸块
		if cidVer > 0 && !rbset {
			rawblks = true
		}

		//	有向无环图 获取CID信息 （CID 可以生成hash字符串）
		//	PrefixForCidVersion 返回CID版本 Protobuf 前缀信息
		//	prefix包含CID信息：the Version, the Codec, the Multihash type and the Multihash length.
		prefix, err := dag.PrefixForCidVersion(cidVer)
		if err != nil {
			res.SetError(err, cmdkit.ErrNormal)
			return
		}

		//	ToLower转成小写的辅助函数，Names是映射到hashFunStr对应的编码
		hashFunCode, ok := mh.Names[strings.ToLower(hashFunStr)]
		if !ok {
			res.SetError(fmt.Errorf("unrecognized hash function: %s", strings.ToLower(hashFunStr)), cmdkit.ErrNormal)
			return
		}

		//	TODO：Multihash具体指代什么意思，为什么 length 要为-1？
		prefix.MhType = hashFunCode
		prefix.MhLength = -1

		//	hash：只计算hash，不写入内容到ipfs网络中
		//	NewNode() 返回一个空ipfs节点，如果选择了hash，节点赋值为空节点
		if hash {
			nilnode, err := core.NewNode(n.Context(), &core.BuildCfg{
				//TODO: need this to be true or all files
				// hashed will be stored in memory!
				NilRepo: true,
			})
			if err != nil {
				res.SetError(err, cmdkit.ErrNormal)
				return
			}
			n = nilnode
		}

		//	addblockstore继承了 Blockstore 和 GCLocker
		//	Blockstore包装了一个以数据存储块为中心的方法，并提供了一个抽象层，允许添加不同的缓存策略。
		//	GCLocker抽象了在执行垃圾收集操作时锁定块存储区的功能。
		//	一个可以回收的块存储
		addblockstore := n.Blockstore

		//	等价于：fscache==false && nocopy==false
		//	fscache:为已有块检查filestore
		if !(fscache || nocopy) {
			//	使用filestore添加文件
			addblockstore = bstore.NewGCBlockstore(n.BaseBlocks, n.GCLocker)
		}

		//	数据库交换
		exch := n.Exchange
		local, _ := req.Options["local"].(bool)
		if local {
			//	本机执行
			exch = offline.Exchange(addblockstore)
		}
		//	初始化块存储服务，通过块服务构建一个新的块启用块的交换策略
		bserv := blockservice.New(addblockstore, exch) // hash security 001
		//	初始化dag服务，将块交给dag服务管理
		dserv := dag.NewDAGService(bserv)

		//	新建输出通道
		outChan := make(chan interface{}, adderOutChanSize)

		//	初始化文件操作指针，用于添加新文件
		fileAdder, err := coreunix.NewAdder(req.Context, n.Pinning, n.Blockstore, dserv)
		if err != nil {
			res.SetError(err, cmdkit.ErrNormal)
			return
		}

		//	填写文件对象的字段，和Option一样
		fileAdder.Out = outChan
		fileAdder.Chunker = chunker
		fileAdder.Progress = progress
		fileAdder.Hidden = hidden
		fileAdder.Trickle = trickle
		fileAdder.Wrap = wrap
		fileAdder.Pin = dopin
		fileAdder.Silent = silent
		fileAdder.RawLeaves = rawblks
		fileAdder.NoCopy = nocopy
		fileAdder.Prefix = &prefix

		if hash {
			//	DAG service 获取一个新的线程安全的dag
			md := dagtest.Mock()
			//	在ipfs Merkle DAG 中的节点
			emptyDirNode := ft.EmptyDirNode()
			// Use the same prefix for the "empty" MFS root as for the file adder.
			emptyDirNode.Prefix = *fileAdder.Prefix
			mr, err := mfs.NewRoot(req.Context, md, emptyDirNode, nil)
			if err != nil {
				res.SetError(err, cmdkit.ErrNormal)
				return
			}

			fileAdder.SetMfsRoot(mr)
		}

		//	遍历文件列表，构建文件上传io
		addAllAndPin := func(f files.File) error {
			// Iterate over each top-level file and add individually. Otherwise the
			// single files.File f is treated as a directory, affecting hidden file
			// semantics.
			//	每次读取一个文件保存到新的文件对象fileAdder中
			for {
				file, err := f.NextFile()
				if err == io.EOF {
					// Finished the list of files.
					break
				} else if err != nil {
					return err
				}
				if err := fileAdder.AddFile(file); err != nil {
					return err
				}
			}

			// copy intermediary nodes from editor to our actual dagservice
			//	Finalize刷新mfs根目录并返回mfs根节点。
			_, err := fileAdder.Finalize()
			if err != nil {
				return err
			}

			if hash {
				return nil
			}

			//	PinRoot：递归地固定加法器的根节点，并将pin节点状态写入支持数据存储（后台数据存储）。
			//
			return fileAdder.PinRoot()
		}

		errCh := make(chan error)
		go func() {
			var err error
			defer func() { errCh <- err }()
			defer close(outChan)
			//	传输文件并返回错误信息
			err = addAllAndPin(req.Files)
		}()

		defer res.Close()

		//	TODO：Emit这个方法效果
		err = res.Emit(outChan)
		if err != nil {
			log.Error(err)
			return
		}
		err = <-errCh
		if err != nil {
			res.SetError(err, cmdkit.ErrNormal)
		}
	},
	//	返回执行结果到命令行，执行顺序 prerun postrun run ...TODO：这个执行顺序是否正确？
	PostRun: cmds.PostRunMap{
		cmds.CLI: func(req *cmds.Request, re cmds.ResponseEmitter) cmds.ResponseEmitter {
			//	返回的是 ResponseEmitter 和 Response
			reNext, res := cmds.NewChanResponsePair(req)
			outChan := make(chan interface{})

			//	传递size的信息的通道
			sizeChan := make(chan int64, 1)

			//	如果失败了，进度条将会不知道文件大小
			sizeFile, ok := req.Files.(files.SizeFile)
			if ok {
				// Could be slow.
				go func() {
					size, err := sizeFile.Size()
					if err != nil {
						log.Warningf("error getting files size: %s", err)
						// see comment above
						return
					}

					sizeChan <- size
				}()
			} else {
				// we don't need to error, the progress bar just
				// won't know how big the files are
				log.Warning("cannot determine size of input file")
			}

			progressBar := func(wait chan struct{}) {
				defer close(wait)

				quiet, _ := req.Options[quietOptionName].(bool)
				quieter, _ := req.Options[quieterOptionName].(bool)
				quiet = quiet || quieter

				progress, _ := req.Options[progressOptionName].(bool)

				var bar *pb.ProgressBar
				if progress {
					bar = pb.New64(0).SetUnits(pb.U_BYTES)
					bar.ManualUpdate = true
					bar.ShowTimeLeft = false
					bar.ShowPercent = false
					bar.Output = os.Stderr
					bar.Start()
				}

				lastFile := ""
				lastHash := ""
				var totalProgress, prevFiles, lastBytes int64

			LOOP:
				for {
					select {
					case out, ok := <-outChan:
						if !ok {
							if quieter {
								fmt.Fprintln(os.Stdout, lastHash)
							}

							break LOOP
						}
						output := out.(*coreunix.AddedObject)
						if len(output.Hash) > 0 {
							lastHash = output.Hash
							if quieter {
								continue
							}

							if progress {
								// clear progress bar line before we print "added x" output
								fmt.Fprintf(os.Stderr, "\033[2K\r")
							}
							if quiet {
								fmt.Fprintf(os.Stdout, "%s\n", output.Hash)
							} else {
								fmt.Fprintf(os.Stdout, "added %s %s\n", output.Hash, output.Name)
							}

						} else {
							if !progress {
								continue
							}

							if len(lastFile) == 0 {
								lastFile = output.Name
							}
							if output.Name != lastFile || output.Bytes < lastBytes {
								prevFiles += lastBytes
								lastFile = output.Name
							}
							lastBytes = output.Bytes
							delta := prevFiles + lastBytes - totalProgress
							totalProgress = bar.Add64(delta)
						}

						if progress {
							bar.Update()
						}
					case size := <-sizeChan:
						if progress {
							bar.Total = size
							bar.ShowPercent = true
							bar.ShowBar = true
							bar.ShowTimeLeft = true
						}
					case <-req.Context.Done():
						// don't set or print error here, that happens in the goroutine below
						return
					}
				}
			}

			//	控制文件上传时的制度条显示
			go func() {
				// defer order important! First close outChan, then wait for output to finish, then close re
				defer re.Close()

				if e := res.Error(); e != nil {
					defer close(outChan)
					re.SetError(e.Message, e.Code)
					return
				}

				wait := make(chan struct{})
				go progressBar(wait)

				defer func() { <-wait }()
				defer close(outChan)

				for {
					v, err := res.Next()
					if !cmds.HandleError(err, res, re) {
						break
					}

					select {
					case outChan <- v:
					case <-req.Context.Done():
						re.SetError(req.Context.Err(), cmdkit.ErrNormal)
						return
					}
				}
			}()

			return reNext
		},
	},
	Type: coreunix.AddedObject{},
}

```



### IPFS源码 - GET模块解读

#### GET命令的使用

```shell
# 基本命令
ipfs get <ipfs-path>...
# 可选参数（Option）
ipfs get [--output=<output> | -o] [--archive | -a] [--compress | -C] [--compression-level=<compression-level> | -l] [--] <ipfs-path>

-o,		 --output           	 string - 对象内容输出路径，-o后边接本地存储路径
-a, 	 --archive          	 bool   - 是否输出为TAR包，默认值：false
-C,		 --compress         	 bool   - 是否使用GZIP算法压缩输出文件，默认值：false
-l,		 --compression-level	 int    - 压缩等级，1-9，默认值：-1
```



#### 第一层

- 获得压缩等级

  ```go
  cmplvl, err := getCompressOptions(req)
  ```

- 获得当前环境的节点


  ```go
  node, err := GetNode(env)
  ```

- 解析传入的文件 hash ，生成 cid

  ```go
  cid, _ := cids.Decode(req.Arguments[0])
  ```

- 

- 



##### 随记：

- ipld.Node 是一个interface ，被 ProtoNode 和 RowNode 实现，里头有许多功能...
- ResolveIPNS() 只对前缀 "/ipns/" 的路径进行处理
- r.ResolveLinks(ctx, nd, parts) 从一个节点 返回多个节点





####  实现GET命令的源码





### IPFS源码 - Bitswap模块解读





### 交易模块（新增）

#### 交易模块的数据结构

##### Account账户

```go
type Account struct {
	UserID          string    // 自身UID
	NodeID          peer.ID   // 自身PeerID
	Status          uint8     // 账户状态
	PubKey          []byte    // 自身公钥
	CreateAt        int64     // 时间戳
	Sign            []byte    // 签名
	TransactionList *UTXOBook // 交易记录
}
```

​	

​	每个账户绑定一个账簿，账簿是用来记录该账户的所有交易（包括发送的和接收的交易）



##### UTXOBook账簿

```go
type UTXOBook struct {
	UserID       string       // 用户ID
	Balance      uint64       // 用户余额
	Transactions []*UTXOEntry // 交易记录
}
```

- 每个账簿用切片存储交易记录，每个 UTXOEntry 都是一笔交易

  ```go
  type UTXOEntry struct {
  	EID        string       // 下面数据的哈希
  	Record     *UTXORecord  // UTXO交易记录数据
  	Validators []*Validator // UTXORecord列表
  }
  ```

  - 交易中存在两个数据模块，一个是存储交易数据的：UTXORecord，一个是存储验证人列表的：Validator

    - UTXORecord

      ```go
      type UTXORecord struct {
      	RID             string // 交易记录ID，是下面数据的Hash
      	TransactionID   string // 交易ID
      	Success         bool   // 交易结果
      	SenderID        string // 交易转出方ID
      	ReceiverID      string // 交易转入方ID
      	SenderUTXO      []byte // 交易前转出方UTXO记录ID
      	ReceiverUTXO    []byte // 交易前转入方UTXO记录ID
      	SenderBalance   uint64 // 交易转出方余额
      	ReceiverBalance uint64 // 交易转入方余额
      	Amount          uint64 // 交易金额
      	TransferAt      int64  // 交易时间
      	ExtraData       []byte // 交易额外数据
      	Sign            []byte // 交易签名，由Transaction中获取
      }
      ```

    - Validator

      ```go
      type Validator struct {
          Transaction *UTXORecord	// 存储的是指针
          UserID      string
          Timestamp   int64
          Nonce       []byte
          PoW         []byte
          Sign        []byte
      }
      ```






#### 交易模块中的三个对象

##### WantManager





##### Entry

```go
type Entry struct {
	Cid      *cid.Cid
	Priority int

	// GFS
	Token     *orders.OrderToken
	Timestamp int64
	Event     EventType
	Origin    string
	Data      []byte

	SesTrk map[uint64]struct{}
}
```







##### wantSet

```go
type wantSet struct {
	entries []*bsmsg.Entry	
	targets []peer.ID		//	发送的目标节点，为nil代表向全网广播
	from    uint64			//	sessionID,会话id
}
```





#### 收到信号的处理逻辑

```go
case want_set := <-pm.incoming:
    // is this a broadcast or not? 是否向全网广播
    shouldBroadcast := len(want_set.targets) == 0
    //fmt.Println("pm.incoming want_set.from:", want_set.from)
    // add changes to our wantlist
    for _, entry := range want_set.entries {
        switch entry.Event {
            case wantlist.EventGetBlock:
            // 所有需要使用原生转发机制的走这里
            if entry.Cancel {
                if shouldBroadcast {
                    pm.bcwl.Remove(entry.Cid, entry.Event, want_set.from)
                }
                if pm.wl.Remove(entry.Cid, entry.Event, want_set.from) {
                    pm.wantlistGauge.Dec()
                }
            } else {
                if shouldBroadcast {
                    pm.bcwl.AddEntry(entry.Entry, want_set.from)
                }
                if pm.wl.AddEntry(entry.Entry, want_set.from) {
                    pm.wantlistGauge.Inc()
                }
            }
            default:
            // 所有不需要使用原生转发机制的走这里
            if shouldBroadcast {
                if entry.Cancel {
                    pm.bcwl.Remove(entry.Cid, entry.Event, want_set.from)
                }
            }
        }
    }
    // broadcast those wantlist changes
    if shouldBroadcast {
        //	peers是指全网节点的消息队列
        for _, peer := range pm.peers {
            //	添加到每个节点的消息队列中
            peer.addMessage(want_set.entries, want_set.from)
        }
    } else {
        for _, t := range want_set.targets {
            //	只提取需要发送的节点的消息队列，再添加消息
            peer, ok := pm.peers[t]
            if !ok {
                log.Infof("tried sending wantlist change to non-partner peer: %s", t)
                continue
            }
            peer.addMessage(want_set.entries, want_set.from)
        }
    }
```



#### transfer.go浏览需要记录的东西

- wantmanager.go 里头的 BroadcastTransferInfo() 中，通过 TID 生成的 orderToken 是用来标识每一笔交易，防止多次处理多笔交易
- 接收交易函数 receiveTransactionMessage() 









## 任务

### 需求一：备份config文件中的Identity字段

> - init命令执行时，添加如下功能：
>   - 将config文件中的Identity的字段备份在本地磁盘中，路径不能和config相同
>   - 每次生成备份文件以时间戳区分，在文件名中体现



#### 实现过程

##### 在路径后添加时间戳

> **知识点：**时间处理

```go
timeStamp:=time.Now().Unix()
tm:=time.Unix(timeStamp,0)
path = path + tm.Format(`2006-01-02 03：04：05`)
```



##### 生成目录

> **知识点：**os 包 和 filepath包的使用

```go
//	生成存储备份的目录，Dir()会去除最后一个路径元素
err := os.MkdirAll(filepath.Dir(path), 0770)
if err != nil {
    return err
}
```



##### 获得写入文件的 io.writer

> **知识点：**filepath，ioutil 包 和 os 包的使用

```go
//	生成目标文件的 io.writer，传入的路径中，包含到文件名
f, err := atomicfile.New(path, 0660)
if err != nil {
    return err
}
defer f.Close()
```

`atomicfile.New()`方法是封装好的方法，展开代码如下：

```go
func New(path string, mode os.FileMode) (*File, error) {
	//	路径：C:/Users/ad/.ipfs/backup  第一个参数：C:/Users/ad/.ipfs  第二个参数：backup
	//	在 C:/Users/ad/.ipfs 目录下创建一个以 backup 为前缀的临时文件指针
	f, err := ioutil.TempFile(filepath.Dir(path), filepath.Base(path))
	if err != nil {
		fmt.Println("TempFile err")
		return nil, err
	}
	if err := os.Chmod(f.Name(), mode); err != nil {
		f.Close()
		os.Remove(f.Name())
		return nil, err
	}
	return &File{File: f, path: path}, nil
}
```



##### 获得写入信息并写入目标文件

>  **知识点：**go语言对 json 的处理，结构体和 json 之间的操作转换

```go
type backupCfg struct {
	Identity  config.Identity
}
...

//	生成需要写入的 配置信息对象
idCfg := &backupCfg{}
idCfg.Identity = conf.Identity

//	有fsrepo.encode() 可以将json写入文件中，参数一：io.writer，参数二：配置信息对象
return encode(f, idCfg)
```

`encode()`方法展开代码：

```go
func encode(w io.Writer, value interface{}) error {
	// need to prettyprint, hence MarshalIndent, instead of Encoder
	buf, err := config.Marshal(value)
	if err != nil {
		return err
	}
	_, err = w.Write(buf)
	return err
}
```

`Marshal()`方法展开代码：

```go
func Marshal(value interface{}) ([]byte, error) {
	// need to prettyprint, hence MarshalIndent, instead of Encoder
   	// 将结构体转成json，并以标准格式输出的函数 
	return json.MarshalIndent(value, "", "  ")
}
```

##### 完成（finished）





### 需求二：替换config文件中的Identity字段

> - 用户指定备份文件的路径，将备份文件的字段提取并替换源config文件
> - 用户若有需求，可以指定备份路径，再次备份 Identity 字段
> - 最终将系统内存中的节点替换成新节点，identity 中有 peerID 和 privateKey，需要将这两个字段替换



#### 命令效果

​	`ipfs config replaceId <path>`  

​	`ipfs config backup <path>`

##### 解释


```shell
<path> 	-	用户指定的替换文件路径（必须要有）
```



#### 实现过程

##### 熟悉IPFS系统中命令的组件

> **注意：**这里在  **IPFS源码解读 - 新建command**中有详细说明



##### 获取源config文件的 config 对象

> **知识点：**os包，json转换为结构体

```go
//	获得配置文件路径
cfgPath := req.InvocContext().ConfigRoot + `\config`
//	获取源config文件的 config 对象
sf, err := os.OpenFile(cfgPath, os.O_RDWR, 0777)
if err != nil {
    res.SetError(errors.New("failed to open source config file"), cmdkit.ErrNormal)
    return
}

if err = json.NewDecoder(sf).Decode(cfg); err != nil {
    res.SetError(err, cmdkit.ErrNormal)
    return
}
```



##### 获取用户指定替换文件的 config 对象

> **知识点：**IPFS系统中Request对象，json转换为结构体

```go
//	获取用户指定的文件的 config 对象
clif, err := req.Files().NextFile()
if err != nil {
    res.SetError(errors.New("failed to get the client config file"), cmdkit.ErrNormal)
    return
}
defer clif.Close()
if err = json.NewDecoder(clif).Decode(clicfg); err != nil {
    res.SetError(err, cmdkit.ErrNormal)
    return
}
```

- 用户在命令行参数指定文件，所以是 `cmdkit.FileArg` ，在Run函数中，`req.Files().NextFile()`可以获得用户指定文件的句柄

  ```go
  Arguments: []cmdkit.Argument{
      cmdkit.FileArg("file", true, false, "The file to use to replace Identity field in the original config."),
  },
  ```

- 不存在文件只包含identity字段的问题。。。





##### 替换并写入

```go
cfg.Identity = clicfg.Identity
//	中间省略了获取文件的操作
err = encode(sf, cfg)
if err != nil {
    res.SetError(errors.New("failed to write field to source config "), cmdkit.ErrNormal)
    return
}
```



##### 获取用户指定文件的 io.writer

> **知识点：**IPFS系统中Request对象，go语言文件操作

```go
//	获取用户指定路径
backupPath, ok := req.Options()["backupPath"].(string)
if !ok {
    res.SetError(errors.New("failed to get backupPath "), cmdkit.ErrNormal)
    return
}
//	生成目标文件的 io.writer ,通过atomicfile.New(filename, 0660)获得 io.writer
backupF, err := atomicfile.New(backupPath, 0660)
if err != nil {
    res.SetError(errors.New("failed to get backupPath io.writer "), cmdkit.ErrNormal)
    return
}
defer backupF.Close()
```

- 用户在可选参数中，需要传入 字符串类型的 可选参，所以需要使用`cmdkit.StringOption()`，然后通过`req.Options()`字典获取用户传入的字符串

  ```go
  Options: []cmdkit.Option{
      cmdkit.StringOption("backupPath", "o", "自定义替换后的备份目录"),
  },
  ...
  backupPath, ok := req.Options()["backupPath"].(string)
  ```



##### 写入备份文件

```go
err = encode(backupF, idCfg)
if err != nil {
    res.SetError(errors.New("failed to write field to backup config file"), cmdkit.ErrNormal)
    return
}
```



##### 更改本地 ipfsNode 的字段

- 目前只对Identity字段做了备份，所以需要更改的字段为：
  - peerID
  - privateKey

###### 获取系统的ipfsNode对象

> **知识点：**IPFS源码中的 commands包，包里的Request 对象 和 context 对象；**commands包路径：**`gfs/core/commonds`

```go
node, err := GetNode(req.InvocContext())
if err != nil {
    fmt.Println(err)
    return
}
```

- commands 包中的 GetNode() 方法 和 GetConfig()方法
  - 在commands文件夹下的 env.go 中，有两个全局方法 GetNode() 和 GetConfig()
  - GetNode() 和 GetConfig() 方法都是通过 `*commands.Context` 获得 `*core.IpfsNode`
  - 本质上都是通过 `*commands.Context` 绑定的 GetNode() 和 GetConfig() 方法

- commands 包中的 Context 对象，在 request.go 文件中

  > Context代表的是用户对命令的调用，就是包含

  ```go
  type Context struct {
  	Online     bool		
  	ConfigRoot string	//	应该是存放配置文件的目录路径
  	ReqLog     *ReqLog
  
  	config     *config.Config	//	config对象就是配置文件里的数据
  	LoadConfig func(path string) (*config.Config, error)
  
  	node          *core.IpfsNode	//	IpfsNode字段
  	ConstructNode func() (*core.IpfsNode, error)	//	构建IpfsNode的方法
  }
  ```

  - Context 绑定的 GetConfig() 方法

    ```go
    //	若config为空，通过 LoadConfig 加载出来
    func (c *Context) GetConfig() (*config.Config, error) {
    	var err error
    	if c.config == nil {
    		if c.LoadConfig == nil {
    			return nil, errors.New("nil LoadConfig function")
    		}
    		c.config, err = c.LoadConfig(c.ConfigRoot)
    	}
    	return c.config, err
    }
    ```

    ​	TODO：找到 LoadConfig() 的函数体，展开代码

    - Context 绑定的 GetNode() 方法

  ```go
  //	若IpfsNode为空，通过 ConstructNode 加载出来
  func (c *Context) GetNode() (*core.IpfsNode, error) {
  	var err error
  	if c.node == nil {
  		if c.ConstructNode == nil {
  			return nil, errors.New("nil ConstructNode function")
  		}
  		c.node, err = c.ConstructNode()
  	}
  	return c.node, err
  }
  ```

  ​	TODO：找到 LoadConfig() 的函数体，展开代码

  - Context 绑定的 Context() 方法

    - 返回节点的Context，是context包里的Context对象

    ```go
    func (c *Context) Context() context.Context {
        //	先获得节点
    	n, err := c.GetNode()
    	if err != nil {
    		log.Debug("error getting node: ", err)
    		return context.Background()
    	}
    
        //	调用的是IPFSNode绑定的获得context的方法
    	return n.Context()
    }
    ```

  - 还有其他方法，后边遇到再说... ...



- context包中的Context对象
  后边再详细说明……



###### 反推 peerID 并 替换

```go
//	idCfg.Identity.PeerID 是base58编码之后的字符串
//	使用peer包的IDB58Decode方法反推 peerID
id, err := peer.IDB58Decode(idCfg.Identity.PeerID)
if err != nil {
    fmt.Println(err)
    return
}
//	替换
node.Identity = id
```



###### 替换privateKey

```go
//	替换PrivateKey
identity := config.Identity{
    PeerID:  idCfg.Identity.PeerID,
    PrivKey: idCfg.Identity.PrivKey,
}
//	通过Identity获得privateKey
privateKey,err:=identity.DecodePrivateKey("hope success！")
if err != nil {
    fmt.Println(err)
    return
}
node.PrivateKey = privateKey
```

- config 包里边的Identity对象中有生成私钥对象的方法 `identity.DecodePrivateKey("hope success！")`,中间的字符串不会使用，可以取任意值
- config.Identity 只绑定了 `identity.DecodePrivateKey()`方法







#### 检测，保证所有都已经替换

- 向老 ID get  的时候不能成功，向新 ID get 的时候成功
- gfs/gfs.go 文件中的private key 是否更改，控制删除块
- gfs/account.go 文件中的 private key 是否更改，控制可变块块



- 老节点上传，新节点是否能删除
- 用可变块上传...



`ipfs  --config=<path>  init` 



### 需求四：初始化节点记录文件

> - init 函数 初始化节点记录文件
> - 记录在线状态下链接过的节点到记录文件，记录之前需要判断文件是否存在
> - 每有节点下线，删除对应记录的节点
> - 每次启动链接，记录文件内所有节点



#### 效果

1. init 命令会在 configRoot 目录下生成一个名为 `availabepeerlist.json` 的文件，用来存储已连接的节点链接；
2. 每链接上一个节点，将该节点存入缓存中；每断开一个节点，将该从缓存中删除；
3. 当缓存操作次数达到一定量 或者 到达一定的时间，更新本地文件一次；

#### 目前的思路

1. 在 `bitswap.go` 中的 init 函数中调用 `halo.Init()` 初始化 `AvailablePeerListCache `

   ```go
   if runtime.GOOS == "windows" {
       halo.Init(PeerRecordsPathWindows)
   } else {
       halo.Init(PeerRecordsPathLinux)
   }
   ```

2. 分别在`PeerConnected()` 和 `PeerDisconnected()`函数中调用`AvailablePeerListCache`的Add()  和 Del() 方法

   ```go
   //	PeerConnected()中的调用
   go halo.AvailablePeerListCache.Add(p.Pretty())
   //	PeerDisconnected()中的调用
   go halo.AvailablePeerListCache.Del(p.Pretty())
   ```

3. `save()`方法中，遍历缓存(available_peer_list_cache)中的 key 和 上边 map 中的信息，全部更新到本地文件中，全部替换







### 需求五：

#### 需求描述：

> - 编写UTXOBook的增删改查
> - 将存入数据库



>  交易TID（交易记录ID：上述数据的Hash）
>
> 交易结果（成功或失败）
>
> 转出方ID  转入方ID
>
> 交易前转出方UTXO账簿条目ID（UTXO交易记录ID，UTXORecord）
>
> 交易前转入方UTXO账簿条目ID（UTXO交易记录ID，UTXORecord）
>
> 交易后转出方余额
>
> 交易后转入方余额
>
> 交易时间戳
>
> 其它交易附带信息
>
> 注： 如果交易失败，则交易后两方余额与交易前UTXO账簿条目中的余额是相同的，之所以要有失败的交易记录，是为了通知其它节点将交易撤销



> UTXO交易记录 （Validator）
>
> 验证人列表
>
> 验证人PID
>
> 验证时间
>
> POW 
>
> Nonce
>
> POW结果（以交易记录组、记账人、记账时间为基础做POW）
>
> 验证人对 UTXO交易记录、PID、验证时间、Nonce以及共识结果的签名
>
> 交易包ID：对上述数据的Hash
>
> 注： 每个交易包中，同一个转出人的交易记录只能有一条
>
> ​	意思是，一个用户发起一笔交易，只能够等交易确认后再发起新交易，同一个交易包(可以理解为一个块) 中，只能包含一笔该用户发起的交易，不能够有第二笔。



#### 获得记账权前后的验证：

```go
//1.获得记账权之前的验证：
// 1. 收到一笔交易，提取交易中的 转出方和转入方 的UserID
// 2. 将提取出的 UserID 和本地存储的 UserID 进行对比，若存在就写入，不存在就不写入
//    这里需要知道：UserID 和 NodeID 的转换关系，调用 gfs/utils 的相似哈希方法
// （初始化账户的时候，会广播，然后用户存储在本地）
// 3. 验证交易的有效性，通过发送者账户的公钥（每个节点都会保存每个账户的数据）验证交易的签名数据，...
// TODO: 账户公钥从哪里获取？
// 4. 验证 交易中转入账户的账簿ID 和 本地存储的账簿ID 是否相同，不匹配结束，并把消息发给其他节点

//2.获得记账权后的验证：
// 1.验证 A 的余额是否足够支付
```



- 有UTXOBook数据结构
  - 如何生成的？我需要模拟生成
  - 

- 两个验证：
  - 验证签名数据
  - 验证阙值之类的。。。



#### 思路

##### utxobook.go 文件中的函数

###### DeleteTransactionEntry 函数

- 主要功能：删除 UTXOBook 中的 UTXOEntry

- 思路：

  - 遍历 `book.Transactions` ，找到已经存储的具有与新传入的 UTXOEntry 相同 TransactionID 的 UTXOEntry；若没有，则表示没有删除对象，返回 false

    ```go
    func (book *UTXOBook) DeleteTransactionEntry(e *UTXOEntry) (bool, []*UTXOEntry) {
        for key, utxoEntry := range book.Transactions {
                if utxoEntry.Record.TransactionID == e.Record.TransactionID {
                    ...
                }
        }
        return false, nil
    }
    ```

  - 若存在，则替换并切除后边生成的 UTXOEntry（需要删除的交易后边可能还会存在交易），后边生成的 UTXOEntry 需要返回，并通知相关账户进行删除操作，再更新余额

    ```go
    //	切掉后边的 UTXOEntry 再用新的替换
    deletedTransactions := book.Transactions[key+1:]
    book.Transactions = append(book.Transactions[:key], e)
    
    //	更新余额
    if e.Record.SenderID == book.UserID {
        book.Balance = book.Balance + e.Record.Amount
    } else if e.Record.ReceiverID == book.UserID {
        book.Balance = book.Balance - e.Record.Amount
    }
    return true, deletedTransactions
    ```



###### FindTransactionEntry 函数

- 主要功能：用于从 UTXOBook 中找到对应的 UTXOEntry

- 思路：

  - 遍历 `UTXOBook .Transactions`，找到与参数匹配的 UTXOEntry

    ```go
    for _, utxoEntry := range book.Transactions {
        if eid == utxoEntry.EID{
            return true,utxoEntry
        }
    }
    return false, nil
    ```



###### Verify 函数

- 主要功能：验证 Validator 的 Sign

- 思路：

  - 需要公钥，并且调用 `utils.VerifySignature` 函数进行验证

    ```go
    ok, err := utils.VerifySignature(validatorPubkey, v.getSignData(), v.Sign)
    if err != nil {
        return false, err
    }
    return ok, nil
    ```




##### utxobookDb.go 文件中的函数

###### GetAccount 函数

- 主要功能：通过传入的 peerIDStr（使用 Pretty() 方法转成的string类型） 从数据库中获取对应 Account

- 思路：

  - 通过传入的 peerIDStr 从数据库中获得 cid

    ```go
    keyID := globalAccountDB.NewKey(peerIDStr, 0)
    cidByte, err := globalAccountDB.Get(keyID)
    if err != nil {
        return nil, nil, err
    }
    cid, err := cid.Cast(cidByte)
    if err != nil {
        return nil, nil, err
    }
    ```

  - 再通过得到的 cid 从数据库中获取 account ，并返回

    ```go
    keyCID := globalAccountDB.NewKey(cid.String(), 1)
    accountByte, err := globalAccountDB.Get(keyCID)
    if err != nil {
        return nil, nil, err
    }
    account, err := stellar.ParseAccount(accountByte)
    if err != nil {
        return nil, nil, err
    }
    return account, keyCID, nil
    ```

  - 返回 keyCID 的原因是：更新 UTXOBook 的时候，往数据库中写入需要使用



###### UpdateUTXOBook 函数

- 主要功能：更新数据库中的 UTXOBook 

- 思路：

  - 调用 GetAccount() 函数，获得 账户 和 keyCID

    ```go
    account, keyCID, err := GetAccount(book.UserID)
    if err != nil {
        return err
    }
    ```

  - 更新对应Account的账簿并写入数据库

    ```go
    account.TransactionList = book
    accountByte := account.Data()
    err = globalAccountDB.Put(keyCID, accountByte)
    if err != nil {
        return err
    }
    return nil
    ```


###### FindUTXOBook 函数

- 主要功能：通过传入的 peerIDStr（使用 Pretty() 方法转成的string类型） 从数据库中查出对应 UTXOBook

- 思路：

  - 调用 GetAccount() 函数，获得 账户，将账户的 TransactionList 返回

    ```go
    account, _, err := GetAccount(peerIDStr)
    if err != nil {
        return nil, err
    }
    return account.TransactionList, nil
    ```



###### VerifyValidator 函数

- 主要功能：用于验证 Validator 中的签名(Sign)
- 思路：
  - 首先要获取 Validator 的公钥，这个功能还没有写 TODO
  - 调用 `v.Verify()` 将公钥传入进行验证



## 学到的知识

### go语言循环导包解决方案

#### 方案一

- 循环导包有很多原因，要学会看循环导包的错误信息，看如下例码：

  ```go
  import cycle not allowed
  package github.com/gfs/cmd/ipfs
  	imports github.com/gfs/assets
  	imports github.com/gfs/core
  	imports github.com/gfs/blockservice
  	imports gx/ipfs/Qmc2faLf7URkHpsbfYM4EMbr8iSAcGAe8VPgVi64HVnwji/go-ipfs-exchange-interface
  	imports github.com/gfs/galaxy/stellar
  	imports github.com/gfs/core/commands  // 这里是循环导包的原因
  	imports github.com/gfs/blocks/blockstoreutil
  	imports github.com/gfs/pin
  	imports github.com/gfs/merkledag
  	imports github.com/gfs/blockservice
  ```

  - 在上面遇到的案例中，循环导包的原因并不是最开始的一行，也不是中间重复的那行，而是因为stellar包中导入了commands包，导致后边的循环导包；
  - 要想看出循环导包的原因，需要对整个系统中的整体框架 和 各个包之间的分工合作 了解透彻，这样就能够清晰分辨出那个包本不应该导入这个包，而这里却导入了，这时候就要想办法 不使用该包达到同样的功能；

- 这里有个一方法是：使用接口将需要调用的方法提取出来，再用本包中的全局变量来实例化该接口，实例化的函数由上游调用（就是在使用前实例化好）

  - 将需要使用的方法用接口提取

    - 新建一个包，定义一个接口，将需要使用的函数写入接口中
    - `GetPubkeyFromNet()` 

    ```go
    type GetPubkey interface {
    	GetPubkeyFromNet(sendpid peer.ID, frompid []peer.ID, findpid peer.ID, ctx context.Context) ([]byte, error)
    }
    ```

    - 


  ```go
  
  ```



- 区块链要解决的是 如何用一种可信的的方式记录数据，使得用户可以信任区块链系统记录的数据，而无需假设记账人的可信性（比如现在的银行，支付宝）



问题：

- 加上判断daemon是否启动
- 更改公钥验证环节
- 注释

>>>>>>> 
