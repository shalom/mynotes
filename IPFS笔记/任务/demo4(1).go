package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

var name string

//func init() {
//	flag.StringVar(&name, "name", "everyone", "The greeting object.")
//}

func main() {
	macStrArray := make([]string, 0, 20)
	//linuxStrArray := make([]string, 0, 20)
	//windowsStrArray := make([]string, 0, 20)

	macPeerIDArray := make([]string, 0, 20)
	//linuxPeerIDArray := make([]string, 0, 20)
	//windowsPeerIDArray := make([]string, 0, 20)
	//flag.Parse()
	//generator := hello(name)
	// n := <- generator
	//
	//s := runtime.Stack([]byte(n), true)
	//fmt.Println(string(16))

	for i := 1; i <= 20; i ++ {

		stri := strconv.Itoa(i)
		macb, err := ioutil.ReadFile("/Users/pc001/testTX/test" + stri + "/config")
		if err != nil {
			fmt.Print(err)
		}
		//fmt.Println(macb)
		macstr := string(macb)

		port := strconv.Itoa(32 + i)
		macstr = Replace(macstr, port)

		macStrArray = append(macStrArray, macstr)
		var macdat map[string]interface{}
		if err := json.Unmarshal(macb, &macdat); err == nil {
			Addresses := macdat["Identity"].(map[string]interface{})
			macPeerIDArray = append(macPeerIDArray, " \"/ip4/127.0.0.1/tcp/400"+port+"/ipfs/"+Addresses["PeerID"].(string)+"\"")

		}
	}

	//linuxb, err := ioutil.ReadFile("/Users/pc001/linuxTX/test" + stri + "/config")
	//if err != nil {
	//	fmt.Print(err)
	//}
	//fmt.Println(linuxb)
	//linuxstr := string(linuxb)
	//linuxstr = Replace(linuxstr, port)
	//linuxStrArray = append(linuxStrArray, linuxstr)
	//var linuxdat map[string]interface{}
	//if err := json.Unmarshal(linuxb, &linuxdat); err == nil {
	//	Addresses := linuxdat["Identity"].(map[string]interface{})
	//	linuxPeerIDArray = append(linuxPeerIDArray, Addresses["PeerID"].(string))
	//
	//}
	//
	//windowsb, err := ioutil.ReadFile("/Users/pc001/windowsTX/test" + stri + "/config")
	//if err != nil {
	//	fmt.Print(err)
	//}
	//fmt.Println(windowsb)
	//windowsstr := string(windowsb)
	//windowsstr = Replace(windowsstr, port)
	//windowsStrArray = append(windowsStrArray, windowsstr)
	//var windowsdat map[string]interface{}
	//if err := json.Unmarshal(windowsb, &windowsdat); err == nil {
	//	Addresses := windowsdat["Identity"].(map[string]interface{})
	//	windowsPeerIDArray = append(windowsPeerIDArray, Addresses["PeerID"].(string))
	//}
	//}

	for i := 1; i <= 20; i ++ {
		stri := strconv.Itoa(i)
		//port :=strconv.Itoa(32+i)
		macStr := macStrArray[i-1]

		var Bootstrapstr string = " \"Bootstrap\": [\n        "
		for index, value := range macPeerIDArray {
			if index != (i - 1) {
				Bootstrapstr += value
			}
			if i == 20 {
				if index != (len(macPeerIDArray) - 1) {
					Bootstrapstr += ",\n"
				}
			} else {
				if index != (i-1) && index != (len(macPeerIDArray)-1) {
					Bootstrapstr += ",\n"
				}
			}
		}

		macStr = ReplaceBootstrap(macStr, "", "", "", Bootstrapstr)
		fmt.Println(macStr)
		writeFile(macStr, "/Users/pc001/testTX/test"+stri+"/config")
		//linuxStr := linuxStrArray[i-1]
		//linuxStr = ReplaceBootstrap(linuxStr, port, "192.168.1.110", windowsPeerIDArray[i-1])
		//writeFile(linuxStr, "/Users/pc001/linuxTX/test"+stri+"/config")
		//windowsStr := windowsStrArray[i-1]
		//if i == 20 {
		//	windowsStr = ReplaceBootstrap(windowsStr, "1", "192.168.1.101", macPeerIDArray[0])
		//
		//} else {
		//	windowsStr = ReplaceBootstrap(windowsStr, port, "192.168.1.101", macPeerIDArray[i])
		//}
		//writeFile(windowsStr, "/Users/pc001/windowsTX/test"+stri+"/config")
	}
	// str :="{"
	//for _,e := range macPeerIDArray {
	//	str +="\""
	//	str += e
	//	str +="\""
	//	str +=","
	//}
	//str +="}"
	//fmt.Println(str)

}

func Replace(str string, i string) string {
	str = strings.Replace(str, "\"Discovery\": {\n    \"MDNS\": {\n      \"Enabled\": true", "\"Discovery\": {\n    \"MDNS\": {\n      \"Enabled\": false", -1)
	str = strings.Replace(str, "  \"Addresses\": {\n    \"Swarm\": [\n      \"/ip4/0.0.0.0/tcp/4001\",\n      \"/ip6/::/tcp/4001\"", "  \"Addresses\": {\n    \"Swarm\": [\n      \"/ip4/0.0.0.0/tcp/400"+i+"\",\n      \"/ip6/::/tcp/400"+i+"\"", -1)
	str = strings.Replace(str, "    \"API\": \"/ip4/127.0.0.1/tcp/5001\",\n    \"Gateway\": \"/ip4/127.0.0.1/tcp/8080\"", "    \"API\": \"/ip4/127.0.0.1/tcp/500"+i+"\",\n    \"Gateway\": \"/ip4/127.0.0.1/tcp/80"+i+"\"", -1)
	return str
}

func ReplaceBootstrap(str string, i string, ip string, peer string, x string) string {
	str = strings.Replace(str, "  \"Bootstrap\": [\n    \"/dnsaddr/bootstrap.libp2p.io/ipfs/QmNnooDu7bfjPFoTZYxMNLWUQJyrVwtbZg5gBMjTezGAJN\",\n"+
		"    \"/dnsaddr/bootstrap.libp2p.io/ipfs/QmQCU2EcMqAqQPR2i9bChDtGNJchTbq5TbXJJ16u19uLTa\",\n"+
		"    \"/dnsaddr/bootstrap.libp2p.io/ipfs/QmbLHAnMoJPWSCR5Zhtx6BHJX9KiKNN6tpvbUcqanj75Nb\",\n"+
		"    \"/dnsaddr/bootstrap.libp2p.io/ipfs/QmcZf59bWwK5XFi76CZX8cbJ4BhTzzA3gU1ZjYZcYW3dwt\",\n"+
		"    \"/ip4/104.131.131.82/tcp/4001/ipfs/QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ\",\n"+
		"    \"/ip4/104.236.179.241/tcp/4001/ipfs/QmSoLPppuBtQSGwKDZT2M73ULpjvfd3aZ6ha4oFGL1KrGM\",\n"+
		"    \"/ip4/128.199.219.111/tcp/4001/ipfs/QmSoLSafTMBsPKadTEgaXctDQVcqN88CNLHXMkTNwMKPnu\",\n"+
		"    \"/ip4/104.236.76.40/tcp/4001/ipfs/QmSoLV4Bbm51jM9C4gDYZQ9Cy3U6aXMJDAbzgu2fzaDs64\",\n"+
		"    \"/ip4/178.62.158.247/tcp/4001/ipfs/QmSoLer265NRgSp2LA3dPaeykiS1J6DifTC88f5uVQKNAd\",\n"+
		"    \"/ip6/2604:a880:1:20::203:d001/tcp/4001/ipfs/QmSoLPppuBtQSGwKDZT2M73ULpjvfd3aZ6ha4oFGL1KrGM\",\n"+
		"    \"/ip6/2400:6180:0:d0::151:6001/tcp/4001/ipfs/QmSoLSafTMBsPKadTEgaXctDQVcqN88CNLHXMkTNwMKPnu\",\n"+
		"    \"/ip6/2604:a880:800:10::4a:5001/tcp/4001/ipfs/QmSoLV4Bbm51jM9C4gDYZQ9Cy3U6aXMJDAbzgu2fzaDs64\",\n"+
		"    \"/ip6/2a03:b0c0:0:1010::23:1001/tcp/4001/ipfs/QmSoLer265NRgSp2LA3dPaeykiS1J6DifTC88f5uVQKNAd\"", x, -1) //"  \"Bootstrap\": [\n        \"/ip4/"+ip+"/tcp/400"+i+"/ipfs/"+peer+"\"", -1)
	return str
}

func writeFile(str string, filePath string) {
	f, err := os.Create(filePath)
	check(err)

	defer f.Close()
	f.Sync()

	w := bufio.NewWriter(f)
	w.WriteString(str)

	w.Flush()
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
