﻿# <center>GFS</center> #
<center>GFS是基于ipfs修改系统。</center>
***************
**<center>2019-01-17 V0.1</center>**

**************
[TOC]
*************
## 一. IPFS命令简介
### &nbsp;1.1. 命令行
​	``` ipfs [--config=<config> | -c][--debug= | -D] 
​     [--help=<help>][-h=] [--local=<local> | -L][--api=] <command> ...```

### &nbsp;1.2. 命令行选项
​	-c, --config string - 配置文件路径
​	-D, --debug  bool   - 开启调试模式，默认值：false
​	--help       bool   - 是否显示完整的命令帮助文档，默认值：false
​	-h           bool   - 显示简明版的命令帮助文档，默认值：false
​	-L, --local  bool   - 仅在本地执行命令，不使用后台进程。默认值：false
​	--api        string - 使用指定的API实例，默认值：`/ip4/127.0.0.1/tcp/5001`

### &nbsp;1.3. 基本子命令
​	init          初始化ipfs本地配置
​	add <path>    将指定文件添加到IPFS
​	cat <ref>     显示指定的IPFS对象数据
​	get <ref>     下载指定的IPFS对象
​	ls <ref>      列表显示指定对象的链接
​	refs <ref>    列表显示指定对象的链接哈希

### &nbsp;1.4.数据结构子命令
​	block         操作数据仓中的裸块
​	object        操作有向图中的裸节点
​	files         以unix文件系统方式操作IPFS对象
​	dag           操作IPLD文档，目前处于实验阶段
### &nbsp;1.5. 高级子命令
​	daemon        启动后台服务进程
​	mount         挂接只读IPFS
​	resolve       名称解析
​	name          发布、解析IPNS名称
​	key           创建、列表IPNS名称键值对
​	dns           解析DNS链接
​	pin           在本地存储中固定IPFS对象
​	repo          操作IPFS仓库
​	stats         各种运营统计
​	filestore     管理文件仓，目前处于实验阶段

### &nbsp;1.6. 网络子命令
​	id            显示IPFS节点信息
​	bootstrap     添加、删除启动节点
​	swarm         管理p2p网络的连接
​	dht           查询分布哈希表中的值或节点信息
​	ping          检测连接延时
​	diag          打印诊断信息
### &nbsp;1.7. 工具子命令
​	config        管理配置信息
​	version       显示ipfs版本信息
​	update        下载并应用go-ipfs更新
​	commands      列表显示全部可用命令
[[原始命令参考]](http://cw.hubwiz.com/card/c/ipfs/1/1/2/)

## 二. GFS添加修改命令
### &nbsp;2.1. 新增命令
​	init [--supernode]           将自身设置为超级节点
​    daemon [--supernode]         以超级节点模式启动
​    daemon [--not-load-peerlist] 启动时是否加载缓存节点
​    config [replaceId]           替换配置文件信息PEER账户及私钥信息
​	config [backup]              备份配置文件信息PEER账户及私钥信息
​	del                          删除本地块及子块并广播所有节点删除
​	put                          上传文件
​	get                          获取文件
​	gv                           发起交易交易
​    mutable put                  可变块上传文件
​    mutable update               可变块修改
​    mutable get                  可变块获取
​	

### &nbsp;2.2. 修改命令

|原命令|修改后命令 |
|:----:|:---------:|
|add   | raw add   |
|get   | raw get   |
|cat   | raw cat   |
|put   | raw put   |
## 三.IPFS 结构图
![IPFS结构.png-359.1kB][1]
## 四.启动流程
![启动流程.png-141.7kB][2]
## 五.发现节点流程
![发现节点流程.png-131.3kB][3]
## 六.查找节点流程
![查找节点.png-434.1kB][4]
## 七.ADD流程
![add.png-188kB][5]
## 八.CAT或GET流程
![cat或get.png-390.6kB][6]
## 九.删除流程
![删除.png-204.6kB][7]
## 十.缓存内容源相关流程
![缓存相关.png-51.9kB][8]
## 十一.可变块流程
![可变块.png-451.6kB][9]
## 十二.交易流程
![交易流程.png-365.8kB][10]


[1]: http://static.zybuluo.com/JXC/11c9pcfllbj13dcup3aool4x/IPFS%E7%BB%93%E6%9E%84.png
[2]: http://static.zybuluo.com/JXC/afvqebv7vefpr8d123toekia/%E5%90%AF%E5%8A%A8%E6%B5%81%E7%A8%8B.png
[3]: http://static.zybuluo.com/JXC/m39f6yswwte194pe2ll8my3x/%E5%8F%91%E7%8E%B0%E8%8A%82%E7%82%B9%E6%B5%81%E7%A8%8B.png
[4]: http://static.zybuluo.com/JXC/2hkyg58tp28irex7u1mo05h9/%E6%9F%A5%E6%89%BE%E8%8A%82%E7%82%B9.png
[5]: http://static.zybuluo.com/JXC/bf00qck9ejuvr0n6pj9qogpv/add.png
[6]: http://static.zybuluo.com/JXC/wyaypzh966e3hn92e966xwia/cat%E6%88%96get.png
[7]: http://static.zybuluo.com/JXC/1lqolsnc924096yvteqqmmq9/%E5%88%A0%E9%99%A4.png
[8]: http://static.zybuluo.com/JXC/lokwg7hx42pml25o8cdhhn40/%E7%BC%93%E5%AD%98%E7%9B%B8%E5%85%B3.png
[9]: http://static.zybuluo.com/JXC/jjtv4zynziuatgz9740cgpi4/%E5%8F%AF%E5%8F%98%E5%9D%97.png
[10]: http://static.zybuluo.com/JXC/f7xrxr64trd6088fa74lbzbj/%E4%BA%A4%E6%98%93%E6%B5%81%E7%A8%8B.png



